﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnswerButtonBehaviour : MonoBehaviour
{

    public GameObject questionWidget;
    public bool sayYes = true;
    QuestionsListBehaviour questionList = null;

    // Start is called before the first frame update
    void Start()
    {
        questionList = questionWidget.GetComponent<QuestionsListBehaviour>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AnswerQuestion()
    {
        questionList.AnswerQuestion(sayYes);
    }
}
