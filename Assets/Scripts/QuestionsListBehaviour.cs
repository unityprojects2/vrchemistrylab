﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionsListBehaviour : MonoBehaviour
{

    public GameObject[] questions;

    GameObject activeQuestion = null;
    int activeQuestionIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        TurnOffQuestions();
        SetupQuestion(activeQuestionIndex);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetupQuestion(int index)
    {
        if (index >= 0 && index < questions.Length)
        {
            GameObject questionObject = questions[index];
            questionObject.SetActive(true);

            if (activeQuestion) activeQuestion.SetActive(false);
            activeQuestion = questionObject;
        }
    }

    public void TurnOffQuestions()
    {
        foreach (GameObject item in questions)
        {
            item.SetActive(false);
        }
    }

    public bool AnswerQuestion(bool answer)
    {
        QuestionBehaviour questionObject = activeQuestion.GetComponent<QuestionBehaviour>();
        if (answer == questionObject.answer)
        {
            SetupNextQuestion();
            return true;
        }

        return false;
    }

    public void SetupNextQuestion()
    {
        if (activeQuestionIndex >= questions.Length) activeQuestionIndex = 0;
        else activeQuestionIndex++;

        SetupQuestion(activeQuestionIndex);
    }
}
