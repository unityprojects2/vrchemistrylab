﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionBehaviour : MonoBehaviour
{
    #region Properties

    public string description;
    public bool answer;

    #endregion

    // Start is called before the first frame update
    void Start()
    { 
        GameObject questionText = transform.Find("Question Text").gameObject;
        Text textMesh = questionText.GetComponent<Text>();
        if (textMesh)
        {
            textMesh.text = description;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
