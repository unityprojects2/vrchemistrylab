﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlaskBehaviour : MonoBehaviour
{
    private GameObject thisObject;
    private Material liquidMaterial;

    #region MaterialProperties

    public Ingredient ingredient;
    public float liquidAmount = 0.0f;
    public float flaskArea = 2.0f;

    #endregion

    // Start is called before the first frame update
    void Start()
    {
        thisObject = this.gameObject;
        liquidMaterial = thisObject.GetComponent<MeshRenderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        //Update material properties
        liquidMaterial.SetFloat("_LiquidAmount", liquidAmount);
        liquidMaterial.SetFloat("_MidAmount", ingredient.midAmount);

        liquidMaterial.SetColor("_TopColor", ingredient.topColor);
        liquidMaterial.SetColor("_MidColor", ingredient.midColor);
        liquidMaterial.SetColor("_BaseColor", ingredient.baseColor);

        /* poor out in real time */

        Vector3 upVector = transform.TransformDirection(transform.up);
	Debug.Log(upVector);
	RaycastHit hit = MakeUpRaycast();
        if (upVector.y < 0.0f)
        {
            
            PoorOutLiquid(hit.collider);
        }
    }

    /// <summary>
    /// Make raycast to up vector
    /// </summary>
    /// <returns>Raycast hit settings</returns>
    RaycastHit MakeUpRaycast()
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position, transform.TransformDirection(transform.up));
        Physics.Raycast(ray, out hit);
        Debug.DrawLine(ray.origin, hit.point, Color.red);   

        return hit;
    }

    /// <summary>
    /// Poor out liquid from flask to before 0
    /// </summary>
    /// <param name="collider">Raycast collider object</param>
    public void PoorOutLiquid(Collider collider)
    {
        //Minus small part of liquid
        float liquidDecrement = flaskArea * transform.TransformDirection(transform.up).y * Time.deltaTime;
        if (liquidAmount > 0) liquidAmount -= Mathf.Abs(liquidDecrement);

        if (collider != null)
        {
            //to overfill a liquid must be child of collider object must have name "Liquid"
            GameObject otherFlask = collider.transform.Find("Liquid").gameObject;

            if (otherFlask != null && liquidAmount > 0) 
                OverfillLiquid(otherFlask, liquidDecrement);
        }

    }

    /// <summary>
    /// Overfill liquid to another flask
    /// </summary>
    /// <param name="flask">Flask game object</param>
    /// <param name="liquidDelta">Part of liquid to overfill</param>
    private void OverfillLiquid(GameObject flask, float liquidDelta)
    {
        FlaskBehaviour flaskProperties = flask.GetComponent<FlaskBehaviour>();

        if (flaskProperties.ingredient.name == this.ingredient.name)
        {
            flaskProperties.TopLiquidUp(liquidDelta);
        }
        else
        {
            //Debug.Log("Start reaction!");
            StartReaction(flaskProperties, liquidDelta);
        }
    }

    /// <summary>
    /// Add liquidDelta to flask liquid level
    /// </summary>
    /// <param name="liquidDelta">Liquid part to add</param>
    public void TopLiquidUp(float liquidDelta)
    {
        liquidAmount += Mathf.Abs(liquidDelta);
    }

    /// <summary>
    /// Start applying reaction for blending ingredients
    /// </summary>
    /// <param name="flaskProperties">Other flask</param>
    /// <param name="liquidDelta">Delta of ingredient</param>
    private void StartReaction(FlaskBehaviour flaskProperties, float liquidDelta)
    {
        Reaction[] reactions = FindObjectsOfType<Reaction>();
        Reaction resultReaction = null;

        //Find convenient reaction to blend
        foreach (Reaction reaction in reactions)
        {
            if
                (
                reaction.firstIngredient == this.ingredient &&
                reaction.secondIngredient == flaskProperties.ingredient
                )
            {
                resultReaction = reaction;
                break;
            }
        }

        if (resultReaction != null) flaskProperties.ApplyReaction(resultReaction);
        else flaskProperties.TopLiquidUp(liquidDelta);
    }

    /// <summary>
    /// Complete applying reaction
    /// </summary>
    /// <param name="reaction"></param>
    public void ApplyReaction(Reaction reaction)
    {
        Debug.Log(reaction.name);
        ingredient = reaction.resultIngredient;
        Instantiate(reaction.particleEffect, transform);
    }
}
