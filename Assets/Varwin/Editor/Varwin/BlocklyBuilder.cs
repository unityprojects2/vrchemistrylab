﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using Varwin.Core;
using Varwin.Core.Behaviours;
using Varwin.Public;

namespace Varwin.Editor
{
    public class BlocklyBuilder : MonoBehaviour
    {
        private static BlocklyConfig _config;
        private static Type _wrapperType;

        private static Regex _nullableRegex = new Regex(@"System\.Nullable`1\[\[([A-Za-z_.?0-9]+?), .*?\]\]");
        private static Regex _dictionaryRegex = new Regex(@"([A-Za-z_.?0-9]+?)`2\[\[([A-Za-z_.?0-9]+?),.*?\],\[([A-Za-z_.?0-9]+?),.*?\]\]");
        private static Regex _listRegex = new Regex(@"([A-Za-z_.?0-9]+?)`1\[\[([A-Za-z_.?0-9]+?),.*?\]\]");

        public class MethodLocale
        {
            public MethodInfo MemberInfo;
            public LocaleAttribute[] LocaleAttributies;
        }

        public static string CreateBlocklyConfig(Type type, ObjectBuildDescription objectBuild)
        {
            if (!Initialize(type, objectBuild))
            {
                return null;
            }

            AddPropertiesToConfig();
            AddFieldsToConfig();
            AddGenericValueListsToConfig(type);
            AddCustomEnumValueListsToConfig(objectBuild.ContainedObjectDescriptor);
            AddCustomValueListsToConfig(objectBuild);
            AddMethodsToConfig();
            AddEventsToConfig();

            var jsonSerializerSettings = new JsonSerializerSettings {NullValueHandling = NullValueHandling.Ignore};
            string jsonConfig = JsonConvert.SerializeObject(_config, Formatting.None, jsonSerializerSettings);

            return jsonConfig;
        }

        private static bool Initialize(Type type, ObjectBuildDescription objectBuild)
        {
            if (string.IsNullOrWhiteSpace(type.FullName))
            {
                return false;
            }

            _wrapperType = type;
            VarwinObjectDescriptor varwinObjectDescriptor = objectBuild.ContainedObjectDescriptor;

            string builtAt = $"{DateTimeOffset.UtcNow:s}Z";

            if (DateTimeOffset.TryParse(varwinObjectDescriptor.BuiltAt, out DateTimeOffset builtAtDateTimeOffset))
            {
                builtAt = $"{builtAtDateTimeOffset.UtcDateTime:s}Z";
            }

            _config = new BlocklyConfig
            {
                Guid = varwinObjectDescriptor.Guid,
                RootGuid = varwinObjectDescriptor.RootGuid,
                Locked = varwinObjectDescriptor.Locked,
                Embedded = varwinObjectDescriptor.Embedded,
                
                MobileReady = SdkSettings.MobileFeature.Enabled && varwinObjectDescriptor.MobileReady,
                SourcesIncluded = varwinObjectDescriptor.SourcesIncluded,
                DisableSceneLogic = varwinObjectDescriptor.DisableSceneLogic,
                Config = new Config
                {
                    type = $"{varwinObjectDescriptor.Name}_{varwinObjectDescriptor.RootGuid.Replace("-", "")}.{varwinObjectDescriptor.Name}Wrapper",
                    blocks = new List<Block>(),
                },
                Author = new JsonAuthor
                {
                    Name = varwinObjectDescriptor.AuthorName,
                    Email = varwinObjectDescriptor.AuthorEmail,
                    Url = varwinObjectDescriptor.AuthorUrl,
                },
                BuiltAt = $"{builtAt}",
                License = new JsonLicense
                {
                    Code = varwinObjectDescriptor.LicenseCode,
                    Version = varwinObjectDescriptor.LicenseVersion,
                },
                SdkVersion = VarwinVersionInfo.VersionNumber
            };

            if (File.Exists(objectBuild.TagsPath))
            {
                var tags = File.ReadAllLines(objectBuild.TagsPath);
                _config.Tags = tags.ToList();
            }

            if (_config.Name == null)
            {
                _config.Name = new I18n();
            }
            
            if (varwinObjectDescriptor.DisplayNames != null && varwinObjectDescriptor.DisplayNames.Count > 0)
            {
                _config.Name = varwinObjectDescriptor.DisplayNames.ToI18N();
            }
            else
            {
                IWrapperAware iWrapperAware = varwinObjectDescriptor.gameObject.GetComponent<VarwinObject>();

                if (iWrapperAware != null)
                {
                    _config.Name = LocalizationUtils.GetI18n(iWrapperAware.GetType());
                }

                if (_config.Name == null || _config.Name.IsEmpty())
                {
                    iWrapperAware = varwinObjectDescriptor.gameObject.GetComponents<IWrapperAware>().FirstOrDefault(x => !(x is VarwinObjectDescriptor || x is VarwinObject));
                    _config.Name = iWrapperAware != null ? LocalizationUtils.GetI18n(iWrapperAware.GetType()) : new I18n {en = varwinObjectDescriptor.Name};
                }
            }
            
            if (_config.Name.IsEmpty())
            {
                throw new Exception($"{objectBuild.ObjectName} does not have a localized name.");
            }

            if (_config.Description == null)
            {
                _config.Description = new I18n();
            }
            
            if (varwinObjectDescriptor.Description != null && varwinObjectDescriptor.Description.Count > 0)
            {
                _config.Description = varwinObjectDescriptor.Description.ToI18N();
            }
            else
            {
                _config.Description = _config.Name;
            }
            
            return true;
        }

        private static void AddPropertiesToConfig()
        {
            foreach (PropertyInfo property in _wrapperType.GetProperties())
            {
                var attributes = property.GetCustomAttributes(true);
                var locales = property.GetCustomAttributes<LocaleAttribute>(true);
                string blockName = GetBlockGroupName(attributes);
                VariableAttribute variable = null;

                foreach (object attribute in attributes)
                {
                    Block block = null;
                    Block secondBlock = null;
                    var item = new Item {property = property.Name};

                    switch (attribute)
                    {
                        case GetterAttribute getter:
                            block = GetBlock(getter.Name, "getter");

                            break;
                        case SetterAttribute setter:
                            block = GetBlock(setter.Name, "setter");

                            break;
                        case VariableAttribute var:

                            if (property.GetMethod != null)
                            {
                                block = blockName == null ? GetBlock(property.Name + "Getter", "getter") : GetBlock(blockName + "Getter", "getter");
                            }

                            if (property.SetMethod != null)
                            {
                                secondBlock = blockName == null ? GetBlock(property.Name + "Setter", "setter") : GetBlock(blockName + "Setter", "setter");
                            }

                            variable = var;

                            break;
                        default:
                            continue;
                    }

                    bool isDynamic = property.GetCustomAttributes(typeof(DynamicAttribute), true).Length > 0;

                    if (block != null)
                    {
                        block.valueType = GetValidTypeName(property.PropertyType, isDynamic);
                    }

                    if (secondBlock != null)
                    {
                        secondBlock.valueType = GetValidTypeName(property.PropertyType, isDynamic);
                    }

                    if (property.GetCustomAttributes(typeof(DynamicAttribute), true).Length > 0)
                    {
                        if (block != null)
                        {
                            block.valueType = "dynamic";
                        }

                        if (secondBlock != null)
                        {
                            secondBlock.valueType = "dynamic";
                        }
                    }

                    if (variable != null)
                    {
                        item.i18n = variable.LocalizedNames;
                    }
                    else
                    {
                        foreach (var locale in locales)
                        {
                            if (locale.I18n != null)
                            {
                                item.i18n = locale.I18n;

                                break;
                            }
                            else
                            {
                                if (locale.Strings.Length == 1)
                                {
                                    item.SetLocale(locale.Code, locale.Strings[0]);
                                }
                                else
                                {
                                    Debug.LogError(SdkTexts.PropertyLocaleAttributeMustHaveString);
                                }
                            }
                        }
                    }

                    block?.AddItem(item);

                    secondBlock?.AddItem(item);
                }
            }
        }

        private static void AddFieldsToConfig()
        {
            foreach (FieldInfo field in _wrapperType.GetFields())
            {
                var attributes = field.GetCustomAttributes<ValueAttribute>(true);
                var locales = field.GetCustomAttributes<LocaleAttribute>();
                var valueListAttribute = field.GetCustomAttribute<ValueListAttribute>();

                if (valueListAttribute != null)
                {
                    continue;
                }

                foreach (ValueAttribute valueAttribute in attributes)
                {
                    Block block = GetBlock(valueAttribute.Name, "values");
                    var item = new Item {name = field.Name};

                    foreach (var locale in locales)
                    {
                        if (locale.I18n != null)
                        {
                            item.i18n = locale.I18n;
                            break;
                        }
                        else
                        {
                            if (locale.Strings.Length == 1)
                            {
                                item.SetLocale(locale.Code, locale.Strings[0]);
                            }
                            else
                            {
                                Debug.LogError(SdkTexts.PropertyLocaleAttributeMustHaveString);
                            }
                        }
                    }

                    block.AddItem(item);
                }
            }
        }

        private static void AddGenericValueListsToConfig(Type type)
        {
            var mainTypefields = type.GetFields(BindingFlags.Public | BindingFlags.Instance);
            var valueListValueDescriptors = WrapperGenerator.GetValueListValueDescriptors(mainTypefields);

            foreach (var valueListValueDescriptor in valueListValueDescriptors)
            {
                Block block = GetBlock(valueListValueDescriptor.Key, "valueList");
                block.valueType = "System.Object";

                foreach (var valueDescriptor in valueListValueDescriptor.Value)
                {
                    var item = new Item {name = valueDescriptor.FieldName, i18n = valueDescriptor.Locale};
                    block.AddItem(item);
                }
            }
        }

        private static void AddCustomEnumValueListsToConfig(VarwinObjectDescriptor varwinObjectDescriptor)
        {
            var componentReferences = varwinObjectDescriptor.Components.ComponentReferences;

            foreach (ComponentReference componentReference in componentReferences)
            {
                if (componentReference.Component is VarwinBehaviour)
                {
                    continue;
                }

                var typeMethods = componentReference.Type.GetMethods(BindingFlags.Public | BindingFlags.Instance);
                var customEnumValueDescriptors = WrapperGenerator.GetCustomEnumValueDescriptors(typeMethods);
                string prefix = componentReference.PrefixName;

                foreach (var customEnumValueList in customEnumValueDescriptors)
                {
                    Block block = GetBlock(prefix + customEnumValueList.Key, "valueList");
                    block.valueType = "System.Object";

                    foreach (var customEnumValue in customEnumValueList.Value)
                    {
                        var item = new Item {name = prefix + customEnumValue.FieldName, i18n = customEnumValue.Locale};
                        block.AddItem(item);
                    }
                }
            }
        }

        private static void AddCustomValueListsToConfig(ObjectBuildDescription objectBuild)
        {
            ComponentReference animationsContainerComponentReference = 
                objectBuild.ContainedObjectDescriptor.Components.FirstOrDefault(x => x.Type == typeof(VarwinAnimationPlayer));
            if (animationsContainerComponentReference == null)
            {
                return;
            }
            var customAnimations = new List<VarwinCustomAnimation>();
            var valueListName = "";
            Component component = animationsContainerComponentReference.Component;
            
            if (component is VarwinAnimationPlayer varwinAnimationPlayer)
            {
                customAnimations = varwinAnimationPlayer.GetCustomAnimations();
                valueListName = varwinAnimationPlayer.GetCustomAnimationsValueListName();
                
                Block block = GetBlock($"{animationsContainerComponentReference.PrefixName}{valueListName}", "valueList");
                block.valueType = "System.Object";
                if (customAnimations.Count > 0)
                {
                    for (int i = 0; i < customAnimations.Count; i++)
                    {
                        if (!customAnimations[i].Clip)
                        {
                            continue;
                        }
                        var clipNameSpecified = false;
                        foreach (LocalizationString localizationString in customAnimations[i].Name.LocalizationStrings)
                        {
                            clipNameSpecified = clipNameSpecified || !string.IsNullOrWhiteSpace(localizationString.value);
                        }
                        I18n itemI18n = clipNameSpecified 
                            ? customAnimations[i].Name.ToI18N() 
                            : new I18n {en = ObjectHelper.ConvertToNiceName(customAnimations[i].Clip.name)};
                        var item =  new Item {name = i.ToString(), i18n = itemI18n};
                        block.AddItem(item);
                    }
                }
                else
                {
                    var item = new Item {name = "no animation", i18n = new I18n {en = "no animation", ru = "нет анимации"}};
                    block.AddItem(item);
                }
            }
        }

        private static void AddMethodsToConfig()
        {
            foreach (MethodInfo method in _wrapperType.GetMethods())
            {
                var attributes = method.GetCustomAttributes(true);
                var locales = method.GetCustomAttributes<LocaleAttribute>().ToArray();
                var argsFormat = method.GetCustomAttribute<ArgsFormatAttribute>();
                string blockName = GetBlockGroupName(attributes);

                foreach (object attribute in attributes)
                {
                    Block block;
                    Item item = new Item {method = method.Name};

                    switch (attribute)
                    {
                        case CheckerAttribute checker:
                            if (method.ReturnType != typeof(bool))
                            {
                                Debug.LogError(string.Format(SdkTexts.MethodMustReturnBoolFormat, method.Name));

                                continue;
                            }

                            block = blockName == null ? GetBlock(checker.Name ?? method.Name, "checker") : GetBlock(blockName, "checker");
                            item.i18n = checker.LocalizedNames;

                            break;
                        case ActionAttribute action:
                            block = blockName == null ? GetBlock(action.Name ?? method.Name, "action") : GetBlock(blockName, "action");
                            item.i18n = action.LocalizedNames;

                            break;
                        case FunctionAttribute function:
                            block = blockName == null ? GetBlock(method.Name, "function") : GetBlock(blockName, "function");
                            item.i18n = function.LocalizedNames;

                            break;
                        
                        case CoroutineAttribute coroutine: 
                            block = blockName == null ? GetBlock(method.Name, "coroutine") : GetBlock(blockName, "coroutine");
                            item.i18n = coroutine.LocalizedNames;
                            
                            break;
                        default:
                            continue;

                            break;
                    }

                    foreach (var locale in locales)
                    {
                        if (locale.I18n != null)
                        {
                            item.i18n = locale.I18n;

                            break;
                        }
                        else
                        {
                            if (locale.Strings.Length > 0)
                            {
                                item.SetLocale(locale.Code, locale.Strings[0]);
                            }
                        }
                    }

                    if (argsFormat != null)
                    {
                        item.variablesFormat = argsFormat.LocalizedFormat;
                    }
                    
                    foreach (var blockItem in block.items)
                    {
                        if (!I18n.Equals(item.variablesFormat, blockItem.variablesFormat))
                        {
                            Debug.LogErrorFormat(SdkTexts.ArgsFormatIsNotEqualsFormat, block.name);
                            throw new BlocklyArgsFormatIsNotEqualsException();
                        }
                    }

                    int i = 1;

                    var parameters = method.GetParameters();

                    if (parameters.Length > 0)
                    {
                        var args = new List<Arg>();

                        foreach (ParameterInfo param in parameters)
                        {
                            bool isDynamic = param.GetCustomAttributes(typeof(DynamicAttribute), true).Length > 0;
                            var arg = new Arg {valueType = GetValidTypeName(param.ParameterType, isDynamic)};

                            UseValueListAttribute useValueListAttribute = param.GetCustomAttribute<UseValueListAttribute>();

                            int j = 0;

                            foreach (var locale in locales)
                            {
                                if (locale.I18n != null)
                                {
                                    if (j == i)
                                    {
                                        arg.i18n = locale.I18n;

                                        break;
                                    }
                                }
                                else
                                {
                                    if (locale.Strings.Length > i)
                                    {
                                        arg.SetLocale(locale.Code, locale.Strings[i]);
                                    }
                                }

                                j++;
                            }

                            if (useValueListAttribute != null)
                            {
                                arg.valueLists = useValueListAttribute.ListNames;
                            }
                            else
                            {
                                int k = 1;

                                foreach (object atr in attributes)
                                {
                                    if (atr is ValuesAttribute valuesAttribute)
                                    {
                                        if (i == k)
                                        {
                                            arg.values = valuesAttribute.Name;
                                        }

                                        k++;
                                    }
                                }
                            }

                            if (param.GetCustomAttributes(typeof(DynamicAttribute), true).Length > 0)
                            {
                                arg.valueType = "dynamic";
                            }

                            args.Add(arg);

                            i++;
                        }

                        if (block.args == null)
                        {
                            block.args = new List<Arg>();
                            block.args.AddRange(args);
                        }
                        else
                        {
                            if (block.args.Count != args.Count)
                            {
                                Debug.LogError(SdkTexts.CountArgumentsMethodsIsNotEquals);

                                throw new BlocklyArgumentsException();
                            }

                            if (args.Where((t, pIndex) => t.valueType != block.args[pIndex].valueType).Any())
                            {
                                Debug.LogError(SdkTexts.CountArgumentsMethodsIsNotEquals);

                                throw new BlocklyArgumentsException();
                            }
                        }
                    }


                    block.AddItem(item);
                }
            }
        }

        private static string GetBlockGroupName(object[] attributes)
        {
            foreach (object attribute in attributes)
            {
                if (attribute is GroupAttribute groupAttribute)
                {
                    return groupAttribute.GroupName;
                }
            }

            return null;
        }

        private static void AddEventsToConfig()
        {
            var eventDelegates = new Dictionary<string, MethodLocale>();

            foreach (EventInfo eventInfo in _wrapperType.GetEvents())
            {
                var attributes = eventInfo.GetCustomAttributes<EventAttribute>(true);
                var locales = eventInfo.GetCustomAttributes<LocaleAttribute>();
                var paramsFormat = eventInfo.GetCustomAttribute<ArgsFormatAttribute>();
                var customAttributes = eventInfo.GetCustomAttributes(true);
                string blockGroup = GetBlockGroupName(customAttributes);

                foreach (EventAttribute eventAttribute in attributes)
                {
                    string eventName;

                    if (blockGroup != null)
                    {
                        eventName = blockGroup;
                    }
                    else
                    {
                        eventName = eventAttribute.Name ?? eventInfo.Name;
                    }

                    Block block = GetBlock(eventName, "event");
                    var item = new Item {method = eventInfo.Name, i18n = eventAttribute.LocalizedNames};

                    foreach (var locale in locales)
                    {
                        if (locale.I18n != null)
                        {
                            item.i18n = locale.I18n;

                            break;
                        }
                        else
                        {
                            if (locale.Strings.Length > 0)
                            {
                                item.SetLocale(locale.Code, locale.Strings[0]);
                            }
                        }
                    }

                    if (paramsFormat != null)
                    {
                        item.variablesFormat = paramsFormat.LocalizedFormat;
                    }

                    block.items.Add(item);
                    Type delegateType = eventInfo.EventHandlerType;
                    MethodInfo method = delegateType.GetMethod("Invoke");

                    if (eventDelegates.ContainsKey(eventName))
                    {
                        continue;
                    }

                    if (method == null)
                    {
                        continue;
                    }

                    if (method.DeclaringType == null)
                    {
                        continue;
                    }

                    var localeAttributes = method.DeclaringType.GetCustomAttributes<LocaleAttribute>();

                    var methodLocale = new MethodLocale
                    {
                        MemberInfo = method,
                        LocaleAttributies = localeAttributes.ToArray()
                    };

                    eventDelegates.Add(eventName, methodLocale);
                }
            }

            foreach (var eventDelegate in eventDelegates)
            {
                Block block = GetBlock(eventDelegate.Key);
                int i = 0;

                foreach (ParameterInfo param in eventDelegate.Value.MemberInfo.GetParameters())
                {
                    if (block.@params == null)
                    {
                        block.@params = new List<Param>();
                    }

                    bool isDynamic = param.GetCustomAttributes(typeof(DynamicAttribute), true).Length > 0;

                    var paramBlockly = new Param
                    {
                        name = param.Name,
                        valueType = GetValidTypeName(param.ParameterType, isDynamic)
                    };

                    int j = 0;

                    foreach (var locale in eventDelegate.Value.LocaleAttributies)
                    {
                        if (locale.I18n != null)
                        {
                            if (i == j)
                            {
                                paramBlockly.i18n = locale.I18n;

                                break;
                            }
                        }
                        else
                        {
                            if (locale.Strings.Length > i)
                            {
                                paramBlockly.SetLocale(locale.Code, locale.Strings[i]);
                            }
                        }

                        j++;
                    }

                    if (param.GetCustomAttributes(typeof(DynamicAttribute), true).Length > 0)
                    {
                        paramBlockly.valueType = "dynamic";
                    }

                    block.@params.Add(paramBlockly);

                    i++;
                }
            }
        }

        public static string GetValidTypeName(Type type, bool isDynamic = false)
        {
            string typeName = type.FullName;

            if (_nullableRegex.IsMatch(typeName))
            {
                typeName = _nullableRegex.Replace(typeName, "$1?");
            }

            if (_dictionaryRegex.IsMatch(typeName))
            {
                typeName = _dictionaryRegex.Replace(typeName, "$1<$2,$3>");
            }

            if (_listRegex.IsMatch(typeName))
            {
                typeName = _listRegex.Replace(typeName, "$1<$2>");
            }

            if (isDynamic)
            {
                typeName = Regex.Replace(typeName, @"\bSystem\.Object\b", "dynamic");
            }

            string assemblyName = type.Assembly.GetName().Name;

            if (type.Assembly == _wrapperType.Assembly)
            {
                return typeName;
            }
            
            if (!assemblyName.StartsWith("VarwinCore") && !assemblyName.StartsWith("UnityEngine") && !assemblyName.StartsWith("mscorlib") && !assemblyName.StartsWith("System"))
            {
                throw new Exception(string.Format(SdkTexts.CustomTypesForbiddenFormat, typeName));
            }

            return typeName;
        }

        private static bool IsValuesArgAdded(List<Arg> args)
        {
            foreach (Arg arg in args)
            {
                if (arg.values != null)
                {
                    return true;
                }
            }

            return false;
        }

        private static Block GetBlock(string blockName, string blockType = null, string valueType = null)
        {
            foreach (Block block in _config.Config.blocks)
            {
                if (string.Equals(block.name, blockName, StringComparison.Ordinal))
                {
                    return block;
                }
            }

            var newBlock = new Block
            {
                name = blockName,
                items = new List<Item>(),
                type = blockType,
                valueType = valueType
            };
            _config.Config.blocks.Add(newBlock);

            return newBlock;
        }

        internal class BlocklyArgumentsException : Exception
        {
        }
        
        internal class BlocklyArgsFormatIsNotEqualsException : Exception
        {
        }
    }


}
