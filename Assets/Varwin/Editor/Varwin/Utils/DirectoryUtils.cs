namespace Varwin.Editor
{
    public static class DirectoryUtils
    {
        public static void OpenFolder(string path)
        {
            var p = new System.Diagnostics.Process
            {
                StartInfo =
                {
                    FileName = "cmd.exe", 
                    WorkingDirectory = path.Replace(@"/", @"\"), 
                    UseShellExecute = false, 
                    Arguments = "/C start .",
                    CreateNoWindow = true,
                    WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden
                }
            };
            p.Start();
        }
    }
}