namespace Varwin.Editor
{
    public class CustomEnumValueDescriptor
    {
        public string FieldName;
        public I18n Locale;
        public int Value;
        
        public CustomEnumValueDescriptor(){}
        
        public CustomEnumValueDescriptor(string fieldName, I18n locale, int value)
        {
            FieldName = fieldName;
            Locale = locale;
            Value = value;
        }
    }
}