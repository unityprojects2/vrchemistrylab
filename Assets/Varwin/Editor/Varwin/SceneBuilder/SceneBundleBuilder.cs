﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Ionic.Zip;
using Newtonsoft.Json;
using Varwin.Editor.PreviewGenerators;
using Varwin.Data;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

namespace Varwin.Editor
{
    public class SceneBundleBuilder
    {
        private string _bakedDirectory;
        private string _sourcesDirectory;
        private string _bundleDirectory;
        private string _objectName;
        private string _prefabPath;
        private string _zipFilePath;
        private string _iconPath;
        private Object _createdObject;
        private bool _mobileReady;
        private bool _sourcesIncluded;

        private const int SaveIter = 50;

        public string BuildAndShow(WorldDescriptor descriptor, SceneCamera sceneCamera)
        {
            SaveCurrentScene();
            UnityEngine.SceneManagement.Scene scene = SceneManager.GetActiveScene();
            Debug.Log(scene.path);
            
            _bakedDirectory = SdkSettings.SceneTemplateBuildingFolderPath;
            if (!Directory.Exists(_bakedDirectory))
            {
                try
                {
                    Directory.CreateDirectory(_bakedDirectory);
                }
                catch
                {
                    var message = string.Format(SdkTexts.CannotCreateDirectoryFormat, _bakedDirectory);
                    Debug.LogError(message);
                    
                    EditorUtility.DisplayDialog(SdkTexts.CannotCreateDirectoryTitle,
                        message,
                        "OK");
                    throw;
                }
            }
            
            string tempBundleIconFile = _bakedDirectory + "/bundle.png";
            CreateIcon(tempBundleIconFile, sceneCamera.GetTexturePngBytes(ImageSize.Square256));
            
            string tempThumbnailFile = _bakedDirectory + "/thumbnail.jpg";
            CreateIcon(tempThumbnailFile, sceneCamera.GetTextureJpgBytes(ImageSize.MarketplaceLibrary));
            
            string tempViewFile = _bakedDirectory + "/view.jpg";
            CreateIcon(tempViewFile, sceneCamera.GetTextureJpgBytes(ImageSize.FullHD));
            
            _bundleDirectory = $"{UnityProject.Path}/AssetBundles";
            if (!Directory.Exists(_bundleDirectory))
            {
                try
                {
                    Directory.CreateDirectory(_bundleDirectory);
                }
                catch
                {
                    var message = string.Format(SdkTexts.CannotCreateDirectoryFormat, _bakedDirectory);
                    Debug.LogError(message);
                    
                    EditorUtility.DisplayDialog(SdkTexts.CannotCreateDirectoryTitle,
                        message,
                        "OK");
                    throw;
                }
            }
            
            _sourcesDirectory = $"{UnityProject.Path}/SourcePackages";
            if (!Directory.Exists(_sourcesDirectory))
            {
                try
                {
                    Directory.CreateDirectory(_sourcesDirectory);
                }
                catch
                {
                    var message = string.Format(SdkTexts.CannotCreateDirectoryFormat, _sourcesDirectory);
                    Debug.LogError(message);
                    EditorUtility.DisplayDialog(SdkTexts.CannotCreateDirectoryTitle,
                        message,
                        "OK");
                    throw;
                }
            }
            
            _objectName = descriptor.LocalizedName.Get(SystemLanguage.English)?.value;
            if (string.IsNullOrEmpty(_objectName))
            {
                _objectName = descriptor.LocalizedName.FirstOrDefault(x => !string.IsNullOrEmpty(x.value))?.value;
            }
            _prefabPath = scene.path;
            _zipFilePath = _bakedDirectory + $"/{_objectName}.vwst";
            _sourcesIncluded = descriptor.SourcesIncluded;
            _mobileReady = SdkSettings.MobileFeature.Enabled && descriptor.MobileReady;

            PackSources();
            BuildAssetBundles();

            CreateFolders(descriptor);

            string bundleFile = _bundleDirectory + "/" + _objectName;
            string tempBundlePath = _bakedDirectory + "/bundle";
            string tempBundleManifest = tempBundlePath + ".manifest";
            CopyBundles(bundleFile, tempBundlePath, tempBundleManifest);

            var androidBundleFile = _bundleDirectory + "/android_" + _objectName;
            var tempAndroidBundlePath = _bakedDirectory + "/android_bundle";
            var tempAndroidBundleManifest = tempAndroidBundlePath + ".manifest";
            if (_mobileReady)
            {   
                CopyBundles(androidBundleFile, tempAndroidBundlePath, tempAndroidBundleManifest);
            }
            
            string sourcesPath = _sourcesDirectory + "/" + _objectName + ".unitypackage";
            string tempSourcesPath = _bakedDirectory + "/sources.unitypackage";
            if (_sourcesIncluded)
            {
                File.Copy(sourcesPath, tempSourcesPath, true);
            }

            string tempInstallJsonFile = _bakedDirectory + "/install.json";
            string tempBundleJsonFile = _bakedDirectory + "/bundle.json";
            CreateConfig(descriptor, tempInstallJsonFile, tempBundleJsonFile);

            List<string> filesToZip = new string[]
            {
                tempBundlePath,
                tempBundleManifest,
                tempBundleJsonFile,
                tempInstallJsonFile,
                tempBundleIconFile,
                tempThumbnailFile,
                tempViewFile
            }.ToList();

            if (_sourcesIncluded)
            {
                filesToZip.Add(tempSourcesPath);
            }

            if (_mobileReady)
            {
                filesToZip.Add(tempAndroidBundlePath);
                filesToZip.Add(tempAndroidBundleManifest);
            }

            foreach (var dll in descriptor.DllNames)
            {
                string output = _bakedDirectory + "/" + Path.GetFileName(dll);
                File.Copy(dll, output);
                filesToZip.Add(output);
            }

            return ZipFiles(filesToZip);
        }

        private void CreateIcon(string path, byte[] icon)
        {
            File.WriteAllBytes(path, icon);
        }

        private void SaveCurrentScene()
        {
            try
            {
                EditorSceneManager.SaveScene(SceneManager.GetActiveScene());
            }
            catch
            {
                Debug.LogError(SdkTexts.CannotApplyPrefab);
            }
        }

        private Action OnCompile;

        private void PackSources()
        {
            if (!_sourcesIncluded)
            {
                return;
            }
            
            var collector = new VarwinDependenciesCollector();
            var paths = collector.CollectPathsForScene(_prefabPath);
            
            var filePath = _sourcesDirectory + "/" + _objectName + ".unitypackage";
            AssetDatabase.ExportPackage(paths.ToArray(), filePath, ExportPackageOptions.Default);
        }
        
        private void BuildAssetBundles()
        {
            if (!Directory.Exists(_bakedDirectory))
            {
                try
                {
                    Directory.CreateDirectory(_bakedDirectory);
                }
                catch
                {
                    var message = string.Format(SdkTexts.CannotCreateDirectoryFormat, _bakedDirectory);
                    Debug.LogError(message);
                    
                    EditorUtility.DisplayDialog(
                        SdkTexts.CannotCreateDirectoryTitle,
                        message,
                        "OK");
                    throw;
                }
            }

            var buildTargets = new List<BuildTarget>()
            {
                BuildTarget.StandaloneWindows64, 
                BuildTarget.Android 
            };
            BuildAssetBundles(buildTargets);
        }
        
        private void BuildAssetBundles(List<BuildTarget> buildTargets)
        {
            var currentTarget = EditorUserBuildSettings.activeBuildTarget;
            if (buildTargets.Contains(currentTarget))
            {
                var buildHandler = GetBuildBundlesHandler(currentTarget);
                buildHandler?.Invoke();
                buildTargets.Remove(currentTarget);
            }

            foreach (var target in buildTargets)
            {
                var buildHandler = GetBuildBundlesHandler(target);
                buildHandler?.Invoke();
            }
        }
        
        private Action GetBuildBundlesHandler(BuildTarget buildTarget)
        {
            switch (buildTarget)
            {
                case BuildTarget.StandaloneWindows64: 
                    return BuildWindowsBundles;
                case BuildTarget.Android: 
                    return BuildAndroidBundles;
                default:
                    Debug.LogError(string.Format(SdkTexts.BuildTargetNoSupportedFormat, buildTarget));
                    return null;
            }
        }
        
        private void BuildWindowsBundles()
        {
            AssetBundleBuild assetBundleBuild = default;

            assetBundleBuild.assetNames = new[] {_prefabPath};
            assetBundleBuild.assetBundleName = _objectName;

            BuildPipeline.BuildAssetBundles(
                _bundleDirectory,
                new[] {assetBundleBuild},
                BuildAssetBundleOptions.UncompressedAssetBundle,
                BuildTarget.StandaloneWindows64);
        }
        
        private void BuildAndroidBundles()
        {
            if (SdkSettings.MobileFeature.Enabled && _mobileReady)
            {
                AssetBundleBuild assetBundleBuild = default;

                assetBundleBuild.assetNames = new[] {_prefabPath};
                assetBundleBuild.assetBundleName = "android_" + _objectName;

                AndroidTextureOverrider.OverrideTextures(_prefabPath);
                
                BuildPipeline.BuildAssetBundles(
                    _bundleDirectory,
                    new[] {assetBundleBuild},
                    BuildAssetBundleOptions.UncompressedAssetBundle,
                    BuildTarget.Android);
            }
        }

        private void CopyBundles(string bundlePath, string bakedBundlePath, string bakedBundleManifest)
        {
            string bundleManifestPath = bundlePath + ".manifest";

            File.Copy(bundlePath, bakedBundlePath, true);
            File.Copy(bundleManifestPath, bakedBundleManifest, true);
        }

        private void CreateFolders(WorldDescriptor descriptor)
        {
            if (Directory.Exists(_bakedDirectory))
            {
                return;
            }

            try
            {
                Directory.CreateDirectory(_bakedDirectory);
            }
            catch (Exception e)
            {
                Debug.LogError(SdkTexts.CannotCreateDirectoryTitle + "\n" + e);
            }
        }

        private void CreateConfig(WorldDescriptor descriptor, string installJsonPath, string configJsonPath)
        {
            Debug.Log(string.Format(SdkTexts.CoreVersionFormat, VarwinVersionInfo.VarwinVersion));
            
            string builtAt = $"{DateTimeOffset.UtcNow:s}Z";
            if (DateTimeOffset.TryParse(descriptor.BuiltAt, out DateTimeOffset builtAtDateTimeOffset))
            {
                builtAt = $"{builtAtDateTimeOffset.UtcDateTime:s}Z";
            }

            string defaultName = descriptor.LocalizedName.Get(SystemLanguage.English)?.value ?? descriptor.LocalizedName.FirstOrDefault(x => !string.IsNullOrEmpty(x.value))?.value;
            string defaultDescription = descriptor.LocalizedDescription.Get(SystemLanguage.English)?.value ?? descriptor.LocalizedDescription.FirstOrDefault(x => !string.IsNullOrEmpty(x.value))?.value;

            var installConfig = new CreateSceneTemplateWindow.InstallConfig
            {
                Name = descriptor.LocalizedName.ToI18N(),
                Description = descriptor.LocalizedDescription.ToI18N(),
                Guid = descriptor.Guid,
                RootGuid = descriptor.RootGuid,
                Author = new CreateSceneTemplateWindow.Author()
                {
                    Name = descriptor.AuthorName,
                    Email = descriptor.AuthorEmail,
                    Url = descriptor.AuthorUrl,
                },
                BuiltAt = builtAt,
                License = new CreateSceneTemplateWindow.License()
                {
                    Code = descriptor.LicenseCode,
                    Version = descriptor.LicenseVersion,
                },
                SourcesIncluded = descriptor.SourcesIncluded,
                MobileReady = SdkSettings.MobileFeature.Enabled && descriptor.MobileReady,
                SdkVersion = VarwinVersionInfo.VersionNumber
            };
            
            var sceneConfig = new CreateSceneTemplateWindow.SceneConfig
            {
                name = defaultName,
                description = defaultDescription,
                image = descriptor.Image,
                assetBundleLabel = descriptor.AssetBundleLabel,
                dllNames = descriptor.DllNames.Select(Path.GetFileName).ToArray()
            };

            var settings = new JsonSerializerSettings()
            {
                TypeNameHandling = TypeNameHandling.Auto,
                NullValueHandling = NullValueHandling.Ignore
            };

            if (installConfig.Name.IsEmpty())
            {
                throw new Exception($"Scene does not have a localized name.");
            }
            
            string json = JsonConvert.SerializeObject(installConfig, settings);
            
            File.WriteAllText(installJsonPath, json);
            File.WriteAllText(configJsonPath, sceneConfig.ToJson());
        }

        private string ZipFiles(List<string> files)
        {
            try
            {
                using (ZipFile loanZip = new ZipFile())
                {
                    loanZip.AddFiles(files, false, "");
                    loanZip.Save(_zipFilePath);
                }

                foreach (string file in files)
                {
                    File.Delete(file);
                }

                Debug.Log(SdkTexts.ZipCreateSuccessMessage);

                return _zipFilePath;
            }
            catch (Exception e)
            {
                Debug.LogError(SdkTexts.ZipCreateFailMessage + "\n" + e.Message);
            }

            return default(string);
        }
    }
}
