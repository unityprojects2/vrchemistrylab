using System.Diagnostics;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Varwin.Editor
{
    public static class PackageBuilder
    {
        public static void BuildVarwinSdk()
        {
            AssetDatabase.ExportPackage("Assets", $"{GetOutputPath()}/VarwinSDK.unitypackage", ExportPackageOptions.Recurse);
            Process.GetCurrentProcess().Kill();
        }

        public static void BuildVarwinSdkDebugPlugin()
        {
            var assets = new []
            {
                $"Assets/Varwin/Core/Resources/PlayerRig",
                $"Assets/Varwin/Scenes",
                $"Assets/Varwin/Standalone Input"
            };
            
            AssetDatabase.ExportPackage(assets, $"{GetOutputPath()}/VarwinSDK Debug Plugin.unitypackage", ExportPackageOptions.Recurse);
            Process.GetCurrentProcess().Kill();
        }

        public static void BuildVarwinSdkAssetStore()
        {
            var assets = AssetDatabase.GetAllAssetPaths()
                .Where(x => !x.StartsWith("Assets/Varwin/Core/Resources/PlayerRig"))
                .Where(x => !x.StartsWith("Assets/Varwin/Scenes"))
                .Where(x => !x.StartsWith("Assets/Varwin/Standalone Input"))
                .Where(x => !x.StartsWith("Assets/SteamVR_Input"))
                .ToArray();

            AssetDatabase.ExportPackage(assets, $"{GetOutputPath()}/VarwinSDK Assetstore.unitypackage", ExportPackageOptions.Default);
            Process.GetCurrentProcess().Kill();
        }

        private static string GetOutputPath()
        {
            var output = $"{UnityProject.Path}/Output";
            if (!Directory.Exists(output))
            {
                Directory.CreateDirectory(output);
            }
            return output;
        }
    }
}