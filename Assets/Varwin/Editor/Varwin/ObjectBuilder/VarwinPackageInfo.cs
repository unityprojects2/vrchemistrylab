using System;
using Newtonsoft.Json;
using UnityEditor;
using Varwin.Data;
using Varwin.Public;

namespace Varwin.Editor.ObjectBuilding
{
    public class VarwinPackageInfo : IJsonSerializable
    {
        public string Name;
        public string Config;
        public string IconPath;
        public string ViewPath;
        public string ThumbnailPath;

        public VarwinPackageInfo()
        {
        }
        
        public VarwinPackageInfo(VarwinPackageDescriptor varwinPackageDescriptor)
        {
            Name = varwinPackageDescriptor.name;
            Config = GenerateInstallJson(varwinPackageDescriptor);
            IconPath = varwinPackageDescriptor.Icon ? AssetDatabase.GetAssetPath(varwinPackageDescriptor.Icon) : null;
            ViewPath = varwinPackageDescriptor.View ? AssetDatabase.GetAssetPath(varwinPackageDescriptor.View) : null;
            ThumbnailPath = varwinPackageDescriptor.Thumbnail ? AssetDatabase.GetAssetPath(varwinPackageDescriptor.Thumbnail) : null;
        }

        private string GenerateInstallJson(VarwinPackageDescriptor varwinPackageDescriptor)
        {
            string builtAt = $"{DateTimeOffset.UtcNow:s}Z";

            if (DateTimeOffset.TryParse(varwinPackageDescriptor.BuiltAt, out DateTimeOffset builtAtDateTimeOffset))
            {
                builtAt = $"{builtAtDateTimeOffset.UtcDateTime:s}Z";
            }

            var installJson = new InstallJson
            {
                Name = varwinPackageDescriptor.Name.ToI18N(),
                Description = varwinPackageDescriptor.Description.ToI18N(),
                Guid = varwinPackageDescriptor.Guid,
                RootGuid = varwinPackageDescriptor.RootGuid,
                Author = new JsonAuthor
                {
                    Email = varwinPackageDescriptor.AuthorEmail,
                    Name = varwinPackageDescriptor.AuthorName,
                    Url = varwinPackageDescriptor.AuthorUrl,
                },
                License = new JsonLicense
                {
                    Code = varwinPackageDescriptor.LicenseCode,
                    Version = varwinPackageDescriptor.LicenseVersion,
                },
                BuiltAt = builtAt,
                SdkVersion = VarwinVersionInfo.VersionNumber,
            };
            
            var jsonSerializerSettings = new JsonSerializerSettings {NullValueHandling = NullValueHandling.Ignore};
            string jsonConfig = JsonConvert.SerializeObject(installJson, Formatting.None, jsonSerializerSettings);

            return jsonConfig;
        }

        private class InstallJson : IJsonSerializable
        {
            public I18n Name;
            public I18n Description;
            public string Guid;
            public string RootGuid;
            public JsonAuthor Author;
            public JsonLicense License;
            public string BuiltAt;
            public string SdkVersion;
        }
    }
}