using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Varwin.Editor.ObjectBuilding
{
    public class SourcePackagingState : BaseState
    {
        public SourcePackagingState(ObjectBuilder objectBuilder) : base(objectBuilder)
        {
            Label = "Source packing";
        }

        protected override void OnEnter()
        {
            if (Directory.Exists(ObjectBuildingPath.SourcePackages))
            {
                return;
            }

            try
            {
                Directory.CreateDirectory(ObjectBuildingPath.SourcePackages);
            }
            catch
            {
                string message = string.Format(SdkTexts.CannotCreateDirectoryFormat, ObjectBuildingPath.SourcePackages);
                Debug.LogError(message);
                EditorUtility.DisplayDialog(SdkTexts.CannotCreateDirectoryTitle, message, "OK");
                    
                EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows64);
            }
        }

        protected override void Update(ObjectBuildDescription currentObjectBuildDescription)
        {
            Label = string.Format(SdkTexts.PackSourcesStep, currentObjectBuildDescription.ObjectName);
            
            try
            {
                if (!currentObjectBuildDescription.ContainedObjectDescriptor.SourcesIncluded)
                {
                    return;
                }
                
                var collector = new VarwinDependenciesCollector();
                var paths = collector.CollectPathsForObject(currentObjectBuildDescription.ContainedObjectDescriptor.Prefab);

                string filePath = $"{ObjectBuildingPath.SourcePackages}/{currentObjectBuildDescription.ContainedObjectDescriptor.Name}.unitypackage";
                AssetDatabase.ExportPackage(paths.ToArray(), filePath, ExportPackageOptions.Default);
            }
            catch (Exception e)
            {
                currentObjectBuildDescription.HasError = true;
                string message = string.Format(SdkTexts.ProblemWhenPackSourcesFormat, e.Message);
                
                if (ObjectBuildDescriptions.Count == 1)
                {
                    EditorUtility.DisplayDialog("Error!", message, "OK");
                }

                Debug.LogError(message + "\n" + e);
                EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows64);
            }
        }
    }
}