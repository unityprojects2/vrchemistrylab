using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Varwin.Editor.ObjectBuilding
{
    public class PreviewGenerationState : BaseState
    {
        public PreviewGenerationState(ObjectBuilder objectBuilder) : base(objectBuilder)
        {
            Label = string.Format(SdkTexts.CreatePreviewStep, "objects");
        }

        protected override void OnEnter()
        {
            if (Directory.Exists(ObjectBuildingPath.ObjectPreviews))
            {
                return;
            }
            
            try
            {
                Directory.CreateDirectory(ObjectBuildingPath.ObjectPreviews);
            }
            catch
            {
                string message = string.Format(SdkTexts.CannotCreateDirectoryFormat, ObjectBuildingPath.ObjectPreviews);
                Debug.LogError(message);
                EditorUtility.DisplayDialog(SdkTexts.CannotCreateDirectoryTitle, message, "OK");
            }
        }

        protected override void Update(ObjectBuildDescription currentObjectBuildDescription)
        {
            Label = string.Format(SdkTexts.CreatePreviewStep, currentObjectBuildDescription.ObjectName);
            
            try
            {
                var spritesheetBuilder = new SpritesheetBuilder();
                spritesheetBuilder.Build(currentObjectBuildDescription.ContainedObjectDescriptor, ObjectBuildingPath.ObjectPreviews);

                var previewBuilder = new ViewBuilder();
                previewBuilder.Build(currentObjectBuildDescription.ContainedObjectDescriptor, ObjectBuildingPath.ObjectPreviews);

                var thumbnailBuilder = new ThumbnailBuilder();
                thumbnailBuilder.Build(currentObjectBuildDescription.ContainedObjectDescriptor, ObjectBuildingPath.ObjectPreviews);
            }
            catch (Exception e)
            {
                currentObjectBuildDescription.HasError = true;
                Debug.LogError(string.Format(SdkTexts.ProblemWhenCreatePreview, e.Message) + "\n" + e);
            }
        }
    }
}