using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Ionic.Zip;
using UnityEditor;
using UnityEngine;
using Varwin.Public;

namespace Varwin.Editor.ObjectBuilding
{
    public class PackageCreationState : BaseState
    {
        private const string DefaultIconPath = @"Assets/Varwin/Editor/Varwin/Resources/DefaultObjectIcon.png";
        private List<string> _filesToZip;
        
        public PackageCreationState(ObjectBuilder objectBuilder) : base(objectBuilder)
        {
            Label = "Package Creation";
        }

        protected override void OnEnter()
        {
            _filesToZip = new List<string>();
        }
        
        protected override void Update(ObjectBuildDescription currentObjectBuildDescription)
        {
            Label = "Package Creation";
            
            try
            {
                _filesToZip.Add($"{ObjectBuildingPath.BakedObjects}/{currentObjectBuildDescription.ObjectName}.vwo");
            }
            catch (Exception e)
            {
                string message = $"{currentObjectBuildDescription.ObjectName} error: Problem when package creation";
                Debug.LogError(message + "\n" + e);
                currentObjectBuildDescription.HasError = true;
                if (ObjectBuildDescriptions.Count == 1)
                {
                    EditorUtility.DisplayDialog("Error!", message, "OK");
                }
                    
                EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows64);
            }
        }

        protected override void OnExit()
        {
            string outputFilename = ObjectBuilder.PackageInfo.Name;
            string outputExtension = SdkSettings.PackageFeature.Enabled ? "vwpkg" : "zip";
            string outputPath = $"{ObjectBuildingPath.BakedObjects}/{outputFilename}.{outputExtension}";

            if (SdkSettings.PackageFeature.Enabled)
            {
                _filesToZip.Add(WriteInstallJson(ObjectBuilder.PackageInfo.Config));

                _filesToZip.Add(GenerateIcon(ObjectBuilder.PackageInfo.IconPath ?? DefaultIconPath));
                _filesToZip.Add(GenerateView(ObjectBuilder.PackageInfo.ViewPath ?? DefaultIconPath));
                _filesToZip.Add(GenerateThumbnail(ObjectBuilder.PackageInfo.ThumbnailPath ?? DefaultIconPath));
            }
            
            ZipFiles(_filesToZip, outputPath);
        }

        private string WriteInstallJson(string installJson)
        {
            string targetInstallJsonFile = $"{ObjectBuildingPath.BakedObjects}/install.json";
            File.WriteAllText(targetInstallJsonFile, installJson);
            return targetInstallJsonFile;
        }

        private string GenerateIcon(string iconPath)
        {
            string targetPath = $"{ObjectBuildingPath.BakedObjects}/icon.png";
            return GenerateImageFromTexture(iconPath, targetPath, false);
        }

        private string GenerateView(string iconPath)
        {
            string targetPath = $"{ObjectBuildingPath.BakedObjects}/view.jpg";
            return GenerateImageFromTexture(iconPath, targetPath, true);
        }

        private string GenerateThumbnail(string iconPath)
        {
            string targetPath = $"{ObjectBuildingPath.BakedObjects}/thumbnail.jpg";
            return GenerateImageFromTexture(iconPath, targetPath, true);
        }

        private string GenerateImageFromTexture(string sourcePath, string targetPath, bool isJpg)
        {
            var image = AssetDatabase.LoadAssetAtPath<Texture2D>(sourcePath);
            var generatedImage = new Texture2D(image.width, image.height, TextureFormat.RGBA32, false);
            if (generatedImage.LoadImage(File.ReadAllBytes(sourcePath)))
            {
                File.WriteAllBytes(targetPath, isJpg ? generatedImage.EncodeToJPG() : generatedImage.EncodeToPNG());
            }
            else
            {
                File.Copy(sourcePath, targetPath);
            }

            return targetPath;
        }
        
        private void ZipFiles(List<string> files, string zipFilePath)
        {
            using (var loanZip = new ZipFile())
            {
                loanZip.AddFiles(files, false, "");
                loanZip.Save(zipFilePath);
            }

            foreach (string file in files)
            {
                TryDeleteFile(file);
            }
        }

        private void TryDeleteFile(string path)
        {
            if (!File.Exists(path))
            {
                return;
            }

            try
            {
                File.Delete(path);
            }
            catch
            {
                Debug.LogError(string.Format(SdkTexts.CannotDeleteFileFormat, path));
            }
        }
    }
}