using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Varwin.Editor.ObjectBuilding
{
    public abstract class BaseState
    {
        protected ObjectBuilder ObjectBuilder;
        protected int CurrentIndex;
        
        protected List<ObjectBuildDescription> ObjectBuildDescriptions => ObjectBuilder.ObjectsToBuild;
        
        public string Label { get; protected set; }
        public bool IsFinished { get; protected set; }

        public float Progress { get; private set; }

        public BaseState(ObjectBuilder objectBuilder)
        {
            ObjectBuilder = objectBuilder;
            CurrentIndex = -1;
            Progress = 0;
            IsFinished = false;
        }
        
        public void Update()
        {
            if (IsFinished)
            {
                return;
            }
            
            if (CurrentIndex < 0)
            {
                OnEnter();
                CurrentIndex = 0;
            }
            else if (CurrentIndex < ObjectBuildDescriptions.Count)
            {
                ObjectBuildDescription currentObjectBuildDescription = ObjectBuildDescriptions[CurrentIndex];
                if (currentObjectBuildDescription != null && !currentObjectBuildDescription.HasError)
                {
                    Update(currentObjectBuildDescription);
                }

                CurrentIndex++;
            }
            else
            {
                IsFinished = true;
                OnExit();
            }

            Progress = Mathf.Clamp01(Mathf.Max(Progress, (float) CurrentIndex / ObjectBuildDescriptions.Count));
        }
        
        protected virtual void OnEnter()
        {
            
        }

        protected virtual void OnExit()
        {
            
        }

        protected abstract void Update(ObjectBuildDescription currentObjectBuildDescription);
    }
}