using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Varwin.Public;

namespace Varwin.Editor.ObjectBuilding
{
    public class PreparationState : BaseState
    {
        public PreparationState(ObjectBuilder objectBuilder) : base(objectBuilder)
        {
            Label = $"Preparing";
        }

        protected override void Update(ObjectBuildDescription currentObjectBuildDescription)
        {
            Label = $"Prepare {currentObjectBuildDescription.ObjectName}";
            
            try
            {
                GameObject go = UnityEngine.Object.Instantiate(currentObjectBuildDescription.GameObject);
                
                var varwinObjectDescriptor = go.GetComponent<VarwinObjectDescriptor>();
                varwinObjectDescriptor.PreBuild();
                CreateObjectUtils.SetupAsmdef(varwinObjectDescriptor);
                CreateObjectUtils.SetupComponentReferences(varwinObjectDescriptor);
                go.SetActive(true);
                CreateObjectUtils.ApplyPrefabInstanceChanges(go);

                var varwinObjectDescriptorsOnScene = UnityEngine.Object.FindObjectsOfType<VarwinObjectDescriptor>();
                foreach (VarwinObjectDescriptor varwinObjectDescriptorOnScene in varwinObjectDescriptorsOnScene)
                {
                    if (varwinObjectDescriptorOnScene.Guid == varwinObjectDescriptor.Guid)
                    {
                        CreateObjectUtils.RevertPrefabInstanceChanges(varwinObjectDescriptorOnScene.gameObject);
                    }
                }

                UnityEngine.Object.DestroyImmediate(go);

                string anyGlobalScript = CreateObjectUtils.GetGlobalScriptsPaths(currentObjectBuildDescription.ContainedObjectDescriptor).FirstOrDefault(); 
                if (anyGlobalScript != null)
                {
                    throw new Exception(string.Format(SdkTexts.ScriptWithoutAsmdefInAssetsFormat, anyGlobalScript));
                }
            }
            catch (Exception e)
            {
                string message = $"{currentObjectBuildDescription.ObjectName} error: Problem when preparation object";
                Debug.LogError(message + "\n" + e);
                currentObjectBuildDescription.HasError = true;
                if (ObjectBuildDescriptions.Count == 1)
                {
                    EditorUtility.DisplayDialog("Error!", message, "OK");
                }
                    
                EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows64);
            }
        }
    }
}