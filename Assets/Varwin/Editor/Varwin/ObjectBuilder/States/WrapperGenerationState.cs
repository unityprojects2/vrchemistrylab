using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using UnityEditor;
using UnityEngine;

namespace Varwin.Editor.ObjectBuilding
{
    public class WrapperGenerationState : BaseState
    {
        public WrapperGenerationState(ObjectBuilder objectBuilder) : base(objectBuilder)
        {
            Label = "Generating wrappers";
        }
        
        protected override void Update(ObjectBuildDescription currentObjectBuildDescription)
        {
            Label = "Generating wrapper for " + currentObjectBuildDescription.ObjectName;
            
            try
            {
                WrapperGenerator.GenerateWrapper(currentObjectBuildDescription.ContainedObjectDescriptor);
            }
            catch (Exception e)
            {
                Debug.LogErrorFormat(currentObjectBuildDescription.ContainedObjectDescriptor, SdkTexts.ProblemWhenRunBuildVarwinObjectFormat, "Can't build " + currentObjectBuildDescription.ObjectName, e);
                currentObjectBuildDescription.HasError = true;
            }
        }
        
        protected override void OnExit()
        {
            ObjectBuilder.Serialize();
            AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
        }
    }
}