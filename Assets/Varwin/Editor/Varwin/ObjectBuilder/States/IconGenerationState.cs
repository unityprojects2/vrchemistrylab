using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Varwin.Editor.ObjectBuilding
{
    public class IconGenerationState : BaseState
    {
        public IconGenerationState(ObjectBuilder objectBuilder) : base(objectBuilder)
        {
            Label = string.Format(SdkTexts.CreatingIconStepFormat, "objects");
        }

        protected override void Update(ObjectBuildDescription currentObjectBuildDescription)
        {
            Label = string.Format(SdkTexts.CreatingIconStepFormat, currentObjectBuildDescription.ObjectName);

            try
            {
                if (currentObjectBuildDescription.ContainedObjectDescriptor.Icon)
                {
                    return;
                }
                
                var builder = new IconBuilder();
                builder.Build(currentObjectBuildDescription);
            }
            catch (Exception e)
            {
                currentObjectBuildDescription.HasError = true;
                Debug.LogError(string.Format(SdkTexts.ProblemWhenCreatePreview, e.Message) + "\n" + e);
            }
        }

        protected override void OnExit()
        {
            ObjectBuilder.Serialize();
        }
    }
}