﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Varwin.Editor.Varwin.ObjectBuilder;
using Varwin.Public;

namespace Varwin.Editor.ObjectBuilding
{
    public class ObjectBuilderWindow : EditorWindow
    {
        public static ObjectBuilderWindow Instance { get; private set; }
        
        public bool IsFinished { get; private set; }

        private ObjectBuilder _objectBuilder;
        
        private int _objectsToBuildCount;
        private int _objectsToBuildWithErrorsCount;
        private string[] _objectsToBuildWithErrors;
        private float _displayProgress;
        private Vector2 _scrollPosition;
        
        public static ObjectBuilderWindow GetWindow(string title = "Building varwin objects")
        {
            ObjectBuilderWindow window = Instance;
            if (!window)
            {
                window = GetWindow<ObjectBuilderWindow>(true, title, true);
            }
            else
            {
                window.titleContent = new GUIContent(title);
            }

            window.minSize = new Vector2(350, 216);
            window.maxSize = window.minSize;
            window.Show();

            return window;
        }

        public void Build(VarwinObjectDescriptor varwinObjectDescriptor)
        {
            Build(new [] {varwinObjectDescriptor});
        }
        
        public void Build(IEnumerable<VarwinObjectDescriptor> varwinObjectDescriptors)
        {
            BuildTimeUtils.StartTime = DateTime.Now;
            _objectBuilder = new ObjectBuilder();
            _objectBuilder.Build(varwinObjectDescriptors);
            IsFinished = false;
            _displayProgress = 0f;
        }

        public void Build(VarwinPackageDescriptor varwinPackageDescriptor)
        {
            BuildTimeUtils.StartTime = DateTime.Now;
            _objectBuilder = new ObjectBuilder();
            _objectBuilder.Build(varwinPackageDescriptor);
            IsFinished = false;
            _displayProgress = 0f;
        }

        private void Awake()
        {
            Instance = this;
        }

        private void OnEnable()
        {
            Instance = this;
        }

        private void OnDestroy()
        {
            Instance = null;
        }

        private void OnGUI()
        {
            if (!Instance)
            {
                Instance = this;
            }
            
            if (!VersionExists())
            {
                Exit();
                return;
            }

            GUILayout.BeginVertical();

            if (!IsFinished)
            {
                DrawHint();
                GUILayout.FlexibleSpace();
                DrawProgress();
                GUILayout.FlexibleSpace();
                EditorGUI.ProgressBar(EditorGUILayout.GetControlRect(), _displayProgress, _objectBuilder?.Label ?? SdkTexts.CompilingScriptsStep);
            }
            else
            {
                _scrollPosition = GUILayout.BeginScrollView(_scrollPosition);
                
                DisplayBuildingDuration();
                DrawObjectBuildInfo();
                
                GUILayout.FlexibleSpace();
                
                GUILayout.EndScrollView();
                
                if (GUILayout.Button("Show in Explorer"))
                {
                    DirectoryUtils.OpenFolder(ObjectBuildingPath.BakedObjects);
                }
            }

            GUILayout.EndVertical();
        }
        
        #region DRAW
        
        private void DrawHint()
        {
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();

            var waitUntilObjectsCreateStyle = new GUIStyle(EditorStyles.boldLabel)
            {
                alignment = TextAnchor.MiddleCenter
            };
            
            GUILayout.Label(SdkTexts.WaitUntilObjectsCreate, waitUntilObjectsCreateStyle, GUILayout.ExpandHeight(true));
            
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
        }

        private void DrawProgress()
        { 
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();

            var progressStyle = new GUIStyle(EditorStyles.label)
            {
                fontSize = 42,
                normal = {textColor = EditorGUIUtility.isProSkin ? Color.white : Color.black},
                alignment = TextAnchor.MiddleCenter
            };

            var progressRect = new Rect(Vector2.zero, Instance.maxSize);
                
            GUI.Label(progressRect, _displayProgress.ToString("0%"), progressStyle);
            
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
        }

        private void DisplayBuildingDuration()
        {
            string messageTitle = $"Build was finished in {BuildTimeUtils.GetBuildTime()}!";
            GUILayout.Label(messageTitle, ObjectBuilderStyles.GreenResultMessageStyle);
        }

        private void DebugBuildingDuration()
        {
            Debug.Log($"<color=green><b>Build was finished in {BuildTimeUtils.GetBuildTime()}!</b></color>");
        }

        private void DrawObjectBuildInfo()
        {
            string objectsBuiltCount = $"Objects built: {_objectsToBuildCount - _objectsToBuildWithErrorsCount}/{_objectsToBuildCount}";
            GUILayout.Label(objectsBuiltCount, ObjectBuilderStyles.ResultMessageStyle);

            if (_objectsToBuildWithErrorsCount > 0 && _objectsToBuildWithErrors != null)
            {
                GUILayout.Label("Can't build objects:", ObjectBuilderStyles.RedResultMessageStyle);
                foreach (string objectsToBuildWithError in _objectsToBuildWithErrors)
                {
                    GUILayout.Label("    " + objectsToBuildWithError, ObjectBuilderStyles.RedResultInlineMessageStyle);
                }
            }
        }
        
        #endregion
        
        private void Update()
        {
            if (EditorApplication.isCompiling)
            {
                return;
            }

            try
            {
                if (IsFinished)
                {
                    Repaint();
                    return;
                }

                if (_objectBuilder == null)
                {
                    _objectBuilder = ObjectBuilder.Deserialize();

                    if (_objectBuilder == null)
                    {
                        Exit();
                    }
                }
                
                UpdateLocalVariables();
                
                if (_objectBuilder?.ObjectsToBuild == null || _objectBuilder.ObjectsToBuild.Count == 0)
                {
                    Exit();
                    EditorUtility.DisplayDialog("Build Results",  SdkTexts.NoSuitableForBuildObjectsFound, "OK");
                    return;
                }
                
                var objectToBuildWithErrors = _objectBuilder.ObjectsToBuild.Where(x => x != null && x.HasError).ToList();
                if (_objectBuilder.ObjectsToBuild.Where(x => !x.HasError).ToList().Count == 0)
                {
                    string message = "Build failed!";
                    if (objectToBuildWithErrors.Count > 0)
                    {
                        message += "\nCan't build objects: " + string.Join(", ", objectToBuildWithErrors.Select(x => x.ObjectName));
                    }
                    Exit();
                    EditorUtility.DisplayDialog("Build Results", message, "OK");
                    return;
                }
            
                if (!IsFinished)
                {
                    if (_objectBuilder.IsFinished && _objectBuilder.DisplayProgress >= 0.995f)
                    {
                        Finish();
                        return;
                    }
                    
                    _objectBuilder?.Update();
                }
                
                Repaint();
            }
            catch (Exception e)
            {
                ObjectBuilderHelper.DeleteTempFolder();
                
                string message = string.Format(SdkTexts.ProblemWhenBuildObjectsFormat, e.Message);
                EditorUtility.DisplayDialog("Error!", message, "OK");
                Debug.LogException(e);
                Exit();
                
                EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows64);
            }
        }

        private void UpdateLocalVariables()
        {
            if (_objectBuilder == null)
            {
                return;
            }

            _displayProgress = Mathf.Max(_displayProgress, _objectBuilder.DisplayProgress);
            
            _objectsToBuildCount = _objectBuilder.ObjectsToBuild.Count;
            _objectsToBuildWithErrorsCount = _objectBuilder.ObjectsToBuild.Where(x => x != null && x.HasError).ToList().Count;
            if (_objectsToBuildWithErrorsCount > 0)
            {
                _objectsToBuildWithErrors = _objectBuilder.ObjectsToBuild.Where(x => x != null && x.HasError).Select(x => x.ObjectName).ToArray();
            }
        }

        private void Finish()
        {
            IsFinished = true;
            ObjectBuilderHelper.DeleteTempFolder();
            
            BuildTimeUtils.FinishTime = DateTime.Now;
            
            if (File.Exists(ObjectBuilder.BuildListFilename))
            {
                File.Delete(ObjectBuilder.BuildListFilename);
            }
            
            Cleanup();
            DebugBuildingDuration();
        }

        private void Cleanup()
        {
            if (_objectBuilder?.ObjectsToBuild == null)
            {
                return;
            }

            if (!ObjectBuilder.DeleteWrappers)
            {
                return;
            }

            foreach (ObjectBuildDescription objectBuildDescription in _objectBuilder.ObjectsToBuild)
            {
                if (objectBuildDescription != null && objectBuildDescription.ContainedObjectDescriptor)
                {
                    WrapperGenerator.RemoveWrapperIfNeeded(objectBuildDescription.ContainedObjectDescriptor);
                }
            }
        }

        private void Exit()
        {
            if (File.Exists(ObjectBuilder.BuildListFilename))
            {
                File.Delete(ObjectBuilder.BuildListFilename);
            }
            
            Cleanup();
            Close();
            AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
        }
        
        [UnityEditor.Callbacks.DidReloadScripts]
        public static void DeleteTempBuildObjectsIfNeeded()
        {
            if (!File.Exists(ObjectBuilder.BuildListFilename) && !Instance)
            {
                ObjectBuilderHelper.DeleteTempFolder();
            }
        }
        
        public static bool VersionExists()
        {
            if (VarwinVersionInfo.Exists)
            {
                return true;
            }
            
            EditorUtility.DisplayDialog(SdkTexts.VersionNotFoundTitle, SdkTexts.VersionNotFoundMessage, "OK");
            return false;
        }
    }
}
