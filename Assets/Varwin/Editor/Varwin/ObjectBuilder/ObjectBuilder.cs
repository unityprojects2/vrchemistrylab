using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using UnityEditor;
using UnityEngine;
using Varwin.Data;
using Varwin.Public;

namespace Varwin.Editor.ObjectBuilding
{
    public class ObjectBuilder : IJsonSerializable
    {
        public const string BuildListFilename = "ObjectBuilding.json";
        public const bool DeleteTempFolder = true;
        public const bool DeleteWrappers = true;

        public VarwinPackageInfo PackageInfo;
        public List<ObjectBuildDescription> ObjectsToBuild;

        private float _displayProgress;
        private Queue<BaseState> _states;
        private BaseState _currentState;
        
        public float Progress
        {
            get
            {
                if (StateCount == 0)
                {
                    return 0;
                }

                if (IsFinished)
                {
                    return 1f;
                }
                
                if (_currentState != null)
                {
                    return Mathf.Clamp01((CompletedStateCount + _currentState.Progress) / StateCount);
                }

                return Mathf.Clamp01((float) CompletedStateCount / StateCount);
            }
        }

        public string Label => _currentState?.Label ?? SdkTexts.CompilingScriptsStep;

        public float DisplayProgress
        {
            get => ObjectsToBuild.Count < 10 ? _displayProgress : Progress;
            set => _displayProgress = value;
        }
        
        public bool IsFinished { get; private set; }
        public int StateCount { get; private set; }
        public int CompletedStateCount { get; private set; }

        public static ObjectBuilder Deserialize()
        {
            if (!File.Exists(BuildListFilename))
            {
                return null;
            }
            
            string json = File.ReadAllText(BuildListFilename);
            var objectBuilder = JsonConvert.DeserializeObject<ObjectBuilder>(json);
            
            objectBuilder.Initialize();
            
            for (int i = 0; i < objectBuilder.CompletedStateCount; ++i)
            {
                objectBuilder.Skip();
            }

            objectBuilder.DisplayProgress = objectBuilder.Progress;

            return objectBuilder;
        }

        public void Initialize()
        {
            InitializeStates();
            IsFinished = false;
        }

        private void InitializeStates()
        {
            _states = new Queue<BaseState>();
            _states.Enqueue(new PreparationState(this));
            _states.Enqueue(new WrapperGenerationState(this));
            _states.Enqueue(new IconGenerationState(this));
            _states.Enqueue(new PreviewGenerationState(this));
            _states.Enqueue(new AssembliesCollectingState(this));
            _states.Enqueue(new BlocklyConfigGenerationState(this));
            _states.Enqueue(new BundleGenerationState(this));
            _states.Enqueue(new AssetBundleBuildingState(this));
            _states.Enqueue(new SourcePackagingState(this));
            _states.Enqueue(new ZippingFilesState(this));
            if (PackageInfo != null)
            {
                _states.Enqueue(new PackageCreationState(this));
            }

            StateCount = _states.Count;
        }

        public void Update()
        {
            DisplayProgress = Mathf.Lerp(DisplayProgress, Progress, 0.1f);

            if (Mathf.Abs(DisplayProgress - Progress) > 0.01f)
            {
                return;
            }
            
            if (IsFinished)
            {
                return;
            }
            
            _currentState?.Update();

            if (_currentState == null || _currentState.IsFinished)
            {
                if (_states != null && _states.Count > 0)
                {
                    if (_currentState != null)
                    {
                        CompletedStateCount++;
                    }
                    
                    _currentState = _states.Dequeue();
                }
                else
                {
                    IsFinished = true;
                }
            }
        }
        
        public void Build(VarwinObjectDescriptor varwinObjectDescriptor)
        {
            Build(new [] {varwinObjectDescriptor});
        }
        
        public void Build(IEnumerable<VarwinObjectDescriptor> varwinObjectDescriptors)
        {
            PackageInfo = null;
            BuildObjects(varwinObjectDescriptors);
        }

        public void Build(VarwinPackageDescriptor varwinPackageDescriptor)
        {
            var varwinObjectDescriptors = new List<VarwinObjectDescriptor>();
            foreach (var gameObject in varwinPackageDescriptor.Objects)
            {
                var varwinObjectDescriptor = gameObject.GetComponent<VarwinObjectDescriptor>();
                if (varwinPackageDescriptor)
                {
                    varwinObjectDescriptors.Add(varwinObjectDescriptor);
                }
            }

            PackageInfo = new VarwinPackageInfo(varwinPackageDescriptor);
            BuildObjects(varwinObjectDescriptors);
        }
        
        public void Skip()
        {
            if (_states != null && _states.Count > 0)
            {
                _currentState = _states.Dequeue();
            }
        }

        public void Serialize()
        {
            var jsonSerializerSettings = new JsonSerializerSettings {NullValueHandling = NullValueHandling.Ignore};
            string jsonModels = JsonConvert.SerializeObject(this, Formatting.None, jsonSerializerSettings);
            
            File.WriteAllText(BuildListFilename, jsonModels);
        }

        private void BuildObjects(IEnumerable<VarwinObjectDescriptor> varwinObjectDescriptors)
        {
            try
            {
                ObjectsToBuild = new List<ObjectBuildDescription>();

                foreach (VarwinObjectDescriptor varwinObjectDescriptor in varwinObjectDescriptors)
                {
                    if (!varwinObjectDescriptor)
                    {
                        continue;
                    }
                    
                    VarwinObjectDescriptor currentVarwinObjectDescriptor = varwinObjectDescriptor;
                    
                    if (ObjectsToBuild.Any(x => x.ObjectGuid == currentVarwinObjectDescriptor.RootGuid))
                    {
                        continue;
                    }
                    
                    GameObject prefab = CreateObjectUtils.GetPrefabObject(currentVarwinObjectDescriptor.gameObject);
                    string varwinObjectPath = CreateObjectUtils.GetPrefabPath(prefab ? prefab : currentVarwinObjectDescriptor.gameObject);

                    if (!CreateObjectUtils.IsPrefabAsset(currentVarwinObjectDescriptor.gameObject) && prefab)
                    {
                        PrefabUtility.ApplyPrefabInstance(currentVarwinObjectDescriptor.gameObject, InteractionMode.AutomatedAction);
                    }
                    
                    CreateObjectUtils.SetupComponentReferences(currentVarwinObjectDescriptor);
                    
                    VarwinObjectDescriptor prefabVarwinObjectDescriptor = prefab ? prefab.GetComponent<VarwinObjectDescriptor>() : currentVarwinObjectDescriptor;
                    
                    if (prefab)
                    {
                        GameObject tempPrefab = PrefabUtility.LoadPrefabContents(varwinObjectPath);
                        var tempPrefabVarwinObjectDescriptor = tempPrefab.GetComponent<VarwinObjectDescriptor>();
                        CreateObjectUtils.SetupComponentReferences(tempPrefabVarwinObjectDescriptor);
                        CreateObjectUtils.SetupObjectIds(tempPrefab);
                        prefab = PrefabUtility.SaveAsPrefabAsset(tempPrefab, varwinObjectPath);

                        if (!currentVarwinObjectDescriptor)
                        {
                            currentVarwinObjectDescriptor = prefab.GetComponent<VarwinObjectDescriptor>();
                            prefabVarwinObjectDescriptor = currentVarwinObjectDescriptor; 
                        }
                        
                        if (!CreateObjectUtils.IsPrefabAsset(currentVarwinObjectDescriptor.gameObject))
                        {
                            CreateObjectUtils.ApplyPrefabInstanceChanges(currentVarwinObjectDescriptor.gameObject);
                        }
                        
                        var prefabIsChanged = false;
                        
                        if (!string.Equals(prefabVarwinObjectDescriptor.Prefab, varwinObjectPath, StringComparison.Ordinal)) 
                        {
                            prefabVarwinObjectDescriptor.Prefab = varwinObjectPath;
                            prefabIsChanged = true;
                        }

                        string prefabGuid = AssetDatabase.AssetPathToGUID(varwinObjectPath);
                        if (!string.Equals(prefabVarwinObjectDescriptor.PrefabGuid, prefabGuid, StringComparison.Ordinal))
                        {
                            prefabVarwinObjectDescriptor.PrefabGuid = prefabGuid;
                            prefabIsChanged = true;
                        }

                        if (prefabIsChanged)
                        {
                            CreateObjectUtils.ApplyPrefabInstanceChanges(prefabVarwinObjectDescriptor.gameObject);
                        }
                    }
                    else
                    {
                        CreateObjectUtils.SetupObjectIds(currentVarwinObjectDescriptor.gameObject);
                    }

                    string folderPath = null;
                    if (!string.IsNullOrEmpty(varwinObjectPath))
                    {
                        folderPath = Path.GetDirectoryName(varwinObjectPath)?.Replace("\\", "/");
                    }
                    
                    var objectBuildDescription = new ObjectBuildDescription()
                    {
                        ObjectName = prefabVarwinObjectDescriptor.Name,
                        ObjectGuid = prefabVarwinObjectDescriptor.RootGuid,
                        PrefabPath = varwinObjectPath,
                        FolderPath = folderPath,
                        ContainedObjectDescriptor = prefabVarwinObjectDescriptor
                    };

                    if (CreateObjectUtils.CheckNeedBuildWithVarwinTemp(currentVarwinObjectDescriptor))
                    {
                        string folder = $"Assets/VarwinTemp/{prefabVarwinObjectDescriptor.Name}";
                        if (!Directory.Exists(folder))
                        {
                            Directory.CreateDirectory(folder);
                        }

                        string prefabPath = $"{folder}/{prefabVarwinObjectDescriptor.Name}.prefab";

                        GameObject prefabTempInstance;
                        if (prefab)
                        {
                            prefabTempInstance = (GameObject) PrefabUtility.InstantiatePrefab(prefab);
                        }
                        else
                        {
                            prefabTempInstance = UnityEngine.Object.Instantiate(currentVarwinObjectDescriptor.gameObject);
                        }

                        if (!prefabTempInstance)
                        {
                            Debug.LogError($"Can't load prefab at path {prefabPath}. Try to build an object via prefab instance on the scene");
                            continue;
                        }
                                    
                        if (!prefabTempInstance.GetComponent<VarwinObject>())
                        {
                            prefabTempInstance.AddComponent<VarwinObject>();
                        }
                                    
                        prefab = PrefabUtility.SaveAsPrefabAsset(prefabTempInstance, prefabPath);
                        prefabVarwinObjectDescriptor = prefab.GetComponent<VarwinObjectDescriptor>();
                        UnityEngine.Object.DestroyImmediate(prefabTempInstance);
                                    
                        prefabVarwinObjectDescriptor.Prefab = prefabPath;
                        prefabVarwinObjectDescriptor.PrefabGuid = AssetDatabase.AssetPathToGUID(prefabPath);

                        objectBuildDescription.ObjectName = prefabVarwinObjectDescriptor.Name;
                        objectBuildDescription.ObjectGuid = prefabVarwinObjectDescriptor.RootGuid;
                        objectBuildDescription.ContainedObjectDescriptor = prefabVarwinObjectDescriptor;
                        objectBuildDescription.PrefabPath = prefabPath;
                        objectBuildDescription.FolderPath = folder;

                        var asmdefData = new AssemblyDefinitionData
                        {
                            name = prefabVarwinObjectDescriptor.Namespace,
                            references = new[] {"VarwinCore"}
                        };
                        
                        asmdefData.Save($"{folder}/{prefabVarwinObjectDescriptor.Name}.asmdef");
                        
                        CreateObjectUtils.SetupAsmdef(prefabVarwinObjectDescriptor);
                    }
                                    
                    if (!prefab.GetComponent<VarwinObject>())
                    {
                        prefab.AddComponent<VarwinObject>();
                    }

                    ObjectsToBuild.Add(objectBuildDescription);
                }
                
                Initialize();
            }
            catch (Exception e)
            {
                var message = string.Format(SdkTexts.ProblemWhenRunBuildAllObjectsFormat, e.Message);
                EditorUtility.DisplayDialog("Error!", message, "OK");
                Debug.LogException(e);
                
                EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows64);
            }
        }
    }
}