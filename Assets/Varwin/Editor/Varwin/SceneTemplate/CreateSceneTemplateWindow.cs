﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Varwin.Data;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using Varwin.Editor.ObjectBuilding;
using Varwin.Public;

namespace Varwin.Editor
{
    public class CreateSceneTemplateWindow : EditorWindow
    {
        private static LocalizationDictionary _localizedName;
        private static LocalizationDictionaryDrawer _localizedNameDrawer;
        
        private static LocalizationDictionary _localizedDescription;
        private static LocalizationDictionaryDrawer _localizedDescriptionDrawer;
        
        private static string _guid;
        private static string _rootGuid;

        private static Rect _windowRect = new Rect(0, 0, 700, 600);

        private static Camera _camera;
        private static WorldDescriptor _worldDescriptor;
        private static SceneCamera _sceneCamera;

        private const int ImageSize = 200;
        private const int ColumnsPadding = 20;
        private const int LabelWidth = 42;

        private static string _authorName;
        private static string _authorEmail;
        private static string _authorUrl;

        private static string _licenseCode;
        private static string _licenseVersion;
        private LicenseType _selectedLicense;
        private int _selectedLicenseIndex = -1;
        private int _selectedLicenseVersionIndex = -1;

        private static long _createdAt = -1;
        private static bool _sourcesIncluded;
        private static bool _mobileReady;
        private static bool _currentVersionWasBuilt;
        private static bool _currentVersionWasBuiltAsMobileReady;

        private static bool _varwinToolsIsEnabled;

        private static readonly string[] ExcludedDlls =
        {
            "Varwin",
            "Unity",
            "Bakery"
        };
        
        public static void OpenWindow()
        {
            GetWindowWithRect(typeof(CreateSceneTemplateWindow),
                _windowRect,
                false,
                SdkTexts.CreateSceneTemplateWindowTitle);
        }

        private void Awake()
        {
            OnEnable();
        }

        private void OnEnable()
        {
            FindOrCreateWorldDescriptor();
            FindOrCreateCamera();

            InitializeName();
            InitializeDescription();
            
            FillDefaultDataIfNull();
            
            _guid = _worldDescriptor.Guid;
            _rootGuid = _worldDescriptor.RootGuid;
            _sourcesIncluded = _worldDescriptor.SourcesIncluded;
            _mobileReady = SdkSettings.MobileFeature.Enabled && _worldDescriptor.MobileReady;
            _currentVersionWasBuilt = _worldDescriptor.CurrentVersionWasBuilt;
            _currentVersionWasBuiltAsMobileReady = _worldDescriptor.CurrentVersionWasBuiltAsMobileReady;

            if (string.IsNullOrWhiteSpace(_worldDescriptor.AuthorName))
            {
                AuthorSettings.Initialize();
                _authorName = AuthorSettings.Name;
                _authorEmail = AuthorSettings.Email;
                _authorUrl = AuthorSettings.Url;
            }
            else
            {
                _authorName = _worldDescriptor.AuthorName;
                _authorEmail = _worldDescriptor.AuthorEmail;
                _authorUrl = _worldDescriptor.AuthorUrl;
            }
        }

        private void OnDisable()
        {
            if (_worldDescriptor)
            {
                EditorUtility.SetDirty(_worldDescriptor);
                EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
            }
        }

        private static void InitializeName()
        {
            _localizedName = _worldDescriptor.LocalizedName;

            if (_localizedName.Count == 0 && !string.IsNullOrEmpty(_worldDescriptor.Name))
            {
                if (Regex.IsMatch(_worldDescriptor.Name, @"[а-яА-Я]"))
                {
                    _localizedName.Add(SystemLanguage.Russian, _worldDescriptor.Name);
                }
                else
                {
                    _localizedName.Add(SystemLanguage.English, _worldDescriptor.Name);
                }
            }

            _localizedNameDrawer = new LocalizationDictionaryDrawer(_localizedName, typeof(LocalizationString), true, false, true, true)
            {
                Title = "Name",
                ErrorMessage = SdkTexts.AnItemWithLanguageHasAlreadyBeenAddedFormat
            };

            _localizedNameDrawer.Initialize(_localizedName);
        }

        private static void InitializeDescription()
        {
            _localizedDescription = _worldDescriptor.LocalizedDescription;

            if (_localizedDescription.Count == 0 && !string.IsNullOrEmpty(_worldDescriptor.Description))
            {
                if (Regex.IsMatch(_worldDescriptor.Description, @"[а-яА-Я]"))
                {
                    _localizedDescription.Add(SystemLanguage.Russian, _worldDescriptor.Description);
                }
                else
                {
                    _localizedDescription.Add(SystemLanguage.English, _worldDescriptor.Description);
                }
            }

            _localizedDescriptionDrawer = new LocalizationDictionaryDrawer(_localizedDescription, typeof(LocalizationString), true, false, true, true)
            {
                Title = "Description",
                ErrorMessage = SdkTexts.AnItemWithLanguageHasAlreadyBeenAddedFormat
            };

            _localizedDescriptionDrawer.Initialize(_localizedDescription);
        }

        private static void FillDefaultDataIfNull()
        {
            if (_worldDescriptor.LocalizedName.Count == 0)
            {
                _worldDescriptor.LocalizedName.Add(SystemLanguage.English, "Scene Template");
            }

            if (string.IsNullOrEmpty(_worldDescriptor.Guid))
            {
                _worldDescriptor.Guid = Guid.NewGuid().ToString();
                _worldDescriptor.RootGuid = _worldDescriptor.Guid;
                _worldDescriptor.CurrentVersionWasBuilt = false;
                _worldDescriptor.CurrentVersionWasBuiltAsMobileReady = false;
            }
        }

        private static Camera FindOrCreateCamera()
        {
            _sceneCamera = SceneUtils.GetObjectsOnScene<SceneCamera>(true).FirstOrDefault();

            if (_sceneCamera != null)
            {
                _sceneCamera.gameObject.name = "[Camera Preview]";
                _sceneCamera.transform.SetParent(_worldDescriptor.transform, true);
            }
            else
            {
                GameObject cameraPreview = new GameObject("[Camera Preview]");
                cameraPreview.transform.SetParent(_worldDescriptor.transform, true);
                cameraPreview.transform.position = new Vector3(0, 1, 0);

                _sceneCamera = cameraPreview.AddComponent<SceneCamera>();
            }

            if (!_sceneCamera.Camera)
            {
                _sceneCamera.Camera = _sceneCamera.GetComponent<Camera>();
            }

            if (!_sceneCamera.Camera.targetTexture)
            {
                _sceneCamera.Init(256, 256);
            }

            _sceneCamera.gameObject.SetActive(true);

            _camera = _sceneCamera.GetComponent<Camera>();
            _camera.enabled = true;

            return _camera;
        }

        private static void FindOrCreateWorldDescriptor()
        {
            _worldDescriptor = SceneUtils.GetObjectsOnScene<WorldDescriptor>(true).FirstOrDefault();

            if (_worldDescriptor != null)
            {
                _worldDescriptor.gameObject.SetActive(true);
            }
            else
            {
                var worldDescriptorGo = new GameObject("[World Descriptor]");
                worldDescriptorGo.transform.position = Vector3.zero;
                worldDescriptorGo.transform.rotation = Quaternion.identity;
                worldDescriptorGo.transform.localScale = Vector3.one;

                _worldDescriptor = worldDescriptorGo.AddComponent<WorldDescriptor>();
            }

            FindOrCreateSpawnPoint(_worldDescriptor);

            UnityEngine.SceneManagement.Scene scene = _worldDescriptor.gameObject.scene;

            if (string.IsNullOrEmpty(_worldDescriptor.SceneGuid))
            {
                _worldDescriptor.SceneGuid = AssetDatabase.AssetPathToGUID(scene.path);
                EditorSceneManager.SaveScene(scene);
            }
            else
            {
                string sceneGuid = AssetDatabase.AssetPathToGUID(scene.path);
                if (!string.Equals(_worldDescriptor.SceneGuid, sceneGuid) && !string.IsNullOrEmpty(sceneGuid))
                {
                    _worldDescriptor.SceneGuid = sceneGuid;
                    _worldDescriptor.RegenerateGuid();
                    _worldDescriptor.CleanBuiltInfo();
                    EditorSceneManager.SaveScene(scene);
                }
            }
        }

        private static Transform FindOrCreateSpawnPoint(WorldDescriptor worldDescriptor)
        {
            if (!worldDescriptor.PlayerSpawnPoint)
            {
                var spawnPoint = new GameObject("[Spawn Point]");
                spawnPoint.transform.position = Vector3.zero;
                spawnPoint.transform.rotation = Quaternion.identity;
                spawnPoint.transform.localScale = Vector3.one;
                spawnPoint.transform.SetParent(_worldDescriptor.transform);
                worldDescriptor.PlayerSpawnPoint = spawnPoint.transform;
            }
            else
            {
                worldDescriptor.PlayerSpawnPoint.SetParent(_worldDescriptor.transform, true);
                worldDescriptor.PlayerSpawnPoint.gameObject.name = "[Spawn Point]";
            }

            return worldDescriptor.PlayerSpawnPoint;
        }

        private void OnGUI()
        {
            GUILayout.Label(SdkTexts.SceneTemplateSettings, EditorStyles.boldLabel);

            EditorGUILayout.BeginVertical(GUILayout.Width(_windowRect.width - ImageSize - ColumnsPadding * 2));

            DrawLocalizedName();
            EditorGUILayout.Space();

            DrawLocalizedDescription();

            if (SdkSettings.DeveloperModeFeature.Enabled)
            {
                DrawGuidLabel();
            }

            EditorGUILayout.Space();

            DrawSourcesArea();

            if (SdkSettings.MobileFeature.Enabled)
            {
                DrawPlatformsArea();
            }
            else
            {
                _mobileReady = false;
            }

            EditorGUILayout.Space();
            DrawAuthorSettings();

            EditorGUILayout.Space();
            DrawLicenseSettings();

            EditorGUILayout.Space();
            DrawCreateLocationButton();

            EditorGUILayout.EndVertical();

            if (_camera && _camera.targetTexture)
            {
                var cameraRect = new Rect(_windowRect.width - ImageSize - ColumnsPadding,
                    ColumnsPadding,
                    ImageSize,
                    ImageSize);
                EditorGUI.DrawPreviewTexture(cameraRect, _camera.targetTexture);

                var cameraMoveButtonRect = new Rect(cameraRect) {height = 20, y = cameraRect.yMax};

                if (GUI.Button(cameraMoveButtonRect, SdkTexts.MoveCameraToEditorView))
                {
                    SceneUtils.MoveCameraToEditorView(_camera ? _camera : FindOrCreateCamera());
                }
            }
        }

        private void OnDestroy()
        {
            if (_camera)
            {
                _camera.enabled = false;
            }
        }
        
        private void DrawLocalizedName()
        {
            EditorGUILayout.BeginVertical(EditorStyles.inspectorFullWidthMargins);
            if (_localizedNameDrawer == null)
            {
                InitializeName();
            }
            _localizedNameDrawer?.Draw();
            EditorGUILayout.EndVertical();
        }

        private void DrawLocalizedDescription()
        {
            EditorGUILayout.BeginVertical(EditorStyles.inspectorFullWidthMargins);
            if (_localizedDescriptionDrawer == null)
            {
                InitializeDescription();
            }
            _localizedDescriptionDrawer?.Draw();
            EditorGUILayout.EndVertical();
        }
        
        private void DrawGuidLabel()
        {
            EditorGUILayout.LabelField($"Guid: {_guid}", EditorStyles.miniLabel);
            EditorGUILayout.LabelField($"Root Guid: {_rootGuid}", EditorStyles.miniLabel);
        }
        
        private void DrawSourcesArea()
        {
            _sourcesIncluded = EditorGUILayout.Toggle("Include Sources", _sourcesIncluded);
            _worldDescriptor.SourcesIncluded = _sourcesIncluded;
        }

        private void DrawPlatformsArea()
        {
            EditorGUI.BeginDisabledGroup(_currentVersionWasBuiltAsMobileReady);
            _mobileReady = EditorGUILayout.Toggle("Mobile Ready", _mobileReady);
            _worldDescriptor.MobileReady = _mobileReady;
            EditorGUI.EndDisabledGroup();

            if (_currentVersionWasBuiltAsMobileReady)
            {
                EditorGUILayout.HelpBox(SdkTexts.EnableMobileReadyCheckbox, MessageType.Info);
            }
        }

        private void DrawCreateLocationButton()
        {
            bool isDisabled = string.IsNullOrWhiteSpace(_authorName)
                              || !_localizedName.IsValidDictionary 
                              || _localizedName.Count == 0
                              || string.IsNullOrWhiteSpace(_guid)
                              || string.IsNullOrWhiteSpace(_worldDescriptor.SceneGuid)
                              || !VarwinVersionInfo.Exists;

            EditorGUI.BeginDisabledGroup(isDisabled);

            if (GUILayout.Button("Build"))
            {
                if (!CheckLocationForTeleport())
                {
                    if (!EditorUtility.DisplayDialog(SdkTexts.SceneTemplateWindowTitle,
                        SdkTexts.NoTeleportAreaMessage,
                        "Build",
                        "Cancel"))
                    {
                        EditorGUI.EndDisabledGroup();
                        EditorGUILayout.EndVertical();
                        return;
                    }
                }

                CreateScene();
            }

            if (!string.Equals(_guid, _rootGuid) || _currentVersionWasBuilt)
            {
                DrawCreateNewVersionButton();
            }

            EditorGUI.EndDisabledGroup();

            if (!VarwinVersionInfo.Exists)
            {
                EditorGUILayout.HelpBox(SdkTexts.VersionNotFoundMessage, MessageType.Error);
            }

            if (string.IsNullOrWhiteSpace(_worldDescriptor.SceneGuid))
            {
                EditorGUILayout.HelpBox(SdkTexts.SceneIsNotSaved, MessageType.Error);
            }

            if (!CheckLocationForTeleport())
            {
                EditorGUILayout.HelpBox(SdkTexts.NoTeleportAreaWarning, MessageType.Warning);
            }

            if (!CheckAndroidModuleInstalled())
            {
                EditorGUILayout.HelpBox(SdkTexts.NoAndroidModuleWarning, MessageType.Warning);
            }
        }

        private void DrawCreateNewVersionButton()
        {
            bool isDisabled = !_currentVersionWasBuilt || !VarwinVersionInfo.Exists;

            EditorGUI.BeginDisabledGroup(isDisabled);

            if (GUILayout.Button("Create new version"))
            {
                if (EditorUtility.DisplayDialog(SdkTexts.CreateNewVersionSceneTemplate, SdkTexts.CreateNewVersionSceneTemplate, "Yes", "Cancel"))
                {
                    _guid = Guid.NewGuid().ToString();

                    _worldDescriptor.Guid = _guid;

                    _currentVersionWasBuilt = false;
                    _worldDescriptor.CurrentVersionWasBuilt = false;

                    _currentVersionWasBuiltAsMobileReady = false;
                    _worldDescriptor.CurrentVersionWasBuiltAsMobileReady = false;

                    if (EditorUtility.DisplayDialog(SdkTexts.SceneTemplateWindowTitle, SdkTexts.SceneTemplateBuildAsk, "Yes", "No"))
                    {
                        if (!CheckLocationForTeleport())
                        {
                            if (!EditorUtility.DisplayDialog(SdkTexts.SceneTemplateWindowTitle,
                                SdkTexts.NoTeleportAreaMessage,
                                "Build",
                                "Cancel"))
                            {
                                return;
                            }
                        }

                        CreateScene();
                        return;
                    }
                }
            }

            EditorGUI.EndDisabledGroup();
        }

        private void DrawAuthorSettings()
        {
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            GUILayout.Label("Author:", EditorStyles.miniBoldLabel);

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Name:", GUILayout.Width(LabelWidth));
            _authorName = EditorGUILayout.TextField(_authorName);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("E-Mail:", GUILayout.Width(LabelWidth));
            _authorEmail = EditorGUILayout.TextField(_authorEmail);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("URL:", GUILayout.Width(LabelWidth));
            _authorUrl = EditorGUILayout.TextField(_authorUrl);
            EditorGUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();

            if (GUILayout.Button(SdkTexts.ResetDefaultAuthorSettingsButton))
            {
                AuthorSettings.Initialize();

                if (string.IsNullOrWhiteSpace(AuthorSettings.Name))
                {
                    AuthorSettingsWindow.OpenWindow();
                }
                else
                {
                    GUI.FocusControl(null);
                    _authorName = AuthorSettings.Name;
                    _authorEmail = AuthorSettings.Email;
                    _authorUrl = AuthorSettings.Url;
                }
            }

            GUILayout.EndHorizontal();

            _worldDescriptor.AuthorName = _authorName;
            _worldDescriptor.AuthorEmail = _authorEmail;
            _worldDescriptor.AuthorUrl = _authorUrl;

            DrawAuthorCanNotBeEmptyHelpBox();

            EditorGUILayout.EndVertical();
        }

        private void DrawAuthorCanNotBeEmptyHelpBox()
        {
            if (string.IsNullOrWhiteSpace(_authorName))
            {
                EditorGUILayout.HelpBox(SdkTexts.AuthorNameEmptyWarning, MessageType.Error);
            }
        }


        private void DrawLicenseSettings()
        {
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            GUILayout.Label("License:", EditorStyles.miniBoldLabel);

            EditorGUILayout.BeginHorizontal();

            string prevSelectedLicenseCode = _licenseCode;

            _selectedLicense = LicenseSettings.Licenses.FirstOrDefault(x => string.Equals(x.Code, _licenseCode));

            if (_selectedLicense == null)
            {
                _selectedLicense = LicenseSettings.Licenses.FirstOrDefault();

                _selectedLicenseIndex = 0;
                _selectedLicenseVersionIndex = 0;
            }
            else
            {
                _selectedLicenseIndex = LicenseSettings.Licenses.IndexOf(_selectedLicense);
                _selectedLicenseVersionIndex = Array.IndexOf(_selectedLicense.Versions, _licenseVersion);
            }

            var licenseNames = LicenseSettings.Licenses.Select(license => license.Name).ToArray();
            _selectedLicenseIndex = EditorGUILayout.Popup(_selectedLicenseIndex, licenseNames);

            _selectedLicense = LicenseSettings.Licenses.ElementAt(_selectedLicenseIndex);
            _licenseCode = _selectedLicense.Code;

            if (!string.Equals(prevSelectedLicenseCode, _licenseCode))
            {
                _selectedLicenseVersionIndex = 0;
            }

            if (_selectedLicense.Versions != null && _selectedLicense.Versions.Length > 0)
            {
                if (_selectedLicenseVersionIndex >= _selectedLicense.Versions.Length
                    || _selectedLicenseVersionIndex < 0)
                {
                    _selectedLicenseVersionIndex = 0;
                }

                _selectedLicenseVersionIndex = EditorGUILayout.Popup(_selectedLicenseVersionIndex,
                    _selectedLicense.Versions,
                    GUILayout.Width(80));
                _licenseVersion = _selectedLicense.Versions[_selectedLicenseVersionIndex];
            }
            else
            {
                _licenseVersion = string.Empty;
            }

            _worldDescriptor.LicenseCode = _licenseCode;
            _worldDescriptor.LicenseVersion = _licenseVersion;

            EditorGUILayout.EndHorizontal();

            if (_selectedLicense == null)
            {
                VarwinStyles.Link(LicenseSettings.CreativeCommonsLink);
            }
            else
            {
                VarwinStyles.Link(_selectedLicense.GetLink(_licenseVersion));
            }

            EditorGUILayout.EndVertical();
        }

        private bool CheckLocationForTeleport()
        {
            var areas = GameObject.FindGameObjectsWithTag("TeleportArea");

            return areas.Length > 0;
        }

        private static bool CheckAndroidModuleInstalled()
        {
            if (!_mobileReady)
            {
                return true;
            }

            var bindingFlags = System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic;
            var moduleManager = System.Type.GetType("UnityEditor.Modules.ModuleManager,UnityEditor.dll");
            var isPlatformSupportLoaded = moduleManager.GetMethod("IsPlatformSupportLoaded", bindingFlags);
            var getTargetStringFromBuildTarget = moduleManager.GetMethod("GetTargetStringFromBuildTarget", bindingFlags);
            return (bool) isPlatformSupportLoaded.Invoke(null, new object[] {(string) getTargetStringFromBuildTarget.Invoke(null, new object[] {BuildTarget.Android})});
        }

        private static void CreateScene()
        {
            Debug.Log(SdkTexts.SceneTemplateBuildStartMessage);

            _worldDescriptor.LocalizedName = _localizedName;
            _worldDescriptor.LocalizedDescription = _localizedDescription;
            _worldDescriptor.Guid = _guid;
            _worldDescriptor.RootGuid = _rootGuid;
            if (string.IsNullOrEmpty(_worldDescriptor.RootGuid))
            {
                _rootGuid = _guid;
                _worldDescriptor.RootGuid = _guid;
            }

            _worldDescriptor.Image = "bundle.png";
            _worldDescriptor.AssetBundleLabel = "bundle";

            _worldDescriptor.AuthorName = _authorName;
            _worldDescriptor.AuthorEmail = _authorEmail;
            _worldDescriptor.AuthorUrl = _authorUrl;

            _worldDescriptor.LicenseCode = _licenseCode;
            _worldDescriptor.LicenseVersion = _licenseVersion;

            _worldDescriptor.BuiltAt = DateTimeOffset.Now.ToString();
            _worldDescriptor.SourcesIncluded = _sourcesIncluded;
            _worldDescriptor.MobileReady = _mobileReady;

            _currentVersionWasBuilt = true;
            _worldDescriptor.CurrentVersionWasBuilt = true;

            _currentVersionWasBuiltAsMobileReady = _worldDescriptor.MobileReady;
            _worldDescriptor.CurrentVersionWasBuiltAsMobileReady = _worldDescriptor.MobileReady;

            var cams = FindObjectsOfType<Camera>().ToList();

            var scripts = SceneUtils.GetObjectsOnScene<MonoBehaviour>(true).ToList();

            string anyGlobalScript = CreateObjectUtils.GetGlobalScriptsPaths(scripts).FirstOrDefault();
            if (anyGlobalScript != null)
            {
                string message = string.Format(SdkTexts.ScriptWithoutAsmdefInAssetsFormat, anyGlobalScript) + "\n\n" + SdkTexts.ContinueSceneTemplateBuildingProblems;
                if (EditorUtility.DisplayDialog("Scene building error!", message, "Continue", "Cancel"))
                {
                    var globalScripts = CreateObjectUtils.GetGlobalScripts(scripts);
                    foreach (MonoBehaviour script in globalScripts)
                    {
                        scripts.Remove(script);
                    }
                }
                else
                {
                    return;
                }
            }
            
            _worldDescriptor.DllNames = DllHelper.GetForScripts(scripts).Keys.Where(x => !ExcludedDlls.Any(x.Contains)).ToArray();

            var sceneCam = _sceneCamera.GetComponent<Camera>();
            if (sceneCam)
            {
                cams.Remove(sceneCam);
            }

            var scene = _worldDescriptor.gameObject.scene;
            
            ToggleCams(cams, sceneCam, false);
            SetVarwinToolsActivity(false);
            EditorSceneManager.SaveScene(scene);

            try
            {
                string sb = new SceneBundleBuilder().BuildAndShow(_worldDescriptor, _sceneCamera);
                if (sb != default)
                {
                    Debug.Log(SdkTexts.SceneTemplateWasBuilt);
                    DirectoryUtils.OpenFolder(SdkSettings.SceneTemplateBuildingFolderPath);
                    EditorUtility.DisplayDialog(SdkTexts.SceneTemplateWindowTitle, SdkTexts.SceneTemplateWasBuilt, "OK");
                }
                else
                {
                    EditorUtility.DisplayDialog(SdkTexts.SceneTemplateWindowTitle,
                        SdkTexts.SceneTemplateBuildErrorMessage,
                        "OK");
                }

                GetWindow(typeof(CreateSceneTemplateWindow)).Close();
            }
            catch (Exception e)
            {
                Debug.LogError("Encountered exception " + e.Message);
                EditorUtility.DisplayDialog(SdkTexts.SceneTemplateWindowTitle,
                    SdkTexts.SceneTemplateBuildErrorMessage + "\n" + e.Message,
                    "OK");
            }

            SetVarwinToolsActivity(true);
            ToggleCams(cams, sceneCam, true);
            EditorSceneManager.SaveScene(scene);
        }

        private static void SetVarwinToolsActivity(bool isActive)
        {
            VarwinTools varwinToolsObject = SceneUtils.GetObjectsOnScene<VarwinTools>(true).FirstOrDefault();
            
            if (!varwinToolsObject)
            {
                return;
            }

            if (!isActive)
            {
                _varwinToolsIsEnabled = varwinToolsObject.gameObject.activeSelf;
                varwinToolsObject.gameObject.SetActive(false);
            }
            else
            {
                varwinToolsObject.gameObject.SetActive(_varwinToolsIsEnabled);
            }
        }

        private static void ToggleCams(IEnumerable<Camera> cams, Camera preview, bool toggle)
        {
            foreach (Camera cam in cams)
            {
                if (!cam || !cam.gameObject)
                {
                    continue;
                }

                if (!cam.targetTexture)
                {
                    cam.enabled = toggle;
                }
            }

            if (_sceneCamera)
            {
                _sceneCamera.enabled = toggle;
            }

            if (preview)
            {
                preview.enabled = toggle;
            }
        }

        private void Update()
        {
            Repaint();
        }

        public class InstallConfig : IJsonSerializable
        {
            public I18n Name;
            public I18n Description;
            public string Guid;
            public string RootGuid;
            public Author Author;
            public License License;
            public string BuiltAt;
            public bool SourcesIncluded;
            public bool MobileReady;
            public string SdkVersion;
        }

        public class Author : IJsonSerializable
        {
            public string Name;
            public string Email;
            public string Url;
        }

        public class License : IJsonSerializable
        {
            public string Code;
            public string Version;
        }

        public class SceneConfig : IJsonSerializable
        {
            public string name;
            public string description;
            public string image;
            public string assetBundleLabel;
            public string[] dllNames;
        }
    }
}