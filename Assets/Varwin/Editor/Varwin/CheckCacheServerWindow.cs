﻿using UnityEngine;
using UnityEditor;

namespace Varwin.Editor
{
    [InitializeOnLoad]
    public class CheckCacheServerWindow : EditorWindow
    {
        private const string SessionStateKey = "Varwin.SDK.SdkCheckCacheServerWindow.Shown";

        private static bool CacheServerWarning;
        private static bool IsForceOpen;

        private static bool IsLoaded = false;

        private const string DisableCacheServerCheckPrefsKey = "VW_DisableCacheServerCheck";

        private static bool IsDisableAutoCheck
        {
            get
            {
                if (!EditorPrefs.HasKey(DisableCacheServerCheckPrefsKey))
                {
                    EditorPrefs.SetBool(DisableCacheServerCheckPrefsKey, false);
                }

                return EditorPrefs.GetBool(DisableCacheServerCheckPrefsKey);
            }
            set
            {
                EditorPrefs.SetBool(DisableCacheServerCheckPrefsKey, value);
            }
        }

        private void OnGUI()
        {
            if (!IsLoaded)
            {
                OnEditorUpdate();
                IsLoaded = true;
            }

            if (CacheServerWarning)
            {
                EditorGUILayout.Space();

                EditorGUILayout.HelpBox(SdkTexts.CacheServerDisabledMobileSupportWarning,
                    MessageType.Warning);

                EditorGUILayout.HelpBox(SdkTexts.CacheServerSettingsInfo,
                    MessageType.Info);

                EditorGUILayout.Space();

                if (GUILayout.Button(SdkTexts.CacheServerCheckWindowApplyButton))
                {
                    PreferencesHelper.SwitchCacheServerMode(true);
                    PreferencesHelper.SwitchCacheCustomPathChecker(true);
                    SettingsService.OpenUserPreferences("Preferences/Cache Server");

                    Close();
                }
            }
            else
            {
                Close();
            }

            EditorGUILayout.Space();
            IsDisableAutoCheck = EditorGUILayout.Toggle(SdkTexts.DisableAutoCheckToggle, IsDisableAutoCheck);
        }

        private static bool _isSubscribed;
        
        private static void OnEditorUpdate()
        {
            if(!_isSubscribed)
            {
                _isSubscribed = true;
                
                SdkSettings.MobileFeature.OnSave += b =>
                {
                    if (b && PreferencesHelper.CheckIfCacheServerIsOff())
                    {
                        OpenWindow();
                    }
                };
            }
            
            if (!SdkSettings.MobileFeature.Enabled)
            {
                return;
            }

            bool shown = SessionState.GetBool(SessionStateKey, false);

            CacheServerWarning = PreferencesHelper.CheckIfCacheServerIsOff();

            if (shown && !IsForceOpen)
            {
                EditorApplication.update -= OnEditorUpdate;

                return;
            }

            if (EditorApplication.isPlaying)
            {
                EditorApplication.update -= OnEditorUpdate;

                return;
            }

            if (!IsForceOpen && IsDisableAutoCheck)
            {
                EditorApplication.update -= OnEditorUpdate;

                return;
            }

            if (CacheServerWarning && !shown || IsForceOpen)
            {
                SessionState.SetBool(SessionStateKey, true);
                var window = GetWindow<CheckCacheServerWindow>(SdkTexts.CacheServerWindowTitle, true);
                window.minSize = new Vector2(350, 190);
                window.maxSize = new Vector2(350, 190);
            }

            EditorApplication.update -= OnEditorUpdate;
        }

        static CheckCacheServerWindow()
        {
            IsForceOpen = false;
            EditorApplication.update += OnEditorUpdate;
        }

        public static void OpenWindow()
        {
            IsForceOpen = true;
            EditorApplication.update += OnEditorUpdate;
        }
    }
}
