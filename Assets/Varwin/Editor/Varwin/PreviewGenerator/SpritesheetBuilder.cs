﻿using System.IO;
using UnityEditor;
using UnityEngine;
using Varwin.Public;
using Varwin.Editor.PreviewGenerators;

namespace Varwin.Editor
{
    public class SpritesheetBuilder 
    {
        public void Build(VarwinObjectDescriptor descriptor, string folder)
        {
            var settings = new PreviewSettings(ImageSize.MarketplaceLibrary, new Vector2Int(15, 1), false, true);
            var generator = new PreviewGenerator();
            
            var prefab = AssetDatabase.LoadAssetAtPath<GameObject>(descriptor.Prefab);
            var texture = generator.Generate(prefab, settings);

            var bytes = settings.GetBytes(texture);
            var path = GetExportPath(settings, folder, descriptor.RootGuid);
            File.WriteAllBytes(path, bytes);
        }

        private string GetExportPath(PreviewSettings settings, string folder, string name)
        {
            var extension = settings.ExportAsJpg ? ".jpg" : ".png";
            return folder + "/spritesheet_" + name + extension;
        }
    }
}