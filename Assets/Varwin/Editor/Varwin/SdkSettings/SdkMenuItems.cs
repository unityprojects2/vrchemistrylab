﻿using System.IO;
using UnityEditor;
using Varwin.Editor.ObjectBuilding;

namespace Varwin.Editor
{
    public static class SdkMenuItems
    {
        #region CREATING

        [MenuItem("Varwin/Create/Object", false, 0)]
        private static void OpenCreateObjectWindow() => CreateObjectWindow.OpenWindow();
        
        [MenuItem("Varwin/Create/Scene Template", false, 1)]
        private static void OpenCreateSceneTemplateWindow() => CreateSceneTemplateWindow.OpenWindow();
        
        [MenuItem("Varwin/Create/Package", false, 2)]
        private static void OpenCreatePackageWindow() => CreatePackageWindow.OpenWindow();
        
        #endregion

        #region OBJECTS BUILDING

        [MenuItem("Varwin/Build/Selected Objects", false, 1)]
        private static void BuildSelectedObjects() => ObjectBuilderController.BuildSelectedObjects();

        [MenuItem("Varwin/Build/Selected Objects", true)]
        private static bool CanBuildSelectedObjects() => ObjectBuilderController.CanBuildSelectedObjects();

        [MenuItem("Varwin/Build/All Objects On Scene", false, 2)]
        private static void BuildAllObjectsOnScene() => ObjectBuilderController.BuildAllObjectsOnScene();
        
        [MenuItem("Varwin/Build/All Objects On Scene", true)]
        private static bool CanBuildAllObjectsOnScene() => ObjectBuilderController.CanBuildAllObjectsOnScene();
        
        [MenuItem("Varwin/Build/All Objects In Project", false, 3)]
        private static void BuildAllObjectsInProject() => ObjectBuilderController.BuildAllObjectInProject();

        [MenuItem("Varwin/Build/All Objects In Project", true)]
        private static bool CanBuildAllObjectsInProject() => VarwinVersionInfo.Exists;
        
        [MenuItem("Varwin/Build/All Objects By Selected Asmdef", false, 4)]
        private static void BuildAllObjectsByAsmdef() => ObjectBuilderController.BuildAllObjectsBySelectedAsmdef();

        [MenuItem("Varwin/Build/All Objects By Selected Asmdef", true)]
        private static bool CanBuildAllObjectsByAsmdef() => ObjectBuilderController.CanBuildAllObjectsBySelectedAsmdef();
        
        [MenuItem("Varwin/Build/All Objects by Selected AssetBundlePart", true)]
        private static bool CanBuildSelectedAssetBundlePart() => ObjectBuilderController.CanBuildSelectedAssetBundlePart();

        [MenuItem("Varwin/Build/All Objects by Selected AssetBundlePart", false, 5)]
        private static void BuildAllObjectsByAssetBundlePart() => ObjectBuilderController.BuildAllObjectsByAssetBundlePart();
        
        #endregion

        #region SETTINGS
        
        [MenuItem("Varwin/Settings/SDK", false, 90)]
        private static void OpenSdkSettingsWindow() => SdkSettingsWindow.OpenWindow();
        
        [MenuItem("Varwin/Settings/Default Author Info", false, 100)]
        private static void OpenAuthorSettingsWindow() => AuthorSettingsWindow.OpenWindow();
        
        [MenuItem("Varwin/Settings/Project Settings", false, 110)]
        private static void OpenVarwinUnitySettingsWindow() => VarwinUnitySettingsWindow.OpenWindow(true);
        
        #endregion

        #region OTHER
        
        [MenuItem("Varwin/VR Debug Plugin", false, 900)]
        private static void OpenSdkVrDebugPluginWindow() => SdkVrDebugPluginWindow.OpenWindow();    
        
        [MenuItem("Varwin/Check for update", false, 910)]
        private static void OpenSdkUpdateWindow() => SdkUpdateWindow.OpenWindow();      

        [MenuItem("Varwin/About", false, 920)]
        private static void OpenVarwinAboutWindow() => VarwinAboutWindow.OpenWinow();
        
        #endregion

#if SKETCHFAB

        [MenuItem("Varwin/Import/Model...", false, 1)]
        private static void RunImportModelsWindow() => ImportModelsWindow.InitModelImportWindow();

        [MenuItem("Varwin/Import/Folder...", false, 1)]
        private static void RunImportFolderWindow() => ImportModelsWindow.InitFolderImportWindow();

#endif
        
    }
}