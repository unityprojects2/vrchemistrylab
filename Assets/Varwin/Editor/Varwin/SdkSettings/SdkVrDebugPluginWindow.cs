using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;

namespace Varwin.Editor
{
    [InitializeOnLoad]
    public class SdkVrDebugPluginWindow : EditorWindow
    {
        private const string SessionStateKey = "Varwin.SDK.SdkVrDebugPluginWindow.Shown";
        private const string DisableAutoCheckKey = "Varwin.SDK.SdkVrDebugPluginWindow.DisableAutoCheck";
        
        private static bool IsForceOpen;
        
        private const string DownloadLink = @"https://dist.varwin.com/releases/latest/VarwinSDK_VrDebugPlugin.unitypackage";
        private const string HelpLink = @"https://varw.in/doc";

        private static bool VrDebugFoldersExists => Directory.Exists(@"Assets/Varwin/Core/Resources/PlayerRig")
                                                    && Directory.Exists(@"Assets/Varwin/Standalone Input/SteamVR Input");
        
        private static bool IsDisableAutoCheck
        {
            get
            {
                if (!EditorPrefs.HasKey(DisableAutoCheckKey))
                {
                    EditorPrefs.SetBool(DisableAutoCheckKey, false);
                }
                
                return EditorPrefs.GetBool(DisableAutoCheckKey);
            }
            set
            {
                EditorPrefs.SetBool(DisableAutoCheckKey, value);
            }
        }

        private void OnGUI()
        {
            if (!VrDebugFoldersExists)
            {
                EditorGUILayout.Space();
                EditorGUILayout.HelpBox(SdkTexts.VrDebugPluginNotInstalled, MessageType.Warning);                    
                EditorGUILayout.HelpBox(SdkTexts.SdkDownloadVrDebugPluginMessage, MessageType.Info);
                
                EditorGUILayout.Space();
                if (GUILayout.Button(SdkTexts.DownloadVrDebugPluginButton))
                {
                    Application.OpenURL(DownloadLink);
                }
            
                EditorGUILayout.Space();
                IsDisableAutoCheck = EditorGUILayout.ToggleLeft(SdkTexts.DontRemindMeAgain, IsDisableAutoCheck);
            }
            else
            {
                EditorGUILayout.Space();                
                EditorGUILayout.HelpBox(SdkTexts.VrDebugPluginIsInstalledMessage, MessageType.Info);
                
                EditorGUILayout.Space();
                if (GUILayout.Button(SdkTexts.HowToUseVrDebugPluginMessage))
                {
                    Application.OpenURL(HelpLink);
                }
            }
        }

        private static void OnEditorUpdate()
        {
            bool shown = SessionState.GetBool(SessionStateKey, false);
            if (shown && !IsForceOpen)
            {
                EditorApplication.update -= OnEditorUpdate;
                return;
            }

            if (EditorApplication.isPlaying)
            {
                EditorApplication.update -= OnEditorUpdate;
                return;
            }
            
            if (!IsForceOpen && IsDisableAutoCheck)
            {
                EditorApplication.update -= OnEditorUpdate;
                return;
            }
            
            if (!VrDebugFoldersExists && !shown || IsForceOpen)
            {
                SessionState.SetBool(SessionStateKey, true);
                var window = GetWindow<SdkVrDebugPluginWindow>(SdkTexts.SdkVrDebugPluginWindowTitle, true);
                window.minSize = new Vector2(450, 180);
                window.maxSize = new Vector2(450, 180);
            }
            
            EditorApplication.update -= OnEditorUpdate;
        }

        static SdkVrDebugPluginWindow()
        {
            IsForceOpen = false;
            EditorApplication.update += OnEditorUpdate;            
        }
        
        public static void OpenWindow()
        {
            IsForceOpen = true;
            EditorApplication.update += OnEditorUpdate;       
        }
    }
}