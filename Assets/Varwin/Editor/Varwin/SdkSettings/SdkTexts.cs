﻿using UnityEditor;

namespace Varwin.Editor
{
    public static class SdkTexts
    {
        public const string SdkDownloadHelpMessage = "How to update:\n"
                                                     + "1. Download the new version of Varwin SDK\n"
                                                     + "2. Delete folder: Assets/Varwin\n"
                                                     + "3. Import the downloaded Asset Package";

        public const string DisableAutoCheckToggle = "Disable Auto Check";

        public const string DownloadSdkButton = "Download Varwin SDK";

        public const string VersionsFormat = "Your version: {0}\nLatest version: {1}";

        public const string UpdateAvailableMessage = "A new version is available!";

        public const string NotNeedUpdateMessage = "You have the latest version";

        public const string CacheServerDisabledMobileSupportWarning =
            "Looks like your cache server is disabled. Switching it on can make building objects significantly faster.";

        public const string CacheServerSettingsInfo =
            "After turning the cache server on, you might want to choose a suitable directory for it and change its maximum size.";

        public const string CacheServerCheckWindowApplyButton = "Turn cache server on";

        public const string CacheServerWindowTitle = "Cache server config";
        
        public const string VrDebugPluginNotInstalled = "Varwin SDK VR Debug Plugin is not installed. VR Debug of Varwin Objects in Unity is currently disabled.";
            
        public const string SdkDownloadVrDebugPluginMessage = "How to install the VR Debug Plugin:\n"
                                                              + "1. Download the Varwin SDK VR Debug Plugin package\n"
                                                              + "2. Import the downloaded package (Assets → Import Package → Custom Package...)";

        public const string DownloadVrDebugPluginButton = "Download Varwin SDK VR Debug Plugin";
        
        public const string VrDebugPluginIsInstalledMessage = "Varwin SDK VR Debug Plugin is installed";
        
        public const string HowToUseVrDebugPluginMessage = "How to use Varwin SDK VR Debug";
        
        public const string DontRemindMeAgain = "Don't remind me again";



        public const string SdkUpdateWindowTitle = "Varwin SDK Update";

        public const string SdkVrDebugPluginWindowTitle = "Varwin VR Debug Plugin";

        public const string SdkSettingsWindowTitle = "Varwin SDK Settings";

        public const string UnitySettingsWindowTitle = "Varwin Unity Project Settings";

        public const string CreateSceneTemplateWindowTitle = "Create scene template";

        public const string SceneTemplateWindowTitle = "Varwin Scene Template";

        public const string DefaultAuthorWindowTitle = "Default Author Info";

        public const string AboutWindowTitle = "About Varwin SDK";


        public const string ExperimentalFeatures = "Experimental Features";

        public const string DeveloperModeFeature = "Developer mode";
        
        public const string PackageSupportFeature = "Packages support";
        
        public const string MobileBuildSupportFeature = "Mobile build support";

        public const string WebGLBuildSupportFeature = "WebGL build support";
        
        public const string SourceIncludeSupportFeature = "Source include support";
        
        public const string FullHdPreviewFeatureFeature = "FullHD Preview";


        public const string RecommendedProjectSettings = "Recommended project settings for Varwin:";

        public const string AllRecommendedOptionsWereApplied = "All recommended options were applied";



        public const string MoveCameraToEditorView = "Move camera to editor view";

        public const string TookScreenShotFormat = "Took screenshot to: {0}";

        public const string SceneTemplateSettings = "Scene Template Settings";

        public const string EnableMobileReadyCheckbox = "Create a new version of the scene template to enable the Mobile Ready checkbox";

        public const string NoTeleportAreaMessage = "There are no objects with the tag \"TeleportArea\" at the scene. Continue building?";

        public const string NoTeleportAreaWarning = "There are no objects with the tag \"TeleportArea\" at the scene";

        public const string NoAndroidModuleWarning = "Android Module for Unity is not installed";

        public const string CreateNewVersionSceneTemplate = "Create a new version of the scene template?";

        public const string SceneTemplateBuildAsk = "Build new version of scene template? Editor will proceed to scene building.";

        public const string ResetDefaultAuthorSettingsButton = "Reset to default author settings";

        public const string AuthorNameEmptyWarning = "Author name can not be empty!";

        public const string SceneTemplateWasBuilt = "Scene template was built and packed.";

        public const string SceneTemplateBuildErrorMessage = "Something went wrong. Please check Unity console output for more info.";

        public const string SceneTemplateBuildStartMessage = "Starting create scene template...";


        public const string CannotCreateDirectoryTitle = "Can't create directory";

        public const string CannotCreateDirectoryFormat = "Can't create directory \"{0}\"";
        
        public const string CannotDeleteFileFormat = "Can't delete file \"{0}\"";

        public const string CannotApplyPrefab = "Cannot apply changes in prefab";

        public const string BuildBundleSuccessMessageFormat = "Build asset bundles to \"{0}\" successful! Time = {1} sec.";

        public const string BuildTargetNoSupportedFormat = "Build Target \"{0}\" is not supported";

        public const string BakeLocationFormat = "Bake location {0}";

        public const string CoreVersionFormat = "Core version is {0}";

        public const string ZipCreateSuccessMessage = "Zip was created!";

        public const string ZipCreateFailMessage = "Can not zip files!";

        public const string BuildAssetBundleStartMessageFormat = "Build asset bundles to {0}";


        public const string SaveDefaultAuthorInfoQuestion = "Save default author info changes?";

        public const string DefaultAuthorInfoWasUpdatedMessage = "Default author info have been updated.";

        public const string DefaultAuthorInfoWasReloadedMessage = "Default author info have been reloaded.";

        public const string DefaultAuthorInfoWillRevertQuestion = "Default author info will revert";

        public const string DefaultAuthorInfoWasRevertMessage = "Default author info have been reverted.";


        public const string CannotGenerateWrapperAssemblyFormat = "Can't generate wrapper: Assembly {0} has not found!";

        public const string CannotAutoGenerateWrapperFormat = "Wrapper {0} is not autogenerated! It would not be overwrite.";
        
        public const string ValueAttributeWithoutValueListFormat = "Value attribute without the additional ValueList attribute found for {0}.";
        
        public const string InconsistentValueListObjectTypesFormat = "ValueList \"{0}\" values have different types!";
        
        public const string ObjectNameOrLocalesNotFoundAtVarwinObjectFormat = "[ObjectName] or [Locale] attributes not found for {0}!";

        public const string WrapperWasLoadedFormat = "Wrapper {0} is loaded!";

        public const string CannotBuildMoreThanOneWrapperFormat = "Can't build {0}. Only one class with IWapperAware interface can be added to an object";

        public const string CannotBuildNoWrapperFoundFormat = "IWrapperAware not found on {0}! Please add VarwinObject inheritor script to object.";
        
        public const string PropertyLocaleAttributeMustHaveString = "Property locale attribute must have 1 string";
        
        public const string ValueIsPrivateFormat = "No public setter or getter found for the [Variable] \"{0}\"";
        
        public const string GetterIsPrivateFormat = "No public getter found for the [Getter] attribute for \"{0}\"";
        
        public const string SetterIsPrivateFormat = "No public setter found for the [Setter] attribute for \"{0}\"";
        
        public const string ObserveIsPrivateFormat = "No public setter or getter found for the [Observe] \"{0}\"";
        
        public const string EnumIsPrivateFormat = "Enum \"{0}\" is private and can't be used with Actions, Functions and Checkers";
        
        public const string ArgsFormatNumberOfArgumentsMismatchFormat = "Number of arguments for \"{0}\" [ArgsFormat] attribute doesn't match the number of its corresponding parameters";

        public const string MethodMustReturnBoolFormat = "Method {0} must return bool!";

        public const string CountArgumentsMethodsIsNotEquals = "Count of arguments for methods with same actions is not equals";
        
        public const string ArgsFormatIsNotEqualsFormat = "Args format of the block \"{0}\" is not equals";

        public const string CannotBuildActionArgumentMismatchFormat = "Can't build {0}. Actions with the same name must have an identical argument count and types";
        
        public const string CustomTypesForbiddenFormat = "{0}: It is forbidden to use custom types in methods, properties and events.";
        
        public const string EventAttributeNotVoidHandlerFormat = "Event \"{0}\" has non-void delegate return type which is not currently supported.";
        
        public const string MethodShouldReturnValueFormat = "Method \"{0}\" with the [Function]-attribute should return a value.";
        
        public const string CoroutineShouldReturnIEnumeratorFormat = "Method \"{0}\" with the [Coroutine]-attribute should return a IEnumerator.";

        public const string WaitUntilObjectsCreate = "Please wait until all objects are built";

        public const string ObjectClassNameEmptyWarning = "Object Class Name can't be empty";

        public const string ObjectClassNameUnavailableSymbolsWarning = "Object Class Name contains invalid characters";

        public const string ObjectClassNameDuplicateWarning = "An object with the same Object Class Name already exists.";

        public const string ObjectNullWarning = "Object can't be null";

        public const string ObjectContainsComponentWarningFormat = "Object contains <{0}> component";

        public const string CannotCreateObjectErrorFormat = "{0}\nCan't create object";

        public const string ObjectWithNameAlreadyExists = "Object with this name already exists!";

        public const string ObjectWithNameAlreadyExistsFormat = "{0} already exists!";

        public const string PrefabAlreadyExistsWarningFormat = "{0} prefab already exists. Do you want to overwrite it?";

        public const string ConvertingToPrefabFormat = "Converting {0} to prefab.";

        public const string CannotCreateShuteikaFormat = "Can't create {0}: no prefab and no model. How did it happen? Please try again.";

        public const string CannotReadAuthorInfo = "cannot read author name and author url properties";

        public const string CannotReadLicenseInfo = "cannot read license code property";

        public const string TempBuildError = "Temp build file is broken! Can't finish objects creation.";

        public const string CreatingPrefabFormat = "Creating prefab for {0}";

        public const string CannotCreateObjectFormat = "Can't create object {0}. Imported file is incorrect: {1}";

        public const string ObjectsCreateDoneFormat = "{0} objects were created!";
        
        public const string SingleObjectCreateDoneFormat = "Object was created!";

        public const string ObjectsCreateProblemFormat = "{0}:\nProblem when creating objects";

        public const string NoModelsToImport = "No models to import";

        public const string UnsupportedFileFormat = "Unsupported file format";

        public const string ModelCannotImportFormat = "{0} can't be imported";

        public const string ImportModelProblemFormat = "Problem when importing a model for \"{0}\" ({1}): {2}";

        public const string ImportModelProblemStructureFormat = "Problem when importing a model for \"{0}\" ({1}):\nNot valid model structure ({2})";

        public const string CannotCreateObjectTypeFormat = "Can't create object with class name {0}: not a valid type name";

        public const string ImportModelStepImportFormat = "Importing model from {0}...";

        public const string ImportModelForFormat = "Import model for {0}";

        public const string GltfCannotBeEmpty = "glTF file cannot be empty.";

        public const string GltfAuthoringNotFound = "cannot import; no authoring data can be found in the .gltf";

        public const string CannotImportModelFormat = "Can't import models:\n{0}";

        public const string NoObjectsToCreateCode = "No objects to create code";
        
        public const string NoSuitableForBuildObjectsFound = "No suitable for build objects found";

        public const string CreateCodeStepFormat = "Creating code for {0}";

        public const string CompilingScriptsStep = "Compiling scripts...";

        public const string GeneratingWrappersStep = "Generating wrappers...";

        public const string BuildingAssetBundlesStep = "Building asset bundles...";

        public const string PackSourcesStep = "Packing sources for {0}";

        public const string CreatePreviewStep = "Creating preview for {0}";

        public const string BuildingAssetBundlesStepFormat = "Build asset bundles to {0}";

        public const string CreatingIconStepFormat = "Creating icon for {0}";

        public const string ObjectsCreatingCanceled = "Objects creating canceled!";

        public const string ErrorSketchFabImportTitle = "Error with cleaning-up the SketchFab import directory";

        public const string ErrorSketchFabImportMessageFormat = "Unable to clear the SketchFab import directory (\"{0}\")";

        public const string PreparingObjectsStep = "Preparing objects...";

        public const string ProblemWhenRunBuildAllObjectsFormat = "{0} Problem when run build all objects";

        public const string ProblemWhenRunBuildVarwinObjectFormat = "{0}:\nProblem when run build varwin object \"{1}\"";

        public const string ProblemWhenBuildObjectsFormat = "{0}:\nProblem when build objects";

        public const string OpeningObjectsFolderFormat = "Opening objects folder: {0}";

        public const string CannotCreateIconFormat = "Can't create icon for {0}. Using default icon.";

        public const string IconWasCreatedFormat = "Icon was created for {0}!";

        public const string BuildingAssetBundlesFormat = "Building asset bundles to {0} completed in {1} sec.";

        public const string ProblemWhenBuildingAssetBundlesFormat = "{0}\nProblem when building asset bundles";
        
        public const string ProblemWhenPackSourcesFormat = "{0}\nProblem when pack sources";
        
        public const string ProblemWhenCreatePreview = "{0}\nProblem when create preview";
        
        public const string BuildTargetNotSupportFormat = "Build Target \"{0}\" is not supported";

        public const string InvalidAssemblyTitle = "Invalid script assembly";
        
        public const string InvalidAssemblyFormat = "{0} is not a valid assembly! Move this script to the object folder or create a new reference inside the Assembly Definition";

        public const string CreateNewVersionObject = "Create a new version of the object to enable the Mobile Ready checkbox";

        public const string UnityCompiling = "Unity is compiling. Please, wait...";
        
        public const string ScriptCompilationFailed = "Unity script compilation failed! All compiler errors have to be fixed before you can continue!";

        public const string MoveScriptQuestionFormat = "Are you sure you want to move the script \"{0}\" to the folder \"{1}\"?";
        public const string MoveScriptAndCreateAssemblyDefinitionQuestionFormat = "Are you sure you want to move the script \"{0}\" to the folder \"{1}\" and create an Assembly Definition in it?";
        
        public const string VarwinObjectBuildDialogTitle = "Varwin Object Build Dialog";

        public const string EditorWillProceed = "Editor will proceed to object building";

        public const string ObjectTypeNameEmpty = "Object type name is empty";

        public const string ScriptWithoutAsmdefFormat = "The script \"{0}\" does not have Assembly Definition";
        
        public const string ScriptWithoutAsmdefInAssetsWithScriptsAsmdefFormat = "The script \"{0}\" does not have Assembly Definition. Move the script to the \"Assets/Scripts\" folder.";

        public const string ScriptWithoutAsmdefInAssetsFormat = "The script \"{0}\" does not have Assembly Definition. Move the script to the \"Assets/Scripts\" folder and create a Varwin Assembly Definition in it.";
        public const string ContinueSceneTemplateBuildingProblems = "If you continue building scene template now, it will work incorrectly.";
        

        public const string ObjectNotSelectable = "Object is not selectable. Add Rigidbody and Collider to the root object";
        public const string ObjectNotSelectableRigidbody = "Object is not selectable. Add Rigidbody to the root object";
        public const string ObjectNotSelectableCollider = "Object is not selectable. Add Collider to the root object";
        
        public const string SceneIsNotSaved = "You need to save the scene to build the scene template.";

        public const string DuplicateObjectIds = "Object contains duplicate Object Ids";

        public const string DuplicateObjectIdsHelp = "Object contains duplicate Object Ids. Remove duplicates?";

        public const string CreateNewVersion = "Create new version";

        public const string AnItemWithLanguageHasAlreadyBeenAddedFormat = "An item with the language \"{0}\" has already been added";


        public const string CreateNewVersionOfTheObjectMessage = "Create a new version of the object?";
        
        public const string CreateNewVersionOfTheObjectTitle = "Create a new version of the object";

        public const string BuildObjectAsk = "Build new version of object? Editor will proceed to object building.";


        
        public const string VersionNotFoundTitle = "Varwin SDK version is not found";
            
        public const string VersionNotFoundMessage = "Varwin SDK version is not found! Please reinstall the SDK.";
    }
}
