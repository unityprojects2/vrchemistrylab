using System;
using UnityEditor;
using UnityEngine;

namespace Varwin.Editor
{
    /// <summary>
    /// SDK global settings class
    /// </summary>
    public static class SdkSettings
    {
        public const string ObjectCreationFolderPathPrefKey = @"SDKSettings.ObjectCreationFolderPath";
        public const string ObjectBuildingFolderPathPrefKey = @"SDKSettings.ObjectBuildingFolderPath";
        public const string SceneTemplateBuildingFolderPathPrefKey = @"SDKSettings.SceneTemplateBuildingFolderPath";
        
        public static SdkFeature DeveloperModeFeature = new SdkFeature("DeveloperMode");
        public static SdkFeature PackageFeature = new SdkFeature("Package") { Enabled = true };
        public static SdkFeature MobileFeature = new SdkFeature("Mobile");
        public static SdkFeature WebGLFeature = new SdkFeature("WebGL");

        public static string ObjectCreationFolderPath = EditorPrefs.GetString(ObjectCreationFolderPathPrefKey, "Assets/Objects");
        public static string ObjectBuildingFolderPath = EditorPrefs.GetString(ObjectBuildingFolderPathPrefKey, "BakedObjects");
        public static string SceneTemplateBuildingFolderPath = EditorPrefs.GetString(SceneTemplateBuildingFolderPathPrefKey, "BakedSceneTemplates");
    }
}
