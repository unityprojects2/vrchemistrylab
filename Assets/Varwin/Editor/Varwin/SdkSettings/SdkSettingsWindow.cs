using System;
using UnityEditor;
using UnityEngine;

namespace Varwin.Editor
{
    public class SdkSettingsWindow : EditorWindow
    {
        private static SdkSettingsWindow _window;
        
        private static readonly Vector2 WindowSize = new Vector2(560, 220);
        
        public static void OpenWindow()
        {
            _window = GetWindow<SdkSettingsWindow>(false, SdkTexts.SdkSettingsWindowTitle, true);
            
            _window.minSize = WindowSize;
            _window.maxSize = WindowSize;
            
            _window.Show();
        }

        private void OnGUI()
        {
            GUILayout.BeginVertical();
            DrawCreationSettings();
            EditorGUILayout.Space();
            DrawBuildingSettings();
            EditorGUILayout.Space();
            DrawFeatureToggling();
            GUILayout.EndVertical();
        }

        private void DrawCreationSettings()
        {
            GUILayout.BeginVertical();
            
            GUILayout.Label("Creation Settings", EditorStyles.boldLabel);
            
            DrawObjectCreationFolder();
            
            GUILayout.EndVertical();
        }

        private void DrawBuildingSettings()
        {
            GUILayout.BeginVertical();
            
            GUILayout.Label("Building Settings", EditorStyles.boldLabel);
            
            DrawObjectBuildingFolder();
            DrawSceneTemplateBuildingFolder();
            
            GUILayout.EndVertical();
        }

        private void DrawFeatureToggling()
        {
            GUILayout.BeginVertical();
            
            GUILayout.Label(SdkTexts.ExperimentalFeatures, EditorStyles.boldLabel);

            DrawFeatureToggle(SdkTexts.DeveloperModeFeature, SdkSettings.DeveloperModeFeature);
            DrawFeatureToggle(SdkTexts.MobileBuildSupportFeature, SdkSettings.MobileFeature);
            DrawFeatureToggle(SdkTexts.WebGLBuildSupportFeature, SdkSettings.WebGLFeature);
            
            GUILayout.EndVertical();
        }
        
        private void DrawFeatureToggle(string label, SdkFeature feature)
        {
            bool featureEnabled = EditorGUILayout.ToggleLeft(label, feature.Enabled);

            if (featureEnabled != feature.Enabled)
            {
                feature.Enabled = featureEnabled;
                feature.Save();
            }
        }
        
        private void DrawObjectCreationFolder()
        {
            string prevPath = SdkSettings.ObjectCreationFolderPath;
            
            Rect controlRect = EditorGUILayout.GetControlRect();
            
            var labelRect = new Rect(controlRect) {size = EditorUtils.CalcSize("Object Creation Folder Path: ", EditorStyles.label)};
            EditorGUI.LabelField(labelRect, "Object Creation Folder Path: ", EditorStyles.label);

            var pathRect = new Rect(controlRect) {x = labelRect.x + labelRect.width, size = EditorUtils.CalcSize(SdkSettings.ObjectCreationFolderPath + " ", EditorStyles.label)};
            EditorGUI.LabelField(pathRect, SdkSettings.ObjectCreationFolderPath, EditorStyles.label);
            
            var buttonRect = new Rect(pathRect.x + pathRect.width, labelRect.y - 1, 22, labelRect.height);
            if (GUI.Button(buttonRect, "...", EditorStyles.miniButton))
            {
                SdkSettings.ObjectCreationFolderPath = EditorUtility.OpenFolderPanel("Default Object Folder Path", SdkSettings.ObjectCreationFolderPath, "").Trim().TrimEnd('/');

                if (string.IsNullOrEmpty(SdkSettings.ObjectCreationFolderPath))
                {
                    SdkSettings.ObjectCreationFolderPath = prevPath;
                }
                else
                {
                    if (SdkSettings.ObjectCreationFolderPath.IndexOf(UnityProject.Path, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        SdkSettings.ObjectCreationFolderPath = SdkSettings.ObjectCreationFolderPath.Replace(UnityProject.Path, "").TrimStart('/');
                    }
                    else
                    {
                        SdkSettings.ObjectCreationFolderPath = prevPath;
                        EditorUtility.DisplayDialog("Error!", "Object Folder must be in unity project's Assets folder", "OK");
                    }
                }
            }
            
            if (prevPath != SdkSettings.ObjectCreationFolderPath)
            {
                EditorPrefs.SetString(SdkSettings.ObjectCreationFolderPathPrefKey, SdkSettings.ObjectCreationFolderPath);
            }
        }
        
        private void DrawObjectBuildingFolder()
        {
            string prevPath = SdkSettings.ObjectBuildingFolderPath;
            
            Rect controlRect = EditorGUILayout.GetControlRect();
            
            var labelRect = new Rect(controlRect) {size = EditorUtils.CalcSize("Object Building Folder Path: ", EditorStyles.label)};
            EditorGUI.LabelField(labelRect, "Object Building Folder Path: ", EditorStyles.label);

            var pathRect = new Rect(controlRect) {x = labelRect.x + labelRect.width, size = EditorUtils.CalcSize(SdkSettings.ObjectBuildingFolderPath + " ", EditorStyles.label)};
            EditorGUI.LabelField(pathRect, SdkSettings.ObjectBuildingFolderPath, EditorStyles.label);
            
            var buttonRect = new Rect(pathRect.x + pathRect.width, labelRect.y - 1, 22, labelRect.height);
            if (GUI.Button(buttonRect, "...", EditorStyles.miniButton))
            {
                SdkSettings.ObjectBuildingFolderPath = EditorUtility.OpenFolderPanel("Default Object Building Folder Path", SdkSettings.ObjectBuildingFolderPath, "").Trim().TrimEnd('/');

                if (string.IsNullOrEmpty(SdkSettings.ObjectBuildingFolderPath))
                {
                    SdkSettings.ObjectBuildingFolderPath = prevPath;
                }
            }
            
            if (prevPath != SdkSettings.ObjectBuildingFolderPath)
            {
                EditorPrefs.SetString(SdkSettings.ObjectBuildingFolderPathPrefKey, SdkSettings.ObjectBuildingFolderPath);
            }
        }
        
        private void DrawSceneTemplateBuildingFolder()
        {
            string prevPath = SdkSettings.SceneTemplateBuildingFolderPath;
            
            Rect controlRect = EditorGUILayout.GetControlRect();
            
            var labelRect = new Rect(controlRect) {size = EditorUtils.CalcSize("Scene Template Building Folder Path: ", EditorStyles.label)};
            EditorGUI.LabelField(labelRect, "Scene Template Building Folder Path: ", EditorStyles.label);

            var pathRect = new Rect(controlRect) {x = labelRect.x + labelRect.width, size = EditorUtils.CalcSize(SdkSettings.SceneTemplateBuildingFolderPath + " ", EditorStyles.label)};
            EditorGUI.LabelField(pathRect, SdkSettings.SceneTemplateBuildingFolderPath, EditorStyles.label);
            
            var buttonRect = new Rect(pathRect.x + pathRect.width, labelRect.y - 1, 22, labelRect.height);
            if (GUI.Button(buttonRect, "...", EditorStyles.miniButton))
            {
                SdkSettings.SceneTemplateBuildingFolderPath = EditorUtility.OpenFolderPanel("Scene Template Building Folder Path", SdkSettings.SceneTemplateBuildingFolderPath, "").Trim().TrimEnd('/');

                if (string.IsNullOrEmpty(SdkSettings.SceneTemplateBuildingFolderPath))
                {
                    SdkSettings.SceneTemplateBuildingFolderPath = prevPath;
                }
            }
            
            if (prevPath != SdkSettings.SceneTemplateBuildingFolderPath)
            {
                EditorPrefs.SetString(SdkSettings.SceneTemplateBuildingFolderPathPrefKey, SdkSettings.SceneTemplateBuildingFolderPath);
            }
        }
    }
}
