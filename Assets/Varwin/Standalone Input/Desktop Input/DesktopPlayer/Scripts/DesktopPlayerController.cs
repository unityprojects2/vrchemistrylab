﻿using UnityEngine;
#if VARWINCLIENT
using Varwin.Desktop;
using IngameDebugConsole;
#endif
using Varwin.PlatformAdapter;

namespace Varwin.DesktopPlayer
{
    [RequireComponent(typeof(DesktopPlayerInput), typeof(DesktopPlayerInteractionController))]
    public class DesktopPlayerController : MonoBehaviour, IPlayerController
    {
        private const float MaxFallingCheckoutDistance = 1000f;
        
        public static DesktopPlayerController Instance { get; private set; }
        
        public bool IsSprinting { get; private set; }
        public bool IsCrouching { get; private set; }
        public bool IsJumping { get; private set; }
        public bool IsFalling { get; private set; }
        
        public Quaternion Rotation
        {
            get => Quaternion.Euler(_currentRotation);
            set => SetRotation(value);
        }
        
        public Vector3 Position
        {
            get => _targetPosition;
            set => SetPosition(value);
        }

        [Header("Interaction")]
        [SerializeField]
        public LayerMask RaycastIgnoreMask;
        public Camera PlayerCamera;
        public GameObject Hand;
        
        [Header("Camera")] 
        [SerializeField]
        private float _normalFieldOfView = 60f;
        [SerializeField]
        private float _sprintFieldOfViewChange = 2f;
        
        [Header("Body")]
        [SerializeField]
        private float _eyeHeight = 1.7f; 
        [SerializeField]
        private float _crouchEyeHeight = 1f;
        [SerializeField]
        private float _bodyRadius = 0.15f;

        [Header("Movement")]
        [SerializeField]
        private float _movementSpeed = 4f;
        [SerializeField]
        private float _sprintSpeedMultiplier = 2f;
        [SerializeField]
        private float _crouchSpeedMultiplier = 0.3f;
        [SerializeField]
        private float _maximumStepHeight = 0.5f;
        
        [Header("Rotation")]
        [SerializeField]
        private float _normalRotationSpeed = 2.0f;
        [SerializeField]
        private float _sprintRotationSpeed = 1.0f;
        
        [Header("Jump")]
        [SerializeField]
        private float _jumpHeight = 1.0f;
        
        private float _jumpSpeed = 0;

        private float _rotationSpeed;
        private float _speedMultiplier;

        private float _currentFieldOfView;
        
        private Vector3 _targetPosition;
        
        private Vector3 _cameraLocalPosition;
        private Vector3 _currentRotation;
        
        private DesktopPlayerInput _playerInput;
        private DesktopPlayerInteractionController _interactionController;

        public bool PlayerCursorIsVisible = true;
        
        private void Awake()
        {
            if (!Instance)
            {
                Instance = this;
            }
            
            PlayerManager.PlayerRespawned += OnPlayerRespawned;
            ProjectData.GameModeChanged += OnGameModeChanged;
            
            gameObject.SetActive(ProjectData.GameMode != GameMode.Edit);

            FieldOfView = _normalFieldOfView;
        }

        private void Start()
        {
            _playerInput = GetComponent<DesktopPlayerInput>();
            _interactionController = GetComponent<DesktopPlayerInteractionController>();
            InputAdapter.Instance.PlayerController.PlayerTeleported += PlayerControllerOnPlayerTeleported;
            
            if (PlayerCamera)
            {
                _cameraLocalPosition = PlayerCamera.transform.localPosition;
                _currentRotation = PlayerCamera.transform.rotation.eulerAngles;
            }

            _rotationSpeed = _normalRotationSpeed;
            _speedMultiplier = 1f;

            _jumpSpeed = Mathf.Sqrt(2f * Mathf.Abs(Physics.gravity.y) * _jumpHeight);

            _targetPosition = transform.position;
            
            DontDestroyOnLoad(gameObject);
        }

        private void OnEnable()
        {
            if (PlayerCamera)
            {
                _cameraLocalPosition = PlayerCamera.transform.localPosition;
                _currentRotation = PlayerCamera.transform.rotation.eulerAngles;
            }

            SetPosition(transform.position);
            SetRotation(transform.rotation);
        }

        private void OnDisable()
        {
            ResetCursor();
        }

        private void OnDestroy()
        {
            InputAdapter.Instance.PlayerController.PlayerTeleported -= PlayerControllerOnPlayerTeleported;
            PlayerManager.PlayerRespawned -= OnPlayerRespawned;
            ProjectData.GameModeChanged -= OnGameModeChanged;
        }

        private void OnGameModeChanged(GameMode gameMode)
        {
            if (gameMode == GameMode.Edit)
            {
                PlayerManager.Respawn();
            }

            gameObject.SetActive(gameMode != GameMode.Edit);
        }

        private void Update()
        {
            if (ProjectData.GameMode == GameMode.Undefined)
            {
                return;
            }
            
            _interactionController.SetCursorVisibility(PlayerCursorIsVisible && !Cursor.visible);
            
#if VARWINCLIENT
            if (DesktopPopupManager.IsPopupShown)
            {
                ResetCursor();
                return;
            }

            if (DebugLogManager.Instance && DebugLogManager.Instance.IsLogWindowVisible)
            {
                ResetCursor();
                return;
            }
#endif
            
            UpdateInput();
            UpdateMovement();
            UpdateCamera();
            UpdatePosition();
            
            Cursor.visible = false;        
        }

        private void ResetCursor()
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        private void OnPlayerRespawned()
        {
            SetPosition(PlayerAnchorManager.SpawnPoint.position);
            SetRotation(PlayerAnchorManager.SpawnPoint.rotation);
        }

        private void PlayerControllerOnPlayerTeleported(Vector3 position)
        {
            SetPosition(position);
        }

        private void UpdateInput()
        {
            if (!IsFalling)
            {
                if (_playerInput.IsCrouching)
                {
                    IsCrouching = true;
                }
                else
                {
                    if (IsCrouching)
                    {
                        bool raycastUp = Physics.Raycast(
                            PlayerCamera.transform.position,
                            transform.TransformDirection(Vector3.up),
                            out RaycastHit hitUp,
                            _eyeHeight - _crouchEyeHeight, ~RaycastIgnoreMask);
                        
                        var spherePosition = PlayerCamera.transform.position + (_eyeHeight - _crouchEyeHeight - _bodyRadius) * Vector3.up;

                        if (!Physics.CheckSphere(spherePosition, 0.8f * _bodyRadius, ~RaycastIgnoreMask, QueryTriggerInteraction.Ignore)
                            && (!raycastUp || raycastUp && hitUp.collider.isTrigger))
                        {
                            IsCrouching = false;
                        }
                    }
                }
            }

            IsSprinting = _playerInput.IsSprinting;

            if (_playerInput.IsJumping && !IsCrouching)
            {
                if (!IsFalling && !IsJumping)
                {
                    IsJumping = true;
                    IsFalling = true;
                }
            }
        }
        
        private void UpdateMovement()
        {
            if (!PlayerManager.WasdMovementEnabled || !_playerInput.IsMoving)
            {
                return;
            }

            Vector2 offset = _playerInput.PlayerMovement;

            if (offset.sqrMagnitude > 1f)
            {
                offset = offset.normalized;
            }

            Vector3 positionOffset = new Vector3(offset.y, 0.0f, offset.x);

            float targetMovementMultiplier = 1f;
            if (IsCrouching)
            {
                targetMovementMultiplier = _crouchSpeedMultiplier;
            }
            else if (IsSprinting)
            {
                targetMovementMultiplier = _sprintSpeedMultiplier;
            }

            _speedMultiplier = Mathf.Lerp(_speedMultiplier, targetMovementMultiplier, 10f * Time.deltaTime);

            MoveIfPossibleTo(_speedMultiplier * Time.deltaTime * _movementSpeed * positionOffset);

        }

        private void UpdateCamera()
        {
            float cameraHeight = Mathf.Lerp(PlayerCamera.transform.localPosition.y, _cameraLocalPosition.y, 10f * Time.deltaTime);
            PlayerCamera.transform.localPosition = new Vector3(_cameraLocalPosition.x, cameraHeight, _cameraLocalPosition.z);

            UpdateFieldOfView();
            
            Vector2 cursorInput = _playerInput.Cursor;
            
            _rotationSpeed = Mathf.Lerp(_rotationSpeed, IsSprinting ? _sprintRotationSpeed : _normalRotationSpeed, 10f * Time.deltaTime);
            
            if (!_playerInput.IsCameraFixed)
            {
                Cursor.lockState = CursorLockMode.Locked;
                
                if (!_interactionController.IsRotatingObject)
                {
                    _currentRotation.x -= cursorInput.y * _rotationSpeed;
                    _currentRotation.y += cursorInput.x * _rotationSpeed;
                    
                    SetRotation(_currentRotation);

                    if (IsCrouching)
                    {
                        _cameraLocalPosition = _crouchEyeHeight * Vector3.up;
                    }
                    else
                    {
                        _cameraLocalPosition = _eyeHeight * Vector3.up;
                    }
                }
            }
            else
            {
                Cursor.lockState = CursorLockMode.None;
            }
        }

        private void UpdateFieldOfView()
        {
            float targetFieldOfView = _currentFieldOfView;
            float fieldOfViewLerpSpeed = 10f;
            
            if (_playerInput.IsMoving && IsSprinting)
            {
                targetFieldOfView += _sprintFieldOfViewChange;
                fieldOfViewLerpSpeed = 5f;
            }
            
            PlayerCamera.fieldOfView = Mathf.Lerp(PlayerCamera.fieldOfView, targetFieldOfView, fieldOfViewLerpSpeed * Time.deltaTime);
        }

        public float FieldOfView
        {
            get => _currentFieldOfView;
            set
            {
                PlayerCamera.fieldOfView = value;
                _currentFieldOfView = value;
            }
        }

        private void MoveIfPossibleTo(Vector3 position)
        {
            Vector3 futurePosition = transform.position + transform.rotation * position;

            RaycastHit hitDown = GetHighestHit(Physics.RaycastNonAlloc(PlayerCamera.transform.position + transform.rotation * position,
                transform.TransformDirection(Vector3.down),
                _hits,
                MaxFallingCheckoutDistance,
                ~RaycastIgnoreMask,
                QueryTriggerInteraction.Ignore));
            
            if (!hitDown.Equals(default(RaycastHit)))
            {
                int raycastNonAllocForward = Physics.RaycastNonAlloc(
                    PlayerCamera.transform.position, 
                    transform.TransformDirection(position.normalized), 
                    _obstacleHits, 
                    _bodyRadius,
                    ~RaycastIgnoreMask);
                
                RaycastHit hitForward = GetHighestHit(
                    raycastNonAllocForward, 
                    _obstacleHits);
                
                int raycastNonAllocDown = Physics.RaycastNonAlloc(
                    PlayerCamera.transform.position, 
                    transform.TransformDirection(position.normalized) + Vector3.down, 
                    _obstacleHits, 
                    _bodyRadius, 
                    ~RaycastIgnoreMask);
                
                RaycastHit hitFeet = GetHighestHit(
                    raycastNonAllocDown,
                    _obstacleHits);
                
                bool frontObstacle = !hitForward.Equals(default(RaycastHit));
                bool feetObstacle = !hitFeet.Equals(default(RaycastHit));
                
                if (!frontObstacle || !feetObstacle)
                {
                    if (hitDown.transform.CompareTag("TeleportArea") && PlayerCamera.transform.localPosition.y - hitDown.distance <= _maximumStepHeight)
                    {
                        _targetPosition = futurePosition;
                        _targetPosition.y = hitDown.point.y;
                    }
                }
            }
        }

        private readonly RaycastHit[] _hits = new RaycastHit[1000]; //Getting as much space as one can
        private readonly RaycastHit[] _obstacleHits = new RaycastHit[1000];
        
        private RaycastHit GetHighestHit(int size, RaycastHit[] hits = null)
        {
            if(size == 0)
            {
                return default;
            }

            if (hits == null)
            {
                hits = _hits;
            }

            RaycastHit ret = default;

            float yMax = float.MinValue;
            
            for (int index = 0; index < size; index++)
            {
                RaycastHit hit = hits[index];

                if (_interactionController)
                {
                    if (_interactionController.GetGrabbedObject() && _interactionController.GetGrabbedObject().transform == hit.transform)
                    {
                        break;
                    }
                }

                if (hit.collider.isTrigger && hit.rigidbody)
                {
                    continue;
                }

                if (hit.point.y > yMax)
                {
                    ret = hit;
                    yMax = ret.point.y;
                }
            }

            return ret;
        }
        
        private void UpdatePosition()
        {
            if (ProjectData.PlatformMode == PlatformMode.Vr)
            {
                transform.position = _targetPosition;
                return;
            }
            
            float y = transform.position.y;
            float delta = Mathf.Abs(y - _targetPosition.y);

            if (PlayerManager.UseGravity)
            {
                if (transform.position.y < PlayerManager.RespawnHeight)
                {
                    PlayerManager.Respawn();
                }

                if (!IsJumping)
                {
                    RaycastHit hitDown = GetHighestHit(Physics.RaycastNonAlloc(
                        transform.position + _maximumStepHeight * Vector3.up,
                        Vector3.down,
                        _hits,
                        _maximumStepHeight,
                        ~RaycastIgnoreMask,
                        QueryTriggerInteraction.Ignore));

                    if (hitDown.Equals(default(RaycastHit)))
                    {
                        IsFalling = true;

                        _targetPosition.y = transform.position.y
                                            - _maximumStepHeight * (1f + PlayerManager.FallingTime * 1.1f);
                    }
                    else
                    {
                        IsJumping = false;
                        IsFalling = false;
                        _targetPosition.y = hitDown.point.y;
                        y = _targetPosition.y;
                        transform.position = _targetPosition;
                    }

                    RaycastHit hitDownUp = GetHighestHit(Physics.RaycastNonAlloc(
                        transform.position + 0.5f * _maximumStepHeight * Vector3.up,
                        Vector3.down,
                        _hits,
                        _maximumStepHeight,
                        ~RaycastIgnoreMask,
                        QueryTriggerInteraction.Ignore));
                    
                    if (!hitDown.Equals(default(RaycastHit)))
                    {
                        if (hitDownUp.transform && hitDownUp.transform.CompareTag("TeleportArea") && hitDownUp.collider && !hitDownUp.collider.isTrigger)
                        {
                            IsJumping = false;
                            IsFalling = false;
                            _targetPosition.y = hitDownUp.point.y;
                            y = _targetPosition.y;
                            transform.position = _targetPosition;
                        }
                    }
                }
                
                if (!IsFalling)
                {
                    if (y < _targetPosition.y)
                    {
                        y = Mathf.Lerp(y, _targetPosition.y, 8f * Time.deltaTime);
                    }
                    else if (y > _targetPosition.y)
                    {
                        if (delta <= _maximumStepHeight)
                        {
                            y = Mathf.Lerp(y, _targetPosition.y, 8f * Time.deltaTime);
                        }
                        else
                        {
                            IsFalling = true;
                        }
                    }

                    PlayerManager.FallingTime = 0;
                }
                else
                {
                    PlayerManager.FallingTime += Time.deltaTime;

                    float fallingDelta = Physics.gravity.y * PlayerManager.FallingTime * Time.deltaTime;
                    if (IsJumping)
                    {
                        float jumpDelta = _jumpSpeed * Time.deltaTime;

                        if (fallingDelta < jumpDelta)
                        {
                            bool raycastUp = Physics.Raycast(
                                PlayerCamera.transform.position,
                                Vector3.up,
                                out RaycastHit hitUp,
                                jumpDelta, ~RaycastIgnoreMask);

                            var spherePosition = PlayerCamera.transform.position + (jumpDelta - _bodyRadius) * Vector3.up;

                            if (Physics.CheckSphere(spherePosition, 0.8f * _bodyRadius, ~RaycastIgnoreMask, QueryTriggerInteraction.Ignore) || raycastUp && !hitUp.collider.isTrigger)
                            {
                                IsJumping = false;
                                jumpDelta = 0f;
                            }
                        }

                        fallingDelta += jumpDelta;
                    }

                    if (y + fallingDelta > _targetPosition.y)
                    {
                        y += fallingDelta;
                    }
                    else
                    {
                        IsFalling = false;
                        IsJumping = false;
                        y = _targetPosition.y;
                        PlayerManager.FallingTime = 0;
                    }
                }
            }
            else
            {
                IsFalling = false;
                IsJumping = false;
                PlayerManager.FallingTime = 0;
            }

            transform.position = new Vector3(_targetPosition.x, y, _targetPosition.z);
        }

        public void ResetCameraState()
        {
            _currentRotation = Vector3.zero;
            PlayerCamera.transform.localRotation = Quaternion.identity;
        }

        public void SetPosition(Vector3 position)
        {
            _targetPosition = position;
            transform.position = position;
            UpdatePosition();
        }

        public void SetRotation(Quaternion rotation)
        {
            SetRotation(rotation.eulerAngles);
        }

        public void SetRotation(Vector3 rotation)
        {
            _currentRotation = rotation;
            
            if (_currentRotation.x >= 270f)
            {
                _currentRotation.x -= 360f;
            }
                    
            _currentRotation.y = Mathf.Repeat(_currentRotation.y, 360);
            _currentRotation.x = Mathf.Clamp(_currentRotation.x, -90, 90);
            
            transform.rotation = Quaternion.Euler(0, _currentRotation.y, 0);
            PlayerCamera.transform.localRotation = Quaternion.Euler(_currentRotation.x, 0, 0);
        }
        
        public void CopyTransform(Transform targetTransform)
        {
            SetRotation(targetTransform.rotation);
            SetPosition(targetTransform.position);
        }

        public void ForceGrabObject(GameObject gameObject)
        {
            if (_interactionController)
            {
                _interactionController.ForceGrabObject(gameObject);
            }
        }

        public void ForceDropObject(GameObject gameObject)
        {
            if (_interactionController)
            {
                _interactionController.ForceDropObject(gameObject);
            }
        }
        
        public void DropGrabbedObject(bool forced = false)
        {
            if (_interactionController)
            {
                if (forced)
                {
                    _interactionController.ForceDropObject();
                }
                else
                {
                    _interactionController.DropGrabbedObject();
                }
            }
        }
    }
}
