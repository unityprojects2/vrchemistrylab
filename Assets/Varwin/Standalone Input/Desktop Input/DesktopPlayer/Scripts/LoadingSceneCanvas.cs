﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Varwin.UI
{
    public class LoadingSceneCanvas : MonoBehaviour
    {
        [SerializeField] private Canvas _canvas;
        [SerializeField] private Slider _progressSlider;
        [SerializeField] private TMP_Text _progressText;
        
        private float _minDuration = 15f;
        private float averageDurationPerObject = 0.5f;
        private float _progress = 0;

        private void OnEnable()
        {
            _canvas.enabled = ProjectData.PlatformMode == PlatformMode.Desktop;
            
            _progress = 0f;
            _minDuration = averageDurationPerObject * ProjectData.ProjectStructure.Objects.Count;
        }

        private void Update()
        {
            if (_canvas.enabled)
            {
                UpdateProgress();
                RedrawSlider();
            }
        }

        private void UpdateProgress()
        {
            var duration = averageDurationPerObject * _minDuration;

            var paused = false;
      
            if (_progress <= 0.25f)
            {
                duration = Random.Range(2f * _minDuration, 4f * _minDuration);
            }
            else if (_progress <= 0.5f)
            {
                duration = Random.Range(_minDuration, 2f * _minDuration);
            }
            else if (_progress <= 0.75f)
            {
                duration = Random.Range(2f * _minDuration, 4f * _minDuration);
            }
            else if (_progress <= 0.875f)
            {
                duration = Random.Range(4f * _minDuration, 8f * _minDuration);
            }
            else if (_progress <= 0.95f)
            {
                duration = 8f * _minDuration;
            }
            else
            {
                paused = true;
            }
            
            if (!paused)
            {
                if (duration > 0)
                {
                    float speed = Time.unscaledDeltaTime / duration;
                    _progress = Mathf.Clamp01(_progress + speed);
                }
                else
                {
                    _progress = 1f;
                }
            }
        }

        private void RedrawSlider()
        {
            _progressSlider.normalizedValue = _progress;
            _progressText.text = $"{Mathf.RoundToInt(_progress * 100)} %";
        }
    }
}