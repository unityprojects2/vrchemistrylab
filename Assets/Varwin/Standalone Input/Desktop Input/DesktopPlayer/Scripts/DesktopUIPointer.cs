﻿using System;
using UnityEngine;
using Varwin.PlatformAdapter;

public class DesktopUIPointer : MonoBehaviour
{
    [SerializeField] 
    private Camera _camera;

    private Transform _previousContact;
    private float dist;
    private PointableObject _сurrentPointable;
    private PointableObject _pointableOnPress;

    private readonly float _сastDistance = 50f;

    public bool CanClick() => CheckIfOverPointable();

    public void Click()
    {
        _pointableOnPress = _сurrentPointable;
        OnPointerAction(_сurrentPointable.OnPointerDown, "down");
    }

    public void Clicked()
    {
        if (_pointableOnPress == _сurrentPointable)
        {
            OnPointerAction(_сurrentPointable.OnPointerUpAsButton, "up as button");
            _pointableOnPress = null;
        }

        OnPointerAction(_сurrentPointable.OnPointerUp, "up");
    }

    private void OnPointerAction(Action pointerAction, string pointerActionName)
    {
        if (!_сurrentPointable)
        {
            return;
        }

        try
        {
            pointerAction();
        }
        catch (Exception e)
        {
            Debug.LogError($"On pointer {pointerActionName} error in {_сurrentPointable.name}");
            Debug.LogError(e.StackTrace);
        }
    }

    private bool CheckIfOverPointable() => _сurrentPointable != null;

    public void Update()
    {
        dist = 100f;

        var ray = _camera.ScreenPointToRay(Input.mousePosition);
        var mask = LayerMask.GetMask("UI");
        var isHit = Physics.Raycast(ray, out RaycastHit hit, _сastDistance, mask);

        Debug.DrawRay(ray.origin, ray.direction, Color.red);

        if (_previousContact && _previousContact != hit.transform)
        {
            OnPointerAction(_сurrentPointable.OnPointerOut, "out");
            _previousContact = null;
        }

        _сurrentPointable = isHit ? hit.collider.GetComponent<PointableObject>() : null;

        if (isHit && _сurrentPointable != null && _previousContact != hit.transform)
        {
            OnPointerAction(_сurrentPointable.OnPointerIn, "in");
            _previousContact = hit.transform;
        }

        if (!isHit)
        {
            _previousContact = null;
        }

        if (isHit && hit.distance < 100f)
        {
            dist = hit.distance;
        }
    }
}
