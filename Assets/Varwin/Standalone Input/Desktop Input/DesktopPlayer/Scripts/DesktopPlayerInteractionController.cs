﻿using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Varwin.ObjectsInteractions;
using Varwin.Public;
using Varwin.DesktopInput;
using Varwin.PlatformAdapter;


namespace Varwin.DesktopPlayer
{
    public enum DesktopPlayerCursorState
    {
        Idle,
        Touch,
        Use,
        Grab,
        Pointer
    }

    [RequireComponent(typeof(DesktopPlayerController))]
    public class DesktopPlayerInteractionController : MonoBehaviour
    {
        [Header("Interaction")]
        [SerializeField]
        private float minGrabDistance = 0.21f;
        [SerializeField]
        private float maxGrabDistance = 1.5f;
        private float _defaultMaxGrabDistance;

        [SerializeField]
        private DesktopUIPointer _uiPointer;

        [Header("Cursor")]
        [SerializeField]
        private RectTransform _cursorPivot;
        [Space]
        [SerializeField]
        private GameObject _idleCursor;
        [SerializeField]
        private GameObject _touchCursor;
        [SerializeField]
        private GameObject _useCursor;
        [SerializeField]
        private GameObject _grabCursor;

        [Header("Rotation")] 
        public bool IsRotatingObject;
        public float RotationSpeed = 15f;

        private Vector3 _interactionCursorPosition;

        private float _currentCursorDistance;
        private float _targetCursorDistance;
        private const float ForceGrabCursorDistance = 1.5f;
        
        private DesktopPlayerController _playerController;
        private Camera _camera;

        private DesktopInteractableObject _lastTouchedInteractable;
        private JointBehaviour _lastJointBehaviour;
        private Collider _lastTouchedCollider;

        private DesktopInteractableObject _grabbedObject;
        private CollisionController _grabbedCollisionController;
        private Rigidbody _grabbedRigidbody;

        private bool _grabbedWasBlocked;

        private Vector3 _objectPositionOffset;
        private Quaternion _objectRotationOffset;

        private Vector3 _raycastPosition;

        private bool _uiPointerCanClick;

        private DesktopPlayerInput _input;

        private GameObject _forceGrabbedObject;

        private int _numberOfHits;
        private RaycastHit[] _raycastHits = new RaycastHit[20];

        private void Awake()
        {
            _playerController = GetComponent<DesktopPlayerController>();
            _camera = _playerController.PlayerCamera;
            _input = GetComponent<DesktopPlayerInput>();
            _defaultMaxGrabDistance = maxGrabDistance;
        }

        private void Start()
        {
            SetCursorState(DesktopPlayerCursorState.Idle);
        }

        private void OnEnable()
        {
            _input.GrabReleased += OnGrabReleased;
            _input.UsePressed += OnUsePressed;
            _input.UseReleased += OnUseReleased;
            
            SetCursorState(DesktopPlayerCursorState.Idle);
            Cursor.visible = false;
        }

        private void OnDisable()
        {
            DropGrabbedObject();
            ForceDropObject();
            
            _input.GrabReleased -= OnGrabReleased;
            _input.UsePressed -= OnUsePressed;
            _input.UseReleased -= OnUseReleased;
            
            Cursor.visible = true;
        }

        private void Update()
        {
            maxGrabDistance = DistancePointer.CustomSettings ? DistancePointer.CustomSettings.Distance : _defaultMaxGrabDistance;
            
            _cursorPivot.anchoredPosition = Input.mousePosition - 0.5f * new Vector3(Screen.width, Screen.height, 0);
            _targetCursorDistance = ClampCursorDistance(_targetCursorDistance + 0.05f * Input.mouseScrollDelta.y);
            _currentCursorDistance = Mathf.Lerp(_currentCursorDistance, _targetCursorDistance, 12f * Time.deltaTime);

            UpdateUiPointer();
            
            if (_grabbedObject && (!_grabbedCollisionController || !_grabbedCollisionController.IsBlocked()) && _grabbedWasBlocked)
            {
                DropGrabbedObject();
            }

            IsRotatingObject = _grabbedObject && Input.GetMouseButton(2) && !_grabbedObject.HasVarwinAttachPoint;
            if (IsRotatingObject)
            {
                float x = RotationSpeed * Input.GetAxis("Mouse X");
                float y = RotationSpeed * Input.GetAxis("Mouse Y");

                _objectRotationOffset = Quaternion.AngleAxis(y, Vector3.right) * _objectRotationOffset;
                _objectRotationOffset = Quaternion.AngleAxis(-x, Vector3.up) * _objectRotationOffset;
            }

            if (InteractingWithObject())
            {
                MoveAndRotateObject();
            }
            else
            {
                _playerController.Hand.transform.position = _raycastPosition;
            }
            
            if (_grabbedObject && !_forceGrabbedObject)
            {
                return;
            }

            if (HitAnyInteractableObject())
            {
                TouchClosestObject();
            }
            else
            {
                ForgetDroppedObject();
            }
        }

        public GameObject GetGrabbedObject()
        {
            return _grabbedObject ? _grabbedObject.gameObject : null;
        }
        
        public void SetCursorVisibility(bool cursorVisibility)
        {
            _cursorPivot.gameObject.SetActive(cursorVisibility);
        }
        
        #region Update Sub Methods
        
        private void UpdateUiPointer()
        {
            if (_input.IsTeleportActive)
            {
                return;
            }
            
            if (_uiPointer.CanClick())
            {
                SetCursorState(DesktopPlayerCursorState.Pointer);
                _uiPointerCanClick = true;
            }
            else if (_uiPointerCanClick)
            {
                SetCursorState(DesktopPlayerCursorState.Idle);
                _uiPointerCanClick = false;
            }
        }
        
        private bool HitAnyInteractableObject()
        {
            _numberOfHits = Physics.RaycastNonAlloc(_camera.ScreenPointToRay(Input.mousePosition), _raycastHits, maxGrabDistance, ~_playerController.RaycastIgnoreMask);
            for (int i = 0; i < _numberOfHits; i++)
            {
                var interactable = _raycastHits[i].collider.gameObject.GetComponentInParent<DesktopInteractableObject>();
                if (interactable && interactable.IsInteractable)
                {
                    return true;
                }
            }

            return false;
        }

        private void TouchClosestObject()
        {
            var sortedHits = _raycastHits.ToList().Take(_numberOfHits).OrderBy(x => x.distance);
            
            foreach (var hit in sortedHits)
            {
                _raycastPosition = hit.point;
                
                if (hit.collider == _lastTouchedCollider)
                {
                    return;
                }
                
                var interactable = hit.collider.gameObject.GetComponentInParent<DesktopInteractableObject>();

                if (interactable && interactable != _lastTouchedInteractable)
                {
                    if (!interactable.IsInteractable)
                    {
                        continue;
                    }

                    if (_lastTouchedInteractable)
                    {
                        _lastTouchedInteractable.StopUse();
                        _lastTouchedInteractable.StopTouch();
                    }

                    interactable.StartTouch();

                    _lastTouchedInteractable = interactable;
                    _lastJointBehaviour = interactable.GetComponent<JointBehaviour>();
                    _lastTouchedCollider = hit.collider;

                    SetCursorState(interactable.IsGrabbable ? DesktopPlayerCursorState.Grab :
                        interactable.IsUsable ? DesktopPlayerCursorState.Use : DesktopPlayerCursorState.Idle);
                }

                else if (!interactable)
                {
                    SetCursorState(DesktopPlayerCursorState.Idle);
                }
                    
                break;
            }
        }

        private void ForgetDroppedObject()
        {
            if (_lastTouchedInteractable)
            {
                _lastTouchedInteractable.StopUse();
                _lastTouchedInteractable.StopTouch();

                SetCursorState(DesktopPlayerCursorState.Idle);
            }

            _lastTouchedInteractable = null;
            _lastJointBehaviour = null;
            _lastTouchedCollider = null;

            _raycastPosition = _camera.transform.position;
        }
        
        #endregion Update Sub Methods
        
        #region Late Update Sub Methods
        
        private bool InteractingWithObject() => _grabbedObject || _lastTouchedInteractable && _lastTouchedInteractable.IsUsing();

        private void MoveAndRotateObject()
        {
            VarwinAttachPoint attachPoint = _grabbedObject ? _grabbedObject.gameObject.GetComponent<VarwinAttachPoint>() : null;

            Vector3 mousePosition = (attachPoint) ? _interactionCursorPosition : Input.mousePosition;

            Vector3 cursorOffset = GetCursorWorldPoint(mousePosition, _currentCursorDistance) - GetCursorWorldPoint(_interactionCursorPosition);

            Vector3 objectPositionOffset = _camera.transform.rotation * _objectPositionOffset + cursorOffset;

            if (objectPositionOffset.magnitude > maxGrabDistance)
            {
                objectPositionOffset = maxGrabDistance * objectPositionOffset.normalized;
            }
            else if (objectPositionOffset.magnitude < minGrabDistance)
            {
                objectPositionOffset = minGrabDistance * objectPositionOffset.normalized;
            }

            Vector3 objectPosition = _camera.transform.position + objectPositionOffset;
            Quaternion objectRotation = _camera.transform.rotation * _objectRotationOffset;

            _playerController.Hand.transform.position = objectPosition;

            if (!_grabbedObject)
            {
                return;
            }

            // TODO: Здесь делать броски
            _grabbedRigidbody.velocity = Vector3.zero;
            _grabbedRigidbody.angularVelocity = Vector3.zero;

            if (_lastJointBehaviour)
            {
                _lastJointBehaviour.MoveAndRotate(objectPosition, objectRotation);
            }
            else
            {
                _grabbedRigidbody.transform.position = objectPosition;
                _grabbedRigidbody.transform.rotation = objectRotation;
            }
        }
        
        #endregion Late Update Sub Methods
        
        #region Input Handlers

        private void OnGrabReleased()
        {
            if (_grabbedObject)
            {
                _grabbedWasBlocked = _grabbedCollisionController && _grabbedCollisionController.IsBlocked();

                if (!_grabbedWasBlocked)
                {
                    DropGrabbedObject();
                }
            }
            else
            {
                GrabObject();
            }
        }

        private void OnUsePressed()
        {
            if (ProjectData.InteractionWithObjectsLocked || _input.IsTeleportActive)
            {
                return;
            }
            
            if (_uiPointer.CanClick())
            {
                _uiPointer.Click();
            }

            if (_grabbedObject && _grabbedObject.IsUsable && !_forceGrabbedObject)
            {
                _grabbedObject.StartUse(_playerController.Hand);
                SetCursorState(DesktopPlayerCursorState.Use);
            }
            else if (_lastTouchedInteractable && _lastTouchedInteractable.IsUsable && !_lastTouchedInteractable.IsUsing())
            {
                if (!_forceGrabbedObject)
                {
                    SetupHandRelativeParameters(_raycastPosition, Quaternion.identity);
                }

                _lastTouchedInteractable.StartUse(_playerController.Hand);
                SetCursorState(DesktopPlayerCursorState.Use);
            }
        }

        private void OnUseReleased()
        {
            if (ProjectData.InteractionWithObjectsLocked || _input.IsTeleportActive)
            {
                return;
            }
            
            if (_uiPointer.CanClick())
            {
                _uiPointer.Clicked();
            }

            if (_lastTouchedInteractable && _lastTouchedInteractable.IsUsable && _lastTouchedInteractable.IsUsing())
            {
                _lastTouchedInteractable.StopUse();
                SetCursorState(DesktopPlayerCursorState.Idle);
            }
        }
        
        #endregion Input Handlers

        #region Grab and Drod Methods
        
        private void GrabObject(bool forced = false)
        {
            if ((!ProjectData.InteractionWithObjectsLocked || forced) && _lastTouchedInteractable && _lastTouchedInteractable.IsGrabbable && !_grabbedObject)
            {
                if (_lastTouchedCollider)
                {
                    _grabbedRigidbody = _lastTouchedCollider.attachedRigidbody;
                }

                if (!_grabbedRigidbody)
                {
                    return;
                }
                
                var grabPoint = _lastTouchedInteractable.gameObject.GetComponentInChildren<VarwinAttachPoint>();

                if (grabPoint)
                {
                    _lastTouchedInteractable.gameObject.transform.rotation =
                        Quaternion.LookRotation(-_playerController.Hand.transform.forward, _playerController.Hand.transform.up);
                }
                else
                {
                    var grabSettings = _lastTouchedInteractable.gameObject.GetComponentInChildren<GrabSettings>();

                    if (grabSettings)
                    {
                        _lastTouchedInteractable.gameObject.transform.rotation =
                            Quaternion.LookRotation(-_playerController.Hand.transform.forward, _playerController.Hand.transform.up);
                    }
                }

                _lastTouchedInteractable.StartGrab(_playerController.Hand);
                _grabbedObject = _lastTouchedInteractable;
                _grabbedCollisionController = _grabbedObject.GetComponent<CollisionController>();

                SetupHandRelativeParameters(_grabbedObject.transform.position, _grabbedObject.transform.rotation);

                SetCursorState(DesktopPlayerCursorState.Grab);
            }

            _grabbedWasBlocked = false;
        }
        
        public void ForceGrabObject(GameObject gameObject)
        {
            DesktopInteractableObject interactableToGrab = gameObject.GetComponent<DesktopInteractableObject>();
            Collider collider = gameObject.GetComponent<Collider>();

            if (_grabbedObject != _forceGrabbedObject)
            {
                DropGrabbedObject();
            }

            if (interactableToGrab && _grabbedObject != interactableToGrab)
            {
                if (_lastTouchedInteractable)
                {
                    _lastTouchedInteractable.StopUse();
                    _lastTouchedInteractable.StopTouch();
                }

                _lastTouchedInteractable = interactableToGrab;
                _lastTouchedCollider = collider ? collider : gameObject.GetComponentInChildren<Collider>();

                _currentCursorDistance = ForceGrabCursorDistance;
                Vector3 cursorOffset = GetCursorWorldPoint(Input.mousePosition, _currentCursorDistance);

                gameObject.transform.position = cursorOffset;

                _forceGrabbedObject = gameObject;
                GrabObject(true);
            }
        }
        
        public void ForceDropObject(GameObject gameObject)
        {
            if (_forceGrabbedObject == gameObject)
            {
                ForceDropObject();
            }
        }

        public void ForceDropObject()
        {
            if (_forceGrabbedObject)
            {
                _forceGrabbedObject = null;
                DropGrabbedObject();
            }
        }

        public void DropGrabbedObject()
        {
            if (_forceGrabbedObject)
            {
                return;
            }
            
            if(!_grabbedObject && isActiveAndEnabled)
            {
                StartCoroutine(DropGrabbedObjectDelayed());
                return;
            }
            
            StopGrab();
            ForgetDroppedObject();
        }

        private IEnumerator DropGrabbedObjectDelayed()
        {
            yield return null;

            StopGrab();
        }

        private void StopGrab()
        {
            if (!_grabbedObject)
            {
                return;
            }

            _grabbedObject.StopGrab();

            _grabbedObject = null;
            _grabbedCollisionController = null;
            _grabbedWasBlocked = false;

            SetCursorState(DesktopPlayerCursorState.Idle);
        }
        
        #endregion Grab and Drod Methods
        
        private void SetupHandRelativeParameters(Vector3 position, Quaternion rotation)
        {
            _targetCursorDistance = ClampCursorDistance(Vector3.Distance(position, _camera.transform.position) + 0.1f * Input.mouseScrollDelta.y);
            _currentCursorDistance = _targetCursorDistance;
            _interactionCursorPosition = GetCursorScreenPoint(Input.mousePosition, _currentCursorDistance);

            Quaternion inversedCameraRotation = Quaternion.Inverse(_camera.transform.rotation);

            Vector3 relativePosition = position - _camera.transform.position;
            relativePosition = inversedCameraRotation * relativePosition;

            _objectPositionOffset = relativePosition;

            Quaternion relativeRotation = inversedCameraRotation * rotation;
            _objectRotationOffset = relativeRotation;
        }

        private void SetCursorState(DesktopPlayerCursorState state)
        {
            if (!this)
            {
                return;
            }

            _idleCursor.SetActive(state == DesktopPlayerCursorState.Idle);
            _touchCursor.SetActive(state == DesktopPlayerCursorState.Touch);
            _useCursor.SetActive(state == DesktopPlayerCursorState.Use || state == DesktopPlayerCursorState.Pointer);
            _grabCursor.SetActive(state == DesktopPlayerCursorState.Grab);

            if (state == DesktopPlayerCursorState.Pointer && _uiPointer.CanClick())
            {
                _useCursor.GetComponent<Image>().color = Color.cyan;
            }
            else
            {
                _useCursor.GetComponent<Image>().color = Color.white;
            }
        }
        
        private float ClampCursorDistance(float cursorDistance)
        {
            return Mathf.Clamp(cursorDistance, minGrabDistance, maxGrabDistance);
        }

        private Vector3 GetCursorScreenPoint(Vector3 mousePosition, float distance)
        {
            mousePosition.z = ClampCursorDistance(distance);
            return mousePosition;
        }

        private Vector3 GetCursorWorldPoint(Vector3 screenPosition)
        {
            screenPosition.z = ClampCursorDistance(screenPosition.z);
            return _camera.ScreenToWorldPoint(screenPosition);
        }

        private Vector3 GetCursorWorldPoint(Vector3 screenPosition, float distance)
        {
            screenPosition.z = ClampCursorDistance(distance);
            return _camera.ScreenToWorldPoint(screenPosition);
        }
    }
}