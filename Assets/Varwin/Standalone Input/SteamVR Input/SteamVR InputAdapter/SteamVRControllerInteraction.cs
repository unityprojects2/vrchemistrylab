using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
using Varwin.ObjectsInteractions;
using Varwin.PlatformAdapter;

namespace Varwin.VRInput
{
    public class SteamVRControllerInteraction : ControllerInteraction
    {
        public SteamVRControllerInteraction()
        {
            Controller = new ComponentWrapFactory<ControllerSelf, SteamVRController, Hand>();
            Haptics = new SteamVRControllerHaptics();
        }

        private class SteamVRController : ControllerSelf, IInitializable<Hand>
        {
            private Hand _hand;

            private HashSet<Collider> _colliders;

            public override void TriggerHapticPulse(float strength, float duration, float interval)
            {
                if (DeviceHelper.IsVive)
                {
                    ushort microSecondsDuration = (ushort) Mathf.Clamp(duration * 1000000f, ushort.MinValue, ushort.MaxValue);
                    _hand.TriggerHapticPulse(microSecondsDuration);
                }
                else
                {
                    _hand.TriggerHapticPulse(duration, interval, strength);
                }
            }

            public override GameObject GetGrabbedObject()
            {
                try
                {
                    return _hand.AttachedObjects[0].attachedObject;
                }
                catch
                {
                    // ignored
                }

                return null;
            }

            public override void StopInteraction()
            {
                var grabbedObject = _hand.currentAttachedObject;
                if (grabbedObject)
                {
                    DropGrabbedObject(grabbedObject);
                }
            }
            
            public override void ForceGrabObject(GameObject gameObject)
            {
                SteamVRInteractableObject interactable = gameObject.GetComponent<SteamVRInteractableObject>();

                if (!interactable)
                {
                    return;
                }

                CollisionController collisionController = gameObject.GetComponent<CollisionController>();

                if (collisionController)
                {
                    collisionController.ForceDestroy();
                }

                interactable.ForceGrabbed = true;
                interactable.ValidDrop = ObjectInteraction.InteractObject.ValidDropTypes.NoDrop;
                
                _hand.AttachObject(gameObject,
                    GrabTypes.Scripted,
                    Hand.AttachmentFlags.ParentToHand
                    | Hand.AttachmentFlags.DetachFromOtherHand
                    | Hand.AttachmentFlags.DetachOthers
                    | Hand.AttachmentFlags.TurnOnKinematic
                    | Hand.AttachmentFlags.SnapOnAttach,
                    interactable.AttachmentOffset);
                
                _hand.HideGrabHint();
                
                if (_hand.currentAttachedObject == gameObject)
                {
                    collisionController = gameObject.GetComponent<CollisionController>();

                    if (collisionController)
                    {
                        collisionController.enabled = false;
                    }
                }
            }

            public override void ForceDropObject()
            {
                var grabbedObject = _hand.currentAttachedObject;
                if (grabbedObject)
                {
                    DropGrabbedObject(grabbedObject, dropForceGrabbed:true);
                }
            }

            public override void ForceDropObject(GameObject gameObject)
            {
                var grabbedObject = _hand.currentAttachedObject;
                if (grabbedObject && grabbedObject == gameObject)
                {
                    DropGrabbedObject(grabbedObject, dropForceGrabbed:true);
                }
            }

            private void DropGrabbedObject(GameObject grabbedObject, bool dropForceGrabbed = false)
            {
                SteamVRInteractableObject interactable = grabbedObject.GetComponent<SteamVRInteractableObject>();

                if (interactable)
                {
                    if (interactable.ForceGrabbed && !dropForceGrabbed)
                    {
                        return;
                    }

                    interactable.ValidDrop = ObjectInteraction.InteractObject.ValidDropTypes.DropAnywhere;
                    interactable.ForceGrabbed = false;
                }
                    
                _hand.DetachObject(grabbedObject);
            }

            public override void AddColliders(Collider[] colliders)
            {
                if (colliders == null)
                {
                    _colliders = null;
                }
                else
                {
                    _colliders = new HashSet<Collider>(colliders);
                }
            }

            public override bool CheckIfColliderPresent(Collider coll)
            {
                if (_colliders == null)
                {
                    return false;
                }

                return _colliders.Contains(coll);
            }

            public void Init(Hand monoBehaviour)
            {
                _hand = monoBehaviour;
            }
        }

        private class SteamVRControllerHaptics : ControllerHaptics
        {
            public override void TriggerHapticPulse(
                PlayerController.PlayerNodes.ControllerNode playerController,
                float strength,
                float duration,
                float interval)
            {
                playerController.Controller.TriggerHapticPulse(strength, duration, interval);
            }
        }

        public class SteamVRControllerInteractionComponent : ControllerInteractionComponent
        {
        }
    }

    public static class ControllerInteractionEx
    {
        public static ControllerInteraction.ControllerHand GetHand(this SteamVR_Input_Sources self)
        {
            switch (self)
            {
                case SteamVR_Input_Sources.RightHand: return ControllerInteraction.ControllerHand.Right;

                case SteamVR_Input_Sources.LeftHand: return ControllerInteraction.ControllerHand.Left;

                default: return ControllerInteraction.ControllerHand.None;
            }
        }
    }
}
