﻿using UnityEngine;
using Valve.VR.InteractionSystem;
using Varwin.PlatformAdapter;

namespace Varwin.VRInput
{
    public class StvrDistancePointer : DistancePointer
    {
        private Hand Hand;

        private Transform HoverTransform;
        
        private LineRenderer Renderer;
        
        private SteamVRInteractableObject CurrentObject;
        
        private bool _activity;

        private ControllerInteraction.ControllerSelf _controllerReference;
        
        private bool Activity
        {
            get => _activity;
            set
            {
                _activity = value;
                Renderer.enabled = value;
            }
        }

        public override bool CanToggle()
        {
            if (Activity)
            {
                return true;
            }
            
            return !ProjectData.InteractionWithObjectsLocked && CurrentObject && (CurrentObject.IsGrabbable || CurrentObject.IsUsable);
        }

        public override bool CanPress() => false;

        public override bool CanRelease() => false;

        public override void Toggle(bool value)
        {
            Activity = value;
        }

        public override void Toggle()
        {
            Activity = !Activity;
        }        

        public override void Press()
        {
        }

        public override void Release()
        {
        }

        public override bool IsActive() => Activity;

        public override void UpdateState()
        {
            Ray ray = new Ray(Origin.position, Origin.forward);
            if (Physics.Raycast(ray, out RaycastHit hit, Settings.Distance, Settings.LayerMask.value))
            {
                if (hit.collider)
                {
                    CurrentObject = hit.collider.GetComponentInParent<SteamVRInteractableObject>();
                }
            }
            else
            {
                CurrentObject = null;
            }

            // ReSharper disable once PossibleNullReferenceException
            bool isInteractable = !ProjectData.InteractionWithObjectsLocked && CurrentObject
                                  && (CurrentObject.Wrapper != null && CurrentObject.Wrapper.Enabled)
                                  && (CurrentObject.IsGrabbable || CurrentObject.IsUsable)
                                  && !_controllerReference.GetGrabbedObject();
            if (isInteractable)
            {
                HoverTransform.position = hit.point;
                Hand.hoverSphereRadius = 0.001f;
            }
            else
            {
                HoverTransform.localPosition = Vector3.zero;
                Hand.hoverSphereRadius = _hoverSphereRadiusOnInit;
            }
            
            Renderer.enabled = isInteractable;
            Renderer.SetPosition(0, ray.origin);
            Renderer.SetPosition(1, HoverTransform.position);
        }

        private float _hoverSphereRadiusOnInit;

        protected override void OnInit()
        {            
            _controllerReference = InputAdapter.Instance.ControllerInteraction.Controller.GetFrom(gameObject);
            Hand = GetComponent<Hand>();
            _hoverSphereRadiusOnInit = Hand.hoverSphereRadius;
            HoverTransform = Hand.hoverSphereTransform;
            
            Renderer = Settings.CreateRenderer(gameObject);
            Renderer.enabled = false;
        }
    }
}
