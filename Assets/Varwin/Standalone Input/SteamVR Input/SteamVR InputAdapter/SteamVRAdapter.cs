using Varwin.PlatformAdapter;

namespace Varwin.VRInput
{
    public class SteamVRAdapter : SDKAdapter
    {
        public SteamVRAdapter() : base(new SteamVRPlayerAppearance(),
            new SteamVRControllerInput(),
            new SteamVRObjectInteraction(),
            new SteamVRControllerInteraction(),
            new SteamVRPlayerController(),
            new SteamVRPointerController(),
            new ComponentFactory<PointableObject, UniversalPointableObject>(),
            new ComponentWrapFactory<Tooltip, UniversalTooltip, TooltipObject>())
        {
        }
    }
}