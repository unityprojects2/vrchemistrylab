using Varwin.PlatformAdapter;

namespace Varwin.VRInput
{
    public class SteamVRPlayerAppearance : PlayerAppearance
    {
        public SteamVRPlayerAppearance()
        {
            ControllerAppearance = new ComponentWrapFactory<InteractControllerAppearance,
                SteamVRInteractControllerAppearance, SteamVRInteractControllerAppearanceComponent>();
        }


        private class SteamVRInteractControllerAppearance : InteractControllerAppearance,
            IInitializable<SteamVRInteractControllerAppearanceComponent>
        {
            private SteamVRInteractControllerAppearanceComponent _objectAppearanceComponent;

            public override bool HideControllerOnGrab
            {
                set => _objectAppearanceComponent.hideControllerOnGrab = value;
            }

            public override void DestroyComponent()
            {
                _objectAppearanceComponent.Destroy();
            }

            public void Init(SteamVRInteractControllerAppearanceComponent monoBehaviour)
            {
                _objectAppearanceComponent = monoBehaviour;
            }
        }
    }
}