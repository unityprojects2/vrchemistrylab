using System;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
using Varwin.PlatformAdapter;

namespace Varwin.VRInput
{
    public class SteamVRPlayerController : PlayerController
    {
        public SteamVRPlayerController()
        {
            Tracking = new SteamVRPlayerTracking();
            RigInitializer = new SteamVRPlayerRigInitializer();
            Nodes = new SteamVRPlayerNodes();
            Controls = new SteamVRPlayerControls();
        }

        public override void Teleport(Vector3 position)
        {
            PreTeleport();

            if (ProjectData.PlatformMode != PlatformMode.Desktop)
            {
                Transform player = InputAdapter.Instance.PlayerController.Nodes.Rig.Transform;

                if (!player)
                {
                    Debug.LogError("Can not teleport! Player not found");

                    return;
                }

                Transform head = InputAdapter.Instance.PlayerController.Nodes.Head.Transform;

                if (!head)
                {
                    Debug.LogError("Can not teleport! Head not found");

                    return;
                }


                Vector3 playerPosition = position;
                Vector3 headDelta = player.position - head.position;

                headDelta.y = 0;
                playerPosition = playerPosition + headDelta;
                player.position = playerPosition;
            }

            base.Teleport(position);
        }

        public override void Init(GameObject rig)
        {
            Nodes.Init(rig);

            ((SteamVRPlayerControls) InputAdapter.Instance.PlayerController.Controls).Init(rig.transform, this);
        }


        public class SteamVRPlayerNodes : PlayerNodes
        {
            public SteamVRPlayerNodes()
            {
                Rig = new SteamVRNode();
                Head = new SteamVRNode();
                LeftHand = new SteamVRControllerNode();
                RightHand = new SteamVRControllerNode();
            }

            public override void Init(GameObject rig)
            {
                GameObject go = rig
                    .GetComponentInChildren<Camera>()
                    .gameObject;
                Head?.SetNode(go);
                Rig.SetNode(rig);
            }

            public override ControllerNode GetControllerReference(GameObject controller)
            {
                var hand = controller.GetComponent<Hand>();

                if (!hand)
                {
                    return null;
                }

                switch (hand.handType)
                {
                    case SteamVR_Input_Sources.LeftHand: return LeftHand;
                    case SteamVR_Input_Sources.RightHand: return RightHand;
                }

                return null;
            }

            public override ControllerNode GetControllerReference(ControllerInteraction.ControllerHand hand)
            {
                switch (hand)
                {
                    case ControllerInteraction.ControllerHand.Left: return LeftHand;
                    case ControllerInteraction.ControllerHand.Right: return RightHand;
                }

                return null;
            }

            private class SteamVRNode : Node
            {
            }

            private class SteamVRControllerNode : ControllerNode
            {
                public override void SetNode(GameObject gameObject)
                {
                    Controller = InputAdapter.Instance.ControllerInteraction.Controller.GetFrom(gameObject);
                    PointerManager = InputAdapter.Instance.PointerController.Managers.GetFrom(gameObject);
                    base.SetNode(gameObject);
                }

                public override ControllerInteraction.ControllerSelf Controller { get; protected set; }
                public override PointerManager PointerManager { get; protected set; }
            }

            public override GameObject GetModelAliasController(GameObject controllerEventsGameObject) =>
                throw new NotImplementedException();

            public override ControllerInteraction.ControllerHand GetControllerHand(
                GameObject controllerEventsGameObject)
            {
                if (!controllerEventsGameObject)
                {
                    return ControllerInteraction.ControllerHand.None;
                }

                var hand = controllerEventsGameObject.GetComponentInChildren<Hand>();

                return hand
                    ? (ControllerInteraction.ControllerHand) hand.handType
                    : ControllerInteraction.ControllerHand.None;
            }

            public override string GetControllerElementPath(
                ControllerInteraction.ControllerElements findElement,
                ControllerInteraction.ControllerHand controllerHand,
                bool b) => throw new NotImplementedException();
        }

        private class SteamVRPlayerRigInitializer : PlayerRigInitializer
        {
            private GameObject _rig;

            public override GameObject InitializeRig()
            {
                _rig = Resources.Load<GameObject>("PlayerRig/STVRPlayer");
                return _rig;
            }
        }

        private class SteamVRPlayerControls : PlayerControls
        {
            private Transform _player;
            private PlayerController _controller;
            private const int TurnAngle = 45;

            public void Init(Transform playerRig, PlayerController controller)
            {
                _player = playerRig;
                _controller = controller;

                ((SteamVRControllerInput) InputAdapter.Instance.ControllerInput).TurnLeftPressed +=
                    (sender, args) => { Turn(true); };

                ((SteamVRControllerInput) InputAdapter.Instance.ControllerInput).TurnRightPressed +=
                    (sender, args) => { Turn(false); };
            }

            public void Turn(bool isLeft)
            {
                _controller.PreRotate();
                
                var oldRotation = _player.rotation;
                if (isLeft)
                {
                    RotatePlayer(-TurnAngle);
                }
                else
                {
                    RotatePlayer(TurnAngle);
                }

                _controller.Rotate(_player.rotation, oldRotation);
            }

            public override void RotatePlayer(float angle)
            {
                Vector3 headPos = InputAdapter.Instance.PlayerController.Nodes.Head.Transform.position;
                Transform player = InputAdapter.Instance.PlayerController.Nodes.Rig.Transform;

                Vector3 headDelta = player.position - headPos;
                headDelta.y = 0;
                Vector3 aHead = new Vector3(headPos.x, player.position.y, headPos.z);
                _player.position = aHead + Quaternion.AngleAxis(angle, Vector3.up) * headDelta;
                _player.Rotate(_player.up, angle);
            }
        }

        private class SteamVRPlayerTracking : PlayerTracking
        {
            public override GameObject GetBoundaries(Transform transform) => transform.gameObject;
        }
    }
}
