﻿using UnityEngine;
using Valve.VR;
using Varwin;
using Varwin.PlatformAdapter;

public class SteamVRTooltipAdapter : TooltipTransformAdapterBase
{
    public override Transform GetButtonTransform(ControllerInteraction.ControllerElements controllerElement)
    {
        Transform returnedTransform = null;

        var hintsController = gameObject.GetComponentInChildren<SteamVR_RenderModel>();

        if (!hintsController) return returnedTransform;

        hintsController.gameObject.SetActive(true);
        
        switch (controllerElement)
        {
            case ControllerInteraction.ControllerElements.Trigger:
                returnedTransform = FindButtonTransform(hintsController, "trigger");
                break;
            case ControllerInteraction.ControllerElements.GripLeft:

                if (DeviceHelper.IsVive)
                {
                    returnedTransform = FindButtonTransform(hintsController, "lgrip");
                }
                else if (DeviceHelper.IsOculus)
                {
                    returnedTransform = FindButtonTransform(hintsController, "grip");
                }
                else
                {
                    returnedTransform = FindButtonTransform(hintsController, "handgrip");
                }

                break;
            case ControllerInteraction.ControllerElements.Touchpad:
                
                if (DeviceHelper.IsOculus)
                {
                    returnedTransform = FindButtonTransform(hintsController, "thumbstick");
                }
                else
                {
                    returnedTransform = FindButtonTransform(hintsController, "trackpad");
                }
                break;
            case ControllerInteraction.ControllerElements.ButtonOne:
                returnedTransform = FindButtonTransform(hintsController, "");
                break;
            case ControllerInteraction.ControllerElements.ButtonTwo:
                
                if (DeviceHelper.IsVive)
                {
                    returnedTransform = FindButtonTransform(hintsController, "button");
                }
                else if (DeviceHelper.IsOculus)
                {
                    returnedTransform = FindButtonTransform(hintsController, "y_button");
                }
                else
                {
                    returnedTransform = FindButtonTransform(hintsController, "menu_button");
                }

                break;
            case ControllerInteraction.ControllerElements.StartMenu:
                returnedTransform = FindButtonTransform(hintsController, "");
                break;
            default:
                returnedTransform = FindButtonTransform(hintsController, "");
                break;
        }

        return returnedTransform;
    }

    private Transform FindButtonTransform(SteamVR_RenderModel hintsController, string buttonName)
    {
        return hintsController.gameObject.transform.Find(buttonName)?.transform.Find("attach");
    }
}
