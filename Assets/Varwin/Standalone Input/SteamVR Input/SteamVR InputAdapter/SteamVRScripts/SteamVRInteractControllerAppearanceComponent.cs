﻿using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
using Varwin.UI;

public class SteamVRInteractControllerAppearanceComponent : MonoBehaviour
{
    public bool hideControllerOnGrab;

    private GameObject hideButtons;

    private void Start()
    {
    }

    public void OnDetachedFromHand(Hand hand)
    {
        if (!hideControllerOnGrab || !hideButtons)
        {
            return;
        }

        hideButtons.SetActive(true);
        hideButtons = null;
    }

    public void OnAttachedToHand(Hand hand)
    {
        if (!hideControllerOnGrab)
        {
            return;
        }

        if (TooltipManager.CheckShowingToolTip(GetHand(hand.handType)))
        {
            return;
        }

        var hints = hand.GetComponentInChildren<ControllerButtonHints>();

        if (!hints)
        {
            return;
        }

        hideButtons = hints.gameObject;

        if (hideButtons)
        {
            hideButtons.SetActive(false);
        }
    }

    public void Destroy()
    {
        Destroy(this);
    }

    private ControllerTooltipManager.TooltipControllers GetHand(SteamVR_Input_Sources handType) =>
        handType == SteamVR_Input_Sources.LeftHand
            ? ControllerTooltipManager.TooltipControllers.Left
            : ControllerTooltipManager.TooltipControllers.Right;
}
