using System.Collections.Generic;
using Varwin.PlatformAdapter;

namespace Varwin.VRInput.SteamVR
{
    public class SteamVRPointerControllerComponent : PointerControllerComponent
    {
        public bool IsMenuOpened;
        public ControllerInteraction.ControllerHand Hand;

        protected override List<IBasePointer> _pointers { get; set; }
        
        private UiPointer _menuPointer;
        
        public bool ShowUIPointer
        {
            set => _menuPointer.IsPriority = value;
            get => _menuPointer.IsPriority;
        }

        private void Awake()
        {
            TeleportPointer teleport = gameObject.AddComponent<TeleportPointer>();
            _menuPointer = gameObject.AddComponent<UiPointer>();
            _pointers = new List<IBasePointer> {_menuPointer, teleport};
            
            var distancePointer = gameObject.GetComponent<DistancePointer>();
            if (distancePointer)
            {
                _pointers.Add(distancePointer);
            }      

            foreach (IBasePointer pointer in _pointers)
            {
                pointer.Init();
            }
        }

        private void Update()
        {
            UpdatePointers();
        }

        protected override void UpdatePointers()
        {
            if (IsMenuOpened)
            {
                if (_menuPointer.IsActive())
                {
                    _menuPointer.Toggle(false);
                }

                return;
            }
            
            base.UpdatePointers();
        }
    }
}
