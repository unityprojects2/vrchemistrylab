﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR;
using Valve.VR.InteractionSystem;
using Varwin;
using Varwin.PlatformAdapter;
using Varwin.Public;
using Varwin.PUN;
using Varwin.VRInput;

public class SteamVRInteractableObject : MonoBehaviour
{
    public event ObjectInteraction.InteractObject.InteractableObjectEventHandler InteractableObjectTouched;
    public event ObjectInteraction.InteractObject.InteractableObjectEventHandler InteractableObjectUntouched;
    public event ObjectInteraction.InteractObject.InteractableObjectEventHandler InteractableObjectUsed;
    public event ObjectInteraction.InteractObject.InteractableObjectEventHandler InteractableObjectUnused;
    public event ObjectInteraction.InteractObject.InteractableObjectEventHandler InteractableObjectGrabbed;
    public event ObjectInteraction.InteractObject.InteractableObjectEventHandler InteractableObjectUngrabbed;
    
    private SteamVR_Behaviour_Boolean _triggerInput;
    private Detachable _detachable;
    private Hand _grabbingObject;
    private Hand _touchingObject;
    private bool _isUsing;
    private Interactable _interactable;
    private Wrapper _wrapper;
    private bool _wrapperIsInitialized;

    public Transform AttachmentOffset => _detachable.attachmentOffset;
    
    public bool IsGrabbable
    {
        get
        {
            if (!_detachable)
            {
                return false;
            }

            return _detachable.IsGrabbable;
        }

        set
        {
            if (!_detachable)
            {
                return;
            }

            _detachable.IsGrabbable = value;
        }
    }
    
    public bool IsUsable { get; set; }

    public bool IsTouchable { get; set; }
    
    public bool ForceGrabbed { get; set; }
    
    public bool IsGrabbed => _grabbingObject != null;

    public bool IsUsing => _isUsing;
    
    public ObjectInteraction.InteractObject.ValidDropTypes ValidDrop
    {
        get
        {
            if (_detachable && _detachable.CanBeDetached)
            {
                return ObjectInteraction.InteractObject.ValidDropTypes.DropAnywhere;
            }

            return ObjectInteraction.InteractObject.ValidDropTypes.NoDrop;
        }
        set
        {
            if (!_detachable)
            {
                return;
            }

            switch (value)
            {
                case ObjectInteraction.InteractObject.ValidDropTypes.DropAnywhere:
                    _detachable.CanBeDetached = true;
                    break;
                case ObjectInteraction.InteractObject.ValidDropTypes.NoDrop:
                    _detachable.CanBeDetached = false;
                    break;
            }
        }
    }

    public ControllerInput.ButtonAlias UseOverrideButton { get; set; }
    
    public ControllerInput.ButtonAlias GrabOverrideButton { get; set; }

    public bool SwapHandsFlag
    {
        get
        {
            if (!_detachable)
            {
                return false;
            }

            return _detachable.attachmentFlags.HasFlag(Hand.AttachmentFlags.DetachOthers);
        }
        set
        {
            if(!_detachable)
            {
                return;
            }

            if (value)
            {
                _detachable.attachmentFlags |= Hand.AttachmentFlags.DetachOthers;
            }
            else
            {
                _detachable.attachmentFlags &= ~Hand.AttachmentFlags.DetachOthers;
            }
        }
    }

    public Wrapper Wrapper
    {
        get
        {
            if (_wrapperIsInitialized)
            {
                return _wrapper;
            }
            _wrapperIsInitialized = true;
			
            var varwinObjectDescriptor = gameObject.GetComponentInParent<VarwinObjectDescriptor>();
            if (varwinObjectDescriptor)
            {
                _wrapper = varwinObjectDescriptor.Wrapper();
            }

            return _wrapper;
        }
    }

    private void Awake()
    {
        _interactable = gameObject.AddComponent<Interactable>();

        if (GetComponent<Rigidbody>())
        {
            _detachable = gameObject.AddComponent<Detachable>();
            _detachable.onPickUp = new UnityEvent();
            _detachable.onDetachFromHand = new UnityEvent();
            _detachable.restoreOriginalParent = true;
            _detachable.catchingSpeedThreshold = 1f;
            SetAttachmentFlags();
            ProjectData.GameModeChanged += mode => { SetAttachmentFlags(); };
        }

    }

    private void Start()
    {
        StartCoroutine(SetupAction());
    }

    public void Destroy()
    {
        _detachable?.Destroy();
        Destroy(_interactable);
        Destroy(_triggerInput);
        Destroy(this);
    }

    private void OnDisable()
    {
        //TODO: drop grabbed object properly
        
        UseStop(_triggerInput, SteamVR_Input_Sources.Any, false);
        
        if (_touchingObject)
        {
            OnHandHoverEnd(_touchingObject);
        }
    }
    
    public GameObject GetGrabbingObject() => _grabbingObject.gameObject;

    public GameObject GetUsingObject()
    {
        if (Settings.Instance.Multiplayer)
        {
            return PlayerNodesSync.GetUsingObject(gameObject);
        }

        if (_grabbingObject)
        {
            return _grabbingObject.gameObject;
        }

        if (_interactable.isHovering)
        {
            return _interactable.hoveringHand.gameObject;
        }

        return null;
    }
    
    public void ForceStopInteracting()
    {
        if (_grabbingObject)
        {
            _grabbingObject.DetachObject(gameObject);
            ForceGrabbed = false;
        }
    }

    public void DropGrabbedObjectAndDeactivate()
    {
        StartCoroutine(DropGrabbedObjectAndDeactivateCoroutine());
    }

    private IEnumerator DropGrabbedObjectAndDeactivateCoroutine()
    {
        yield return null;

        ForceStopInteracting();

        yield return null;

        gameObject.SetActive(false);
    }

    private void Use(SteamVR_Behaviour_Boolean triggerInput, SteamVR_Input_Sources sources, bool state)
    {
        if (ProjectData.InteractionWithObjectsLocked || !IsUsable)
        {
            return;
        }
        
        if (!ValidInput(triggerInput))
        {
            return;
        }

        _isUsing = true;
        
        ControllerInteraction.ControllerHand handContext = triggerInput.booleanAction.activeDevice.GetHand();

        InteractableObjectUsed?.Invoke(this,
            new ObjectInteraction.InteractableObjectEventArgs {interactingObject = gameObject, Hand = handContext});
    }

    private void UseStop(SteamVR_Behaviour_Boolean triggerInput, SteamVR_Input_Sources sources, bool state)
    {
        if (!_isUsing)
        {
            return;
        }
        
        ControllerInteraction.ControllerHand handContext = triggerInput.booleanAction.activeDevice.GetHand();

        InteractableObjectUnused?.Invoke(this,
            new ObjectInteraction.InteractableObjectEventArgs {interactingObject = gameObject, Hand = handContext});
        
        _isUsing = false;
    }
 
    // DO NOT DELETE - Called implicitly from Steam VR
    private void OnHandHoverBegin(Hand hand)
    {
        _touchingObject = hand;
        ControllerInteraction.ControllerHand handContext = hand.handType.GetHand();

        InteractableObjectTouched?.Invoke(this,
            new ObjectInteraction.InteractableObjectEventArgs {interactingObject = gameObject, Hand = handContext});
    }
    
    // DO NOT DELETE - Called implicitly from Steam VR
    private void OnHandHoverEnd(Hand hand)
    {
        if (_isUsing)
        {
            UseStop(_triggerInput, SteamVR_Input_Sources.Any, false);
        }

        _touchingObject = null;
        ControllerInteraction.ControllerHand handContext = hand.handType.GetHand();

        InteractableObjectUntouched?.Invoke(this,
            new ObjectInteraction.InteractableObjectEventArgs {interactingObject = gameObject, Hand = handContext});
    }

    // DO NOT DELETE - Called implicitly from Steam VR
    private void OnAttachedToHand(Hand hand)
    {
        _grabbingObject = hand;
        ControllerInteraction.ControllerHand handContext = hand.handType.GetHand();

        InteractableObjectGrabbed?.Invoke(this,
            new ObjectInteraction.InteractableObjectEventArgs {interactingObject = gameObject, Hand = handContext});
    }

    // DO NOT DELETE - Called implicitly from Steam VR
    private void OnDetachedFromHand(Hand hand)
    {
        _grabbingObject = null;
        ForceGrabbed = false;
        ControllerInteraction.ControllerHand handContext = hand.handType.GetHand();

        InteractableObjectUngrabbed?.Invoke(this,
            new ObjectInteraction.InteractableObjectEventArgs {interactingObject = gameObject, Hand = handContext});
    }
    
    private void SetAttachmentFlags()
    {
        if (ProjectData.GameMode != GameMode.Edit)
        {
            _detachable.attachmentFlags = 
                Hand.AttachmentFlags.VelocityMovement | 
                Hand.AttachmentFlags.DetachFromOtherHand | 
                Hand.AttachmentFlags.ParentToHand;
        }
    }

    private IEnumerator SetupAction()
    {
        _triggerInput = gameObject.AddComponent<SteamVRBooleanAction>();
        _triggerInput.inputSource = SteamVR_Input_Sources.Any;
        _triggerInput.booleanAction = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("TriggerUse");
        _triggerInput.enabled = false;

        yield return null;
        
        _triggerInput.enabled = true;
        _triggerInput.onPressDown = new SteamVR_Behaviour_BooleanEvent();
        _triggerInput.onPressUp = new SteamVR_Behaviour_BooleanEvent();
        _triggerInput.onPressDown.AddListener(Use);
        _triggerInput.onPressUp.AddListener(UseStop);
    }
   
    private bool ValidInput(SteamVR_Behaviour_Boolean triggerInput)
    {
        if ((_interactable.hoveringHand == null || !_interactable.isHovering) && _grabbingObject == null)
        {
            return false;
        }
        
        SteamVR_Input_Sources usingHandGameObject = triggerInput.booleanAction.activeDevice;

        if (_grabbingObject != null)
        {
            return (usingHandGameObject == _grabbingObject.handType);
        }
        
        SteamVR_Input_Sources hoveringHandGameObject = _interactable.hoveringHand.handType;

        return (usingHandGameObject == hoveringHandGameObject);
    }
}
