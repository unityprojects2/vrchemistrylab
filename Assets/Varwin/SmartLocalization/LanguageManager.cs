﻿using System;
using UnityEngine;

namespace SmartLocalization
{

    public class LanguageManager : MonoBehaviour
    {
        public delegate void ChangeLanguageEventHandler(LanguageManager thisLanguageManager);
        public ChangeLanguageEventHandler OnChangeLanguage;
        
        public static LanguageManager Instance;

        public string GetTextValue(string key) => key;

        public SmartCultureInfo CurrentlyLoadedCulture;
        public static string DefaultLanguage { get; set; }

        public static void SetDontDestroyOnLoad()
        {
        }

        public void ChangeLanguage(string launchArgumentsLang)
        {
        }

        private void Awake()
        {
            if (Instance)
            {
                DestroyImmediate(this);
            }
            else
            {
                CurrentlyLoadedCulture =  new SmartCultureInfo();
                Instance = this;
            }
        }
    }
}