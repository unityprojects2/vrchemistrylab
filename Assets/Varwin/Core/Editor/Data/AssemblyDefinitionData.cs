using Varwin.Data;

namespace Varwin.Editor
{
    public class AssemblyDefinitionData : IJsonSerializable
    {
        public string name;
        public string[] references;
        public string[] optionalUnityReferences;
        public string[] includePlatforms;
        public string[] excludePlatforms;
        public bool overrideReferences;
        public string[] precompiledReferences;
        public bool allowUnsafeCode;
        public bool autoReferenced;
        public string[] defineConstraints;
    }
}
