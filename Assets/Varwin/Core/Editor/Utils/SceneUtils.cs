using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Varwin.Editor
{
    public static class SceneUtils
    {
        public static IEnumerable<GameObject> GetObjectsOnScene(bool includeInactive = false)
        {
            return (Resources.FindObjectsOfTypeAll(typeof(GameObject)) as GameObject[])?.Where(go => !includeInactive || go.activeSelf);
        }
        
        public static IEnumerable<T> GetObjectsOnScene<T>(bool includeInactive = false) where T : Component
        {
            return GetObjectsOnScene()?.Where(x => x.GetComponent<T>()).Select(x => x.GetComponent<T>());
        }
        
        public static void MoveCameraToEditorView(Camera camera)
        {
            if (camera != null)
            {
                var sceneViewCamera = SceneView.lastActiveSceneView.camera;

                if (sceneViewCamera != null)
                {
                    camera.transform.position = sceneViewCamera.transform.position;
                    camera.transform.rotation = sceneViewCamera.transform.rotation;
                }
            }
        }
    }
}