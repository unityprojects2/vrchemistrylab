﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.SceneManagement;
using TMPro;

namespace Varwin.UI
{
    public class HeadsetTrackingWarning : MonoBehaviour
    {
        [SerializeField] private GameObject _widgetRoot;
        [SerializeField] private TMP_Text _text;

        private const float ShowDelay = 3f;

        private bool _xrDeviceLoaded;
        
        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
            SetWidgetActivity(false);
        }

        private void OnEnable()
        {
            InputTracking.trackingLost += InputTrackingOnTrackingLost;
            InputTracking.trackingAcquired += InputTrackingOnTrackingAcquired;
            
            ProjectData.GameModeChanged += WorldDataGameModeChanged;
            ProjectData.PlatformModeChanged += WorldDataPlatformModeChanged;

            XRDevice.deviceLoaded += deviceName =>
            {
                _xrDeviceLoaded = true;
            };
            
            CheckWidgetActivity();
        }
        
        private void OnDisable()
        {
            InputTracking.trackingLost -= InputTrackingOnTrackingLost;
            InputTracking.trackingAcquired -= InputTrackingOnTrackingAcquired;
            
            ProjectData.GameModeChanged -= WorldDataGameModeChanged;
            ProjectData.PlatformModeChanged -= WorldDataPlatformModeChanged;
        }

        private void Start()
        {
            SceneManager.sceneLoaded += (s, m) => StartCoroutine(CheckWithDelay());
            SceneManager.sceneUnloaded += (s) => StartCoroutine(CheckWithDelay());
        }

        private void InputTrackingOnTrackingLost(XRNodeState obj)
        {
            CheckWidgetActivity();
        }
        
        private void InputTrackingOnTrackingAcquired(XRNodeState obj)
        {
            CheckWidgetActivity();
        }
        
        private void WorldDataGameModeChanged(GameMode gameMode)
        {
            CheckWidgetActivity();
        }
        
        private void WorldDataPlatformModeChanged(PlatformMode platformMode)
        {
            CheckWidgetActivity();
        }

        private void CheckWidgetActivity()
        {
            if (ProjectData.PlatformMode == PlatformMode.Desktop)
            {
                SetWidgetActivity(false);
                return;
            }
            
            if (!_xrDeviceLoaded)
            {
                SetWidgetActivity(true);

                return;
            }
            
            var list = new List<XRNodeState>();
            InputTracking.GetNodeStates(list);

            if (list.Count == 0)
            {
                SetWidgetActivity(false);
                return;
            }
            
            var node = list.FirstOrDefault(x => x.nodeType == XRNode.Head);            
            SetWidgetActivity(!node.tracked);
        }
        
        private IEnumerator CheckWithDelay()
        {
            yield return new WaitForSeconds(ShowDelay);

            CheckWidgetActivity();
            
        }

        private void SetWidgetActivity(bool active)
        {
            _widgetRoot.SetActive(active);

            string errorTextKey = "HEADSET_TRACKING_LOST";
            string errorText = SmartLocalization.LanguageManager.Instance.GetTextValue(errorTextKey);

            _text.text = errorText == errorTextKey ? "" : errorText;
        }
    }
}
