﻿using System.Collections.Generic;
using System.Linq;
using Photon.Pun;
using UnityEngine;
using Varwin.PlatformAdapter;

namespace Varwin.PUN
{
    public class RpcInputControllerMethods : MonoBehaviourPunCallbacks, IPunObservable
    {
        private InputController _inputController;
        private Dictionary<int, Transform> _savedTransforms = new Dictionary<int, Transform>();

        public void EnableMultiplayer(int viewId)
        {
            _inputController.EnableMultiplayer(viewId);
        }
        
        public void Init(InputController baseType)
        {
            _inputController = baseType;
        }

        [PunRPC]
        public void OnUseStartRpc(int handInt)
        {
            ControllerInteraction.ControllerHand hand = (ControllerInteraction.ControllerHand) handInt;
            _inputController.UseStart(hand);
        }

        [PunRPC]
        public void OnUseEndRpc()
        {
            _inputController.UseEnd();
        }
         
        [PunRPC]
        public void OnGrabStartRpc(int handInt, int platformModeInt)
        {
            ControllerInteraction.ControllerHand hand = (ControllerInteraction.ControllerHand) handInt;
            _inputController.GrabStart(hand);
        }
        

        [PunRPC]
        private void StopGrabRpc(string userId, int objectId)
        {
            if (PhotonNetwork.LocalPlayer.UserId != userId)
            {
                return;
            }

            var objectIdComponents = _inputController.ObjectController.RootGameObject.GetComponentsInChildren<ObjectId>();
            List<int> objectIds = objectIdComponents.Select(objectIdComponent => objectIdComponent.Id).ToList();

            if (!objectIds.Contains(objectId))
            {
                return;
            }

            if (!Settings.Instance.Education)
            {
                photonView.RPC("SetKinematicsDefaultsRpc", RpcTarget.Others);
            }

            _inputController.DropGrabbedObject();
        }

        [PunRPC]
        public void OnGrabEndRpc(int platformModeInt)
        {
            _inputController.GrabEnd();
        }

        [PunRPC]
        public void OnTouchStartRpc()
        {
            _inputController.TouchStart();
        }
        
        [PunRPC]
        public void OnTouchEndRpc()
        {
            _inputController.TouchEnd();
        }

        [PunRPC]
        public void SetKinematicsOnRpc()
        {
            _inputController.SetKinematicsOn();
        }

        [PunRPC]
        public void SetKinematicsDefaultsRpc()
        {
            _inputController.SetKinematicsDefaults();
        }
        

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
             
        }
    }

}
