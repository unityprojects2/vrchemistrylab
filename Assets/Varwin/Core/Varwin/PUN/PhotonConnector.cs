using System;
using System.Collections;
using ExitGames.Client.Photon;
using NLog;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using Varwin.Data;
using Varwin.PlatformAdapter;
using Varwin.UI.VRErrorManager;
using Hashtable = ExitGames.Client.Photon.Hashtable;

namespace Varwin.PUN
{
    public class PhotonConnector : MonoBehaviourPunCallbacks
    {
        private bool _isConnecting;
        private bool _inRoom;
		private readonly byte _maxPlayersPerRoom = 16;
		private static PhotonConnector _instance;
		private string _roomName;
		private string RoomName
		{
			get => string.IsNullOrEmpty(_roomName) ? CreateRoomName() : _roomName;
			set => _roomName = value;
		}

		private RequiredProjectArguments _requiredProjectArguments;

		public void SetRoomName(string roomName)
		{
			RoomName = string.IsNullOrEmpty(roomName) ? CreateRoomName() : roomName;
		}
		
		private string CreateRoomName()
		{
#if WAVEVR
			string roomGuid = Guid.NewGuid().ToString();
			return $"room_{roomGuid}";
#else
			string projectGuid = _requiredProjectArguments.Guid;
			int sceneId = _requiredProjectArguments.SceneId;
			return $"room_{projectGuid}_{sceneId}";
#endif
		}
		
		public static PhotonConnector Instance => _instance ? _instance : CreateInstance();

		private static PhotonConnector CreateInstance()
		{
			GameObject go = new GameObject("[PhotonConnector]");
			DontDestroyOnLoad(go);
			_instance = go.AddComponent<PhotonConnector>();

			return _instance;
		}

		public void Disconnect()
		{
			if (PhotonNetwork.IsConnected)
			{
				PhotonNetwork.Disconnect();
			}
		}

		public void Connect(RequiredProjectArguments requiredProjectArguments)
		{
			_isConnecting = true;
			_requiredProjectArguments = requiredProjectArguments;
			PhotonNetwork.OfflineMode = false;

			if (PhotonNetwork.IsConnected)
			{
				if (PhotonNetwork.InRoom)
				{
					LogFeedback($"Leave current room...");
					PhotonNetwork.LeaveRoom(false);
					//client will join room after connect
				}

				else
				{
					LogFeedback($"Joining Room <{RoomName}>...");
					JoinOrCreateRoom();
				}
			}
			else
			{
				LogFeedback("Connecting...");
				PhotonNetwork.PhotonServerSettings.AppSettings.AppIdRealtime = Settings.Instance.PhotonAppId;
				PhotonNetwork.NetworkingClient.AppId = PhotonNetwork.PhotonServerSettings.AppSettings.AppIdRealtime;
				PhotonNetwork.LocalPlayer.NickName = Environment.UserName;
				PhotonNetwork.ConnectUsingSettings();
				PhotonNetwork.GameVersion = VarwinVersionInfo.VersionNumber;
			}
		}

		private void LogFeedback(string message)
		{
			LoaderAdapter.LoaderFeedBackText(message);
			LogManager.GetCurrentClassLogger().Info("<Color=White><b>PUN: " + message + "</b></Color>");
		}
		
		public void SetMasterClientReady()
		{
			if (!Settings.Instance.Multiplayer)
			{
				return;
			}

			var masterLoading = FindObjectOfType<MasterLoading>();

			if (masterLoading == null)
			{
				LogManager.GetCurrentClassLogger().Fatal("Multiplayer fatal error! Master loading not found");
			}

			masterLoading.IsLoading = false;
		}

		public void SetSpectatorClientReady()
		{
			if (!Settings.Instance.Multiplayer || PhotonNetwork.IsMasterClient)
			{
				return;
			}

			var masterLoading = FindObjectOfType<MasterLoading>();

			if (masterLoading == null)
			{
				LogManager.GetCurrentClassLogger().Fatal("Multiplayer fatal error! Master loading not found");
			}

			masterLoading.SetClientReady();
		}
		
		#region MonoBehaviourPunCallbacks CallBacks

		public static void RegisterCustomTypes()
		{
			PhotonPeer.RegisterType(typeof(ErrorValue), 1, ErrorValue.Serialize, ErrorValue.Deserialize);
			PhotonPeer.RegisterType(typeof(Color), 2, ColorEx.Serialize, ColorEx.Deserialize);
		}
		
		public override void OnConnectedToMaster()
		{
			if (!_isConnecting)
			{
				return;
			}

			RegisterCustomTypes();
			LogFeedback("Connected to multiplayer server...");
			JoinOrCreateRoom();
		}

		public override void OnJoinRandomFailed(short returnCode, string message)
		{
			LogFeedback("Can't create room. Message = " + message);
		}

		public override void OnDisconnected(DisconnectCause cause)
		{
			LogFeedback("Disconnected from, multiplayer server " + cause);
			_isConnecting = false;
			LoaderAdapter.IsConnectedToMultiplayer = false;
		}

		public override void OnJoinedRoom()
		{
			LogFeedback($"Joined to room <{PhotonNetwork.CurrentRoom.Name}> successful with " + PhotonNetwork.CurrentRoom.PlayerCount + " Player(s)");
			if (PhotonNetwork.IsMasterClient)
			{
				Settings.Instance.Spectator = false;
				
				var go = PhotonNetwork.Instantiate("MasterLoading", Vector3.zero, Quaternion.identity);
				var masterLoading = go.GetComponent<MasterLoading>();
				masterLoading.IsLoading = true;
			}
			else if (Settings.Instance.Education)
			{
				Settings.Instance.Spectator = true;
			}
		}

		public override void OnMasterClientSwitched(Player newMasterClient)
		{
			if (Settings.Instance.Education || Settings.Instance.Spectator)
			{
				int timeLeft = 5;
				if (VRErrorManager.Instance)
				{
					VRErrorManager.Instance.Show(
						$"Master Client has left. You will be disconnected in {timeLeft} seconds.");
				}

				StartCoroutine(CountdownAndDisconnect(timeLeft));
			}
		}

		private IEnumerator CountdownAndDisconnect(int secondsLeft)
		{
			while (secondsLeft > 0)
			{
				yield return new WaitForSeconds(1);

				secondsLeft--;
				
				if (VRErrorManager.Instance)
				{
					VRErrorManager.Instance.UpdateMessage(
						$"Master Client has left. You will be disconnected in {secondsLeft} seconds.");
				}
			}

			ExitToLauncherWindow();
		}

		private void ExitToLauncherWindow()
		{
			PhotonNetwork.Disconnect();
			GameStateData.ClearObjects();
			GameStateData.ClearLogic();

			ProjectData.ProjectStructure = null;

			try
			{
				ProjectData.UpdateSubscribes(new RequiredProjectArguments() {GameMode = GameMode.View, PlatformMode = PlatformMode.Vr});
			}
			catch
			{
			}
			finally
			{
				ProjectDataListener.Instance.LoadScene("MobileLauncher");
				InputAdapter.Instance.PlayerController.Nodes.Rig.Transform.position = Vector3.zero;
				InputAdapter.Instance.PointerController.IsMenuOpened = false;
			}
		}

		public override void OnPlayerEnteredRoom(Player newPlayer)
		{
			LogFeedback($"{newPlayer.NickName} joined!");
		}

		public override void OnPlayerLeftRoom(Player otherPlayer)
		{
			LogFeedback($"{otherPlayer.NickName} left the room!");
		}

		public override void OnCreatedRoom()
		{
			LogFeedback($"Room <{RoomName}> has been created!");
		}

		private void JoinOrCreateRoom()
		{
			PhotonNetwork.JoinOrCreateRoom(RoomName,
				new RoomOptions
				{
					MaxPlayers = _maxPlayersPerRoom,
					CleanupCacheOnLeave = false,
					PublishUserId = true,
					CustomRoomProperties = new Hashtable
					{
						{"ip", Settings.Instance.RemoteAddr},
						{"guid", ProjectData.ProjectStructure.Guid}
					},
					CustomRoomPropertiesForLobby = new string[] {"ip", "guid"}
				},
				null);

		}
		#endregion
    }
}