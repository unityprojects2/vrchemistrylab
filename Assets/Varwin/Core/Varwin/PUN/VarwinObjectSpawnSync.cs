using System.Collections;
using Photon.Pun;
using UnityEngine;
using Varwin.Data;

namespace Varwin.PUN
{
    /// <summary>
    /// Spawns object in Multiplayer mode
    /// </summary>
    public class VarwinObjectSpawnSync : MonoBehaviourPunCallbacks, IPunObservable
    {
        [SerializeField]
        private float SyncTransformsInterval = 0.5f;
        
        private float _sync;
        private ObjectController _objectController;
        private VarwinObjectInputSync _varwinObjectInputSync;
        private VarwinObjectTypeSync _varwinObjectTypeSync;
        private SpawnInitParams _params;

        public void Init(SpawnInitParams param)
        {
            _params = param;
        }

        private void Start()
        {
            DontDestroyOnLoad(gameObject);
            _sync = SyncTransformsInterval;
            _varwinObjectInputSync = GetComponent<VarwinObjectInputSync>();
            _varwinObjectTypeSync = GetComponent<VarwinObjectTypeSync>();
            
            if (!PhotonNetwork.IsMasterClient)
            {
                ProjectData.SceneLoaded += SpawnEntity;
            }
            else
            {
                SpawnEntity();
            }
            
            ProjectData.ObjectSpawned += ProjectDataOnObjectSpawned;
        }

        private void ProjectDataOnObjectSpawned(ObjectController objectController)
        {
            if (_params == null)
            {
                return;
            }
            
            if (objectController.Id != _params.IdInstance)
            {
                if (_params.IdInstance == 0)
                {
                    _params = objectController.GetSpawnInitParams();
                }
                else
                {
                    return;
                }
            }

            _objectController = objectController;
            _varwinObjectTypeSync.Init(_objectController);
            _varwinObjectInputSync.Init(_objectController);
            ProjectData.ObjectSpawned -= ProjectDataOnObjectSpawned;
        }

        private void SpawnEntity()
        {
            ProjectData.SceneLoaded -= SpawnEntity;
            
            if (!this)
            {
                return;
            }
            
            StartCoroutine(SpawnEntityCoroutine());
        }

        private IEnumerator SpawnEntityCoroutine()
        {
            while (_params == null)
            {
                yield return null;
            }
            
            Spawner.Instance.CreateSpawnEntity(_params);
        }

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                stream.SendNext(_params.ToJson());
            }
            else
            {
                _params = ((string) stream.ReceiveNext()).JsonDeserialize<SpawnInitParams>();
            }
        }

        private void Update()
        {
            if (!PhotonNetwork.IsMasterClient || !_objectController || _objectController.Collaboration == Collaboration.SinglePlayer)
            {
                return;
            }

            if (_sync > 0)
            {
                _sync -= Time.deltaTime;
                return;
            }

            // update transforms and sync via OnPhotonSerializeView - LOADS NETWORK
            // BETTER: send all params at once on OnPlayerEnteredRoom (to a particular player)
            _params.Transforms = _objectController.GetTransforms();
            _sync = SyncTransformsInterval;
        }

        public override void OnLeftRoom()
        {
            ProjectData.SceneLoaded -= SpawnEntity;
            ProjectData.ObjectSpawned -= ProjectDataOnObjectSpawned;
            StopAllCoroutines();
            Destroy(gameObject);
        }
    }
}