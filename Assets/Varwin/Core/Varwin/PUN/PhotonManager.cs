﻿using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using GameObject = UnityEngine.GameObject;

namespace Varwin.PUN
{
	// TO BE DELETED
	public class PhotonManager : MonoBehaviourPunCallbacks
	{
		public static PhotonManager Instance;
		public GameObject Spawner;

		[Header("Player prefabs")] public GameObject Head;
		public GameObject LeftHand;
		public GameObject RightHand;
		public GameObject Body;

		[Header("Steam VR Controllers")] public Transform HeadController;
		public Transform LeftHandController;
		public Transform RightHandController;
		private GameObject _instance;

		private void Start()
		{
			Instance = this;

			if (!Settings.Instance.Multiplayer)
			{
				return;
			}

			if (!PhotonNetwork.InRoom)
			{
				Debug.Log("Auto clean up player objects turned off");
			}
		}

		private void Update()
		{

		}

		void ValidateOnLine()
		{
			if (!Settings.Instance.Multiplayer)
			{
				return;
			}
		}

		public override void OnPlayerEnteredRoom(Player other)
		{
			Debug.Log("OnPhotonPlayerConnected() " + other.NickName);

			if (PhotonNetwork.IsMasterClient)
			{
				Debug.Log("OnPhotonPlayerConnected isMasterClient " + PhotonNetwork.IsMasterClient);

			}
		}

		public override void OnMasterClientSwitched(Player newMasterClient)
		{
			ValidateOnLine();
		}

		public override void OnPlayerLeftRoom(Player other)
		{
			Debug.Log("OnPhotonPlayerDisconnected() " + other.NickName); // seen when other disconnects
			ValidateOnLine();

			if (PhotonNetwork.IsMasterClient)
			{
				Debug.Log("OnPhotonPlayerConnected isMasterClient " + PhotonNetwork.IsMasterClient);
			}

		}

		public override void OnLeftRoom()
		{

		}


		public void LeaveRoom()
		{
			PhotonNetwork.LeaveRoom();
		}

		public void QuitApplication()
		{
			Application.Quit();
		}
	}

}
