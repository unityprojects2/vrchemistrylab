﻿using System.Collections;
using System.Collections.Generic;
using NLog;
using Photon.Pun;
using Photon.Realtime;
using Smooth;
using UnityEngine;
using Varwin.Data;
using Varwin.Public;
using Varwin.PlatformAdapter;


namespace Varwin.PUN
{
    public class PlayerNodesSync : MonoBehaviourPunCallbacks, IPunObservable
    {
        [Header("Player Nodes")] public Transform Head;
        public Transform LeftHand;
        public Transform RightHand;


        [Header("Player object")] public VarwinObject playerObject;
        [SerializeField] private bool isMe;
        [SerializeField] private string ownerId;
        [SerializeField] private int playerId;

        public PlatformMode playerPlatformMode;

        private TouchedObject leftHandTouchedObject;
        private TouchedObject rightHandTouchedObject;
        private RpcPlayerManager rpcPlayerManager;

        private SmoothSyncPUN2[] _nodesSync;

        public static readonly Dictionary<string, PlayerNodesSync> PlayerNodes =
            new Dictionary<string, PlayerNodesSync>();


        private void Awake()
        {
            _nodesSync = GetComponentsInChildren<SmoothSyncPUN2>();
        }

        private void Start()
        {
            DontDestroyOnLoad(gameObject);
            isMe = photonView.IsMine;

            leftHandTouchedObject = LeftHand.GetComponent<TouchedObject>();
            rightHandTouchedObject = RightHand.GetComponent<TouchedObject>();

            if (isMe)
            {
                playerPlatformMode = ProjectData.PlatformMode;
            }

            StartCoroutine(WaitingForOwner());

        }

        private IEnumerator WaitingForOwner()
        {
            while (string.IsNullOrEmpty(ownerId))
            {
                if (photonView.Owner != null)
                {
                    ownerId = photonView.Owner.UserId;
                }

                yield return null;
            }

            if (!PlayerNodes.ContainsKey(ownerId))
            {
                PlayerNodes.Add(ownerId, this);
            }

            gameObject.name = $"[{gameObject.name.Replace("(Clone)", "")} {photonView.Owner.NickName}]";


            if (!isMe)
            {
                while (playerId == 0)
                {
                    yield return null;
                }

                StartCoroutine(GetPlayerByIdObject(playerId));
            }
            else
            {
                if (PhotonNetwork.IsMasterClient)
                {
                    SubscribeOrFindPlayerObject();
                    MasterLoading.SpectatorJoined += OnSpectatorJoined;
                }
                else
                {
                    CreatePlayerObject();
                }
            }
        }

        private void SubscribeOrFindPlayerObject()
        {
            List<ObjectController> objects = GameStateData.GetObjectsInScene();

            if (objects.Count != 0)
            {
                ObjectController player = objects.Find(c => c.IsPlayerObject);

                if (player == null)
                {
                    return;
                }

                GetPlayerObject(player);
            }
            else
            {
                ProjectData.ObjectSpawned += GetPlayerObject;
            }
        }

        private void OnSpectatorJoined()
        {
            if (!PhotonNetwork.IsMasterClient)
            {
                return;
            }

            foreach (var smoothSyncPun2 in _nodesSync)
            {
                smoothSyncPun2.teleportOwnedObjectFromOwner();
                smoothSyncPun2.forceStateSendNextOnPhotonSerializeView();
            }
        }

        private void CreatePlayerObject()
        {
            StartCoroutine(CreatePlayerObjectCoroutine());
        }

        private IEnumerator CreatePlayerObjectCoroutine()
        {
            while (GameStateData.PlayerObject == null)
            {
                yield return null;
            }

            photonView.RPC(nameof(SpawnPlayer), RpcTarget.MasterClient);
            photonView.RPC(nameof(SubscribeObjectSpawn), RpcTarget.All);
        }

        [PunRPC]
        public void SubscribeObjectSpawn()
        {
            ProjectData.ObjectSpawned += GetPlayerObject;
        }

        [PunRPC]
        public void SpawnPlayer()
        {
            PrefabObject prefabObject = GameStateData.PlayerObject;
            SpawnInitParams spawnInitParams = new SpawnInitParams
            {
                Embedded = prefabObject.Embedded,
                IdObject = prefabObject.Id,
                IdInstance = 0,
                IdScene = ProjectData.SceneId
            };

            Spawner.Instance.SpawnAsset(spawnInitParams);
        }

        private void GetPlayerObject(ObjectController objectController)
        {
            if (GameStateData.PlayerObject == null)
            {
                return;
            }

            if (objectController.IdObject != GameStateData.PlayerObject.Id)
            {
                return;
            }

            StartCoroutine(GetPlayerByIdObject(objectController.Id));

            ProjectData.ObjectSpawned -= GetPlayerObject;
        }

        private IEnumerator GetPlayerByIdObject(int id)
        {
            WrappersCollection wrappersCollection = GameStateData.GetWrapperCollection();

            while (!wrappersCollection.ContainsKey(id))
            {
                yield return null;
            }

            Wrapper player = wrappersCollection.Get(id);

            GameObject playerGo = player.GetGameObject();

            while (!playerGo)
            {
                playerGo = player.GetGameObject();

                yield return null;
            }

            IPlayerObject component = playerGo.GetComponent<IPlayerObject>();

            playerId = id;

            Player owner = photonView.Owner;

            component.SetNodes(
                new PlayerNodes
                {
                    Head = Head,
                    RightHand = RightHand,
                    LeftHand = LeftHand
                }
            );

            component.SetPlayerInfo(
                new PlayerInfo
                {
                    NickName = owner.NickName,
                    UserId = owner.UserId
                });

            playerObject = player.GetGameObject().GetComponent<VarwinObject>();

            rpcPlayerManager = playerObject.GetComponent<RpcPlayerManager>();

            if (!string.IsNullOrEmpty(rpcPlayerManager.UserId))
            {
                yield break;
            }

            rpcPlayerManager.UserId = owner.UserId;
        }

        private void Update()
        {
            if (!photonView.IsMine)
            {
                return;
            }

            if (InputAdapter.Instance == null || !InputAdapter.Instance.PlayerController.Nodes.Rig.Transform)
            {
                return;
            }

            Head.position = InputAdapter.Instance.PlayerController.Nodes.Head.Transform.position;
            Head.rotation = InputAdapter.Instance.PlayerController.Nodes.Head.Transform.rotation;
            LeftHand.position = InputAdapter.Instance.PlayerController.Nodes.LeftHand.Transform.position;
            LeftHand.rotation = InputAdapter.Instance.PlayerController.Nodes.LeftHand.Transform.rotation;
            RightHand.position = InputAdapter.Instance.PlayerController.Nodes.RightHand.Transform.position;
            RightHand.rotation = InputAdapter.Instance.PlayerController.Nodes.RightHand.Transform.rotation;
        }

        public override void OnPlayerLeftRoom(Player player)
        {
            if (isMe)
            {
                return;
            }

            Destroy(rpcPlayerManager);
            PhotonNetwork.Destroy(gameObject);
            if (PlayerNodes.ContainsKey(ownerId))
            {
                PlayerNodes.Remove(ownerId);
            }

            GameStateData.GetObjectControllerInSceneById(playerId).Delete();
        }

        public override void OnLeftRoom()
        {
            PlayerNodes.Clear();
            Destroy(gameObject);
        }

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                stream.SendNext(gameObject.name);
                stream.SendNext(playerId);
                stream.SendNext((int) playerPlatformMode);
            }
            else
            {
                gameObject.name = (string) stream.ReceiveNext();
                playerId = (int) stream.ReceiveNext();
                playerPlatformMode = (PlatformMode) (int) stream.ReceiveNext();
            }
        }

        public static GameObject GetUsingObject(GameObject go)
        {
            Debug.Log("PUN: GetUsingObject " + go.name, go);
            PhotonView photonView = go.GetComponentInParent<PhotonView>();
            RpcInputControllerMethods inputControllerMethods = go.GetComponent<RpcInputControllerMethods>();

            if (!photonView)
            {
                LogManager.GetCurrentClassLogger().Error("Can't get photonView in " + go.name, go);
                return null;
            }

            if (photonView.Owner == null)
            {
                LogManager.GetCurrentClassLogger().Error("Can't get owner of photonView in " + go.name, go);
                return null;
            }

            string userId = photonView.Owner.UserId;

            if (!PlayerNodes.ContainsKey(userId))
            {
                LogManager.GetCurrentClassLogger().Error("Can't get player node with " + userId);
                return null;
            }

            PlayerNodesSync playerNodesSync = PlayerNodes[userId];
            GameObject usingObject = null;

            switch (ProjectData.PlatformMode)
            {
                case PlatformMode.Vr:
                {
                    if (playerNodesSync.rightHandTouchedObject.TouchedController &&
                        playerNodesSync.rightHandTouchedObject.TouchedController == inputControllerMethods)
                    {
                        usingObject = playerNodesSync.RightHand.gameObject;
                    }

                    if (playerNodesSync.leftHandTouchedObject.TouchedController &&
                        playerNodesSync.leftHandTouchedObject.TouchedController == inputControllerMethods)
                    {
                        usingObject = playerNodesSync.LeftHand.gameObject;
                    }

                    break;
                }
                case PlatformMode.Desktop:
                {
                    usingObject = playerNodesSync.RightHand.gameObject;
                    break;
                }
            }

            if (usingObject)
            {
                Debug.Log("PUN: Return using object: " + usingObject.name, usingObject);
            }
            else
            {
                Debug.Log("PUN: Return null using object");
            }

            return usingObject;
        }
    }
}
