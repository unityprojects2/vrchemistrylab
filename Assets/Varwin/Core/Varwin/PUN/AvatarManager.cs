﻿using System.Collections;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using Varwin;
using Varwin.PlatformAdapter;

namespace Varwin.PUN
{
    public class AvatarManager : MonoBehaviourPunCallbacks, IPunObservable
    {
        private AvatarDescriptor _avatarDescriptor;
        private PlayerNodesSync _playerNodesSync;

        private Transform _leftHandOnStart;
        private Transform _rightHandOnStart;

        private PlatformMode _previousPlatformMode = PlatformMode.Undefined;

        private bool _loaded;
        private bool _isMe;

        private void Start()
        {
            _isMe = photonView.IsMine;
            DontDestroyOnLoad(gameObject);
            StartCoroutine(FindComponents());
        }

        private void SetRender(GameObject go, bool isMe)
        {
            if (!isMe)
            {
                return;
            }

            var renders = go.GetComponentsInChildren<Renderer>();

            foreach (Renderer render in renders)
            {
                render.enabled = false;
            }
        }

        private IEnumerator FindComponents()
        {
            yield return null;

            while (InputAdapter.Instance == null || !InputAdapter.Instance.PlayerController.Nodes.Rig.Transform)
            {
                yield return null;
            }

            while (!_playerNodesSync)
            {
                _playerNodesSync = Helper.FindMyComponent<PlayerNodesSync>(photonView);

                yield return null;
            }

            while (!_avatarDescriptor)
            {
                _avatarDescriptor = Helper.FindMyComponent<AvatarDescriptor>(photonView);

                yield return null;
            }

            SetRender(_avatarDescriptor.gameObject, photonView.IsMine);

            _leftHandOnStart = _avatarDescriptor.LeftHand;
            _rightHandOnStart = _avatarDescriptor.RightHand;
            _avatarDescriptor.Head.transform.SetParent(_playerNodesSync.Head, false);

            UpdateParentNodes();
            _loaded = true;
        }

        private void Update()
        {
            if (!_loaded || _previousPlatformMode == _playerNodesSync.playerPlatformMode)
            {
                return;
            }

            _previousPlatformMode = _playerNodesSync.playerPlatformMode;
            UpdateParentNodes();
        }

        private void UpdateParentNodes()
        {
            if (_playerNodesSync.playerPlatformMode == PlatformMode.Vr)
            {

                _avatarDescriptor.transform.SetParent(_playerNodesSync.RightHand, false);
                _avatarDescriptor.LeftHand.transform.SetParent(_playerNodesSync.LeftHand, false);
                _avatarDescriptor.RightHand.transform.SetParent(_playerNodesSync.RightHand, false);
            }
            else
            {
                _avatarDescriptor.RightHand.transform.SetParent(_avatarDescriptor.Head, false);
                _avatarDescriptor.LeftHand.transform.SetParent(_avatarDescriptor.Head, false);

                _avatarDescriptor.RightHand.transform.localPosition = _rightHandOnStart.localPosition;
                _avatarDescriptor.LeftHand.transform.localPosition = _leftHandOnStart.localPosition;
                _avatarDescriptor.RightHand.transform.localRotation = _rightHandOnStart.localRotation;
                _avatarDescriptor.LeftHand.transform.localRotation = _leftHandOnStart.localRotation;
            }
        }


        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {

        }

        public override void OnPlayerLeftRoom(Player player)
        {
            if (!_isMe)
            {
                PhotonNetwork.Destroy(gameObject);
            }
        }

        public override void OnLeftRoom()
        {
            StopAllCoroutines();

            if (_avatarDescriptor)
            {
                Destroy(_avatarDescriptor.gameObject);
            }

            Destroy(gameObject);
        }
    }
}
