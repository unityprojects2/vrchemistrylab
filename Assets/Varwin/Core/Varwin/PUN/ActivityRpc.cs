﻿using Photon.Pun;

namespace Varwin.PUN
{
    public class ActivityRpc : MonoBehaviourPunCallbacks, IPunObservable
    {
        private ObjectController _objectController;

        public void Init(ObjectController objectController)
        {
            _objectController = objectController;
            MasterLoading.SpectatorJoined += ResendRpcCalls;
        }

        protected virtual void ResendRpcCalls()
        {
            if (!ProjectData.IsMultiplayerAndMasterClient)
            {
                return;
            }
            
            photonView.RPC(nameof(SetActive), RpcTarget.Others, _objectController.ActiveSelf);
        }
        
        [PunRPC]
        public void SetActive(bool isActive)
        {
            _objectController.SetActive(isActive);
        }

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            
        }
    }
}
