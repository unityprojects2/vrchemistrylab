using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Photon.Pun;

namespace Varwin.PUN
{
    public class VarwinObjectInputSync : MonoBehaviourPunCallbacks, IPunObservable
    {
        private ObjectController _objectController;
        private List<int> _viewIds = new List<int>();
        private int _previousViewIdsCount;
        
        public void Init(ObjectController objectController)
        {
            _objectController = objectController;

            if (PhotonNetwork.IsMasterClient)
            {
                AllocateViewIds();
            }
        }

        private void AllocateViewIds()
        {
            var inputs = _objectController.gameObject.GetComponentsInChildren<RpcInputControllerMethods>();

            foreach (var input in inputs)
            {
                var inputPhotonView = input.GetComponent<PhotonView>();

                if (!inputPhotonView)
                {
                    return;
                }
                
                int newPhotonId = inputPhotonView.ViewID == 0 ? PhotonNetwork.AllocateViewID(false) : inputPhotonView.ViewID;
                _viewIds.Add(newPhotonId);
            }
            
            UpdateMultiplayerInputs();
        }
 
        // пиздец не очевидно - переделать на OnPlayerEnteredRoom + RPC
        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                stream.SendNext(_viewIds.ToArray());
            }
            else
            {
                object value = stream.ReceiveNext();
                //Heck. Some times bool here.. wtf???
                if (value is int[] viewIds)
                {
                    _viewIds = viewIds.ToList();
                }
            }

            if (_previousViewIdsCount == _viewIds.Count)
            {
                return;
            }

            // выполняется на late joined clients
            _previousViewIdsCount = _viewIds.Count;
            
            StartCoroutine(WaitAndUpdateMultiplayerInputs());
        }

        private IEnumerator WaitAndUpdateMultiplayerInputs()
        {
            while (!_objectController)
            {
                yield return null;
            }
            
            UpdateMultiplayerInputs();
        }

        private void UpdateMultiplayerInputs()
        {
            var inputs = _objectController.gameObject.GetComponentsInChildren<RpcInputControllerMethods>();

            for (int i = 0; i < inputs.Length; i++)
            {
                inputs[i].EnableMultiplayer(_viewIds[i]);
            }
        }
    }
}