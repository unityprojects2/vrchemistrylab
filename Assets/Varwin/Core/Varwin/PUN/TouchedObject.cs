﻿using UnityEngine;

namespace Varwin.PUN
{
    public class TouchedObject : MonoBehaviour
    {
        public RpcInputControllerMethods TouchedController;

        private void OnTriggerEnter(Collider other)
        {
            RpcInputControllerMethods candidate = other.gameObject.GetComponent<RpcInputControllerMethods>();

            if (candidate == null)
            {
                return;
            }

            TouchedController = candidate;
        }

        private void OnTriggerExit(Collider other)
        {
            RpcInputControllerMethods candidate = other.gameObject.GetComponent<RpcInputControllerMethods>();

            if (candidate == null)
            {
                return;
            }

            TouchedController = null;
        }
    }
}