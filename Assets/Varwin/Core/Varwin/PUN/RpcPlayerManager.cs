using System;
using Photon.Pun;
using UnityEngine;
using Varwin.Data;
using Varwin.Models.Data;

namespace Varwin
{
    public class RpcPlayerManager : MonoBehaviourPunCallbacks
    {
        public string UserId;

        public float FallingTime
        {
            get => PlayerManager.FallingTime;
            set => PlayerManager.FallingTime = value;
        }

        public IPlayerController CurrentRig => PlayerManager.CurrentRig;

        public double RespawnHeight => PlayerManager.RespawnHeight;

        public void LoadScene(string sid)
        {
            if (Settings.Instance.Multiplayer)
            {
                photonView.RPC("LoadSceneRpc", RpcTarget.All, UserId, sid);
            }
            else
            {
                Scene.Load(sid);
            }
        }
        
        [PunRPC]
        private void LoadSceneRpc(string userId, string sid)
        {
            if (PhotonNetwork.LocalPlayer.UserId != userId)
            {
                return;
            }

            Scene.Load(sid);
        }
        
        public void LoadConfiguration(string sid)
        {
            if (Settings.Instance.Multiplayer)
            {
                photonView.RPC("LoadConfigurationRpc", RpcTarget.All, UserId, sid);
            }
            else
            {
                Configuration.Load(sid);
            }
        }
        
        [PunRPC]
        private void LoadConfigurationRpc(string userId, string sid)
        {
            if (PhotonNetwork.LocalPlayer.UserId != userId)
            {
                return;
            }

            Configuration.Load(sid);
        }

        public void Respawn()
        {
            if (Settings.Instance.Multiplayer)
            {
                photonView.RPC("RespawnRpc", RpcTarget.All, UserId);
            }
            else
            {
                PlayerManager.Respawn();
            }
        }

        [PunRPC]
        private void RespawnRpc(string userId)
        {
            if (PhotonNetwork.LocalPlayer.UserId != userId)
            {
                return;
            }

            PlayerManager.Respawn();
        }

        public void SetSpawnPoint(Transform targetTransform)
        {
            if (Settings.Instance.Multiplayer)
            {
                int instanceId = GetTransformId(targetTransform);
                photonView.RPC("SetSpawnPointRpc", RpcTarget.All, instanceId, UserId);
            }

            else
            {
                PlayerAnchorManager.SpawnPoint = targetTransform;
            }
        }

        private int GetTransformId(Transform targetTransform)
        {
            ObjectBehaviourWrapper objectBehaviourWrapper = targetTransform.gameObject.GetComponent<ObjectBehaviourWrapper>();

            if (!objectBehaviourWrapper)
            {
                throw new Exception("Transforms without wrapper not allowed");
            }

            return objectBehaviourWrapper.OwdObjectController.Id;
        }

        [PunRPC]
        private void SetSpawnPointRpc(int instanceId, string userId)
        {
            if (PhotonNetwork.LocalPlayer.UserId != userId)
            {
                return;
            }

            PlayerAnchorManager.SpawnPoint = FindTransform(instanceId);
        }

        private Transform FindTransform(int instanceId)
        {
            WrappersCollection wrappersCollection = GameStateData.GetWrapperCollection();

            if (!wrappersCollection.ContainsKey(instanceId))
            {
                throw new Exception("Transform not found!");
            }

            return wrappersCollection.Get(instanceId).GetGameObject().transform;
        }

        public void Rotate(float angle)
        {
            if (Settings.Instance.Multiplayer)
            {
                photonView.RPC("RotateRpc", RpcTarget.All, angle, UserId);
            }
            else
            {
                PlayerManager.CurrentRig.Rotation = Quaternion.Euler(PlayerManager.CurrentRig.Rotation.eulerAngles + angle * Vector3.up);
            }
        }

        [PunRPC]
        private void RotateRpc(float angle, string userId)
        {
            if (PhotonNetwork.LocalPlayer.UserId != userId)
            {
                return;
            }

            PlayerManager.CurrentRig.Rotation = Quaternion.Euler(PlayerManager.CurrentRig.Rotation.eulerAngles + angle * Vector3.up);
        }

        public void RotateTo(Transform targetTransform)
        {
            if (Settings.Instance.Multiplayer)
            {
                int id = GetTransformId(targetTransform);
                photonView.RPC("RotateToRpc", RpcTarget.All, id, UserId);
            }
            else
            {
                RotateToTransform(targetTransform);
            }
        }

        [PunRPC]
        private void RotateToRpc(int id, string userId)
        {
            if (PhotonNetwork.LocalPlayer.UserId != userId)
            {
                return;
            }

            RotateToTransform(FindTransform(id));
        }

        private void RotateToTransform(Transform targetTransform)
        {
            Vector3 lookPos = targetTransform.position - PlayerManager.CurrentRig.Position;
            lookPos.y = 0;
            PlayerManager.CurrentRig.Rotation = Quaternion.LookRotation(lookPos);
        }

        public void SetRotation(Vector3 targetAngle)
        {
            if (Settings.Instance.Multiplayer)
            {
                string vectorJson = targetAngle.ToVector3Dt().ToJson();
                photonView.RPC("SetRotationRpc", RpcTarget.All, vectorJson, UserId);
            }
            else
            {
                PlayerManager.CurrentRig.Rotation = Quaternion.Euler(targetAngle);
            }
        }

        [PunRPC]
        private void SetRotationRpc(string targetAngle, string userId)
        {
            if (PhotonNetwork.LocalPlayer.UserId != userId)
            {
                return;
            }

            Vector3 vector3 = targetAngle.JsonDeserialize<Vector3DT>().ToUnityVector();
            PlayerManager.CurrentRig.Rotation = Quaternion.Euler(vector3);
        }

        public bool UseGravity
        {
            get => PlayerManager.UseGravity;
            set => SetPlayerGravity(value);
        }

        private void SetPlayerGravity(bool value)
        {
            if (Settings.Instance.Multiplayer)
            {
                photonView.RPC("SetPlayerGravityRpc", RpcTarget.All, value, UserId);
            }
            else
            {
                PlayerManager.UseGravity = value;
            }
        }

        [PunRPC]
        private void SetPlayerGravityRpc(bool value, string userId)
        {
            if (PhotonNetwork.LocalPlayer.UserId != userId)
            {
                return;
            }

            PlayerManager.UseGravity = value;
        }

        public void TeleportTo(Vector3 position)
        {
            if (Settings.Instance.Multiplayer)
            {
                string vector3Json = position.ToVector3Dt().ToJson();
                photonView.RPC("TeleportToRpc", RpcTarget.All, vector3Json, UserId);
            }
            else
            {
                PlayerManager.TeleportTo(position);
            }
        }

        private void TeleportToRpc(string position, string userId)
        {
            if (PhotonNetwork.LocalPlayer.UserId != userId)
            {
                return;
            }

            Vector3 vector3 = position.JsonDeserialize<Vector3DT>().ToUnityVector();
            PlayerManager.TeleportTo(vector3);
        }
    }
}