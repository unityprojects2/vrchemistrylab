﻿using System.Text;
using UnityEngine;

namespace Varwin.PUN
{
    public static class ColorEx
    {
        public static byte[] Serialize(object customType)
        {
            Color value = (Color)customType;
            string htmlStringRgba = ColorUtility.ToHtmlStringRGBA(value);
            return Encoding.UTF8.GetBytes(htmlStringRgba);
        }
        
        public static object Deserialize(byte[] data)
        {
            string htmlStringRgba = Encoding.UTF8.GetString(data);
            ColorUtility.TryParseHtmlString(string.Concat("#", htmlStringRgba), out Color result);
            return result;
        } 
        
    }

    public class ErrorValue
    {
        public string Message;
         
        public static byte[] Serialize(object customType)
        {
            ErrorValue c = (ErrorValue)customType;
            return Encoding.UTF8.GetBytes(c.Message);
        }
        
        public static object Deserialize(byte[] data)
        {
            string message = Encoding.UTF8.GetString(data);
            ErrorValue result = new ErrorValue {Message = message};
            return result;
        }
    }
}
