﻿using UnityEngine;

namespace Varwin.PUN
{
   public class AvatarDescriptor : MonoBehaviour
   {
      public Transform Head;
      public Transform LeftHand;
      public Transform RightHand;
   }
}