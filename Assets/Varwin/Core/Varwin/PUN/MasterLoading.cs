using System;
using Photon.Pun;
using Photon.Realtime;

namespace Varwin.PUN
{
    public class MasterLoading : MonoBehaviourPunCallbacks, IPunObservable
    {
        public static event Action SpectatorJoined;
        
        // true on master client joined room
        // false on scene logic updated (since it is the last step of scene loading)
        public bool IsLoading;
        
        private string _roomName;
        
        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
            _roomName = PhotonNetwork.CurrentRoom.Name;
            gameObject.name = "[Master loading...]";
        }

        public void SetClientReady()
        {
            if (PhotonNetwork.IsMasterClient)
            {
                return;
            }

            photonView.RPC(nameof(SpectatorReady), RpcTarget.MasterClient);
        }

        [PunRPC]
        public void SpectatorReady()
        {
            if (!PhotonNetwork.IsMasterClient)
            {
                return;
            }
			
            SpectatorJoined?.Invoke();
        }
        
        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                stream.SendNext(IsLoading);
            }

            else
            {
                IsLoading = (bool) stream.ReceiveNext();
            }
        }

        public override void OnJoinedRoom()
        {
            if (_roomName != PhotonNetwork.CurrentRoom.Name)
            {
                Destroy(gameObject);
            }
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            Destroy(gameObject);
        }
    }
}