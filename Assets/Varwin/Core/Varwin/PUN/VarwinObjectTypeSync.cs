using System;
using System.Collections.Generic;
using System.Reflection;
using Photon.Pun;
using UnityEngine;

namespace Varwin.PUN
{
    public class VarwinObjectTypeSync : MonoBehaviourPunCallbacks, IPunObservable
    {
        [SerializeField]
        private float _syncTransformsInterval = 0.2f;
        private float _sync;
        
        private ObjectController _objectController;
        private Wrapper _wrapper;
        private bool _isReadySync;
        private bool _hasObservedProperties;

        private readonly List<PropertyInfo> _observedProperties = new List<PropertyInfo>();
        private readonly Dictionary<string, PropertyInfo> _observedPropertiesNames = new Dictionary<string, PropertyInfo>();
        private readonly Dictionary<PropertyInfo, object> _oldValueProperties = new Dictionary<PropertyInfo, object>();
        private readonly Dictionary<PropertyInfo, object> _valueProperties = new Dictionary<PropertyInfo, object>();
        private readonly Dictionary<PropertyInfo, PropertyWrapper> _propertyWrappers = new Dictionary<PropertyInfo, PropertyWrapper>();
        private readonly HashSet<string> _ignoredPropertyNames = new HashSet<string>()
        {
            "IsObjectEnabled",
            "IsObjectActive",
            "GrabEnabled",
            "UseEnabled",
            "TouchEnabled"
        };

        public void Init(ObjectController objectController)
        {
            _objectController = objectController;
            _wrapper = objectController.WrappersCollection.Get(objectController.Id);
            _sync = _syncTransformsInterval;

            FindObservedItems();

            if (_hasObservedProperties)
            {
                MasterLoading.SpectatorJoined += OnSpectatorJoined;
            }
        }

        private void OnDestroy()
        {
            MasterLoading.SpectatorJoined -= OnSpectatorJoined;
        }

        private void OnSpectatorJoined()
        {
            if (!_isReadySync)
            {
                return;
            }
            
            foreach (var property in _observedProperties)
            {
                object value = GetPropertyValue(property);
                photonView.RPC(nameof(UpdatePropertyRpc), RpcTarget.Others, property.Name, value);
            }
        }
        
        [PunRPC]
        public void UpdatePropertyRpc(string propertyName, object value)
        {
            if (!_isReadySync)
            {
                return;
            }
            
            PropertyInfo property = _observedPropertiesNames[propertyName];
            _oldValueProperties[property] = GetPropertyValue(property);
            TrySetProperty(property, value);
            _valueProperties[property] = value;
        }


        private void Update()
        {
            if (!_hasObservedProperties || !_isReadySync || !ProjectData.IsMultiplayerAndMasterClient || PhotonNetwork.CurrentRoom.PlayerCount < 2)
            {
                return;
            }

            if (_sync > 0)
            {
                _sync -= Time.deltaTime;
                return;
            }
            
            _sync = _syncTransformsInterval;
            
            foreach (var property in _observedProperties)
            {
                object value = GetPropertyValue(property);
                _valueProperties[property] = value;

                if (_oldValueProperties[property] != null && value != null && !_oldValueProperties[property].Equals(value))
                {
                    photonView.RPC(nameof(UpdatePropertyRpc), RpcTarget.Others, property.Name, value);
                }
                
                _oldValueProperties[property] = value;
            }
        }

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
        }

        private void TrySetProperty(PropertyInfo propertyInfo, object value)
        {
            if (value is ErrorValue)
            {
                return;
            }
            
            try
            {
                _propertyWrappers[propertyInfo].Setter(_wrapper, value);
            }
            catch (Exception e)
            {
                Debug.LogError($"Can not set field {propertyInfo.Name} Error: {e.Message}");
            }
        }

        private object GetPropertyValue(PropertyInfo propertyInfo)
        {
            object result;

            try
            {
                result = _propertyWrappers[propertyInfo].Getter(_wrapper);
            }
            catch (Exception e)
            {
                result = new ErrorValue();
                ((ErrorValue)result).Message = e.Message;
            }

            return result;
        }


        private void FindObservedItems()
        {
            if (_wrapper == null)
            {
                return;
            }

            Type wrapperType = _wrapper.GetType();
            
            var properties = wrapperType.GetProperties(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);

            foreach (PropertyInfo property in properties)
            {
                var attributes = property.GetCustomAttributes(true);

                foreach (object attribute in attributes)
                {
                    if (!(attribute is ObserveAttribute))
                    {
                        continue;
                    }

                    if (_ignoredPropertyNames.Contains(property.Name))
                    {
                        continue;
                    }
                    
                    _observedProperties.Add(property);
                    _valueProperties.Add(property, null);
                    _oldValueProperties.Add(property, null);
                    _observedPropertiesNames.Add(property.Name, property);
                    
                    _propertyWrappers.Add(property, ReflectionUtils.BuildPropertyWrapper(property));
                }
            }

            _isReadySync = true;
            _hasObservedProperties = _observedProperties.Count > 0;
        }
    }
}
