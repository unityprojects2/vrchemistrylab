using UnityEngine;

namespace Varwin.PlatformAdapter
{
    [RequireComponent(typeof(Arc))]
    public class TeleportPointer : MonoBehaviour, IBasePointer
    {
        public static bool TeleportEnabled = true;
        
        public float Velocity = 10f;
        public float DotUp = 0.9f;
        

        private Arc _arc;
        private Transform _pointerOrigin;
        private ControllerInput.ControllerEvents _events;
        
        private bool _active;
        private bool _click;

        private float _desktopArcOffsetModifier = 0.1f;
        private float _defaultArcOffsetModifier = 0.0f;
        
        
        private TeleportThroughCollisionChecker _checkerCollisionChecker;
        private TeleportThroughCollisionChecker CollisionChecker
        {
            get
            {
                if (!_checkerCollisionChecker)
                {
                    _checkerCollisionChecker = GetComponent<TeleportThroughCollisionChecker>();
                }

                return _checkerCollisionChecker;
            }
        }

        private GameObject _destinationReticle;
        private GameObject DestinationReticle
        {
            get
            {
                if (_destinationReticle == null)
                {
                    _destinationReticle = Instantiate(Resources.Load<GameObject>("Teleport/DestinationReticle"));
                }

                return _destinationReticle;
            }
        }
        
        
        public bool CanRelease() => _events.IsTouchpadReleased() && CanTeleport();

        public bool CanTeleport() => TeleportEnabled && _arc.IsArcValid();

        public bool CanToggle() => _events.IsTouchpadPressed();
        public bool CanPress() => false;

        public void Toggle(bool value)
        {
            if (_active == value)
            {
                return;
            }
            _active = value;
            UpdateArc(_active);
        }

        public void Toggle()
        {
            _active = !_active;
            UpdateArc(_active);
        }

        public bool IsActive() => _active;
        
        private void UpdateArc(bool state)
        {
            if (state)
            {
                _arc.Show();

                return;
            }

            _arc.Hide();
            DestinationReticle.SetActive(false);
        }

        public void Press()
        {
        }

        public void Release()
        {
            TeleportPlayer();
        }

        public void Init()
        {
            Transform origins = transform.Find("PointerOrigins");

            if (origins)
            {
                if (DeviceHelper.IsOculus)
                {
                    _pointerOrigin = origins.Find("Oculus");
                }
                else if (DeviceHelper.IsWmr)
                {
                    _pointerOrigin = origins.Find("WMR");
                }
                else
                {
                    _pointerOrigin = origins.Find("Generic");
                }

                if (!_pointerOrigin)
                {
                    _pointerOrigin = transform;
                }
            }
            else
            {
                _pointerOrigin = transform;
            }

            _arc = GetComponent<Arc>();
            _arc.ControllerHand = InputAdapter.Instance.PlayerController.Nodes.GetControllerHand(gameObject);
            //Ignore raycast and Zones
            _arc.TraceLayerMask = ~((1 << 2) | (1 << 13));  
            _events = InputAdapter.Instance.ControllerInput.ControllerEventFactory.GetFrom(gameObject);
        }

        protected virtual void DisableIfNeeded()
        {
        }

        public void UpdateState()
        {
            DisableIfNeeded();

            Transform originTransform = _pointerOrigin ? _pointerOrigin : transform;
            var arcOffsetModifier = ProjectData.PlatformMode == PlatformMode.Desktop ? _desktopArcOffsetModifier : _defaultArcOffsetModifier;
            Vector3 arcPosition = originTransform.position - originTransform.forward * arcOffsetModifier;
            Vector3 forward = originTransform.forward;
            Vector3 pointerDir = forward;
            Vector3 arcVelocity = pointerDir * Velocity;
            float dotUp = Vector3.Dot(pointerDir, Vector3.up);
            float dotForward = Vector3.Dot(pointerDir, forward);
            bool pointerAtBadAngle = dotForward > 0 && dotUp > DotUp || dotForward < 0.0f && dotUp > 0.5f;
            
            bool badTeleporting = false;
            if (ProjectData.PlatformMode == PlatformMode.Desktop)
            {
                badTeleporting = !CheckBackMoving(_arc.PlayerTeleportTransformCandidate);
            }
            else if (CollisionChecker)
            {
                badTeleporting = !CollisionChecker.PossibleToTeleport;
            }

            _arc.SetArcData(arcPosition,
                arcVelocity,
                true,
                pointerAtBadAngle,
                badTeleporting);
            
            bool hitSomeThing = false;

            if (!_active)
            {
                return;
            }

            if (_arc.DrawArc(out RaycastHit hitInfo))
            {
                hitSomeThing = true;
            }

            if (hitSomeThing && _arc.IsArcValid())
            {
                DestinationReticle.SetActive(true);
                DestinationReticle.transform.position = _arc.PlayerTeleportTransformCandidate;
            }
            else
            {
                DestinationReticle.SetActive(false);
            }
        }
        
        private void TeleportPlayer()
        {
            Vector3 teleportPoint = _arc.PlayerTeleportTransformCandidate;
            InputAdapter.Instance.PlayerController.Teleport(teleportPoint);
        }
        
        private bool CheckBackMoving(Vector3 supposedPos)
        {
            Transform camera = transform.parent;
            Transform playerTransform = camera.parent;
            
            Vector3 cameraForward = playerTransform.forward; 
            Plane cameraPlane = new Plane(cameraForward,playerTransform.position);

            return cameraPlane.GetSide(supposedPos);
        }
    }
}
