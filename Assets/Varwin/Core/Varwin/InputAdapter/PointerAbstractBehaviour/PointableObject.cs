using UnityEngine;

namespace Varwin.PlatformAdapter
{
    public abstract class PointableObject : MonoBehaviour
    {
        public abstract void OnPointerIn();
        
        public abstract void OnPointerOut();
        
        public abstract void OnPointerDown();

        public abstract void OnPointerUp();
        
        public abstract void OnPointerUpAsButton();
        
        protected virtual void Awake()
        {
            gameObject.layer = 5;
        }
    }
}
