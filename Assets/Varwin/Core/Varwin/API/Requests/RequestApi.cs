﻿using System;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine;
using Varwin.Data;

#pragma warning disable 618

namespace Varwin.WWW
{
    public class RequestApi : Request
    {
        private string PostData { get; }
        private RequestType RequestType { get; }

        public RequestApi(string uri, RequestType requestType = RequestType.Get, string postData = "{}", object[] userData = null, bool runInParallel = false)
        {
            Uri = uri;
            RequestType = requestType;
            PostData = postData;
            UserData = userData;
            RunInParallel = runInParallel;
            RequestManager.AddRequest(this);
        }

        public RequestApi(string uri, bool runInParallel)
        {
            Uri = uri;
            RequestType = RequestType.Get;
            PostData = "{}";
            UserData = null;
            RunInParallel = runInParallel;
            RequestManager.AddRequest(this);
        }

        protected override IEnumerator SendRequest()
        {
            if (RequestType == RequestType.Get)
            {
                yield return Get();
            }

            if (RequestType == RequestType.Post)
            {
                yield return Post();
            }
        }


        public static string GetResponseErrorMessage(RequestApi request, ResponseApi response)
        {
            return "API Error" + Environment.NewLine + 
                   Environment.NewLine + "\tREQUEST" + Environment.NewLine +
                   $"TYPE = {request.RequestType}" + Environment.NewLine +
                   $"URI = {request.Uri}" + Environment.NewLine +
                   $"RequestBody = {request.PostData}" + Environment.NewLine +
                   Environment.NewLine + "\tRESPONSE" + Environment.NewLine +
                   $"Message = {response.Message}" + Environment.NewLine +
                   $"Code = {response.Code}" + Environment.NewLine +
                   $"ResponseCode = {response.ResponseCode}" + Environment.NewLine +
                   $"ResponseBody = {response.Data}" + Environment.NewLine + Environment.NewLine;
        }

        public static string GetResponseInfoMessage(RequestApi request, ResponseApi response)
        {
            string code = !string.IsNullOrEmpty(response.Code) ? $"({response.Code})" : "";
            return $"{request.RequestType} {Settings.Instance.ApiHost + request.Uri} : {response.Status} {code}";
        }

        #region POST GET METHODS

        private IEnumerator Post()
        {
            using (var webRequest = new UnityWebRequest(Settings.Instance.ApiHost + Uri, "POST"))
            using (var uploadHandler = new UploadHandlerRaw(new System.Text.UTF8Encoding().GetBytes(PostData)))
            using (var downloadHandler = new DownloadHandlerBuffer())
            {
                webRequest.uploadHandler = uploadHandler;
                webRequest.downloadHandler = downloadHandler;

                if (!string.IsNullOrEmpty(AccessKey))
                {
                    webRequest.SetRequestHeader("Authorization", $"Bearer {AccessKey}");
                }

                webRequest.SetRequestHeader("Content-Type", "application/json");
                webRequest.SendWebRequest();

                float timer = 0;
                bool failed = false;

                while (!webRequest.isDone)
                {
                    if (timer > TimeOut)
                    {
                        failed = true;

                        break;
                    }

                    timer += Time.deltaTime;

                    yield return null;
                }

                if (webRequest.isNetworkError)
                {
                    failed = true;
                }

                if (failed)
                {
                    ((IRequest) this).OnResponseError($"{this} Timeout error", webRequest.responseCode);
                }
                else
                {
                    ResponseApi jSend = JSendEx.Deserialize(webRequest.downloadHandler.text)
                                        ?? new ResponseApi
                                        {
                                            Data = webRequest.downloadHandler.text
                                        };

                    if (jSend.Message == "not string")
                    {
                        jSend.Data = webRequest.downloadHandler.data;
                    }

                    ((IRequest) this).OnResponseDone(jSend, webRequest.responseCode);
                }
            }
        }

        private IEnumerator Get()
        {
            using (UnityWebRequest webRequest = UnityWebRequest.Get(Settings.Instance.ApiHost + Uri))
            {
                if (!string.IsNullOrEmpty(AccessKey))
                {
                    webRequest.SetRequestHeader("Authorization", $"Bearer {AccessKey}");
                }

                webRequest.SendWebRequest();
                float timer = 0;
                bool failed = false;

                while (!webRequest.isDone)
                {
                    if (timer > TimeOut)
                    {
                        failed = true;

                        break;
                    }

                    timer += Time.deltaTime;

                    yield return null;
                }

                if (webRequest.isNetworkError)
                {
                    failed = true;
                }

                if (failed)
                {
                    ((IRequest) this).OnResponseError($"{this} Timeout error", webRequest.responseCode);
                }
                else
                {
                    ResponseApi responseApi = JSendEx.Deserialize(webRequest.downloadHandler.text);

                    if (responseApi.Data == null)
                    {
                        responseApi.Data = webRequest.downloadHandler.text;
                    }

                    ((IRequest) this).OnResponseDone(responseApi, webRequest.responseCode);
                }
            }
        }

        #endregion
    }

    public enum RequestType
    {
        Post,
        Get
    }
}
