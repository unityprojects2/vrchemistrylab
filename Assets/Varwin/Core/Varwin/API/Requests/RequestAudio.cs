﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using Varwin.Data;

namespace Varwin.WWW
{
    public class RequestAudio : Request
    {
        public RequestAudio(string uri)
        {
            Uri = uri;
            RequestManager.AddRequest(this);
        }

        protected override IEnumerator SendRequest()
        {
            using (UnityWebRequest webRequest = UnityWebRequestMultimedia.GetAudioClip(Uri, GetAudioType()))
            {
                if (!string.IsNullOrEmpty(AccessKey))
                {
                    webRequest.SetRequestHeader("Authorization", $"Bearer {AccessKey}");
                }
                
                yield return webRequest.SendWebRequest();
            
                if (webRequest.isNetworkError || webRequest.isHttpError)
                {
                    ((IRequest) this).OnResponseError($"Audio can not be loaded due to web request error: {webRequest.error}");
                    yield break;
                }
                
                try
                {
                    var responseAudio = new ResponseAudio {AudioClip = DownloadHandlerAudioClip.GetContent(webRequest)};
                    ((IRequest) this).OnResponseDone(responseAudio);
                }
                catch (Exception e)
                {
                    ((IRequest) this).OnResponseError("Audio can not be loaded! " + e.Message);
                }
            }
        }

        private AudioType GetAudioType()
        {
            if (Uri.EndsWith(".aif", StringComparison.OrdinalIgnoreCase))
            {
                return AudioType.AIFF;
            }
            if (Uri.EndsWith(".wav", StringComparison.OrdinalIgnoreCase))
            {
                return AudioType.WAV;
            }
            if (Uri.EndsWith(".ogg", StringComparison.OrdinalIgnoreCase))
            {
                return AudioType.OGGVORBIS;
            }

            return AudioType.UNKNOWN;
        }
    }
}