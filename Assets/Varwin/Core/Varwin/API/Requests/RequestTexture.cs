using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using Varwin.Data;

namespace Varwin.WWW
{
    public class RequestTexture : Request
    {
        private TextureFormat _format;

        /// <summary>
        /// Request to load texture
        /// </summary>
        /// <param name="uri">link with out api host</param>
        /// <param name="runInParallel">can manager run this request in parallel</param>
        public RequestTexture(string uri, bool runInParallel = false)
        {
            Uri = uri;
            RunInParallel = runInParallel;
            RequestManager.AddRequest(this);
        }

        protected override IEnumerator SendRequest()
        {
            using (UnityWebRequest webRequest = UnityWebRequestTexture.GetTexture(Uri))
            using (var downloadHandler = new DownloadHandlerTexture(false))
            {
                webRequest.downloadHandler = downloadHandler;
            
                if (!string.IsNullOrEmpty(AccessKey))
                {
                    webRequest.SetRequestHeader("Authorization", $"Bearer {AccessKey}");
                }
            
                yield return webRequest.SendWebRequest();

                if (webRequest.isNetworkError || webRequest.isHttpError)
                {
                    ((IRequest) this).OnResponseError($"Texture can not be loaded due to web request error: {webRequest.error}");
                    yield break;
                }
                
                try
                {
                    var responseTexture = new ResponseTexture {Texture = downloadHandler.texture};
                    ((IRequest) this).OnResponseDone(responseTexture);
                }
                catch (Exception e)
                {
                    ((IRequest) this).OnResponseError("Texture can not be loaded! " + e.Message);
                }
            }
        }
    }
}
