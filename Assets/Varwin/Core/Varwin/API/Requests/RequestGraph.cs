﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using UnityEngine.Networking;
using UnityEngine;
using Varwin.Data;

namespace Varwin.WWW
{
    public class RequestGraph : Request
    {
        private GraphRequestType RequestType { get; set; }
        private string PostData { get; set; }
        
        public RequestGraph(string postData, GraphRequestType requestType)
        {
            Uri = Settings.Instance.ApiHost + "/query";
            RequestType = requestType;
            PostData = postData;
            RequestManager.AddRequest(this);
        }

        protected override IEnumerator SendRequest()
        {
            if (RequestType == GraphRequestType.Query)
            {
                yield return Query();
            }

            if (RequestType == GraphRequestType.Mutation)
            {
                yield return Mutation();
            }
        }

        private IEnumerator Query()
        {
            string jsonData = JsonConvert.SerializeObject(new {query = PostData});

            using (var request = UnityWebRequest.Post(Uri, UnityWebRequest.kHttpVerbPOST))
            using (var uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(jsonData)))
            using (var downloadHandler = new DownloadHandlerBuffer())
            {
                request.uploadHandler = uploadHandler;
                request.downloadHandler = downloadHandler;

                request.SetRequestHeader("Content-Type", "application/json");
                request.SendWebRequest();

                float timer = 0;
                bool failed = false;

                while (!request.isDone)
                {
                    if (timer > TimeOut)
                    {
                        failed = true;
                        break;
                    }

                    timer += Time.deltaTime;
                    yield return null;
                }

                if (request.isNetworkError)
                {
                    failed = true;
                }

                if (failed)
                {
                    ((IRequest) this).OnResponseError($"{this} Timeout error", request.responseCode);
                }
                else
                {
                    var json = request.downloadHandler.text;
                    var errorsDefinition = new {errors = new List<GraphErrorContainer>()};
                    var errors = JsonConvert.DeserializeAnonymousType(json, errorsDefinition).errors;
                    if (errors != null)
                    {
                        var errorMessages = string.Join("\n", errors.Select(x => x.Message));
                        ((IRequest) this).OnResponseError($"{this} GraphQL request errors:\n{errorMessages}", request.responseCode);
                        yield break;
                    }
                    
                    ResponseGraph response = new ResponseGraph { Data = json };

                    ((IRequest) this).OnResponseDone(response, request.responseCode);
                }
            }
        }
        
        private IEnumerator Mutation()
        {
            yield return null;
        }

        public static string GetResponseErrorMessage(RequestGraph request, ResponseGraph response)
        {
            return "API Error" + Environment.NewLine + 
                   Environment.NewLine + "\tREQUEST" + Environment.NewLine +
                   $"TYPE = {request.RequestType}" + Environment.NewLine +
                   $"URI = {request.Uri}" + Environment.NewLine +
                   $"RequestBody = {request.PostData}" + Environment.NewLine +
                   Environment.NewLine + "\tRESPONSE" + Environment.NewLine +
                   $"Message = {response.Message}" + Environment.NewLine +
                   $"Code = {response.Code}" + Environment.NewLine +
                   $"ResponseCode = {response.ResponseCode}" + Environment.NewLine +
                   $"ResponseBody = {response.Data}" + Environment.NewLine + Environment.NewLine;
        }
        
        public static string GetResponseInfoMessage(RequestGraph request, ResponseGraph response)
        {
            string code = !string.IsNullOrEmpty(response.Code) ? $"({response.Code})" : "";
            string url = $"{Settings.Instance.ApiHost + request.Uri.Replace(Settings.Instance.ApiHost, "")}";
            string body = $"{request.PostData ?? string.Empty}\n";
            return $"{request.RequestType} {url} : {response.Status} {code}\n{body}";
        }
    }
    
    public enum GraphRequestType
    {
        Query,
        Mutation
    }
    
    class GraphErrorContainer
    {
        public string Message { get; set; }
    }
}