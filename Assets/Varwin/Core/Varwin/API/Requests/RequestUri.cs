﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Varwin.Data;

//#pragma warning disable 618

namespace Varwin.WWW
{
    public class RequestUri : Request
    {
        public RequestUri(string uri, object[] userData = null, bool runInParallel = false)
        {
            Uri = uri;
            UserData = userData;
            RunInParallel = runInParallel;
            RequestManager.AddRequest(this);
        }

        protected override IEnumerator SendRequest()
        {
            yield return Get();
        }

        #region GET METHOD

        private IEnumerator Get()
        {
            string uri = Settings.Instance.ApiHost;
            if (!uri.EndsWith("/"))
            {
                uri += "/";
            }

            uri += Uri;

            using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
            {
                if (!string.IsNullOrEmpty(Request.AccessKey))
                {
                    webRequest.SetRequestHeader("Authorization", $"Bearer {AccessKey}");
                }

                webRequest.SendWebRequest();

                float timer = 0;
                bool failed = false;

                while (!webRequest.isDone)
                {
                    if (timer > TimeOut)
                    {
                        failed = true;
                        break;
                    }

                    timer += Time.deltaTime;
                    yield return null;
                }

                if (webRequest.isNetworkError)
                {
                    failed = true;
                }

                if (failed)
                {
                    ((IRequest) this).OnResponseError($"{this} Timeout error", webRequest.responseCode);
                }
                else
                {
                    var response = new ResponseUri()
                    {
                        ResponseCode = webRequest.responseCode,
                        TextData = webRequest.downloadHandler.text,
                        ByteData = webRequest.downloadHandler.data
                    };
                    ((IRequest) this).OnResponseDone(response, webRequest.responseCode);
                }
            }
        }

        #endregion
    }
}