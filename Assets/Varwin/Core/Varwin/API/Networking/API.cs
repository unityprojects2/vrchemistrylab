﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using NLog;
using Varwin.Data;
using Varwin.Data.ServerData;
using Varwin.WWW.Models;
using Logger = NLog.Logger;

namespace Varwin.WWW
{
    public static class API
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public static void GetServerConfig(Action<ServerConfig> onFinish = null, Action onError = null)
        {
            const string apiRoute = ApiRoutes.ServerConfig;
            var requestServerSettings = new RequestApi(apiRoute);
            requestServerSettings.TimeOut = 10f;

            requestServerSettings.OnFinish = response =>
            {
                var configResponse = (ResponseApi) response;
                Logger.Info(RequestApi.GetResponseInfoMessage(requestServerSettings, configResponse));

                if (!Helper.IsResponseGood(configResponse))
                {
                    Helper.ProcessBadResponse(requestServerSettings, configResponse);
                    return;
                }

                ServerConfig serverConfig;
                try
                {
                    string configString = configResponse.Data.ToString();
                    serverConfig = JsonConvert.DeserializeObject<ServerConfig>(configString,
                        new JsonSerializerSettings {TypeNameHandling = TypeNameHandling.None});
                }
                catch (Exception e)
                {
                    Logger.Fatal(RequestApi.GetResponseErrorMessage(requestServerSettings, configResponse)
                                 + Environment.NewLine
                                 + $"Exception = {e}");
                    return;
                }

                if (serverConfig != null)
                {
                    onFinish?.Invoke(serverConfig);
                }
            };
            
            requestServerSettings.OnError = s =>
            {
                onError?.Invoke();
            };
        }

        public static void GetProjects(Action<List<ProjectItem>> onFinish = null, Action onError = null, bool onlyMobile = false)
        {
            string apiRoute = onlyMobile ? ApiRoutes.MobileProjects : ApiRoutes.Projects; 
            var requestProjects = new RequestApi(apiRoute);
            requestProjects.TimeOut = 10f;

            requestProjects.OnFinish = response =>
            {
                var projectsResponse = (ResponseApi) response;
                Logger.Info(RequestApi.GetResponseInfoMessage(requestProjects, projectsResponse));
                
                if (!Helper.IsResponseGood(projectsResponse))
                {
                    onFinish?.Invoke(null);
                    Helper.ProcessBadResponse(requestProjects, projectsResponse);
                    return;
                }
                
                List<ProjectItem> projects;
                try
                {
                    string projectsJson = projectsResponse.Data.ToString();
                    projects = projectsJson.JsonDeserializeList<ProjectItem>();
                }
                catch (Exception e)
                {
                    Logger.Fatal(RequestApi.GetResponseErrorMessage(requestProjects, projectsResponse)
                                 + Environment.NewLine
                                 + $"Exception = {e}");
                    return;
                }

                if (projects != null)
                {
                    onFinish?.Invoke(projects);
                }
            };

            requestProjects.OnError = s =>
            {
                Logger.Error($"Can't get projects list: {s}");
                onError?.Invoke();
            };
        }

        public static void GetResources(int limit, int offset, string filter = "", ResourceRequestType requestType = ResourceRequestType.All, Action<List<ResourceDto>> callback = null)
        {
            string formats = string.Empty;
            const string separator = ",";
            
            switch (requestType)
            {
                case ResourceRequestType.All:
                    formats = string.Join(separator, ResourceFormatCodes.AllFormats);
                    break;
                case ResourceRequestType.Image:
                    formats = string.Join(separator, ResourceFormatCodes.ImageFormats);
                    break;
                case ResourceRequestType.Model:
                    formats = string.Join(separator, ResourceFormatCodes.ModelFormats);
                    break;
                case ResourceRequestType.TextFile:
                    formats = string.Join(separator, ResourceFormatCodes.TextFormats);
                    break;
                case ResourceRequestType.Audio:
                    formats = string.Join(separator, ResourceFormatCodes.AudioFormats);
                    break;
                case ResourceRequestType.Video:
                    formats = string.Join(separator, ResourceFormatCodes.VideoFormats);
                    break;
            }
            
            string apiRoute = string.Format(ApiRoutes.Resources, offset, limit, filter, formats);

            var requestApi = new RequestApi(apiRoute, true);
            requestApi.OnFinish = response =>
            {
                var responseApi = (ResponseApi) response;
                List<ResourceDto> result;
                Logger.Info(RequestApi.GetResponseInfoMessage(requestApi, responseApi));

                try
                {
                    string json = responseApi.Data.ToString();
                    result = json.JsonDeserializeList<ResourceDto>();
                }
                catch (Exception e)
                {
                    Logger.Fatal(RequestApi.GetResponseErrorMessage(requestApi, responseApi)
                                 + Environment.NewLine
                                 + $"Exception = {e}");
                    return;
                }

                callback?.Invoke(result);
            };
                
            requestApi.OnError = message =>
            {
                Logger.Error($"Can't get resources list: {message}");
                callback?.Invoke(new List<ResourceDto>());
            };
        }
        
        public static void GetResourceByGuid(string guid, Action<ResourceDto> callback = null)
        {
            string apiRoute = string.Format(ApiRoutes.ResourcesGuid, guid);
            var requestApi = new RequestApi(apiRoute, true);
            requestApi.OnFinish = response =>
            {
                var responseApi = (ResponseApi) response;
                ResourceDto[] result;
                Logger.Info(RequestApi.GetResponseInfoMessage(requestApi, responseApi));

                try
                {
                    string json = responseApi.Data.ToString();
                    result = JsonConvert.DeserializeObject<ResourceDto[]>(json);
                }
                catch (Exception e)
                {
                    Logger.Fatal(RequestApi.GetResponseErrorMessage(requestApi, responseApi)
                                 + Environment.NewLine
                                 + $"Exception = {e}");
                    result = null;
                }

                if (result != null && result.Length == 1)
                {
                    callback?.Invoke(result[0]);
                }
            };
            
            requestApi.OnError = message =>
            {
                Logger.Error($"Can't get resources list: {message}");
                callback?.Invoke(new ResourceDto());
            };
        }


        public static void GetLibraryObjects(string search = "", bool onlyMobile = false, Action<List<PrefabObject>> callback = null, int limit = 50, string after = "")
        {
            string query = Queries.GetLibraryObjectsQuery(ProjectData.SceneId, onlyMobile, limit, after, search);
            var requestGraph = new RequestGraph(query, GraphRequestType.Query);

            requestGraph.OnFinish = response =>
            {
                var responseGraph = (ResponseGraph) response;
                List<PrefabObject> result;
                Logger.Info(RequestGraph.GetResponseInfoMessage(requestGraph, responseGraph));
                
                try
                {
                    string json = responseGraph.Data;
                    var definition = new {data = new {objects = new {pageInfo = new {endCursor = ""}, edges = new List<PrefabObjectContainer>()}}};
                    List<PrefabObjectContainer> edges = JsonConvert.DeserializeAnonymousType(json, definition).data.objects.edges;
                    result = edges.Select(x => x.Node).ToList();

                    PrefabObject.EndCursor = JsonConvert.DeserializeAnonymousType(json, definition).data.objects.pageInfo.endCursor;
                }
                catch (Exception e)
                {
                    Logger.Fatal(RequestGraph.GetResponseErrorMessage(requestGraph, responseGraph)
                                 + Environment.NewLine
                                 + $"Exception = {e}");
                    result = null;
                }
                
                callback?.Invoke(result);
            };

            requestGraph.OnError = message =>
            {
                Logger.Error($"Can't get objects list: {message}");
                callback?.Invoke(new List<PrefabObject>());
            };
        }

        public static void GetSceneObjectLockedIds(int sceneId, Action<HashSet<int>> callback)
        {
            string apiRoute = string.Format(ApiRoutes.SceneObjectsLockedIds, sceneId);
            var requestApi = new RequestApi(apiRoute, true);
            requestApi.OnFinish = response =>
            {
                var responseApi = (ResponseApi) response;
                HashSet<int> result;
                Logger.Info(RequestApi.GetResponseInfoMessage(requestApi, responseApi));

                try
                {
                    var json = responseApi.Data.ToString();
                    result = JsonConvert.DeserializeObject<HashSet<int>>(json);
                }
                catch (Exception e)
                {
                    Logger.Fatal(RequestApi.GetResponseErrorMessage(requestApi, responseApi)
                                 + Environment.NewLine
                                 + $"Exception = {e}");
                    result = null;
                }

                if (result != null)
                {
                    callback?.Invoke(result);
                }
            };
            
            requestApi.OnError = message =>
            {
                Logger.Error($"Can't get scene object locked ids list: {message}");
            };
        }
    }
}
