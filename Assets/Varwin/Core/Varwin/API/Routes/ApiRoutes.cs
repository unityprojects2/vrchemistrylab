namespace Varwin.Data
{
    public static class ApiRoutes
    {
        public const string ProjectStructureRequest = "/v1/project-structure/{0}";
        public const string SaveSceneObjectsRequest = "/v1/update-scene-objects";
        public const string CanDeleteObjectRequest = "/v1/can-delete-scene-object/{0}";
        public const string ServerConfig = "/v1/server-config";
        public const string Projects = "/v1/projects";
        public const string MobileProjects = "/v1/projects?mobile_ready=true";
        public const string LibraryObjects = "/v1/objects-menu?offset={0}&limit={1}&search={2}&scene_id={3}";
        public const string MobileLibraryObjects = LibraryObjects + "&mobile_ready=true";
        public const string Resources = "/v1/resources?offset={0}&limit={1}&search={2}&formats={3}";
        public const string ResourcesGuid = "/v1/resources?limit=1&search={0}";
        public const string ZipProjectStructure = "/v1/project-structure/{0}?build=zip";
        public const string SceneObjectsLockedIds = "/v1/get-scene-objects-locked-ids/{0}";
    }
}
