﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Varwin.Errors;
using NLog;
using Varwin.Data;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;
using UnityEngine;
using Varwin.Data.ServerData;
using Varwin.ECS.Systems.Group;
using Varwin.Models.Data;
using Varwin.Types;
using Varwin.UI;
using Varwin.UI.VRErrorManager;
using Logger = NLog.Logger;

namespace Varwin.WWW
{
    // ReSharper disable once InconsistentNaming
    public static class AMQPClient
    {
        private static IConnection _connection;
        private static IModel _channel;
        private static Rabbitmq _settings;
        private static string _cachePath;
        
        private static readonly Dictionary<string, string> Exchanges = new Dictionary<string, string>();
        
        private static readonly Dictionary<int, string> CompilationSubscriptionIds = new Dictionary<int, string>();
        private static readonly Dictionary<int, string> LogicSubscriptionIds = new Dictionary<int, string>();
        private static readonly Dictionary<int, string> ObjectSubscriptionIds = new Dictionary<int, string>();
        private static readonly Dictionary<int, List<string>> SceneSubscriptionIds = new Dictionary<int, List<string>>();
        private static readonly Dictionary<int, List<string>> ProjectConfigurationSubscriptionIds = new Dictionary<int, List<string>>();

        private static Logger Logger = LogManager.GetCurrentClassLogger();

        private static bool _needsReconnect;
        
        private static string ExchangeDeclareIfNotExist(
            string exchange,
            string type,
            bool durable = false,
            bool autoDelete = false,
            IDictionary<string, object> arguments = null)
        {
            if (Exchanges.ContainsKey(exchange))
            {
                return Exchanges[exchange];
            }
            
            GetChannel().ExchangeDeclare(exchange,
                    type,
                    durable,
                    autoDelete,
                    arguments);
            
            Exchanges.Add(exchange, GetChannel().QueueDeclare().QueueName);

            return Exchanges[exchange];
        }

        private static IConnection GetConnectionChannel()
        {
            if (_connection != null)
            {
                return _connection;
            }

            if (_settings == null)
            {
                const string message = "Settings for RabbitMQ is not initialized!";
                Logger.Fatal(message);
                if (VRErrorManager.Instance)
                {
                    VRErrorManager.Instance.ShowFatal(message);
                }

                return null;
            }

            ConnectionFactory factory = new ConnectionFactory
            {
                HostName = _settings.host.Split(':')[0],
                UserName = _settings.login,
                Password = _settings.password,
                Port = Convert.ToInt32(_settings.host.Split(':')[1]),
                AutomaticRecoveryEnabled = true,
                TopologyRecoveryEnabled = true,
                NetworkRecoveryInterval = new TimeSpan(0,0, 5)
            };

            try
            {
                _connection = factory.CreateConnection();
            }
            catch (Exception e)
            {
                string message = "Can't connect to RabbitMQ! " + e.Message;
                Logger.Fatal(message);
                if (VRErrorManager.Instance)
                {
                    VRErrorManager.Instance.ShowFatal(message);
                }
            }

            return _connection;
        }

        private static IModel GetChannel() => _channel ?? (_channel = GetConnectionChannel().CreateModel());

        
        public static void CloseConnection()
        {
            _connection?.Close();
            _connection = null;

            _channel?.Close();
            _channel = null;
            
            Exchanges.Clear();
            CompilationSubscriptionIds.Clear();
            LogicSubscriptionIds.Clear();
            ObjectSubscriptionIds.Clear();
            SceneSubscriptionIds.Clear();
            ProjectConfigurationSubscriptionIds.Clear();
        }
        
        public static void SubscribeSceneChange(int projectId)
        {
            Logger.Info($"subscribe scene change to project {projectId}");
            IModel channel = GetChannel();
            string queueNameAdd = ExchangeDeclareIfNotExist(ExchangeNames.SceneAdded, "topic", true);
            string key = $"project.{projectId}";

            channel.QueueBind(queueNameAdd,
                ExchangeNames.SceneAdded,
                key);
            EventingBasicConsumer consumerForAdd = new EventingBasicConsumer(channel);

            consumerForAdd.Received += (model, ea) =>
            {
                Logger.Info("Scene was added");
                var body = ea.Body;
                string jsonData = Encoding.UTF8.GetString(body);
                ProjectSceneWithPrefabObjects projectScene = jsonData.JsonDeserialize<ProjectSceneWithPrefabObjects>();

                ProjectDataListener.Instance.ProjectSceneArguments = new ProjectSceneArguments()
                {
                    Scene = projectScene.Scene,
                    SceneTemplate = projectScene.SceneTemplate,
                    State = ProjectSceneArguments.StateProjectScene.Added
                };
            };


            string queueNameDelete = ExchangeDeclareIfNotExist(ExchangeNames.SceneDeleted, "topic", true);
            key = $"project.{projectId}";

            channel.QueueBind(queueNameDelete,
                ExchangeNames.SceneDeleted,
                key);
            EventingBasicConsumer consumerForDelete = new EventingBasicConsumer(channel);

            consumerForDelete.Received += (model, ea) =>
            {
                Logger.Info("Project scene was deleted");
                var body = ea.Body;
                string jsonData = Encoding.UTF8.GetString(body);
                ProjectSceneWithPrefabObjects projectScene = jsonData.JsonDeserialize<ProjectSceneWithPrefabObjects>();

                ProjectDataListener.Instance.ProjectSceneArguments = new ProjectSceneArguments()
                {
                    Scene = projectScene.Scene,
                    SceneTemplate = projectScene.SceneTemplate,
                    State = ProjectSceneArguments.StateProjectScene.Deleted
                };
            };


            string queueNameChange = ExchangeDeclareIfNotExist(ExchangeNames.SceneChanged, "topic", true);
            key = $"project.{projectId}";

            channel.QueueBind(queueNameChange,
                ExchangeNames.SceneChanged,
                key);
            EventingBasicConsumer consumerForChange = new EventingBasicConsumer(channel);

            consumerForChange.Received += (model, ea) =>
            {
                Logger.Info("Project scene was changed");
                var body = ea.Body;
                string jsonData = Encoding.UTF8.GetString(body);
                ProjectSceneWithPrefabObjects projectScene = jsonData.JsonDeserialize<ProjectSceneWithPrefabObjects>();

                ProjectDataListener.Instance.ProjectSceneArguments = new ProjectSceneArguments()
                {
                    Scene = projectScene.Scene,
                    SceneTemplate = projectScene.SceneTemplate,
                    State = ProjectSceneArguments.StateProjectScene.Changed
                };
            };

            List<string> tags = new List<string>
            {
                channel.BasicConsume(queueNameDelete, true, consumerForDelete),
                channel.BasicConsume(queueNameAdd, true, consumerForAdd),
                channel.BasicConsume(queueNameChange, true, consumerForChange)
            };

            if (!SceneSubscriptionIds.ContainsKey(projectId))
            {
                SceneSubscriptionIds.Add(projectId, null);
            }

            SceneSubscriptionIds[projectId] = tags;
        }

        public static void UnSubscribeProjectSceneChange(int projectId)
        {
            if (!SceneSubscriptionIds.ContainsKey(projectId))
            {
                return;
            }

            Logger.Info($"Unsubscribe project scene change to projectId {projectId} started...");
            Exchanges.Remove(ExchangeNames.SceneAdded);
            Exchanges.Remove(ExchangeNames.SceneDeleted);
            Exchanges.Remove(ExchangeNames.SceneChanged);

            IModel channel = GetChannel();
            List<string> tags = SceneSubscriptionIds[projectId];

            foreach (var tag in tags)
            {
                channel.BasicCancel(tag);
            }

            SceneSubscriptionIds.Remove(projectId);
            Logger.Info("Unsubscribe scene successful");
        }

        public static void SubscribeProjectConfigurationChange(int projectId)
        {
            Logger.Info($"subscribe configuration change to project {projectId}");
            IModel channel = GetChannel();
            string queueNameAdd = ExchangeDeclareIfNotExist(ExchangeNames.ProjectConfigurationAdded, "topic", true);
            string key = $"project.{projectId}";

            channel.QueueBind(queueNameAdd,
                ExchangeNames.ProjectConfigurationAdded,
                key);
            EventingBasicConsumer consumerForAdd = new EventingBasicConsumer(channel);

            consumerForAdd.Received += (model, ea) =>
            {
                Logger.Info("Configuration was added");
                var body = ea.Body;
                string jsonData = Encoding.UTF8.GetString(body);
                ProjectConfiguration configuration = jsonData.JsonDeserialize<ProjectConfiguration>();

                ProjectDataListener.Instance.ProjectConfigurationArguments = new ProjectConfigurationArguments()
                {
                    ProjectConfiguration = configuration,
                    State = ProjectConfigurationArguments.StateConfiguration.Added
                };
            };


            string queueNameDelete =
                ExchangeDeclareIfNotExist(ExchangeNames.ProjectConfigurationDeleted, "topic", true);
            key = $"project.{projectId}";

            channel.QueueBind(queueNameDelete,
                ExchangeNames.ProjectConfigurationDeleted,
                key);
            EventingBasicConsumer consumerForDelete = new EventingBasicConsumer(channel);

            consumerForDelete.Received += (model, ea) =>
            {
                Logger.Info("Configuration was deleted");
                var body = ea.Body;
                string jsonData = Encoding.UTF8.GetString(body);
                ProjectConfiguration configuration = jsonData.JsonDeserialize<ProjectConfiguration>();

                ProjectDataListener.Instance.ProjectConfigurationArguments = new ProjectConfigurationArguments()
                {
                    ProjectConfiguration = configuration,
                    State = ProjectConfigurationArguments.StateConfiguration.Deleted
                };
            };


            string queueNameChange =
                ExchangeDeclareIfNotExist(ExchangeNames.ProjectConfigurationChanged, "topic", true);
            key = $"project.{projectId}";

            channel.QueueBind(queueNameChange,
                ExchangeNames.ProjectConfigurationChanged,
                key);
            EventingBasicConsumer consumerForChange = new EventingBasicConsumer(channel);

            consumerForChange.Received += (model, ea) =>
            {
                Logger.Info("Configuration was changed");
                var body = ea.Body;
                string jsonData = Encoding.UTF8.GetString(body);
                ProjectConfiguration configuration = jsonData.JsonDeserialize<ProjectConfiguration>();

                ProjectDataListener.Instance.ProjectConfigurationArguments = new ProjectConfigurationArguments()
                {
                    ProjectConfiguration = configuration,
                    State = ProjectConfigurationArguments.StateConfiguration.Changed
                };
            };

            List<string> tags = new List<string>
            {
                channel.BasicConsume(queueNameDelete, true, consumerForDelete),
                channel.BasicConsume(queueNameAdd, true, consumerForAdd),
                channel.BasicConsume(queueNameChange, true, consumerForChange)
            };

            if (!ProjectConfigurationSubscriptionIds.ContainsKey(projectId))
            {
                ProjectConfigurationSubscriptionIds.Add(projectId, null);
            }

            ProjectConfigurationSubscriptionIds[projectId] = tags;
        }

        public static void UnSubscribeProjectConfigurationsChange(int projectId)
        {
            if (!ProjectConfigurationSubscriptionIds.ContainsKey(projectId))
            {
                return;
            }

            Logger.Info($"Unsubscribe configuration change to projectId {projectId} started...");

            Exchanges.Remove(ExchangeNames.ProjectConfigurationAdded);
            Exchanges.Remove(ExchangeNames.ProjectConfigurationDeleted);
            Exchanges.Remove(ExchangeNames.ProjectConfigurationChanged);

            IModel channel = GetChannel();
            List<string> tags = ProjectConfigurationSubscriptionIds[projectId];

            foreach (var tag in tags)
            {
                channel.BasicCancel(tag);
            }

            ProjectConfigurationSubscriptionIds.Remove(projectId);
            Logger.Info("Unsubscribe project configuration successful");
        }
       
        public static void SubscribeObjectChange(int projectId, int sceneId)
        {
            Logger.Info($"subscribe object change to scene {sceneId}, project {projectId}");
            IModel channel = GetChannel();
            string queueName = ExchangeDeclareIfNotExist(ExchangeNames.SceneObjectChanged, "topic", true);
            string key = $"project.{projectId}.scene.{sceneId}";

            channel.QueueBind(queueName,
                ExchangeNames.SceneObjectChanged,
                key);
            EventingBasicConsumer consumer = new EventingBasicConsumer(channel);

            consumer.Received += (model, ea) =>
            {
                Logger.Info("Object was changed");
                var body = ea.Body;
                string jsonData = Encoding.UTF8.GetString(body);
                SceneObjectDto sceneObjectDto = jsonData.JsonDeserialize<SceneObjectDto>();
                ProjectDataListener.Instance.UpdateObject(sceneObjectDto);
            };

            string tag = channel.BasicConsume(queueName, true, consumer);

            if (!ObjectSubscriptionIds.ContainsKey(sceneId))
            {
                ObjectSubscriptionIds.Add(sceneId, string.Empty);
            }

            ObjectSubscriptionIds[sceneId] = tag;
        }

        public static void UnSubscribeObjectChange(int sceneId)
        {
            if (!ObjectSubscriptionIds.ContainsKey(sceneId))
            {
                return;
            }

            Logger.Info($"Unsubscribe object change to scene {sceneId} started...");
            Exchanges.Remove(ExchangeNames.SceneObjectChanged);
            IModel channel = GetChannel();
            channel.BasicCancel(ObjectSubscriptionIds[sceneId]);
            ObjectSubscriptionIds.Remove(sceneId);
            Logger.Info("Unsubscribe logic successful");
        }

        public static void SubscribeLogicChange(int sceneId)
        {
            try
            {
                Logger.Info($"subscribe logic change to scene {sceneId}");

                IModel channel = GetChannel();
                string queueName = ExchangeDeclareIfNotExist(ExchangeNames.LogicChanged, "topic", true);

                string key = $"scene.{sceneId}";

                channel.QueueBind(queueName,
                    ExchangeNames.LogicChanged,
                    key);

                EventingBasicConsumer consumer = new EventingBasicConsumer(channel);

                consumer.Received += (model, ea) =>
                {
                    Logger.Info("New logic code was received");
                    var body = ea.Body;
                    var groupLogicUpdateSystem = new LogicUpdateSystem(Contexts.sharedInstance, body, sceneId, false);
                    groupLogicUpdateSystem.Execute();
                    GameStateData.UpdateSceneObjectLockedIds();
                };

                string tag = channel.BasicConsume(queueName,
                    true,
                    consumer);

                if (!LogicSubscriptionIds.ContainsKey(sceneId))
                {
                    LogicSubscriptionIds.Add(sceneId, string.Empty);
                }

                LogicSubscriptionIds[sceneId] = tag;
            }
            catch (Exception ex)
            {
                Logger.Fatal($"RabbitMQ error! subscribe to logic error! {ex.Message}");
                if (VRErrorManager.Instance)
                {
                    VRErrorManager.Instance.ShowFatal("RabbitMQ error! Subscribe to logic error!");
                }
            }
        }

        public static void UnSubscribeLogicChange(int sceneId)
        {
            Logger.Info($"Unsubscribe logic change to scene {sceneId} started...");

            if (!LogicSubscriptionIds.ContainsKey(sceneId))
            {
                return;
            }

            Exchanges.Remove(ExchangeNames.LogicChanged);

            IModel channel = GetChannel();
            channel.BasicCancel(LogicSubscriptionIds[sceneId]);

            LogicSubscriptionIds.Remove(sceneId);
            Logger.Info("Unsubscribe logic successful");
        }

        public static void SubscribeCompilationError(int sceneId)
        {
            try
            {
                Logger.Info($"subscribe compilation error to scene {sceneId}");

                IModel channel = GetChannel();
                string queueName = ExchangeDeclareIfNotExist(ExchangeNames.CompilationError, "topic", true);

                string key = $"scene.{sceneId}";

                channel.QueueBind(queueName,
                    ExchangeNames.CompilationError,
                    key);

                EventingBasicConsumer consumer = new EventingBasicConsumer(channel);

                consumer.Received += (model, ea) =>
                {
                    Logger.Info("Compilation error was received");

                    var groupLogicUpdateSystem = new LogicUpdateSystem(Contexts.sharedInstance, null, sceneId, true);
                    groupLogicUpdateSystem.Execute();
                    GameStateData.ClearLogic();
                };

                string tag = channel.BasicConsume(queueName,
                    true,
                    consumer);

                if (!CompilationSubscriptionIds.ContainsKey(sceneId))
                {
                    CompilationSubscriptionIds.Add(sceneId, string.Empty);
                }

                CompilationSubscriptionIds[sceneId] = tag;
            }
            catch (Exception ex)
            {
                Logger
                    .Fatal($"RabbitMQ error! subscribe to compilation error! {ex.Message}");
                if (VRErrorManager.Instance)
                {
                    VRErrorManager.Instance.ShowFatal("RabbitMQ error! Subscribe to compilation error!");
                }
            }
        }

        public static void UnSubscribeCompilationError(int sceneId)
        {
            Logger.Info($"Unsubscribe compilation error to scene {sceneId} started...");

            if (!CompilationSubscriptionIds.ContainsKey(sceneId))
            {
                return;
            }

            Exchanges.Remove(ExchangeNames.CompilationError);

            IModel channel = GetChannel();
            channel.BasicCancel(CompilationSubscriptionIds[sceneId]);

            CompilationSubscriptionIds.Remove(sceneId);
            Logger.Info("Unsubscribe compilation error successful");
        }

        public static void SendRuntimeErrorMessage(LogicException logicException)
        {
            SendRuntimeErrorMessage(logicException.Logic.SceneId,
                logicException.Line,
                logicException.Column,
                logicException.Message);
        }

        public static void SendRuntimeErrorMessage(
            int sceneId,
            int line,
            int column,
            string errorMessage)
        {
            IModel channel = GetChannel();
            ExchangeDeclareIfNotExist(ExchangeNames.RuntimeError, "topic", true);
            string key = $"project.{ProjectData.ProjectId}.{sceneId}";

            LogicBlockError error = new LogicBlockError
            {
                SceneId = sceneId, Line = line, Column = column, ErrorMessage = errorMessage
            };

            var message = Encoding.ASCII.GetBytes(error.ToJson());

            channel.BasicPublish(ExchangeNames.RuntimeError,
                key,
                false,
                null,
                message);
        }

        public static void SendRuntimeBlocks(
            int sceneId,
            string[] blocks)
        {
            IModel channel = GetChannel();
            ExchangeDeclareIfNotExist(ExchangeNames.RuntimeBlocks, "topic", true);
            string key = $"project.{ProjectData.ProjectId}.{sceneId}";

            LogicBlockRuntimeList list = new LogicBlockRuntimeList {SceneId = sceneId, Blocks = blocks};

            var message = Encoding.ASCII.GetBytes(list.ToJson());

            try
            {
                channel.BasicPublish(ExchangeNames.RuntimeBlocks,
                    key,
                    false,
                    null,
                    message);

                if (!_needsReconnect)
                {
                    return;
                }

                _needsReconnect = false;
                Logger.Info("RabbitMQ connection resotred");
            }
            catch (AlreadyClosedException e)
            {
                _needsReconnect = true;
                Logger.Error($"Can't connect to RabbitMQ; retrying in {BlockLoggerInstance.BlockSendInterval} seconds");
            }
        }

        public static void ReadLaunchArgs()
        {
            try
            {
                string key = _settings.key;
                string type = "direct";
                string queueName = ExchangeNames.Launch + "_" + key;
                string exchangeName = ExchangeNames.Launch;

                IModel channel = GetChannel();
                ExchangeDeclareIfNotExist(exchangeName, type, true);
                channel.QueueBind(queueName, exchangeName, key);
                
                EventingBasicConsumer consumer = new EventingBasicConsumer(channel);

                consumer.Received += (model, ea) =>
                {
                    Logger.Info("vw.launch received message!");
                    var body = ea.Body;
                    string message = Encoding.UTF8.GetString(body);

#if UNITY_EDITOR
                    SaveCache(message);
#endif
                    LaunchArguments launchArguments = message.JsonDeserialize<LaunchArguments>();

                    if (launchArguments.extraArgs != null)
                    {
                        Logger.Debug("!!@@##"
                                  + launchArguments
                                      .extraArgs); //not sure where to instantiate local logger with Logger
                        ArgumentStorage.ClearStorage();
                        ArgumentStorage.AddJsonArgsArray(launchArguments.extraArgs);
                    }

                    if (!string.IsNullOrEmpty(launchArguments.accessKey))
                    {
                        Request.AccessKey = launchArguments.accessKey;
                    }

                    ProjectDataListener.Instance.LaunchArguments = launchArguments;
                };

                channel.BasicConsume(queueName,
                    true,
                    consumer);
            }
            catch (Exception e)
            {
                CloseConnection();

#if UNITY_EDITOR
                LaunchArguments launchArguments = LoadFromCache();

                if (launchArguments != null)
                {
                    Logger.Info("Loading previous vw.launch...");
                    ProjectDataListener.Instance.LaunchArguments = launchArguments;
                }
                else
                {
                    LauncherErrorManager.Instance.ShowFatalErrorKey(ErrorHelper.GetErrorKeyByCode(ErrorCode.RabbitNoArgsError), e.ToString());
                }

#else
                Logger.Fatal("vw.launch not found! StackTrace = " + e.StackTrace);
                LauncherErrorManager.Instance.ShowFatalErrorKey(ErrorHelper.GetErrorKeyByCode(Varwin.Errors.ErrorCode.RabbitNoArgsError), e.StackTrace);
#endif
            }
        }

        private static void SaveCache(string message)
        {
            if (!Directory.Exists(_cachePath))
            {
                Directory.CreateDirectory(_cachePath);
            }

            File.WriteAllText(_cachePath + "/launch.args", message);
        }

        private static LaunchArguments LoadFromCache()
        {
            if (!File.Exists(_cachePath + "/launch.args"))
            {
                return null;
            }

            string message = File.ReadAllText(_cachePath + "/launch.args");

            try
            {
                LaunchArguments launchArguments = message.JsonDeserialize<LaunchArguments>();

                return launchArguments;
            }
            catch
            {
                return null;
            }
        }

        public static void Init(Rabbitmq rabbitmq)
        {
            _settings = rabbitmq;
            _cachePath = Application.persistentDataPath + "/cache";
        }
    }
}
