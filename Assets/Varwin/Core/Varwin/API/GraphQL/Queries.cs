﻿namespace Varwin.WWW
{
  public static class Queries
  {
    public static string GetLibraryObjectsQuery(int sceneId, bool isMobileReady = false, int limit = 50, string after = "", string search = "")
    {
      string mobileReadyPart = isMobileReady ? $"mobileReady: true, " : "";
      string searchPart = string.IsNullOrEmpty(search) ? "" : $", search: \"{search}\"";
      string afterPart = string.IsNullOrEmpty(after) ? "" : $", after: \"{after}\"";

      return @"
query getObjects {
objects " + $"({mobileReadyPart}first: {limit}{afterPart}{searchPart}, sceneId: {sceneId})" + @" {
    pageInfo {
      endCursor
    }
    edges {
      node {
        id
        guid
        rootGuid
        name {
          en
          ru
        }
        embedded
        config
        assets
      }
    }
  }
}";
    }
  }
}
