﻿using System;

namespace Varwin.WWW
{
    public class OnRequestBegin : Event<OnRequestBegin>
    {
        public OnRequestBegin()
        {

        }
    }

    public class OnRequestEnded : Event<OnRequestEnded>
    {
        public string data;
        public bool success;
        public Exception exception;

        public OnRequestEnded(string data)
        {
            this.data = data;
            success = true;
        }

        public OnRequestEnded(Exception exception)
        {
            this.exception = exception;
            success = false;
        }
    }

    public class OnSubscriptionHandshakeComplete : Event<OnSubscriptionHandshakeComplete>
    {
        public OnSubscriptionHandshakeComplete()
        {

        }
    }

    public class OnSubscriptionDataReceived : Event<OnSubscriptionDataReceived>
    {
        public string data;

        public OnSubscriptionDataReceived(string data)
        {
            this.data = data;
        }
    }

    public class OnSubscriptionCanceled : Event<OnSubscriptionCanceled>
    {
        public OnSubscriptionCanceled()
        {

        }
    }
    
    public abstract class Event<T> where T : Event<T>
    {
        private bool _hasFired;

        private static event Action<T> listeners;

        public static void RegisterListener(Action<T> listener)
        {
            if (listener == null)
            {
                return;
            }
            
            listeners += listener;
        }

        public static void UnregisterListener(Action<T> listener)
        {
            if (listener == null)
            {
                return;
            }
            
            listeners -= listener;
        }

        public void FireEvent()
        {
            if (_hasFired)
            {
                return;
            }

            _hasFired = true;
            listeners?.Invoke(this as T);
        }
    }
}
