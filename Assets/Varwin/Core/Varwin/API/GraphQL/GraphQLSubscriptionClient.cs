﻿using System;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using Logger = NLog.Logger;

namespace Varwin.WWW
{
    public static class GraphQLSubscriptionClient
    {
	    private static string Url => Settings.Instance.ApiHost + "/query";
        private static Logger Logger = LogManager.GetCurrentClassLogger();
        
        private static ClientWebSocket _libraryChangedSubscription;

        
        public static async void SubscribeLibraryChanged()
        {
	        if (_libraryChangedSubscription != null)
	        {
		        return;
	        }
	        
	        _libraryChangedSubscription = await Subscribe(nameof(Subscriptions.LibraryChanged), Subscriptions.LibraryChanged, LibraryChangedCallback);
        }

        public static async void UnsubscribeLibraryChanged()
        {
            if (_libraryChangedSubscription == null)
            {
                return;
            }
            
            await Unsubscribe(nameof(Subscriptions.LibraryChanged), _libraryChangedSubscription, LibraryChangedCallback);
            _libraryChangedSubscription = null;
        }

        private static void LibraryChangedCallback(OnSubscriptionDataReceived subscriptionDataReceived)
        {
            Logger.Info("GraphQL: Library was changed");
            if (!ProjectDataListener.Instance)
            {
	            return;
            }
            
            ProjectDataListener.Instance.LibraryWasUpdated();
        }

        private static async Task<ClientWebSocket> Subscribe(string subscriptionName, string subscription, Action<OnSubscriptionDataReceived> subscriptionCallback = null)
        {
	        ClientWebSocket subscriptionWebSocket = null;
	        try
	        {
		        subscriptionWebSocket = await WebsocketConnect(Url, subscription);
		        OnSubscriptionDataReceived.RegisterListener(subscriptionCallback);
		        Logger.Info($"Subscribed to GraphQL.{subscriptionName}");
	        }
	        catch (Exception e)
	        {
		        Logger.Error($"Subscription to GraphQL.{subscriptionName} failed. Error: {e.Message}");
	        }
	        
	        return subscriptionWebSocket;
        }

        private static async Task Unsubscribe(string subscriptionName, ClientWebSocket subscriptionWebSocket, Action<OnSubscriptionDataReceived> subscriptionCallback = null)
        {
	        try
	        {
		        await WebsocketDisconnect(subscriptionWebSocket);
		        OnSubscriptionDataReceived.UnregisterListener(subscriptionCallback);
		        Logger.Info($"Unsubscribed from {subscriptionName}");
	        }
	        catch (Exception e)
	        {
		        Logger.Error($"Can not unsubscribe from GraphQL.{subscriptionName}. Error: {e.Message}");
	        }
        }
        
        #region Websocket

		private static async Task<ClientWebSocket> WebsocketConnect(string subscriptionUrl, string subscription,
			string authToken = null, string socketId = "1", string protocol = "graphql-ws")
		{
			ClientWebSocket clientWebSocket = new ClientWebSocket();
			clientWebSocket.Options.AddSubProtocol(protocol);
			if (!String.IsNullOrEmpty(authToken))
			{
				clientWebSocket.Options.SetRequestHeader("Authorization", "Bearer " + authToken);
			}

			Uri uri = new Uri(subscriptionUrl.Replace("http", "ws"));
			try
			{
				await clientWebSocket.ConnectAsync(uri, CancellationToken.None);
				await WebsocketInit(clientWebSocket);
				await WebsocketSend(clientWebSocket, socketId, subscription);
			}
			catch (Exception e)
			{
				throw new ApplicationException($"GraphQL subscription failed: {e.Message}");
			}

			return clientWebSocket;
		}

		private static async Task WebsocketDisconnect(ClientWebSocket clientWebSocket, string socketId = "1")
		{
			string jsonData = $"{{\"type\":\"stop\",\"id\":\"{socketId}\"}}";
			ArraySegment<byte> b = new ArraySegment<byte>(Encoding.ASCII.GetBytes(jsonData));
			await clientWebSocket.SendAsync(b, WebSocketMessageType.Text, true, CancellationToken.None);
			await clientWebSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Closed", CancellationToken.None);
			OnSubscriptionCanceled subscriptionCanceled = new OnSubscriptionCanceled();
			subscriptionCanceled.FireEvent();
		}
		
		private static async Task WebsocketInit(ClientWebSocket clientWebSocket)
		{
			string jsonData = "{\"type\":\"connection_init\"}";
			ArraySegment<byte> b = new ArraySegment<byte>(Encoding.ASCII.GetBytes(jsonData));
			await clientWebSocket.SendAsync(b, WebSocketMessageType.Text, true, CancellationToken.None);
			GetWsReturn(clientWebSocket);
		}

		private static async Task WebsocketSend(ClientWebSocket clientWebSocket, string id, string details)
		{
			string jsonData = JsonConvert.SerializeObject(new {id = $"{id}", type = "start", payload = new {query = details}});
			ArraySegment<byte> b = new ArraySegment<byte>(Encoding.ASCII.GetBytes(jsonData));
			await clientWebSocket.SendAsync(b, WebSocketMessageType.Text, true, CancellationToken.None);
		}

		//Call GetWsReturn to wait for a message from a websocket. GetWsReturn has to be called for each message
		private static async void GetWsReturn(ClientWebSocket clientWebSocket)
		{
			ArraySegment<byte> buffer = new ArraySegment<byte>(new byte[1024]);
			buffer = WebSocket.CreateClientBuffer(1024, 1024);
			WebSocketReceiveResult webSocketReceiveResult;
			string result = "";
			do
			{
				webSocketReceiveResult = await clientWebSocket.ReceiveAsync(buffer, CancellationToken.None);
				result += Encoding.UTF8.GetString(buffer.Array ?? throw new ApplicationException("Buf = null"), buffer.Offset,
					webSocketReceiveResult.Count);
			} while (!webSocketReceiveResult.EndOfMessage);

			if (String.IsNullOrEmpty(result))
			{
				return;
			}

			JObject obj = new JObject();
			try
			{
				obj = JObject.Parse(result);
			}
			catch (JsonReaderException e)
			{
				throw new ApplicationException(e.Message);
			}

			string subType = (string) obj["type"];
			switch (subType)
			{
				case "connection_ack":
				{
					OnSubscriptionHandshakeComplete subscriptionHandshakeComplete = new OnSubscriptionHandshakeComplete();
					subscriptionHandshakeComplete.FireEvent();
					GetWsReturn(clientWebSocket);
					break;
				}
				case "data":
				{
					OnSubscriptionDataReceived subscriptionDataReceived = new OnSubscriptionDataReceived(result);
					subscriptionDataReceived.FireEvent();
					GetWsReturn(clientWebSocket);
					break;
				}
				case "ka":
				{
					GetWsReturn(clientWebSocket);
					break;
				}
				case "error":
				case "connection_error":
				{
					throw new ApplicationException("The handshake failed. Error: " + result);
				}
				case "subscription_fail":
				{
					throw new ApplicationException("The subscription data failed");
				}

			}
		}

		#endregion
    }
}