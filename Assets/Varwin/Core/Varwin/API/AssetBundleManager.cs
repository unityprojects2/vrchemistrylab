﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NLog;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using Varwin.Data.ServerData;
using Varwin.ECS.Systems;
using Logger = NLog.Logger;

namespace Varwin.WWW
{
    public class AssetBundleManager : MonoBehaviour
    {
        public static AssetBundleManager Instance;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private static AssetBundle _currentBundle;

        private readonly HashSet<AssetBundle> _alwaysLoaded = new HashSet<AssetBundle>();

        private AssetBundle _previousScene;
        private string _previousSceneName;

        private static IEnumerator DownloadAssetBundle(string url, RequestAsset requestAsset)
        {
            while (!Caching.ready)
            {
                yield return null;
            }

            using (UnityWebRequest webRequest = UnityWebRequestAssetBundle.GetAssetBundle(url, requestAsset.BundleCacheSettings))
            {
                if (!string.IsNullOrEmpty(Request.AccessKey))
                {
                    webRequest.SetRequestHeader("Authorization", $"Bearer {Request.AccessKey}");
                }

                yield return webRequest.SendWebRequest();

                try
                {
                    _currentBundle = DownloadHandlerAssetBundle.GetContent(webRequest);
                }
                catch
                {
                    _currentBundle = null;
                }

                Logger.Info($"Loaded {requestAsset.AssetName} from {(webRequest.responseCode == 0 ? $"cache ({requestAsset.BundleCacheSettings.name})" : "server and cached")}. " +
                            $"Total cache size: {Caching.currentCacheForWriting.spaceOccupied / 1024 / 1024} Mb");
            }
        }

        private static IEnumerator LoadAssetBundleFromMemory(byte[] bytes)
        {
            while (!Caching.ready)
            {
                yield return null;
            }

            AssetBundleCreateRequest createRequest = AssetBundle.LoadFromMemoryAsync(bytes);

            while (!createRequest.isDone)
            {
                yield return null;
            }

            _currentBundle = createRequest.assetBundle;
        }

        private static IEnumerator LoadAssetBundleFromFile(string path)
        {
            while (!Caching.ready)
            {
                yield return null;
            }

            AssetBundleCreateRequest createRequest = AssetBundle.LoadFromFileAsync(path);

            while (!createRequest.isDone)
            {
                yield return null;
            }

            _currentBundle = createRequest.assetBundle;
        }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;

                SceneManager.sceneUnloaded += ClearAllLoadedDataOnExit;
            }
            else if (Instance != this)
            {
                Destroy(gameObject);
            }
        }

        private void ClearAllLoadedDataOnExit(UnityEngine.SceneManagement.Scene scene)
        {
            if (_alwaysLoaded.Any(x => x.GetAllScenePaths()[0] == scene.path))
            {
                return;
            }

            if (scene.buildIndex >= 0) // У загружаемых сцен из AssetBundle buildIndex всегда -1
            {
                return;
            }

            var destroyAssetSystem = new DestroyAssetSystem(Contexts.sharedInstance);
            destroyAssetSystem.Execute();

            GameStateData.ClearAllData();
            Resources.UnloadUnusedAssets();
            UnloadAssetBundles(AssetBundle.GetAllLoadedAssetBundles());
            GC.Collect();
        }

        private void UnloadAssetBundles(IEnumerable<AssetBundle> bundles)
        {
            foreach (AssetBundle assetBundle in bundles)
            {
                if (!_alwaysLoaded.Contains(assetBundle))
                {
                    assetBundle.Unload(true);
                }
            }
        }

        public IEnumerator InstantiateGameObjectAsync(RequestAsset requestAsset)
        {
            string path = requestAsset.Uri;
            yield return StartCoroutine(DownloadAssetBundle(path, requestAsset));
            AssetBundle bundle = _currentBundle;
            if (bundle)
            {
                AssetBundleRequest loadAssetAsync = bundle.LoadAssetAsync(requestAsset.AssetName);
                yield return loadAssetAsync;
                var response = new ResponseAsset {Asset = loadAssetAsync.asset, UserData = requestAsset.UserData};
                ((IRequest) requestAsset).OnResponseDone(response);
            }
            else
            {
                string message = "Can not load asset " + requestAsset.AssetName;
                ((IRequest) requestAsset).OnResponseError(message);
            }

            yield return true;
        }

        public IEnumerator InstantiateAssetFromMemoryAsync(RequestLoadAssetFromMemory requestAsset)
        {
            var bytes = requestAsset.Bytes;

            yield return StartCoroutine(LoadAssetBundleFromMemory(bytes));
            AssetBundle bundle = _currentBundle;
            if (bundle)
            {
                AssetBundleRequest loadAssetAsync = bundle.LoadAssetAsync(requestAsset.AssetName);
                yield return loadAssetAsync;
                var response = new ResponseAsset {Asset = loadAssetAsync.asset, UserData = requestAsset.UserData};
                ((IRequest) requestAsset).OnResponseDone(response);
            }
            else
            {
                string message = "Can not load asset " + requestAsset.AssetName;
                ((IRequest) requestAsset).OnResponseError(message);
            }
        }

        public IEnumerator InstantiateAssetFromFileAsync(RequestLoadAssetFromFile requestAsset)
        {
            yield return StartCoroutine(LoadAssetBundleFromFile(requestAsset.Uri));
            AssetBundle bundle = _currentBundle;
            if (bundle)
            {
                AssetBundleRequest loadAssetAsync = bundle.LoadAssetAsync(requestAsset.AssetName);
                yield return loadAssetAsync;
                var response = new ResponseAsset {Asset = loadAssetAsync.asset, UserData = requestAsset.UserData};
                ((IRequest) requestAsset).OnResponseDone(response);
            }
            else
            {
                string message = "Can not load asset " + requestAsset.AssetName;
                ((IRequest) requestAsset).OnResponseError(message);
            }
        }

        public IEnumerator InstantiateSceneFromMemoryAsync(RequestLoadSceneFromMemory requestScene)
        {
            if (_previousScene && string.Equals(requestScene.AssetName, _previousSceneName, StringComparison.CurrentCultureIgnoreCase))
            {
                string path = _previousScene.GetAllScenePaths()[0];
                var response = new ResponseAsset {Asset = _previousScene, Path = path, UserData = requestScene.UserData};
                ((IRequest) requestScene).OnResponseDone(response);
                yield break;
            }

            yield return StartCoroutine(LoadAssetBundleFromMemory(requestScene.Bytes));
            AssetBundle bundle = _currentBundle;

            if (requestScene.AlwaysLoaded)
            {
                if (!_alwaysLoaded.Contains(bundle))
                {
                    _alwaysLoaded.Add(bundle);
                }
            }

            if (bundle)
            {
                string path = bundle.GetAllScenePaths()[0];
                var response = new ResponseAsset {Asset = bundle, Path = path, UserData = requestScene.UserData};
                ((IRequest) requestScene).OnResponseDone(response);

                if (_previousScene)
                {
                    if (!_alwaysLoaded.Contains(_previousScene))
                    {
                        _previousScene.Unload(false);
                    }
                }

                GCManager.Collect();
                _previousScene = bundle;
                _previousSceneName = requestScene.AssetName;
            }
            else
            {
                string message = "Can not load asset " + requestScene.AssetName;
                ((IRequest) requestScene).OnResponseError(message);
            }
        }

        public IEnumerator InstantiateSceneFromFileAsync(RequestLoadSceneFromFile requestScene)
        {
            if (_previousScene && string.Equals(requestScene.AssetName, _previousSceneName, StringComparison.CurrentCultureIgnoreCase))
            {
                string path = _previousScene.GetAllScenePaths()[0];
                var response = new ResponseAsset {Asset = _previousScene, Path = path, UserData = requestScene.UserData};
                ((IRequest) requestScene).OnResponseDone(response);
                yield break;
            }

            yield return StartCoroutine(LoadAssetBundleFromFile(requestScene.Uri));
            AssetBundle bundle = _currentBundle;

            if (requestScene.AlwaysLoaded)
            {
                if (!_alwaysLoaded.Contains(bundle))
                {
                    _alwaysLoaded.Add(bundle);
                }
            }

            if (bundle)
            {
                string path = bundle.GetAllScenePaths()[0];
                var response = new ResponseAsset {Asset = bundle, Path = path, UserData = requestScene.UserData};
                ((IRequest) requestScene).OnResponseDone(response);

                if (_previousScene)
                {
                    if (!_alwaysLoaded.Contains(_previousScene))
                    {
                        _previousScene.Unload(false);
                    }
                }

                GCManager.Collect();
                _previousScene = bundle;
                _previousSceneName = requestScene.AssetName;
            }
            else
            {
                string message = "Can not load asset " + requestScene.AssetName;
                ((IRequest) requestScene).OnResponseError(message);
            }
        }

        public void UnloadPreviousScene()
        {
            if (_previousScene)
            {
                _previousScene.Unload(true);
            }
        }
    }
}