﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using Varwin.Public;
using Varwin.PlatformAdapter;

namespace Varwin.ObjectsInteractions
{
    public class CollisionController : MonoBehaviour
    {
        private bool _forceDestroy;
        [SerializeField]
        private List<Collider> _childColliders;

        [SerializeField]
        private bool _isBlocked;

        private List<CollisionControllerElement> _tempColliders;
        private List<VarwinHighlightEffect> _highlights = new List<VarwinHighlightEffect>();

        private HightLightConfig _defaultHighlightConfig;
        private HightLightConfig _collisionHighlightConfig;
        private HightLightConfig _jointHighlightConfig;

        private InputController _inputController;

        public bool IsBlocked() => _isBlocked && !_jointBehaviour || _isBlocked && _jointBehaviour && !_jointBehaviour.Connecting;

        private float fallDistance = 10f;

        private ControllerInput.ControllerInteractionEventHandler _eventHandler;
        private ControllerInput.ControllerInteractionEventArgs _controllerInteractionEventArgs;
        private Rigidbody[] _allRigidBodies;

        private JointBehaviour _jointBehaviour;
        
        [SerializeField]
        private ChainedJointController _chainedJointController;

        
        public void InitializeController(InputController inputController = null)
        {
            _inputController = inputController;

            if (_inputController != null)
            {
                _defaultHighlightConfig = _inputController.DefaultHighlightConfig;
            }
            else
            {
                var defHighlighter = gameObject.AddComponent<DefaultHighlighter>();
                _defaultHighlightConfig = defHighlighter.HighlightConfig();
            }

            _jointBehaviour = gameObject.GetComponent<JointBehaviour>();

            if (_jointBehaviour)
            {
                _jointBehaviour.OnJointEnter += OnJointEnter;
                _jointBehaviour.OnJointExit += OnJointExit;
            }
            
            _collisionHighlightConfig = DefaultHighlightConfigs.CollisionHighlight;
            _jointHighlightConfig = DefaultHighlightConfigs.JointHighlight;
            
            _tempColliders = new List<CollisionControllerElement>();
            _childColliders = new List<Collider>();

            
            _highlights.Clear();
            var objectsWithColliders = new List<GameObject>();
            var children = new List<Collider>();

            var objectController = _inputController?.ObjectController;
            if (objectController != null && objectController.LockChildren)
            {
                var hierarchyControllers = new List<ObjectController>();
                hierarchyControllers.Add(objectController.LockParent);
                hierarchyControllers.AddRange(objectController.LockParent.Descendants);

                foreach (var child in hierarchyControllers)
                {
                    children.AddRange(child.gameObject.GetComponentsInChildren<Collider>().ToList());
                    AddHighlight(child.gameObject);
                }
            }
            else
            {
                children.AddRange(gameObject.GetComponentsInChildren<Collider>().ToList());
                AddHighlight(gameObject);
            }

            foreach (Collider child in children)
            {
                if (child.isTrigger || !child.enabled)
                {
                    continue;
                }

                _childColliders.Add(child);

                if (!objectsWithColliders.Contains(child.gameObject))
                {
                    objectsWithColliders.Add(child.gameObject);
                }
            }

            foreach (GameObject objectWithCollider in objectsWithColliders)
            {
                CollisionControllerElement triggerObject = CreateTriggersColliders(objectWithCollider);
                triggerObject.OnCollisionEnterDelegate += OnCollide;
                triggerObject.OnTriggerExitDelegate += OnColliderExit;
                _tempColliders.Add(triggerObject);
            }

            ProjectData.GameModeChanged += GameModeChanged;
        }

        private void AddHighlight(GameObject gameObject)
        {
            var highlight = gameObject.GetComponent<VarwinHighlightEffect>();
            
            if (!highlight)
            {
                highlight = gameObject.AddComponent<VarwinHighlightEffect>();
            }
            _highlights.Add(highlight);
        }

        private void Update()
        {
            _getCollidersDebounce.Update();
            _getRigidbodiesDebounce.Update();

            if (_inputController == null)
            {
                return;
            }

            //CheckFalling();

            if (_isBlocked)
            {
                if (_jointBehaviour && _jointBehaviour.IsNearJointPointHighlighted)
                {
                    if (!_inputController.IsDropEnabled())
                    {
                        _inputController.EnableDrop();
                    }

                    return;
                }

                _inputController.DisableDrop();
            }
            else if (!_inputController.IsDropEnabled())
            {
                _inputController.EnableDrop();
                StartCoroutine(DropAfterOneFrame());
            }
        }

        private IEnumerator DropAfterOneFrame()
        {
            yield return new WaitForEndOfFrame();
            _inputController.ForceDropIfNeeded();
        }

        private void OnDestroy()
        {
            if (_childColliders?.Count > 0)
            {
                foreach (Collider childCollider in _childColliders)
                {
                    if (childCollider)
                    {
                        childCollider.enabled = true;
                    }
                }
            }

            if (_tempColliders != null)
            {
                foreach (CollisionControllerElement collisionControllerElement in _tempColliders)
                {
                    if (!collisionControllerElement)
                    {
                        continue;
                    }
                    
                    collisionControllerElement.OnTriggerExitDelegate -= OnColliderExit;
                    collisionControllerElement.OnCollisionEnterDelegate -= OnCollide;

                    if (collisionControllerElement.gameObject)
                    {
                        Destroy(collisionControllerElement.gameObject);
                    }
                }
            }

            _inputController?.EnableDrop();
            UnsubscribeFromJointControllerEvents(_chainedJointController);

            ProjectData.GameModeChanged -= GameModeChanged;
        }

        public void ForceDestroy()
        {
            _forceDestroy = true;
            DestroyImmediate(this);
        }

        private void OnEnable()
        {
            if (_childColliders?.Count > 0)
            {
                foreach (Collider childCollider in _childColliders)
                {
                    if (childCollider)
                    {
                        childCollider.enabled = false;
                    }
                }
            }
            
            if (_tempColliders == null)
            {
                return;
            }

            foreach (CollisionControllerElement element in _tempColliders)
            {
                if (!element)
                {
                    continue;
                }
                
                element.OnCollisionEnterDelegate += OnCollide;
                element.OnTriggerExitDelegate += OnColliderExit;
            }
        }

        private void OnDisable()
        {
            if (_childColliders?.Count > 0)
            {
                foreach (Collider childCollider in _childColliders)
                {
                    if (childCollider)
                    {
                        childCollider.enabled = true;
                    }
                }
            }
            
            if (_tempColliders == null)
            {
                return;
            }

            foreach (CollisionControllerElement element in _tempColliders)
            {
                if (!element)
                {
                    continue;
                }
                
                element.OnCollisionEnterDelegate -= OnCollide;
                element.OnTriggerExitDelegate -= OnColliderExit;
            }

            Unblock();
        }

        public void SubscribeToJointControllerEvents(ChainedJointController controller)
        {
            _chainedJointController = controller;
            _chainedJointController.OnCollisionEnter += CollisionEnterCheck;
            _chainedJointController.OnCollisionExit += CollisionExitCheck;
        }

        public void UnsubscribeFromJointControllerEvents(ChainedJointController controller)
        {
            _chainedJointController = null;
            
            if (!controller)
            {
                return;
            }
            
            controller.OnCollisionEnter -= CollisionEnterCheck;
            controller.OnCollisionExit -= CollisionExitCheck;
        }

        private void GameModeChanged(GameMode gm)
        {
            Unblock();

            if (_inputController.ControllerEvents == null)
            {
                return;
            }

            _inputController.EnableDrop();
            StartCoroutine(DropAfterOneFrame());
            _inputController.ControllerEvents.OnGripReleased(_controllerInteractionEventArgs);
        }

        private CollisionControllerElement CreateTriggersColliders(GameObject originalColliderHolder)
        {
            GameObject collidersContainer = new GameObject("TempCollidersContainer");
            collidersContainer.transform.parent = originalColliderHolder.transform;
            collidersContainer.transform.localPosition = Vector3.zero;
            collidersContainer.transform.localRotation = Quaternion.identity;
            collidersContainer.transform.localScale = Vector3.one;

            Collider[] colliders = originalColliderHolder.GetComponents<Collider>();

            foreach (Collider collider in colliders)
            {
                if (collider is CharacterController)
                {
                    CharacterController characterController = collider as CharacterController;
                    CapsuleCollider newCollider = collidersContainer.AddComponent<CapsuleCollider>();

                    newCollider.direction = 1;
                    newCollider.center = characterController.center;
                    newCollider.radius = characterController.radius;
                    newCollider.height = characterController.height;

                    newCollider.isTrigger = true;

                    if (ProjectData.GameMode == GameMode.Edit)
                    {
                        collider.enabled = false;
                    }
                    
                    continue;
                }
                
                if (!collider.isTrigger)
                {
                    Collider newCollider = DuplicateComponent(collider, collidersContainer);

                    if (newCollider.GetType() == typeof(MeshCollider))
                    {
                        MeshCollider meshCollider = (MeshCollider) newCollider;
                        meshCollider.convex = true;
                    }

                    newCollider.isTrigger = true;

                    if (ProjectData.GameMode == GameMode.Edit)
                    {
                        collider.enabled = false;
                    }
                }
            }

            CollisionControllerElement element = collidersContainer.AddComponent<CollisionControllerElement>();

            return element;
        }

        private T DuplicateComponent<T>(T original, GameObject destination) where T : Component
        {
            Type type = original.GetType();
            T dst = destination.AddComponent(type) as T;
            var fields = type.GetFields();

            foreach (FieldInfo field in fields)
            {
                if (field.IsStatic)
                {
                    continue;
                }

                field.SetValue(dst, field.GetValue(original));
            }

            var props = type.GetProperties();

            foreach (PropertyInfo prop in props)
            {
                if (!prop.CanWrite || !prop.CanRead || prop.Name == "name")
                {
                    continue;
                }

                prop.SetValue(dst, prop.GetValue(original, null), null);
            }

            return dst;
        }

        private void OnCollide(Collider other)
        {
            WakeUpCollidedRigidbody(other);
            
            if (_chainedJointController)
            {
                _chainedJointController.CollisionEnter(other);
                return;
            }

            CollisionEnterCheck(other);
        }

        private void WakeUpCollidedRigidbody(Collider other)
        {
            var otherRigidbody = other.GetComponentInParent<Rigidbody>();
            if (otherRigidbody)
            {
                otherRigidbody.WakeUp();
            }
        }

        private void OnColliderExit(Collider other)
        {
            if (_chainedJointController)
            {
                _chainedJointController.CollisionExit(other);

                return;
            }

            CollisionExitCheck(other);
        }

        private void CollisionExitCheck(Collider other)
        {
            Unblock();
        }

        private void CollisionEnterCheck(Collider other)
        {
            GetObjectColliders();

            if(_childColliders.Contains(other))
            {
                return;
            }

            if (other.CompareTag("Player"))
            {
                return;
            }

            Rigidbody otherBody = other.attachedRigidbody;

            if (otherBody)
            {
                if (other.isTrigger)
                {
                    return;
                }

                if (otherBody.isKinematic)
                {
                    Block();

                    return;
                }

                var otherJointBehaviour = otherBody.GetComponentInParent<JointBehaviour>();

                if (!otherJointBehaviour || !_jointBehaviour)
                {
                    return;
                }

                if (!otherJointBehaviour.IsGrabbed || !_jointBehaviour.IsGrabbed)
                {
                    return;
                }

                CollisionController otherCollisionController = otherJointBehaviour.CollisionController;

                if (!otherCollisionController)
                {
                    return;
                    //Debug.Break();
                }

                if (_chainedJointController
                    && otherCollisionController._chainedJointController
                    && _chainedJointController == otherCollisionController._chainedJointController)
                {
                    return;
                }

                if (!otherCollisionController._isBlocked)
                {
                    Block();
                }
            }
            else if (!other.isTrigger)
            {
                Block();
            }
        }

        /// <summary>
        /// Подсвечиваем и убираем коллайдеры
        /// </summary>
        private void Block()
        {
            if (_isBlocked)
            {
                return;
            }

            TurnCollidersOn(false);

            foreach (var highlight in _highlights)
            {
                highlight.SetHighlightEnabled(true);
                if (_chainedJointController)
                {
                    highlight.SetConfiguration(_chainedJointController.Connecting ? _jointHighlightConfig : _collisionHighlightConfig, null, false);
                }
                else if (_jointBehaviour)
                {
                    highlight.SetConfiguration(_jointBehaviour.Connecting ? _jointHighlightConfig : _collisionHighlightConfig, null, false);
                }
                else
                {
                    highlight.SetConfiguration(_collisionHighlightConfig, null, false);
                }
            }

            _jointFlag = gameObject.GetComponent<JointBehaviour>();
            
            if (_inputController?.ControllerEvents != null
                && (ProjectData.GameMode == GameMode.Edit || _jointFlag)
                && !_isBlocked)
            {
                _inputController.ControllerEvents.GripPressed += OnGripPressed;
            }

            _isBlocked = true;
        }

        private bool _jointFlag;

        public void OnGripPressed(object sender, ControllerInput.ControllerInteractionEventArgs e)
        {
            _controllerInteractionEventArgs = e;

            if (_inputController != null
                && (ProjectData.GameMode == GameMode.Edit || _jointFlag)
                && _isBlocked)
            {
                _jointFlag = false;
                _inputController.ControllerEvents.GripPressed -= OnGripPressed;

                Unblock();

                _inputController.EnableDrop();
                
                if (_jointBehaviour)
                {
                    var connectedJoints = _jointBehaviour.GetAllConnectedJoints();
                    foreach (JointBehaviour connectedJoint in connectedJoints)
                    {
                        connectedJoint.CollisionController._inputController.EnableDrop();
                    }
                }
                
                StartCoroutine(DropAfterOneFrame());
                _inputController.ReturnPosition();
                _inputController.ControllerEvents.OnGripReleased(e);
            }
        }

        public void ForcedUnblock()
        {
            Unblock();
        }

        /// <summary>
        /// Рассвечиваем и возвращаем коллайдеры
        /// </summary>
        private void Unblock()
        {
            if (ProjectData.GameMode != GameMode.Edit)
            {
                TurnCollidersOn(true);
            }

            foreach (var highlight in _highlights)
            {
                if (!highlight)
                {
                    continue;
                }
                
                if (_jointBehaviour && _jointBehaviour.Connecting || _chainedJointController && _chainedJointController.Connecting)
                {
                    highlight.SetHighlightEnabled(true);
                    highlight.SetConfiguration(_jointHighlightConfig, null, false);
                }
                else
                {
                    highlight.SetHighlightEnabled(false);
                }
            }

            _isBlocked = false;

            if (_inputController?.ControllerEvents != null && ProjectData.GameMode == GameMode.Edit)
            {
                _inputController.ControllerEvents.GripPressed -= OnGripPressed;
            }
        }

        private void TurnCollidersOn(bool mode)
        {
            foreach (Collider childCollider in _childColliders)
            {
                if (childCollider)
                {
                    childCollider.enabled = mode;
                }
            }
        }

        private Debounce _getCollidersDebounce = new Debounce(0.5f);
        private Debounce _getRigidbodiesDebounce = new Debounce(0.5f);

        private void GetObjectColliders()
        {
            //In chained object it will be too much of a pain to collect them on every event so just don't
            if (!_getCollidersDebounce.CanReset())
            {
                return;
            }

            _getCollidersDebounce.Reset();
        }

        private void GetObjectRigidbodies()
        {
            //Same goes for this one
            if (!_getRigidbodiesDebounce.CanReset())
            {
                return;
            }

            _getRigidbodiesDebounce.Reset();

            _allRigidBodies = GetComponentsInChildren<Rigidbody>(true);
        }

        /// <summary>
        /// TODO: need to check where this is needed
        /// </summary>
        private void CheckFalling()
        {
            GameObject controller = _inputController.ControllerEvents?.GetController();

            if (!controller)
            {
                return;
            }

            Vector3 controllerPosition = controller.transform.position;

            float distance = Vector3.Distance(controllerPosition, transform.position);

            if (!(distance > fallDistance))
            {
                return;
            }

            GetObjectRigidbodies();

            foreach (Rigidbody rb in _allRigidBodies)
            {
                rb.isKinematic = true;
                rb.useGravity = false;
            }

            transform.position =
                new Vector3(controllerPosition.x, controllerPosition.y + 0.5f, controllerPosition.z);
            Unblock();

            foreach (Rigidbody rb in _allRigidBodies)
            {
                rb.ResetInertiaTensor();
                rb.isKinematic = false;
                rb.useGravity = true;
            }

            Destroy(this);
        }

        private void OnJointEnter(JointPoint senderJoint, JointPoint nearJoint)
        {
            if (_chainedJointController)
            {
                _chainedJointController.JointEnter(senderJoint, nearJoint);
                return;
            }
            
            JointEnter(senderJoint, nearJoint);
        }

        private void OnJointExit(JointPoint senderJoint, JointPoint nearJoint)
        {
            if (_chainedJointController)
            {
                 _chainedJointController.JointExit(senderJoint, nearJoint);
                 return;
            }
            
            JointExit(senderJoint, nearJoint);
        }
        
        public void JointEnter(JointPoint senderJoint, JointPoint nearJoint)
        {
            if (_highlights == null)
            {
                return;
            }

            foreach (var highlight in _highlights)
            {
                highlight.SetHighlightEnabled(true);
                highlight.SetConfiguration(_jointHighlightConfig, null, false);
            }
        }

        public void JointExit(JointPoint senderJoint, JointPoint nearJoint)
        {
            if (_highlights == null)
            {
                return;
            }

            foreach (var highlight in _highlights)
            {
                if (_isBlocked && (!_jointBehaviour.Connecting || _chainedJointController && !_chainedJointController.Connecting))
                {
                    highlight.SetHighlightEnabled(true);
                    highlight.SetConfiguration(_collisionHighlightConfig, null, false);
                }
                else
                {
                    highlight.SetHighlightEnabled(false);
                }
            }
        }
    }
}
