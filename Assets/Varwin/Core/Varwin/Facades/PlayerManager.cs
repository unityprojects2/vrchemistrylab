using System;
using JetBrains.Annotations;
using UnityEngine;
using Varwin.PlatformAdapter;
using Object = UnityEngine.Object;

namespace Varwin
{
    [UsedImplicitly]
    public static class PlayerManager
    {
        public static event Action PlayerRespawned;

        [Obsolete("Не использовать, будет удалено")]
        public static bool GravityFreeze = false;
        public static bool UseGravity = true;
        public static float FallingTime;
        public static bool WasdMovementEnabled = true;

        public static bool IsInteractable
        {
            get => ProjectData.InteractionWithObjectsLocked;
            set => ProjectData.InteractionWithObjectsLocked = !value;
        }
        
        /// <summary>
        /// Height at which player will be respawned
        /// </summary>
        public static float RespawnHeight = -1000f;

        public static IPlayerController CurrentRig => InputAdapter.Instance.PlayerController;
        
        public static GameObject Avatar => null;

        public static void TeleportTo(Vector3 position)
        {
            CurrentRig.SetPosition(position);
        }

        public static void SetTeleportEnabled(bool isEnabled)
        {
            TeleportPointer.TeleportEnabled = isEnabled;
        }

        public static void SetDesktopWasdMovementEnabled(bool isEnabled)
        {
            WasdMovementEnabled = isEnabled;
        }
        
        public static void Respawn()
        {
            var anchor = Object.FindObjectOfType<PlayerAnchorManager>();
            if (anchor)
            {
                anchor.SetPlayerPosition();
                PlayerRespawned?.Invoke();
            }
            FallingTime = 0;
        }
    }
}