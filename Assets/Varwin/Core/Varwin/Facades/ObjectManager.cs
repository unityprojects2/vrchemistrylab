﻿using System.Linq;
using JetBrains.Annotations;
using UnityEngine;
using Varwin.Public;

namespace Varwin
{
    [UsedImplicitly]
    public static class ObjectManager
    {
        public static GameObject Instantiate(GameObject original, Transform parent = null, bool worldPositionStays = false)
        {
            GameObject gameObject = Object.Instantiate(original, parent, worldPositionStays);
            
            InitializeInputControl(gameObject);

            return gameObject;
        }
        
        private static void InitializeInputControl(GameObject gameObject)
        {
            var behaviours = gameObject.GetComponentsInChildren<MonoBehaviour>(true).Where(x => x is IVarwinInputAware);
            
            foreach (var monoBehaviour in behaviours)
            {
                var objectId = monoBehaviour.GetComponent<ObjectId>() ?? 
                               monoBehaviour.gameObject.AddComponent<ObjectId>();
            
                objectId.Id = objectId.GetInstanceID();
                gameObject.GetWrapper()?.GetObjectController()?.AddInputControl(monoBehaviour, objectId, out bool haveInputControl);
            }
        }
    }
}