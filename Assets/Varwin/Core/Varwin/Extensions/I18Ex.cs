using System;
using System.Linq;
using System.Reflection;
using UnityEngine;
using Varwin;

public static class I18nEx
{
    public static string LocalizedString(this I18n self)
    {
        string lang = Settings.Instance.Language;
        Type localeType = typeof(I18n);
        string value = localeType.GetProperty(lang)?.GetValue(self)?.ToString();

        if (!string.IsNullOrEmpty(value))
        {
            return value;
        }

        if (!string.IsNullOrEmpty(self.en))
        {
            value = self.en;
        }
        else
        {
            var properties = localeType.GetProperties();

            value = properties.FirstOrDefault(x => x.CanRead && string.IsNullOrEmpty(x.GetValue(self)?.ToString()))
                ?.ToString();
        }

        return value ?? string.Empty;
    }

    public static void SetLocale(this I18n self, string lang, string value)
    {
        typeof(I18n).GetProperty(lang)?.SetValue(self, value);
    }

    public static void SetLocale(this ILocalizable self, string lang, string value)
    {
        if (self.i18n == null)
        {
            self.i18n = new I18n();
        }

        self.i18n.SetLocale(lang, value);
    }

    public static string GetCurrentLocale(this I18n self)
    {
        string result = string.Empty;

        PropertyInfo property = typeof(I18n).GetProperty(Settings.Instance.Language);

        if (property == null)
        {
            return result;
        }

        try
        {
            result = property.GetValue(self).ToString();
        }
        catch (Exception e)
        {
            Debug.Log($"Exception was raised while getting localized strings; will try to fallback to EN\n{e.Message}\n{e.StackTrace}");
            result = !string.IsNullOrEmpty(self.en) ? self.en : "ERROR: NO VALID STRING";
        }

        return result;
    }
}
