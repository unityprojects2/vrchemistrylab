using System.CodeDom;
using System.CodeDom.Compiler;
using System.IO;

public static class StringEx
{
    public static string SubstringAfter(this string str, string value)
    {
        int index = str.IndexOf(value);
        if (index < 0)
        {
            return str;
        }
        return str.Substring(index, str.Length - index);
    }
    
    public static string SubstringAfterLast(this string str, string value)
    {
        int index = str.LastIndexOf(value);
        if (index < 0)
        {
            return str;
        }
        return str.Substring(index, str.Length - index);
    }
    
    public static string SubstringBefore(this string str, string value)
    {
        return str.Substring(0, str.IndexOf(value));
    }
    
    public static string SubstringBeforeLast(this string str, string value)
    {
        return str.Substring(0, str.LastIndexOf(value));
    }
    
    public static string ToEscaped(this string input)
    {
#if NET_STANDARD_2_0
        return input.Replace("\"", "\\\"");
#else
        using (var writer = new StringWriter())
        {
            using (var provider = CodeDomProvider.CreateProvider("CSharp"))
            {
                provider.GenerateCodeFromExpression(new CodePrimitiveExpression(input), writer, null);
                var escapedString = writer.ToString();
                string escapedStringWithoutWrappingQuotes = escapedString.Substring(1, escapedString.Length - 2);
                return escapedStringWithoutWrappingQuotes;
            }
        }
#endif
    }
}