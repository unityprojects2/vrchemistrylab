using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using UnityEngine;

namespace Varwin.Data.ServerData
{
    public class ResourceDto : IJsonSerializable
    {
        public int Id { get; set; }
        public string Guid { get; set; }
        public I18n Name { get; set; }
        
        public I18n Description { get; set; }
        
        public int Usages { get; set; }
        public string Path { get; set; }
        public string Resources { get; set; }

        public string Preview => Resources + "/preview.png";
        
        public string Format { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        [JsonIgnore] public bool OnDemand { get; set; }
        [JsonIgnore] public bool ForceLoad { get; set; }

        [JsonIgnore] public TextureFormat? TextureFormat { get; set; } 
        
        public string GetLocalizedName()
        {
            return Name.GetCurrentLocale();
        }
        
        public string GetLocalizedDescription()
        {
            return Description.GetCurrentLocale();
        }
    }

    public static class ResourceDtoEx
    {
        /// <summary>
        /// Id 1 = png, Id 2 = jpeg
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static bool IsPicture(this ResourceDto self) => ResourceFormatCodes.ImageFormats.Contains(self.Format);

        /// <summary>
        /// Id 3 = txt 
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static bool IsText(this ResourceDto self) => ResourceFormatCodes.TextFormats.Contains(self.Format);

        public static bool IsModel(this ResourceDto self) => ResourceFormatCodes.ModelFormats.Contains(self.Format);
        
        public static bool IsAudio(this ResourceDto self) => ResourceFormatCodes.AudioFormats.Contains(self.Format);
        
        public static bool IsVideo(this ResourceDto self) => ResourceFormatCodes.VideoFormats.Contains(self.Format);
    }

    public static class ResourceFormatCodes
    {
        public static readonly IEnumerable<string> TextFormats = new List<string>
        {
            "txt"
        };

        public static readonly IEnumerable<string> ImageFormats = new List<string>
        {
            "png",
            "jpg"
        };

        public static readonly IEnumerable<string> ModelFormats =new List<string>
        {
            "fbx",
            "obj",
            "dae",
            "glb",
            "gltf"        
        };
        
        public static readonly IEnumerable<string> AudioFormats = new List<string>
        {
            "aif",
            "ogg",
            "wav",
        };
        
        public static readonly IEnumerable<string> VideoFormats = new List<string>
        {
            "mp4",
            "mov",
            "webm",
            "wmv",
        };
        
        public static readonly IEnumerable<string> AllFormats =new List<string>
        {
            // text
            "txt",
            // image
            "png",
            "jpg",
            // model
            "fbx",
            "obj",
            "dae",
            "glb",
            "gltf",
            // audio
            "aif",
            "ogg",
            "wav",
            // video
            "mp4",
            "mov",
            "webm",
            "wmv",
        };
    }
    
    public enum ResourceRequestType
    {
        All,
        Image,
        TextFile,
        Model,
        Audio,
        Video
    }
}