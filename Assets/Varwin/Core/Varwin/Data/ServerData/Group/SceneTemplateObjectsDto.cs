﻿using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

namespace Varwin.Data.ServerData
{
    public class SceneTemplateObjectsDto : IJsonSerializable
    {
        public int SceneId { get; set; }
        public List<SceneObjectDto> SceneObjects { get; set; }
        public CustomSceneData SceneData { get; set; }
        public List<ObjectBehavioursData> ObjectBehaviours { get; set; }
    }

    public class CustomSceneData
    {
        public Vector3 CameraSpawnPosition;
        public Quaternion CameraSpawnRotation;
        public Dictionary<int, bool> HierarchyExpandStates;
        public HashSet<string> OnDemandedResourceGuids;
    }
    
    public class SceneObjectDto : IJsonSerializable
    {
        /// <summary>
        /// Server Id
        /// </summary>
        public int Id { get; set; }

        public int InstanceId { get; set; }
        public string Name { get; set; }
        [JsonIgnore] public int? ParentId { get; set; }
        public int ObjectId { get; set; }
        public Collaboration Collaboration { get; set; }
        public List<ResourceDto> Resources { get; set; }
        public ObjectData Data { get; set; }
        public List<SceneObjectDto> SceneObjects { get; set; }
        public bool DisableSceneLogic { get; set; }
    }

    public class ObjectBehavioursData : IJsonSerializable
    {
        public int ObjectId { get; set; }
        public List<string> Behaviours { get; set; }
    }
}