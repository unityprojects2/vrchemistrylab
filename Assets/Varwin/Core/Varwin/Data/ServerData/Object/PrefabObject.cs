﻿// ReSharper disable once CheckNamespace

using System;

namespace Varwin.Data
{
    class PrefabObjectContainer : IJsonSerializable
    {
        public PrefabObject Node { get; set; }
    }
    
    public class PrefabObject : IJsonSerializable
    {
        public static string EndCursor { get; set; }
        
        public int Id { get; set; }
        public string Guid { get; set; }
        public string RootGuid { get; set; }
        public I18n Name { get; set; }
        public bool Embedded { get; set; }
        public Config Config { get; set; }
        public string Resources { get; set; }
        public string Assets { get; set; }

        public string ResourcesPath => string.IsNullOrEmpty(Resources) ? Assets : Resources;
        
        public string BundleResource => ResourcesPath + "/bundle";
        public string AndroidBundleResource => ResourcesPath + "/android_bundle";        
        public string BundleManifest => ResourcesPath + "/bundle.manifest";
        public string AndroidBundleManifest => ResourcesPath + "/android_bundle.manifest";
        public string ConfigResource => ResourcesPath + "/bundle.json";
        public string IconResource => ResourcesPath + "/bundle.png";

        public string GetLocalizedName()
        {
            return Name.GetCurrentLocale();
        }
    }
}
