﻿using System.Collections;
using UnityEngine;

namespace Varwin
{
    public class UIFadeInOutController : MonoBehaviour
    {
        public static UIFadeInOutController Instance
        {
            get
            {
                if (!_instance)
                {
                    _instance = FindObjectOfType<UIFadeInOutController>();
                }

                return _instance;
            }
        }

        private static UIFadeInOutController _instance;

        public float FadeInDuration = 0.66f;
        public float FadeOutDuration = 0.33f;

        public FadeInOutStatus FadeStatus { get; protected set; }

        public float Alpha
        {
            get
            {
                if (!_canvasGroup)
                {
                    _canvasGroup = GetComponentInChildren<CanvasGroup>();
                }

                return _canvasGroup.alpha;
            }
        }

        public bool IsComplete => FadeStatus == FadeInOutStatus.FadingInComplete || FadeStatus == FadeInOutStatus.FadingOutComplete;

        private CanvasGroup _canvasGroup;
        private Canvas _canvas;

        protected float _fadeInSpeed;
        protected float _fadeOutSpeed;

        private void Start()
        {
            if (_instance && _instance != this)
            {
                Destroy(gameObject);
                return;
            }

            _instance = this;

            if (!_canvasGroup)
            {
                _canvasGroup = GetComponentInChildren<CanvasGroup>();
            }

            if (!_canvas)
            {
                _canvas = GetComponentInChildren<Canvas>();
            }

            InstantFadeIn();
            StartCoroutine(SetFadeStatus(FadeInOutStatus.FadingOut));
            CalculateSpeeds();
        }

        private void Reset()
        {
            _canvasGroup = GetComponentInChildren<CanvasGroup>();
            _canvas = GetComponentInChildren<Canvas>();
        }

        private void LateUpdate()
        {
            if (FadeStatus == FadeInOutStatus.None || FadeStatus == FadeInOutStatus.FadingInComplete || FadeStatus == FadeInOutStatus.FadingOutComplete)
            {
                return;
            }

            if (!_canvasGroup)
            {
                return;
            }

            if (FadeStatus == FadeInOutStatus.FadingIn)
            {
                _canvasGroup.alpha += _fadeInSpeed * Time.smoothDeltaTime;

                _canvas.gameObject.SetActive(true);

                if (_canvasGroup.alpha >= 1f - Mathf.Epsilon)
                {
                    _canvasGroup.alpha = 1f;
                    FadeStatus = FadeInOutStatus.FadingInComplete;
                }
            }
            else if (FadeStatus == FadeInOutStatus.FadingOut)
            {
                _canvasGroup.alpha -= _fadeOutSpeed * Time.smoothDeltaTime;

                if (_canvasGroup.alpha <= 0f + Mathf.Epsilon)
                {
                    _canvas.gameObject.SetActive(false);
                    _canvasGroup.alpha = 0f;
                    FadeStatus = FadeInOutStatus.FadingOutComplete;
                }
            }
        }

        public virtual void FadeIn()
        {
            FadeStatus = FadeInOutStatus.None;
            StartCoroutine(SetFadeStatus(FadeInOutStatus.FadingIn));
            CalculateSpeeds();
        }

        public virtual void FadeOut()
        {
            FadeStatus = FadeInOutStatus.None;
            StartCoroutine(SetFadeStatus(FadeInOutStatus.FadingOut));
            CalculateSpeeds();
        }

        private IEnumerator SetFadeStatus(FadeInOutStatus status)
        {
            yield return null;
            FadeStatus = status;
        }

        public void InstantFadeIn()
        {
            _canvasGroup.alpha = 1f;
            _canvas.gameObject.SetActive(true);
            FadeStatus = FadeInOutStatus.FadingInComplete;
        }

        public void InstantFadeOut()
        {
            _canvasGroup.alpha = 0f;
            _canvas.gameObject.SetActive(false);
            FadeStatus = FadeInOutStatus.FadingOutComplete;
        }

        private void CalculateSpeeds()
        {
            _fadeInSpeed = Mathf.Abs(FadeInDuration) > Mathf.Epsilon ? 1f / FadeInDuration : float.MaxValue;
            _fadeOutSpeed = Mathf.Abs(FadeOutDuration) > Mathf.Epsilon ? 1f / FadeOutDuration : float.MaxValue;
        }
    }

    public enum FadeInOutStatus
    {
        None = 0,
        FadingIn,
        FadingInComplete,
        FadingOut,
        FadingOutComplete
    }
}