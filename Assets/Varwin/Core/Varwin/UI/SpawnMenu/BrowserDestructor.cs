﻿using System.Collections;
using UnityEngine;
#if VARWINCLIENT && !PLATFORM_ANDROID
using ZenFulcrum.EmbeddedBrowser;
#endif

namespace Varwin
{
    public class BrowserDestructor : MonoBehaviour
    {
        public static BrowserDestructor Instance;
#if VARWINCLIENT && !PLATFORM_ANDROID
        private VRMainControlPanel _vrMainControlPanel;
#endif

        private void Awake()
        {
            Instance = this;
        }

#if VARWINCLIENT && !PLATFORM_ANDROID
        public void Init(VRMainControlPanel panel)
        {
            _vrMainControlPanel = panel;
        }

        public void DestroyPad(VRBrowserPanel pane)
        {
            StartCoroutine(DestroyBrowser(pane));
        }

        private IEnumerator DestroyBrowser(VRBrowserPanel pane)
        {
            if (_vrMainControlPanel == null)
            {
                yield break;
            }
            
            //drop the pane and destroy it
            _vrMainControlPanel.allBrowsers.Remove(pane);
            if (!pane) yield break;

            var t0 = Time.time;
            while (Time.time < t0 + 3)
            {
                if (!pane) yield break;
                _vrMainControlPanel.MoveToward(pane.transform, Vector3.zero);
                yield return null;
            }

            Destroy(pane.gameObject);
        }
#endif
    }
}
