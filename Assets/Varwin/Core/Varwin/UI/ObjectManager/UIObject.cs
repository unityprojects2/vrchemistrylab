﻿using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Varwin.UI.ObjectManager
{
    public class UIObject: MonoBehaviour
    {
        public Text ItemNameText;
        public Text ServerIdText;
        public Text GroupIdText;
        public Text ComlexIdText;
        public Text DeleteText;
        public UIDeleteButton UiDeleteButton;
        public TextMeshProUGUI TypeText;
        private GameEntity _gameEntity;
        private Transform _parentTransform;
        private ObjectController _objectController; 
        private float _height;

        public void Init(Transform parent, ObjectController objectController)
        {
            _objectController = objectController;
            _gameEntity = objectController.Entity;
            _parentTransform = parent;
            objectController.UiObject = this;
            MoveUi();
            UiDeleteButton.Init(objectController);
            _height = CalculateHeight();
            gameObject.SetActive(false);

            if (Settings.Instance.OnboardingMode)
            {
                UiDeleteButton.gameObject.SetActive(false);
                DeleteText.gameObject.SetActive(false);
            }
        }

        private float CalculateHeight()
        {
            var children = _parentTransform.gameObject.GetComponentsInChildren<Renderer>();
            var heights = children.Select(child => child.bounds.size.y).ToList();

            return heights.Count > 0 ? heights.Max() : 0;
        }

        public ObjectController GetBaseType() => _objectController;

        public void MoveUi()
        {
            if (_gameEntity.gameObject == null)
            {
                return;
            }

            try
            {
                Vector3 position = _objectController.gameObject.transform.position;
                gameObject.transform.position = position + _height * Vector3.up;
            }
            catch (Exception e)
            {
                Debug.Log($"Can't move UI Object: {e.Message}");
            }
        }

        public void Update()
        {
            if (_gameEntity == null)
            {
                return;
            }

            if (_gameEntity.id.Value == 0)
            {
                gameObject.SetActive(false);
            }

            ItemNameText.text = $"{_gameEntity.name.Value}";
            ServerIdText.text = $"server_id = {_gameEntity.idServer.Value}";
            TypeText.text = _objectController.GetLocalizedName();
            //GroupIdText.text = $"G:{_objectController.IdGroup}";
            ComlexIdText.text = $"Free";
            MoveUi();
            gameObject.transform.LookAt(GameObjects.Instance.Head);
        }
    }

    
}
