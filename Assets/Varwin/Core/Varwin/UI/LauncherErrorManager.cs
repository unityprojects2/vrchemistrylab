﻿using SmartLocalization;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Varwin.Data;
using Varwin.PUN;

namespace Varwin.UI
{
    // ReSharper disable once InconsistentNaming
    public class LauncherErrorManager : MonoBehaviour
    {
        public GameObject Panel;
        public GameObject LoadAnim;
        public TMP_Text MessageHeader;
        public TMP_InputField MessageText;
        public TMP_Text Feedback;
        public Button CopyErrorButton;
        private bool fatalError;
        public static LauncherErrorManager Instance;
        public TMP_Text LicensedTo;

        public UnityEvent showedError;

        private string _lastErrorLocalizedKey;

        private void Awake()
        {
            Instance = this;
            Panel.SetActive(false);
            
            LanguageManager.Instance.OnChangeLanguage += OnChangeLanguage;
            CopyErrorButton.onClick.AddListener(CopyError);
        }

        private void OnDestroy()
        {
            LanguageManager.Instance.OnChangeLanguage -= OnChangeLanguage;
            CopyErrorButton.onClick.RemoveAllListeners();
        }

        public void Show(string message)
        {
#if !UNITY_ANDROID
            Feedback.text = "";
#endif
            MessageHeader.text = message;
            Panel.SetActive(true);
            LoadAnim.SetActive(false);
            
            showedError?.Invoke();
        }

        public void ShowFatal(string message, string details)
        {
#if !UNITY_ANDROID
            Feedback.text = "";
#endif
            MessageHeader.text = message;
            MessageText.text = details;
            fatalError = true;
            //ToDo retry rename to send and action change to send
            Panel.SetActive(true);
            LoadAnim.SetActive(false);
            
            showedError?.Invoke();
        }

        public void ShowFatalErrorKey(string errorLocalizedKey, string details)
        {
            _lastErrorLocalizedKey = errorLocalizedKey;
            ShowFatal(LanguageManager.Instance.GetTextValue(_lastErrorLocalizedKey), details);
        }

        private void Hide()
        {
            Panel.SetActive(false);
            LoadAnim.SetActive(true);
            _lastErrorLocalizedKey = null;
        }

        public void ReTryOrSendReport()
        {
            if (fatalError)
            {
                Hide();
                ProjectDataListener.Instance.ForceReload();
                Launcher.Instance?.Init();
            }
        }

        public void License(License license)
        {
            string user;

            if (string.IsNullOrEmpty(license.Company))
            {
                user = license.FirstName + " " + license.LastName;
            }
            else
            {
                user = license.Company;
            }
            
            LicensedTo.gameObject.SetActive(true);
            LicensedTo.text = $"Licensed to {user}\n<size=19><color=#000b>{license.EditionId} Edition</color></size>";
        }

        public void Exit()
        {
            Application.Quit();
        }

        private void OnChangeLanguage(LanguageManager languageManager)
        {
            if (!string.IsNullOrEmpty(_lastErrorLocalizedKey))
            {
                MessageHeader.text = LanguageManager.Instance.GetTextValue(_lastErrorLocalizedKey);
            }
        }

        private void CopyError()
        {
            var textEditor = new TextEditor();
            textEditor.text = MessageText.text;
            textEditor.SelectAll();
            textEditor.Copy();
        }
    }
}
