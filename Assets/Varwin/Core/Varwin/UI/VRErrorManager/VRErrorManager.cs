﻿using System;
using SmartLocalization;
using SmartLocalization.Editor;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Varwin.PlatformAdapter;

namespace Varwin.UI.VRErrorManager
{
    /// <summary>
    /// Temp class! Wil be remaked!
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public class VRErrorManager : MonoBehaviour
    {
        private const float Duration = 5f;
        private bool fatalError = false;
        
        public GameObject Panel;
        public TMP_Text Header;
        public TMP_InputField Message;
        public static VRErrorManager Instance;
        public bool IsShowing => Panel.activeSelf;

        private void Awake()
        {
            Instance = this;
        }

        public void Show(string message, Action retry = null)
        {
            NotificationWindowManager.Show(message, Duration);
        }

        public void UpdateMessage(string message)
        {
            NotificationWindowManager.UpdateMessage(message);
        }

        public void ShowFatal(string message, string stackTrace = "")
        {
            Helper.HideUi();
            fatalError = true;
            Header.text = message;
            Message.text = stackTrace;
            Panel.SetActive(true);
            
            InputAdapter.Instance.PointerController.IsMenuOpened = true;
        }

        public void Hide()
        {
            Panel.SetActive(false);
            InputAdapter.Instance.PointerController.IsMenuOpened = false;
        }
    }
}
