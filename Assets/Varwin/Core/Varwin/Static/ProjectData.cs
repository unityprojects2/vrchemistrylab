﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLog;
using Photon.Pun;
using UnityEngine;
using Varwin.Data;
using Varwin.Data.ServerData;
using Varwin.PlatformAdapter;
using Varwin.WWW;

namespace Varwin
{

    public static class ProjectData
    {
        /// <summary>
        /// Current project id
        /// </summary>
        public static int ProjectId { get; private set; }

        /// <summary>
        /// Current scene id
        /// </summary>
        public static int SceneId { get; private set; }
        
        public static bool IsRichSceneTemplate { get; set; }
        
        /// <summary>
        /// Current scene template id
        /// </summary>
        public static int SceneTemplateId => ProjectStructure.Scenes.GetSceneTemplateId(SceneId);

        /// <summary>
        /// Current project configuration id
        /// </summary>
        public static int ProjectConfigurationId { get; private set; }

        /// <summary>
        /// Object Id in hand
        /// </summary>
        public static int SelectedObjectIdToSpawn { get; set; }

        private static bool _interactionWithObjectsLocked;
        /// <summary>
        /// If it possible to player to interact with objects
        /// </summary>
        public static bool InteractionWithObjectsLocked
        {
            get => _interactionWithObjectsLocked;
            set
            {
                _interactionWithObjectsLocked = value;

                if (_interactionWithObjectsLocked)
                {
                    var leftHand = InputAdapter.Instance?.PlayerController?.Nodes?.GetControllerReference(ControllerInteraction.ControllerHand.Left);
                    leftHand?.Controller?.StopInteraction();

                    var rightHand = InputAdapter.Instance?.PlayerController?.Nodes?.GetControllerReference(ControllerInteraction.ControllerHand.Right);
                    rightHand?.Controller?.StopInteraction();
                }
            }
        }

        /// <summary>
        /// All objects are loaded
        /// </summary>
        public static bool ObjectsAreLoaded { get;  set; }
        
        /// <summary>
        /// All resources are loaded
        /// </summary>
        public static bool ResourcesAreLoaded { get;  set; }

        /// <summary>
        /// Called when scene is changed in Editor (or Edit Mode)
        /// </summary>
        public static event Action SceneChanged;

        /// <summary>
        /// User modify something?
        /// </summary>
        [Obsolete("The property will be changed to SceneAreChanged and the setter will be removed")]
        public static bool ObjectsAreChanged { get; set; }
        
        /// <summary>
        /// Loading scenes with project structure key
        /// </summary>
        public static Dictionary<int, string> LoadingScenePaths { get; set; } = new Dictionary<int, string>();

        /// <summary>
        /// Current project structure data
        /// </summary>
        public static ProjectStructure ProjectStructure { get; set; }

        public static bool IsMultiplayerAndMasterClient => Settings.Instance.Multiplayer && PhotonNetwork.IsMasterClient && IsPlayMode;
        
        public delegate void ObjectsLoadedHandler ();
        /// <summary>
        /// Action when objects are loaded
        /// </summary>
        public static event ObjectsLoadedHandler ObjectsLoaded;
        
        public delegate void ResourcesLoadedHandler ();

        /// <summary>
        /// Action when resources are loaded
        /// </summary>
        public static event ResourcesLoadedHandler ResourcesLoaded;
        
        public delegate void SceneLoadedHandler ();

        /// <summary>
        /// Action when scene is loaded
        /// </summary>
        public static event SceneLoadedHandler SceneLoaded;
        
        public delegate void SceneClearedHandler ();
        
        /// <summary>
        /// Action when objects are loaded
        /// </summary>
        public static event SceneClearedHandler SceneCleared;

        /// <summary>
        /// Action when user save data
        /// </summary>
        public static Action OnSave { get; set; }
        public static Action OnPreSave { get; set; }
        
        public delegate void SelectObjectsHandler(List<ObjectController> objectControllers);
        public static event SelectObjectsHandler SelectObjects;
        
        public delegate void ParentChangedObjectsHandler(List<ObjectController> objectControllers);
        public static event ParentChangedObjectsHandler ParentChangedObjects;
        
        public delegate void DeleteObjectHandler(ObjectController objectControllers);
        public static event DeleteObjectHandler DeletingObject;

        public delegate void ObjectSpawnedHandler(ObjectController objectController);

        public static event ObjectSpawnedHandler ObjectSpawned;

        public delegate void GameModeChangeHandler(GameMode newGameMode);
        public delegate void PlatformModeChangedHandler(PlatformMode newPlatformMode);

        public static event GameModeChangeHandler GameModeChanging;
        public static event GameModeChangeHandler GameModeChanged;
        
        public static event PlatformModeChangedHandler PlatformModeChanging;
        public static event PlatformModeChangedHandler PlatformModeChanged;

        private static ExecutionMode _executionMode = ExecutionMode.Undefined;
        private static GameMode _gm = GameMode.Undefined;
        private static PlatformMode _platform = PlatformMode.Undefined;

        public static Dictionary<int, JointData> Joints { get; set; }

        public static bool ModeChangingEnabled { get; set; } = true;

        public static GameMode GameMode
        {
            get => _gm;
            set => GameModeChange(value);
        }

        /// <summary>
        /// Default value - VR
        /// </summary>
        public static PlatformMode PlatformMode
        {
            get => _platform;
            set => PlatformModeChange(value);
        }

        public static ExecutionMode ExecutionMode
        {
            get => _executionMode;
            set => ExecutionModeChange(value);
        }

        public static bool IsPlayMode => GameMode == GameMode.Preview || GameMode == GameMode.View;

        public static bool IsDesktopEditor => PlatformMode == PlatformMode.Desktop && GameMode == GameMode.Edit;
        
        public static bool IsMobileVr()
        {
#if UNITY_EDITOR || !UNITY_ANDROID
            return false;
#else
            return IsMobileClient && PlatformMode == PlatformMode.Vr;
#endif
        }
        
        /// <summary>
        /// Returns whether current platform is Mobile
        /// </summary>
        public static bool IsMobileClient
        {
            get
            {
#if UNITY_EDITOR || !UNITY_ANDROID
                return false;
#else
                return true;
#endif
            }
        }
        
        public static Data.ServerData.Scene CurrentScene => ProjectStructure.Scenes.FirstOrDefault(x => x.Id == SceneId);

        public static void UpdateSubscribes(RequiredProjectArguments arguments)
        {
            if (ProjectId != arguments.ProjectId && LoaderAdapter.LoaderType == typeof(ApiLoader))
            {
                AMQPClient.UnSubscribeProjectSceneChange(ProjectId);
                AMQPClient.UnSubscribeProjectConfigurationsChange(ProjectId);
                AMQPClient.SubscribeSceneChange(arguments.ProjectId);
                AMQPClient.SubscribeProjectConfigurationChange(arguments.ProjectId);
                GraphQLSubscriptionClient.SubscribeLibraryChanged();
            }
            
            ProjectId = arguments.ProjectId;
            SceneId = arguments.SceneId;
            ProjectConfigurationId = arguments.ProjectConfigurationId;
            GameMode = arguments.GameMode;
            
            if (_executionMode == ExecutionMode.EXE && GameMode == GameMode.View)
            {
                ProjectConfiguration configuration = ProjectStructure.ProjectConfigurations.GetProjectConfigurationById(ProjectConfigurationId);
                PlatformMode = (PlatformMode) configuration.PlatformMode;
            }
            else
            {
                PlatformMode = arguments.PlatformMode;
                GameStateData.UpdateSceneObjectLockedIds();
            }

            Debug.Log("Project data was updated! " + arguments);
        }

        private static void PlatformModeChange(PlatformMode newValue)
        {
            if (newValue == _platform)
            {
                return;
            }

            PlatformMode oldValue = _platform;
            _platform = newValue;
            
            PlatformModeChanging?.Invoke(oldValue);
            PlatformModeChanged?.Invoke(_platform);
            // теперь оно вызывается в AdapterLoader
            // GameStateData.PlatformModeChanged(_platform, oldValue);
        }

        private static void GameModeChange(GameMode newValue)
        {
            if (newValue == _gm)
            {
                return;
            }

            InteractionWithObjectsLocked = false;
            
            GameMode oldGm = _gm;
            _gm = newValue;
            Helper.HideUi();
            
            GameModeChanging?.Invoke(_gm);
            GameStateData.GameModeChanged(_gm, oldGm);

            if (ProjectDataListener.Instance)
            {
                ProjectDataListener.Instance.RestoreJoints(Joints);
            }
            
            GameModeChanged?.Invoke(_gm);
            LogManager.GetCurrentClassLogger().Info($"<Color=Yellow>Game mode changed to {_gm.ToString()}</Color>");
        }

        private static void ExecutionModeChange(ExecutionMode newValue)
        {
            if (newValue == _executionMode)
            {
                return;
            }
            
            if (_executionMode == ExecutionMode.Undefined)
            {
                _executionMode = newValue;
            }
        }

        public static void ObjectsWasLoaded()
        {
            ObjectsLoaded?.Invoke();
        }

        public static void SceneWasLoaded()
        {
            SceneLoaded?.Invoke();
            ProjectDataListener.Instance.ReadyToGetNewMessages();
        }

        public static void OnSceneCleared()
        {
            SceneCleared?.Invoke();
        }

        public static void OnObjectSpawned(ObjectController objectController)
        {
            ObjectSpawned?.Invoke(objectController);
        }

        public static void ResourcesWasLoaded()
        {
            ResourcesLoaded?.Invoke();
        }

        public static void OnSelectObjects(List<ObjectController> objectControllers)
        {
            List<int> selectedIds = new List<int>();
            
            if (objectControllers != null)
            {
                selectedIds = objectControllers.Select(objectController => objectController.Id).ToList();
            }

            GameStateData.SelectObjects(selectedIds);
            SelectObjects?.Invoke(objectControllers);
        }

        public static void OnParentChangedObjects(List<ObjectController> objectControllers)
        {
            ParentChangedObjects?.Invoke(objectControllers);
        }

        public static void OnDeletingObject(ObjectController objectController)
        {
            DeletingObject?.Invoke(objectController);
        }

        public static void UpdateSceneData(CustomSceneData sceneData)
        {
            var currentScene = ProjectStructure.Scenes.GetProjectScene(SceneId);
            currentScene.Data = sceneData;
        }
        
        public static void OnSceneChanged()
        {
            ObjectsAreChanged = true;
            SceneChanged?.Invoke();
        }
    }

    public enum ExecutionMode
    {
        Undefined = 0,
        RMS = 1,
        EXE = 2
    }

    public enum GameMode
    {
        Undefined = 1,
        Edit = 2,
        Preview = 3,
        View = 0,
    }

    public enum PlatformMode
    {
        Vr = 1,
        Desktop = 2,
        Undefined = 3
    }
}
