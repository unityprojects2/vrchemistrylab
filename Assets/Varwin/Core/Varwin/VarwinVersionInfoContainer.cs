public static class VarwinVersionInfoContainer
{
public static string VersionNumber => "0.13.2";
public static string VersionString => "Version {0} Beta 298";
}
