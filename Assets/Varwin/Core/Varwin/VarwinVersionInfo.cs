﻿using System;
using TMPro;
using UnityEngine;

public class VarwinVersionInfo : MonoBehaviour
{
    public static string VarwinVersion => string.Format(VarwinVersionInfoContainer.VersionString, VarwinVersionInfoContainer.VersionNumber);

    public static string VersionNumber => VarwinVersionInfoContainer.VersionNumber;

    private static string _versionNumber = "";
    private static string _versionString = "Version {0} beta";

    public TMP_Text VersionTextObject;

    public static bool Exists => !string.IsNullOrEmpty(VersionNumber);
    
    private void Start()
    {
        if (VersionTextObject != null)
        {
            VersionTextObject.text = VarwinVersion;
        }
    }
}
