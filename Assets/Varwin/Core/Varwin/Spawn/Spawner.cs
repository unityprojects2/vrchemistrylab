﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using Photon;
using Varwin.Data;
using Varwin.Models.Data;
using Varwin.Commands;
using Varwin.PUN;

namespace Varwin 
{
    public class Spawner : MonoBehaviourPunCallbacks
    {
        public static Spawner Instance;
 
        private void Awake()
        {           
            if (Instance == null) 
            {
                Instance = this;
            } 
            else if (Instance != this) 
            {
                Destroy (gameObject);
            }
        }
        
        public void SpawnAsset(SpawnInitParams param)
        {
            if (Settings.Instance.Multiplayer)
            {
                StartCoroutine(WaitJoinInRoomThenSpawn(param));
            }
            else
            {
                CreateSpawnEntity(param);
            }
        }

        private IEnumerator WaitJoinInRoomThenSpawn(SpawnInitParams param)
        {
            while (!PhotonNetwork.InRoom)
            {
                yield return null;
            }
            
            if (!PhotonNetwork.IsMasterClient)
            {
                yield break;
            }

            int idPhoton = PhotonNetwork.AllocateViewID(false);
            param.IdPhoton = idPhoton;

            GameObject clientSpawner = PhotonNetwork.Instantiate("VarwinObjectSync", Vector3.zero, Quaternion.identity);
            clientSpawner.GetComponent<VarwinObjectSpawnSync>().Init(param);
        }

        public void CreateSpawnEntity(SpawnInitParams paramObject)
        {
            if (GameStateData.GetWrapperCollection().Exist(paramObject.IdInstance))
            {
                Debug.Log($"Object with id {paramObject.IdInstance} already exist!");
                return;
            }
            
            Contexts contexts = Contexts.sharedInstance;
            var entity = contexts.game.CreateEntity();
            entity.AddSpawnAsset(paramObject);
        }
    }
}