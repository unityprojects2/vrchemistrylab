﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Varwin.Commands
{
    public class ModifyPropertyCommand : Command
    {
        private readonly object _newValue;
        private readonly bool _isResource;
        private readonly Action<string, object> _callback;

        // (controllerId, inspectorPropertyName)
        private readonly Dictionary<int, string> _properties;
        // (controllerId, oldPropertyValue)
        private readonly Dictionary<int, object> _oldValues;
        
        /// <summary>
        /// Modify inspector property command
        /// </summary>
        public ModifyPropertyCommand(IEnumerable<InspectorPropertyInfo> infos, object newValue, Action<string, object> callback = null, bool isResource = false, bool addCommand = true)
        {
            _oldValues = new Dictionary<int, object>();
            _properties = new Dictionary<int, string>();
            
            foreach (InspectorPropertyInfo info in infos)
            {
                _oldValues.Add(info.ControllerId, addCommand ? info.StartValue : info.OldValue);
                _properties.Add(info.ControllerId, info.Property.Name);
            }
            
            _newValue = newValue;
            _isResource = isResource;
            _callback = callback;

            if (addCommand)
            {
                CommandsManager.AddCommand(this);
            }
        }
        
        protected override void Execute()
        {
            foreach (var controllerProperty in _properties)
            {
                ObjectController controller = GameStateData.GetObjectControllerInSceneById(controllerProperty.Key);
                
                if (controller == null)
                {
                    Debug.LogError($"Object Controller with id {controllerProperty.Key} was lost");
                    return;
                }

                controller.SetInspectorPropertyValue(controllerProperty.Value, _newValue, _isResource);
            }
            
            _callback?.Invoke(_properties.First().Value, _newValue);
            
            ProjectData.ObjectsAreChanged = true;
        }

        protected override void Undo()
        {
            foreach (var controllerProperty in _properties)
            {
                ObjectController controller = GameStateData.GetObjectControllerInSceneById(controllerProperty.Key);
                
                if (controller == null)
                {
                    Debug.LogError($"Object Controller with id {controllerProperty.Key} was lost");
                    return;
                }

                controller.SetInspectorPropertyValue(controllerProperty.Value, _oldValues[controller.Id], _isResource);
            }

            object oldValue = _oldValues.Values.Distinct().Count() > 1 ? null : _oldValues.First().Value;
            _callback?.Invoke(_properties.First().Value, oldValue);
            
            ProjectData.ObjectsAreChanged = true;
        }
    }
}
