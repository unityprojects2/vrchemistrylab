﻿using System.Collections.Generic;
using System.Linq;

namespace Varwin.Commands
{
    public enum HierarchyDropAction
    {
        SetLastChild,
        SetNextSibling,
        SetPrevSibling
    }


    public class SetParentCommand : Command
    {
        private readonly int _newParentId;
        // (childId, parentId)
        private readonly Dictionary<int, int> _oldParents;
        // (childId, childIndex)
        private readonly Dictionary<int, int> _oldIndexes;
        // (childId, lockState)
        private readonly Dictionary<int, bool> _oldLockChildren;
        
        private int _index;
        
        /// <summary>
        /// Set object parent command
        /// </summary>
        /// <param name="children">Objects to modify</param>
        /// <param name="newParent">New parent object</param>
        /// <param name="dropTarget">Drop target, can be sibling or parent</param>
        /// <param name="dropAction">Drop action defines what is the drop target</param>
        public SetParentCommand(List<ObjectController> children, ObjectController newParent, ObjectController dropTarget, HierarchyDropAction dropAction)
        {
            _newParentId = newParent?.Id ?? -1;

            List<ObjectController> parentChildren = newParent != null ? newParent.Children : GameStateData.GetRootObjectsScene();
            List<ObjectController> clearedChildren = parentChildren.Except(children).ToList();
            List<ObjectController> childrenExceptParent = children.Where(x => x != newParent).ToList();

            switch (dropAction)
            {
                case HierarchyDropAction.SetLastChild:
                    _index = clearedChildren.Count;
                    break;
                
                case HierarchyDropAction.SetNextSibling:
                    _index = clearedChildren.IndexOf(dropTarget) + 1;
                    break;
                
                case HierarchyDropAction.SetPrevSibling:
                    _index = clearedChildren.IndexOf(dropTarget);
                    break;
            }
            
            _oldParents = new Dictionary<int, int>();
            _oldIndexes = new Dictionary<int, int>();
            _oldLockChildren = new Dictionary<int, bool>();
            
            foreach (ObjectController child in childrenExceptParent)
            {
                _oldParents.Add(child.Id, child.ParentId);
                _oldIndexes.Add(child.Id, child.Index);
                _oldLockChildren.Add(child.Id, child.LockChildren);
            }

            SaveObjects(childrenExceptParent);
            CommandsManager.AddCommand(this);
        }

        protected override void Execute()
        {
            List<ObjectController> children = GetObjects();
            int index = _index;

            if (children == null)
            {
                return;
            }

            foreach (ObjectController objectController in children)
            {
                ObjectController newParent = GameStateData.GetObjectControllerInSceneById(_newParentId);
                objectController.SetParent(newParent, index);
                index++;
            }

            ProjectData.OnParentChangedObjects(children);
            
            ProjectData.ObjectsAreChanged = true;
        }

        protected override void Undo()
        {
            List<ObjectController> children = GetObjects();
            
            if (children == null)
            {
                return;
            }
            
            foreach (ObjectController child in children)
            {
                ObjectController oldParent = GameStateData.GetObjectControllerInSceneById(_oldParents[child.Id]);
                child.LockChildren = _oldLockChildren[child.Id];
                child.SetParent(oldParent, _oldIndexes[child.Id]);
            }

            ProjectData.OnParentChangedObjects(children);
            
            ProjectData.ObjectsAreChanged = true;
        }
    }
}
