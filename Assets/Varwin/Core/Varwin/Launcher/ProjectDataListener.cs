﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Varwin.Errors;
using NLog;
using Photon.Pun;
using SmartLocalization;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Varwin.Data;
using Varwin.Data.ServerData;
using Varwin.Public;
using Varwin.UI;
using Varwin.UI.VRErrorManager;
using Varwin.PlatformAdapter;
using Varwin.PUN;
using Varwin.WWW;
using Logger = NLog.Logger;

namespace Varwin
{
    public class ProjectDataListener : MonoBehaviour
    {
        #region PUBLIC VARS
        public static ProjectDataListener Instance;
        public LaunchArguments LaunchArguments { get; set; }
        public ProjectSceneArguments ProjectSceneArguments { get; set; }
        public ProjectConfigurationArguments ProjectConfigurationArguments { get; set; }
        public static Action OnUpdate { get; set; }
        #endregion

        #region PRIVATE VARS
        private int _lastSceneTemplateId;
        private bool _launch;
        private static Logger Logger = LogManager.GetCurrentClassLogger();
        private ProjectSceneArguments _storedSceneArguments;
        private LaunchArguments _storedLaunchArguments;
        private ProjectConfigurationArguments _storedProjectConfigurationArguments;
        #endregion

        private void Awake()
        {           
            if (Instance == null) 
            {
                Instance = this;
            } 
            else if (Instance != this) 
            {
                Destroy (gameObject);
            }
        }

        private void Start()
        {
#if !UNITY_EDITOR
           Application.logMessageReceived += ErrorHelper.ErrorHandler; 
#endif
        }

        public void ForceReload()
        {
            if (!_launch)
            {
                return;
            }

            LaunchArguments = _storedLaunchArguments;
            ProjectSceneArguments = _storedSceneArguments;
            ProjectConfigurationArguments = _storedProjectConfigurationArguments;
            _launch = false;
        }

        private void Update()
        {
            DoOnUpdate();

            if (LoaderAdapter.LoaderType == null || LoaderAdapter.LoaderType == typeof(StorageLoader))
            {
                return;
            }

            if (LaunchArguments != null && !_launch)
            {
                _launch = true;
                _storedLaunchArguments = LaunchArguments;
                AskUserToSave();
            }

            if (ProjectSceneArguments != null)
            {
                switch (ProjectSceneArguments.State)
                {
                    case ProjectSceneArguments.StateProjectScene.Added:
                        SceneTemplateAdded(ProjectSceneArguments);

                        break;

                    case ProjectSceneArguments.StateProjectScene.Deleted:
                        SceneTemplateDeleted(ProjectSceneArguments);

                        break;

                    case ProjectSceneArguments.StateProjectScene.Changed:
                        SceneTemplateChanged(ProjectSceneArguments);

                        break;
                }

                _storedSceneArguments = ProjectSceneArguments;
                ProjectSceneArguments = null;
            }

            if (ProjectConfigurationArguments != null)
            {
                switch (ProjectConfigurationArguments.State)
                {
                    case ProjectConfigurationArguments.StateConfiguration.Added:
                        ConfigurationAdded(ProjectConfigurationArguments.ProjectConfiguration);

                        break;

                    case ProjectConfigurationArguments.StateConfiguration.Deleted:
                        ConfigurationDeleted(ProjectConfigurationArguments.ProjectConfiguration);

                        break;

                    case ProjectConfigurationArguments.StateConfiguration.Changed:
                        ConfigurationChanged(ProjectConfigurationArguments.ProjectConfiguration);

                        break;
                }

                _storedProjectConfigurationArguments = ProjectConfigurationArguments;
                ProjectConfigurationArguments = null;
            }
        }
        
        private void OnDestroy()
        {
            if (Directory.Exists(RequestVideo.VideosDirectory))
            {
                Directory.Delete(RequestVideo.VideosDirectory, true);
            }

            AMQPClient.CloseConnection();
        }
 
        private void AskUserToSave()
        {
            if (ProjectData.GameMode == GameMode.Edit && ProjectData.ObjectsAreChanged)
            {
                Helper.AskUserToDo(LanguageManager.Instance.GetTextValue("GROUP_NOT_SAVED"),
                    //YES 
                    () =>
                    {
                        ProjectData.OnSave = ApplyConfig;
                        Helper.SaveSceneObjects();
                    },
                    //NO
                    ApplyConfig,
                    () =>
                    //CANCEL
                    {
                        LaunchArguments = null;
                        ReadyToGetNewMessages();
                    });
            }
            else 
            {
                ApplyConfig();
            }
          
        }

        private void DoOnUpdate()
        {
            try
            {
                OnUpdate?.Invoke();
            }
            catch (Exception e)
            {
                Logger.Error("Error Invoke OnUpdate! " + e.Message + " " + e.StackTrace);
            }
        }

        private void ApplyConfig()
        {
            if (LaunchArguments == null)
            {
                _launch = true;

                return;
            }

            LoaderAdapter.LoadProject(LaunchArguments);
            UpdateSettings(LaunchArguments);
            LaunchArguments = null;
        }

        public void ReadyToGetNewMessages()
        {
            Debug.Log("<Color=Lime><b></b>Listener ready to listen!</Color>");
            Logger.Info($"Listener ready to listen!");
            _launch = false;
        }

        public void UpdateSettings(LaunchArguments launchArguments)
        {
            Logger.Info("New launch arguments: " + launchArguments.ToJson());
            Settings.ReadLaunchArguments(launchArguments);
        }
        
        public void BeforeSceneLoaded(bool isOtherSceneTemplate)
        {
            HideUiMenu();
            HidePopUpAndToolTips();
            StartCoroutine(BeforeLoadSceneCoroutine(isOtherSceneTemplate));
        }

        public void BackToMainMenu()
        {
            StartCoroutine(BackToMainMenuCoroutine());
        }

        private IEnumerator BackToMainMenuCoroutine()
        {
            yield return StartCoroutine(FadeIn());
            GameStateData.ClearLogic();
            GameStateData.ClearObjects();
            yield return StartCoroutine(LoadSceneCoroutine("MobileLauncher"));
            yield return StartCoroutine(UnloadSceneAndResources());
            AssetBundleManager.Instance.UnloadPreviousScene();
            InputAdapter.Instance.PointerController.IsMenuOpened = false;
            yield return StartCoroutine(FadeOut());
        }

        private IEnumerator BeforeLoadSceneCoroutine(bool isOtherSceneTemplate)
        {
            yield return StartCoroutine(FadeIn());
            
            if (isOtherSceneTemplate)
            {
                GameStateData.ClearLogic();
                GameStateData.ClearObjects();
                yield return StartCoroutine(LoadLoaderScene());
                yield return StartCoroutine(UnloadSceneAndResources());
            }
            else
            {
                Helper.ReloadSceneObjects();
            }

            ResetPlayerRigPosition();
            ProjectData.OnSceneCleared();
            yield return StartCoroutine(FadeOut());
        }

        private void ResetPlayerRigPosition()
        {
            if (!GameObjects.Instance)
            {
                return;
            }

            WorldDescriptor worldDescriptor = FindObjectOfType<WorldDescriptor>();

            if (!worldDescriptor)
            {
                ReturnLoadError("WorldDescriptor not found!");
                return;
            }

            PlayerAnchorManager playerAnchorManager = FindObjectOfType<PlayerAnchorManager>();

            if (!playerAnchorManager)
            {
                playerAnchorManager = worldDescriptor.PlayerSpawnPoint.gameObject.AddComponent<PlayerAnchorManager>();
            }

            playerAnchorManager.SetPlayerPosition();

        }

        private void HideUiMenu()
        {
            if (!GameObjects.Instance)
            {
                return;
            }

            if (GameObjects.Instance.UiMenu)
            {
                GameObjects.Instance.UiMenu.HideMenu();
            }
        }

        public void AddLoadingScene(int id,string scenePath)
        {
            if (!ProjectData.LoadingScenePaths.ContainsKey(id))
            {
                ProjectData.LoadingScenePaths.Add(id, scenePath);
            }
        }

        public void LoadScene(string scenePath)
        {
            StartCoroutine(LoadSceneBeautifulCoroutine(scenePath));
        }

        private IEnumerator LoadSceneBeautifulCoroutine(string scenePath)
        {
            yield return StartCoroutine(FadeIn());
            yield return StartCoroutine(LoadSceneCoroutine(scenePath));

            while (InputAdapter.Instance.PlayerController.Nodes.Head.GameObject == null)
            {
                yield return null;
            }
            
            ProjectData.SceneWasLoaded();
            
            yield return StartCoroutine(WaitAndLoadUiMenu());
            yield return StartCoroutine(FadeOut());
        }

        private void LoadWorldDescriptor()
        {
            Logger.Info("Waiting to load WorldDescriptor...");
            WorldDescriptor worldDescriptor = FindObjectOfType<WorldDescriptor>();

            if (!worldDescriptor)
            {
                ReturnLoadError("WorldDescriptor not found!");
                return;
            }

            if (worldDescriptor.PlayerSpawnPoint)
            {
                PlayerAnchorManager playerAnchorManager = FindObjectOfType<PlayerAnchorManager>();

                if (!playerAnchorManager)
                {
                    playerAnchorManager = worldDescriptor.PlayerSpawnPoint.gameObject.AddComponent<PlayerAnchorManager>();
                }

                playerAnchorManager.RestartPlayer();
            }
            else
            {
                Logger.Error("Player Spawn Point not found in Scene Template Config");
                ReturnLoadError("Player Spawn Point not found in Scene Template Config");
            }

        }

        private void ReturnLoadError(string message)
        {
            Logger.Error("Error to load scene. " + message);
            GameObject go = new GameObject("Default player rig");
            go.transform.position = new Vector3(0, 1.8f, 0);
            go.transform.rotation = Quaternion.identity;
            go.AddComponent<PlayerAnchorManager>();
            StartCoroutine(ShowSpawnError());
        }

        private IEnumerator ShowSpawnError()
        {
            while (!VRErrorManager.Instance)
            {
                yield return null;
            }

            string errorMsg = ErrorHelper.GetErrorDescByCode(Varwin.Errors.ErrorCode.SpawnPointNotFoundError);
            CoreErrorManager.Error(new Exception(errorMsg));
            VRErrorManager.Instance.Show(errorMsg);
            yield return true;
        }

        private IEnumerator WaitAndLoadUiMenu()
        {
            UIMenu uiMenu = null;

            if (ProjectData.PlatformMode == PlatformMode.Vr && !ProjectData.IsMobileVr() && ProjectData.GameMode != GameMode.View)
            {

                while (InputAdapter.Instance == null || InputAdapter.Instance.PlayerController.Nodes == null)
                {
                    yield return null;
                }
                
                while (!uiMenu)
                {
                    uiMenu = InputAdapter.Instance.PlayerController.Nodes.Rig.Transform.GetComponentInChildren<UIMenu>();
                    yield return new WaitForEndOfFrame();
                }

                while (!uiMenu.IsReady)
                {
                    yield return null;
                }
                
                yield return true;
            }
            else
            {
                yield return true;
            }
            
        }

        private IEnumerator WaitingForMasterLoading()
        {
            if (!Settings.Instance.Multiplayer)
            {
                yield break;
            }

            while (!PhotonNetwork.InRoom)
            {
                yield return null;
            }

            if (PhotonNetwork.IsMasterClient)
            {
                yield break;
            }

            string message = "Waiting for master loading...";
            Logger.Info(message);
            LoaderAdapter.LoaderFeedBackText(message);

            MasterLoading masterLoading = FindObjectOfType<MasterLoading>();

            while (!masterLoading)
            {
                masterLoading = FindObjectOfType<MasterLoading>();

                yield return null;
            }
            
            while (masterLoading.IsLoading)
            {
                 yield return null;
            }
            
            message = "Entering...";
            Logger.Info(message);
            LoaderAdapter.LoaderFeedBackText(message);
        }


        private IEnumerator LoadSceneCoroutine(string scenePath)
        {
            Debug.Log("Loading new Scene: " + Time.time);

            var activeScene = SceneManager.GetActiveScene();

            if (activeScene.path == scenePath)
            {
                Debug.Log("Scene already loaded");
                yield break;
            }
            
            AsyncOperation loadSceneOperation = SceneManager.LoadSceneAsync(scenePath, LoadSceneMode.Additive);

            while (!loadSceneOperation.isDone) 
            {
                yield return null;
            }
            
            Debug.Log("Unloading old Scene: " + Time.time);
            
            AsyncOperation unloadSceneOperation = SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
            
            while (!unloadSceneOperation.isDone)
            {
                yield return null;
            }
            
            if(SceneManager.GetActiveScene().buildIndex < 0)
            {
                yield return WaitingForMasterLoading();
            }
            
            LoadWorldDescriptor();
            Logger.Info("Scene Template is loaded: " + Time.time);
           
            yield return true;
        }

        private IEnumerator FadeIn()
        {
            if (!UIFadeInOutController.Instance)
            {
                yield break;
            }
            
            UIFadeInOutController.Instance.FadeIn();

            while (UIFadeInOutController.Instance && !UIFadeInOutController.Instance.IsComplete)
            {
                yield return new WaitForEndOfFrame();
            }

            yield return true;
        }

        private IEnumerator FadeOut()
        {
            if (!UIFadeInOutController.Instance)
            {
                yield break;
            }

            UIFadeInOutController.Instance.FadeOut();
        }

        private IEnumerator UnloadSceneAndResources()
        {
            Logger.Info("Start unload current scene");
            
            AsyncOperation unloadSceneOperation = SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());

            if (unloadSceneOperation == null)
            {
                yield break;
            }

            while (!unloadSceneOperation.isDone)
            {
                yield return null;
            }
            
            Resources.UnloadUnusedAssets();
            
            yield return true;
        }

        private string TryGetLoadingScene()
        {
            if (ProjectData.ProjectStructure == null)
            {
                return string.Empty;
            }

            var configuration = ProjectData.ProjectStructure.ProjectConfigurations.Find(p => p.Id == ProjectData.ProjectConfigurationId);

            if (configuration == null)
            {
                return string.Empty;
            }

            if (configuration.LoadingSceneTemplateId != 0 && ProjectData.LoadingScenePaths.ContainsKey(configuration.Id))
            {
                return ProjectData.LoadingScenePaths[ProjectData.ProjectConfigurationId];
            }

            return string.Empty;
        }

        private IEnumerator LoadLoaderScene()
        {
            if (InputAdapter.Instance.PlayerController.Nodes.Head.GameObject == null)
            {
                yield break;
            }

            Logger.Info("Load loader scene");

            string customLoadingScene = TryGetLoadingScene();

            if (!string.IsNullOrEmpty(customLoadingScene))
            {
                AsyncOperation loadSceneOperation = SceneManager.LoadSceneAsync(customLoadingScene, LoadSceneMode.Additive);

                while (!loadSceneOperation.isDone) 
                {
                    yield return null;
                }

                TurnOnFeedBackScripts();
            }

            else
            {
                AsyncOperation loadLoading = SceneManager.LoadSceneAsync("Loading", LoadSceneMode.Additive);

                while (!loadLoading.isDone)
                {
                    yield return null;
                }

                yield return true;
            }
        }

        private void TurnOnFeedBackScripts()
        {
            LoaderFeedBackText feedBackText = FindObjectOfType<LoaderFeedBackText>();

            if (!feedBackText)
            {
                return;
            }
            
            Text text = feedBackText.GetComponent<Text>();
            TMP_Text tmpText = feedBackText.GetComponent<TMP_Text>();

            if (text)
            {
                text.enabled = true;
            }
            
            if (tmpText)
            {
                tmpText.enabled = true;
            }

            feedBackText.enabled = true;
        }

        public void RestoreJoints(Dictionary<int, JointData> joints)
        {
            if (joints == null)
            {
                return;
            }

            StartCoroutine(RestoreJointsOnNextFrame(joints));

        }

        private IEnumerator RestoreJointsOnNextFrame(Dictionary<int, JointData> joints)
        {
            JointBehaviour.IsTempConnectionCreated = true;

            yield return new WaitForEndOfFrame();

            Debug.Log("<Color=Olive>Restore joints started!</Color>"); 

            var jointsScene = FindObjectsOfType<JointBehaviour>();

            foreach (JointBehaviour joint in jointsScene)
            {
                joint.UnLockAndDisconnectPoints();
            }

            var behaviours = new List<JointBehaviour>();
            
            foreach (var joint in joints)
            {
                int instanseId = joint.Key;
                JointData jointData = joint.Value;
                ObjectController objectController = GameStateData.GetObjectControllerInSceneById(instanseId);

                if (objectController == null)
                {
                    continue;
                }

                JointBehaviour jointBehaviour = objectController.RootGameObject.GetComponent<JointBehaviour>();

                if (!jointBehaviour)
                {
                    continue;
                }
                
                behaviours.Add(jointBehaviour);
                 
                var jointPoints = Helper.GetJointPoints(objectController.RootGameObject);

                foreach (var jointConnectionsData in jointData.JointConnectionsData)
                {
                    int pointId = jointConnectionsData.Key;
                    if (!jointPoints.ContainsKey(pointId))
                    {
                        Debug.LogError($"Cannot find joint point for id {pointId} (ConnectedObjectInstanceId: {jointConnectionsData.Value.ConnectedObjectInstanceId}; ConnectedObjectJointPointId: {jointConnectionsData.Value.ConnectedObjectJointPointId})");
                        continue;
                    }
                    JointPoint myJointPoint = jointPoints[pointId];
                    JointConnectionsData connectionData = jointConnectionsData.Value;

                    ObjectController otherObjectController =
                        GameStateData.GetObjectControllerInSceneById(connectionData.ConnectedObjectInstanceId);
                    var otherJointPoints = Helper.GetJointPoints(otherObjectController.RootGameObject);

                    if (!otherJointPoints.ContainsKey(connectionData.ConnectedObjectJointPointId))
                    {
                        Debug.LogError(
                            $"Cannot find object id {connectionData.ConnectedObjectJointPointId} for joint {myJointPoint.gameObject.name} in object {myJointPoint.GetComponentInParent<VarwinObjectDescriptor>()?.Name}", myJointPoint);
                        continue;
                    }
                    JointPoint otherJointPoint = otherJointPoints[connectionData.ConnectedObjectJointPointId];
                    jointBehaviour.ConnectToJointPoint(myJointPoint, otherJointPoint);
                    myJointPoint.CanBeDisconnected = !connectionData.ForceLocked;
                    otherJointPoint.CanBeDisconnected = !connectionData.ForceLocked;
                }
            }

            yield return new WaitForEndOfFrame();

            JointBehaviour.IsTempConnectionCreated = false;
            yield return true;
        }

        public delegate void ObjectUpdate(GameEntity entity, SceneObjectDto dto);
        public event ObjectUpdate OnUpdateObject;

        public event Action LibraryUpdated;

        public void LibraryWasUpdated()
        {
            LibraryUpdated?.Invoke();
        }
  
        public void UpdateObject(SceneObjectDto sceneObjectDto)
        {
            GameEntity entityObject = GameStateData.GetEntity(sceneObjectDto.InstanceId);
            entityObject.name.Value = sceneObjectDto.Name;
            OnUpdateObject?.Invoke(entityObject, sceneObjectDto);
        }

        public void SceneTemplateAdded(ProjectSceneArguments newSceneTemplate)
        {
            ProjectData.ProjectStructure.Scenes.Add(newSceneTemplate.Scene);
            ProjectData.ProjectStructure.UpdateOrAddSceneTemplatePrefab(newSceneTemplate.SceneTemplate);
        }
        
        public void SceneTemplateChanged(ProjectSceneArguments changedSceneTemplate)
        {
            ProjectData.ProjectStructure.UpdateProjectScene(changedSceneTemplate.Scene);
            ProjectData.ProjectStructure.UpdateOrAddSceneTemplatePrefab(changedSceneTemplate.SceneTemplate);

            if (ProjectData.SceneId == changedSceneTemplate.Scene.Id && ProjectData.SceneTemplateId != changedSceneTemplate.Scene.SceneTemplateId)
            {
                LoaderAdapter.LoadProject(ProjectData.ProjectId, changedSceneTemplate.Scene.Id, ProjectData.ProjectConfigurationId);
            }
        }

        public void SceneTemplateDeleted(ProjectSceneArguments deletedSceneTemplate)
        {
            if (ProjectData.SceneId == deletedSceneTemplate.Scene.Id)
            {
                Logger.Error("Current scene template was deleted");
                string message = LanguageManager.Instance.GetTextValue("CURRENT_SCENE_TEMPLATE_DELETED");
                if (VRErrorManager.Instance)
                {
                    VRErrorManager.Instance.ShowFatal(message);
                }
            }
            else
            {
                ProjectData.ProjectStructure.RemoveProjectScene(deletedSceneTemplate.Scene);
            }
        }

        public void ConfigurationAdded(ProjectConfiguration projectConfiguration)
        {
            ProjectData.ProjectStructure.ProjectConfigurations.Add(projectConfiguration);
        }
        
        public void ConfigurationDeleted(ProjectConfiguration projectConfiguration)
        {
            ProjectData.ProjectStructure.RemoveProjectConfiguration(projectConfiguration);
        }
        
        public void ConfigurationChanged(ProjectConfiguration projectConfiguration)
        {
            ProjectData.ProjectStructure.UpdateProjectConfiguration(projectConfiguration);
        }
        
        private void HidePopUpAndToolTips()
        {
            PopupWindowManager.ClosePopup();
            TooltipManager.HideControllerTooltip(ControllerTooltipManager.TooltipControllers.Both, ControllerTooltipManager.TooltipButtons.Grip);
            TooltipManager.HideControllerTooltip(ControllerTooltipManager.TooltipControllers.Both, ControllerTooltipManager.TooltipButtons.Trigger);
            TooltipManager.HideControllerTooltip(ControllerTooltipManager.TooltipControllers.Both, ControllerTooltipManager.TooltipButtons.Touchpad);
            TooltipManager.HideControllerTooltip(ControllerTooltipManager.TooltipControllers.Both, ControllerTooltipManager.TooltipButtons.ButtonOne);
            TooltipManager.HideControllerTooltip(ControllerTooltipManager.TooltipControllers.Both, ControllerTooltipManager.TooltipButtons.ButtonTwo);
            TooltipManager.HideControllerTooltip(ControllerTooltipManager.TooltipControllers.Both, ControllerTooltipManager.TooltipButtons.StartMenu);
        }

       
    }

    
}
