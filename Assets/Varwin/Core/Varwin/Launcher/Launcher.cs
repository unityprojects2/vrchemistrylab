﻿using System;
using System.Text;
using Varwin.Errors;
using NLog;
using NLogger;
using SmartLocalization;
using UnityEngine;
using Varwin.Data;
using Varwin.Data.ServerData;
using Varwin.ECS.Systems.Loader;
using Varwin.UI;
using Varwin.WWW;
using ErrorCode = Varwin.Errors.ErrorCode;

namespace Varwin.PUN
{
    public class Launcher : MonoBehaviour
    {
        #region PUBLIC VARS

        public static Launcher Instance;

        [Tooltip("The UI Loader Anime")] 
        public LoaderAnime LoaderAnime;

        public bool LoadTarFile;
        public string TarFilePath;

        #endregion

        #region PRIVATE VARS

        private NLog.Logger _logger;
        private LoaderSystems _loaderSystems;

        #endregion

        private void OnApplicationQuit()
        {
            Debug.Log("Application ending after " + Time.time + " seconds");
            AMQPClient.CloseConnection();
        }

        private void Awake()
        {
            _logger = LogManager.GetCurrentClassLogger();
            _logger.Info("Launcher started");

            #region Magic code

#if !NET_STANDARD_2_0
#pragma warning disable 219
            dynamic inside = "Create Microsoft.CSharp.dll in project folder";
            _logger.Info(inside);
#pragma warning restore 219
#endif

            #endregion

            Instance = this;
            NLoggerSettings.Init();
        }

        private void Start()
        {
            Init();
        }

        public void Init()
        {
            LoaderAnime.gameObject.transform.position = new Vector3(0, (float) Screen.height / 1000 - 1, 0);
            LanguageManager.SetDontDestroyOnLoad();
            Contexts.sharedInstance.game.DestroyAllEntities();

#if !UNITY_EDITOR
	        ReadStartUpSettings();
#else
            Settings.ReadTestSettings();
            
            Rabbitmq amqmSettings = new Rabbitmq
            {
                host = Settings.Instance.RabbitMqHost + ":" + Settings.Instance.RabbitMqPort,
                login = Settings.Instance.RabbitMqUserName,
                password = Settings.Instance.RabbitMqPass,
                key = Settings.Instance.RabbitMqDebugKey
            };

            AMQPClient.Init(amqmSettings);

            if (LoadTarFile)
            {
                Settings.CreateStorageSettings(TarFilePath);
                StartLoadFile();

                return;
            }

            try
            {
                AMQPClient.ReadLaunchArgs();
            }
            catch (Exception e)
            {
                LauncherErrorManager.Instance.ShowFatalErrorKey(ErrorHelper.GetErrorKeyByCode(ErrorCode.RabbitNoArgsError), e.ToString());
                _logger.Fatal("Can not read launch args in rabbitMq");
            }

            LogManager.GetCurrentClassLogger().Info("RabbitMQ init settings = " + amqmSettings);

#endif
            StartLoaderSystems();
        }

        private void StartLoaderSystems()
        {
            if (_loaderSystems != null)
            {
                return;
            }

            _loaderSystems = new LoaderSystems(Contexts.sharedInstance);
            _loaderSystems.Initialize();
            ProjectDataListener.OnUpdate = () => _loaderSystems.Execute();
        }

        private void StartLoadFile()
        {
            LogManager.GetCurrentClassLogger().Info("File name = " + Settings.Instance.StoragePath);

            ProjectData.ExecutionMode = ExecutionMode.EXE;
            ProjectData.PlatformMode = PlatformMode.Desktop;
            ProjectData.GameMode = GameMode.View;

            if (LoaderAnime != null)
            {
                LoaderAnime.StartLoaderAnimation();
            }

            LoaderAdapter.LoadProjectStructure(0, OnProjectStructureRead);


            void OnProjectStructureRead(ProjectStructure projectStructure)
            {
                ProjectData.ProjectStructure = projectStructure;
                
                
                _logger.Info("Checking license info...");


                if (projectStructure.LicenseKey == null)
                {
                    LauncherErrorManager.Instance.ShowFatal(ErrorHelper.GetErrorDescByCode(ErrorCode.LicenseKeyError), Environment.StackTrace);
                    _logger.Fatal("License information error");

                    return;
                }
                
                _logger.Info($"License key provided: {projectStructure.LicenseKey}");

                License license = ADec.GetProjectDecJson(projectStructure.LicenseKey).JsonDeserialize<License>();

                if (license == null)
                {
                    
                    return;
                }
                
                LicenseInfo.Value = license;

//                if (LauncherErrorManager.Instance)
//                {
//                    LauncherErrorManager.Instance.License(LicenseInfo.Value);
//                }

                _logger.Info("License is ok! Edition: " + LicenseInfo.Value.EditionId);

                AuthorAttribution.ProjectAttribution =
                    AuthorAttribution.ParseProjectStructure(projectStructure);
                
                if (ProjectData.ProjectStructure.ProjectConfigurations.Count == 0)
                {
                    string errorMsg = ErrorHelper.GetErrorDescByCode(ErrorCode.ProjectConfigNullError);
                    CoreErrorManager.Error(new Exception(errorMsg));
                    LauncherErrorManager.Instance.Show(errorMsg);

                    return;
                }

                if (ProjectData.ProjectStructure.ProjectConfigurations.Count == 1)
                {
                    var projectConfigurationId = ProjectData.ProjectStructure.ProjectConfigurations[0].Id;
                    LoadConfigurationFromStorage(projectConfigurationId);
                }
                else
                {
                    UIChooseProjectConfig.Instance.UpdateDropDown(
                        ProjectData.ProjectStructure.ProjectConfigurations.GetNames(),
                        LoadConfigurationFromStorage);
                    UIChooseProjectConfig.Instance.Show();
                    LoaderAnime.StopLoaderAnimation();
                }
            }
        }

        private void LoadConfigurationFromStorage(int projectConfigurationId)
        {
            LoaderAnime.StartLoaderAnimation();
            LoaderSystems loaderSystems = new LoaderSystems(Contexts.sharedInstance);
            loaderSystems.Initialize();
            ProjectDataListener.OnUpdate = () => loaderSystems.Execute();
            LoaderAdapter.LoadProjectConfiguration(projectConfigurationId);
        }

        // Init rabbit from first launch not on editor!
        // ReSharper disable once InconsistentNaming
        // ReSharper disable once UnusedMember.Local
        private void ReadStartUpSettings()
        {
            try
            {
                string[] args = Environment.GetCommandLineArgs();
                string parametersBase64 = args[1];
                string parametersJson = Base64Decode(parametersBase64);
                _logger.Info(parametersJson); //currently it's somewhat the only way to debug client

                LoaderAdapter.Init(new ApiLoader());
                LaunchParameters parameters = parametersJson.JsonDeserialize<LaunchParameters>();

                RequestServerConfig(parameters.baseUrl, parameters.key, parameters.accessKey);

            }
            catch (Exception e)
            {
                try
                {
                    ReadStartUpFile();
                }
                catch (Exception exception)
                {
                    string message = "Cant read AMQP settings! Trying to get file params";
                    LogManager.GetCurrentClassLogger().Fatal(message + " stackTrace = " + e.StackTrace);
                    LauncherErrorManager.Instance.ShowFatal(message, exception.ToString());
                }
            }
        }

        private void RequestServerConfig(string baseUrl, string key, string accessKey)
        {
            _logger.Info("Server address: " + baseUrl);

            if (!string.IsNullOrEmpty(accessKey))
            {
                Request.AccessKey = accessKey;
            }

            Settings.SetApiUrl(baseUrl);

            ProjectData.ExecutionMode = ExecutionMode.RMS;

            API.GetServerConfig((serverConfig) =>
            {

                _logger.Info("Checking API version...");

                if (!string.IsNullOrEmpty(VarwinVersionInfo.VersionNumber) && !string.IsNullOrEmpty(serverConfig.AppVersion) && !string.Equals(VarwinVersionInfo.VersionNumber, serverConfig.AppVersion))
                {
                    _logger.Info($"VarwinVersionInfo.VersionNumber = `{VarwinVersionInfo.VersionNumber}`; serverConfig.AppVersion = `{serverConfig.AppVersion}`");
                    LauncherErrorManager.Instance.ShowFatalErrorKey(ErrorHelper.GetErrorKeyByCode(ErrorCode.ApiAndClientVersionMismatchError), Environment.StackTrace);
                    _logger.Fatal("Api and client version mismatch");

                    return;
                }

                Settings.ReadServerConfig(serverConfig);

                var rabbitmq = new Rabbitmq
                {
                    host = Settings.Instance.RabbitMqHost + ":" + Settings.Instance.RabbitMqPort, login = Settings.Instance.RabbitMqUserName, password = Settings.Instance.RabbitMqPass, key = key
                };

                _logger.Info("Rabbit json: " + rabbitmq.ToJson());
                AMQPClient.Init(rabbitmq);

                try
                {
                    AMQPClient.ReadLaunchArgs();
                }
                catch (Exception e)
                {
                    LauncherErrorManager.Instance.ShowFatalErrorKey(ErrorHelper.GetErrorKeyByCode(ErrorCode.RabbitNoArgsError), e.ToString());
                    _logger.Fatal("Can not read launch args in rabbitMq");
                }

                LogManager.GetCurrentClassLogger().Info("RabbitMQ init settings = " + rabbitmq);
            },
                () =>
                {
                    Debug.LogError($"Can't get server config");
                
                    LauncherErrorManager.Instance.ShowFatalErrorKey(ErrorHelper.GetErrorKeyByCode(ErrorCode.ServerNoConnectionError), Environment.StackTrace);

                    LogManager.GetCurrentClassLogger().Fatal("Can not get the server config");
                });
        }

        private void ReadStartUpFile()
        {
            LogManager.GetCurrentClassLogger().Info("Opening file...");
            string[] args = Environment.GetCommandLineArgs();
            string file = args[1];
            Settings.CreateStorageSettings(file);
            StartLoadFile();
        }

        private static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
            return Encoding.UTF8.GetString(base64EncodedBytes);
        }

    }
}
