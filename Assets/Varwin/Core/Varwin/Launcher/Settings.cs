﻿using System;
using System.Globalization;
using System.IO;
using Varwin.Errors;
using Newtonsoft.Json;
using NLog;
using SmartLocalization;
using UnityEngine;
using Varwin.Data;
using Varwin.Data.ServerData;
using Varwin.PUN;
using Varwin.UI;

namespace Varwin
{
    public class Settings
    {
        public string RabbitMqHost { get; set; }
        public string RabbitMqUserName { get; set; }
        public string RabbitMqPass { get; set; }
        public string RabbitMqPort { get; set; }
        public string ApiHost { get; set; }

        public string RemoteAddr { get; set; }

        // For test purposes in Unity Editor. Change in settings.txt file. Values: {debug - run with ctrl + ~, debug1 - run with ctrl + 1}
        public string RabbitMqDebugKey { get; set; }
        public string Language { get; set; }
        public string StoragePath { get; set; }
        public string DebugFolder { get; set; }
        public string WebHost { get; set; }
        public bool Multiplayer { get; set; }
        public bool Spectator { get; set; }
        public bool Education { get; set; }
        public string PhotonHost { get; set; }
        public string PhotonPort { get; set; }
        public string PhotonAppId { get; set; } = "1a198491-9d70-4f23-a274-cfbd2ce068a2";
        public bool HighlightEnabled { get; set; }
        public bool TouchHapticsEnabled { get; set; }
        public bool GrabHapticsEnabled { get; set; }
        public bool UseHapticsEnabled { get; set; }
        public bool OnboardingMode { get; set; }

        private static Settings _instance;

        public static Settings Instance
        {
            get => _instance ?? (_instance = new Settings()
            {
                HighlightEnabled = true,
                TouchHapticsEnabled = false,
                GrabHapticsEnabled = false,
                UseHapticsEnabled = false,
                Multiplayer = false,
                Education = true,
                OnboardingMode = false,
                Language = LanguageManager.DefaultLanguage
            });
            set => _instance = value;
        }

        public Settings()
        {
            LanguageManager.Instance.OnChangeLanguage += UpdateLanguage;
        }

        private void UpdateLanguage(LanguageManager languageManager)
        {
            Language = languageManager.CurrentlyLoadedCulture.languageCode;
        }

        // unity editor
        public static void ReadTestSettings()
        {
            string json = File.ReadAllText(Application.dataPath + "/StreamingAssets/settings.txt");
            Instance = JsonConvert.DeserializeObject<Settings>(json,
                new JsonSerializerSettings {TypeNameHandling = TypeNameHandling.None});
            if (!Launcher.Instance.LoadTarFile)
            {
                ProjectData.ExecutionMode = ExecutionMode.RMS;
                LoaderAdapter.Init(new ApiLoader());
            }
            else
            {
                ProjectData.ExecutionMode = ExecutionMode.EXE;
            }
        }

        // build
        public static void CreateStorageSettings(string folder)
        {
            //string outFolder = Application.dataPath + "/tempUnpacked";
            LogManager.GetCurrentClassLogger().Info($"Create storage settings. Path = {folder}");
            Instance = new Settings
            {
                StoragePath = folder,
                HighlightEnabled = true,
                Language = CultureInfo.CurrentCulture.TwoLetterISOLanguageName
#if WAVEVR
                ,
                Education = true,
                Multiplayer = true
#endif
            };

            LoaderAdapter.Init(new StorageLoader());
        }

        // debug laucher (sdk)
        public static void CreateDebugSettings(string path)
        {
            if (Instance != null)
            {
                return;
            }
            
            Instance = new Settings
            {
                DebugFolder = path,
                HighlightEnabled = true,
                TouchHapticsEnabled = false,
                GrabHapticsEnabled = false,
                UseHapticsEnabled = false,
                Language = LanguageManager.DefaultLanguage
            };

            LoaderAdapter.Init(new ApiLoader());
        }

        public static void ReadLaunchArguments(LaunchArguments launchArguments)
        {
            try
            {
                Instance.Language = launchArguments.lang ?? _instance.Language ?? LanguageManager.DefaultLanguage;
                Instance.OnboardingMode = launchArguments.onboarding;

                LanguageManager.Instance.ChangeLanguage(launchArguments.lang);
            }
            catch (Exception e)
            {
                LauncherErrorManager.Instance.ShowFatal(
                    ErrorHelper.GetErrorDescByCode(ErrorCode.ReadLaunchArgsError), e.ToString());
            }
        }

        public static void ReadServerConfig(ServerConfig serverConfig)
        {
            var uri = new Uri(Instance.ApiHost);

            Instance.WebHost = $@"{uri.Scheme}://{uri.Host}:{serverConfig.WebWidgetsPort}";
            Instance.RabbitMqHost = uri.Host;
            Instance.RemoteAddr = $@"{uri.Scheme}://{serverConfig.RemoteAddr}:{serverConfig.RemoteAddrPort}";

            Instance.RabbitMqPort = serverConfig.RabbitMqPort;
            Instance.RabbitMqUserName = serverConfig.RabbitMqUserName;
            Instance.RabbitMqPass = serverConfig.RabbitMqPass;

            Instance.Language = Instance.Language ?? LanguageManager.DefaultLanguage;
        }

        public static void SetApiUrl(string url)
        {
            Instance.ApiHost = url;
        }
    }
}