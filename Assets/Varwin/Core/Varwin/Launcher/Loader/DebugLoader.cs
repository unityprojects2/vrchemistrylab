using System;
using System.Collections.Generic;
using Varwin.Data.ServerData;
using Varwin.WWW;

namespace Varwin.Data
{
    public class DebugLoader : BaseLoader
    {
        public override void LoadObjects(List<PrefabObject> objects)
        {
             
        }

        public override void LoadResources(List<ResourceDto> resources)
        {
            
        }

        protected override void LoadMeshResource(ResourceDto resource)
        {
            
        }

        protected override void LoadTextureResource(ResourceDto resource)
        {
            
        }

        protected override void LoadTextResource(ResourceDto resource)
        {
            
        }
        
        protected override void LoadAudioResource(ResourceDto resource)
        {
            
        }
        
        protected override void LoadVideoResource(ResourceDto resource)
        {
            
        }

        protected override void LoadAssetBundlePart(AssetInfo assetInfo, PrefabObject o)
        {
            
        }

        public override void LoadSceneTemplate(int sceneTemplateId, bool isLoadingScene)
        {
             
        }

        public override void LoadProjectStructure(int projectId, Action<ProjectStructure> onFinish)
        {
             
        }
    }
}