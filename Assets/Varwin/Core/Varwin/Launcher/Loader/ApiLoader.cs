using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NLog;
using SmartLocalization;
using UnityEngine;
using Varwin.Data.AssetBundle;
using Varwin.Data.ServerData;
using Varwin.Errors;
using Varwin.UI;
using Varwin.WWW;

namespace Varwin.Data
{
    public class ApiLoader : BaseLoader
    {
        public delegate void ProgressUpdate(float val);

        public ProgressUpdate OnLoadingUpdate;

        public override void LoadObjects(List<PrefabObject> objects)
        {
            Logger.Info("Scene objects list loaded (API). Number of objects: " + objects.Count);
            ResetObjectsCounter(objects.Count);

            foreach (PrefabObject prefabObject in objects)
            {
                LoadPrefabObject(prefabObject);
            }

            Logger.Info("Required objects list has been loaded (API)");
        }

        public override void LoadResources(List<ResourceDto> resources)
        {
            Logger.Info("Resources list loaded (API). Number of resources: " + resources.Count);
            ResetResourcesCounter(resources.Count);

            foreach (ResourceDto resource in resources)
            {
                LoadResource(resource);
            }

            Logger.Info("Required resources list has been loaded (API)");
        }

        protected override void LoadMeshResource(ResourceDto resource)
        {
            string path = Settings.Instance.ApiHost + resource.Resources;
            var request = new Request3dModel(path);

            request.OnFinish += response =>
            {
                var response3DModel = (Response3dModel) response;
                CreateResourceEntity(resource, response3DModel.GameObject);
            };

            request.OnError += message => LoadResourceDefaultErrorHandler(resource, message);
        }

        protected override void LoadTextureResource(ResourceDto resource)
        {
            var url = Settings.Instance.ApiHost + resource.Path;
            
            var requestTexture = new RequestTexture(url, true);

            requestTexture.OnFinish += response =>
            {
                var responseTexture = (ResponseTexture) response;
                UpdateResourceEntity(resource, responseTexture.Texture);
            };

            requestTexture.OnError += message => LoadResourceDefaultErrorHandler(resource, message);
        }

        protected override void LoadTextResource(ResourceDto resource)
        {
            var requestApi = new RequestUri(resource.Path, null, true);

            requestApi.OnFinish += response =>
            {
                var responseUri = (ResponseUri) response;
                CreateResourceEntity(resource, new TextAsset(responseUri.TextData));
            };
                
            requestApi.OnError += message => LoadResourceDefaultErrorHandler(resource, message);
        }

        protected override void LoadAudioResource(ResourceDto resource)
        {
            var url = Settings.Instance.ApiHost + resource.Path;
            var request = new RequestAudio(url);
            
            request.OnFinish += response =>
            {
                var responseAudio = (ResponseAudio) response;
                CreateResourceEntity(resource, responseAudio.AudioClip);
            };

            request.OnError += message => LoadResourceDefaultErrorHandler(resource, message);
        }
        
        protected override void LoadVideoResource(ResourceDto resource)
        {
            var url = Settings.Instance.ApiHost + resource.Path;
            var request = new RequestVideo(url);
            
            request.OnFinish += response =>
            {
                var responseVideo = (ResponseVideo) response;
                CreateResourceEntity(resource, responseVideo.VideoUrl);
            };

            request.OnError += message => LoadResourceDefaultErrorHandler(resource, message);
        }

        protected override void LoadAssetBundlePart(AssetInfo assetInfo, PrefabObject prefabObject)
        {
            if (assetInfo?.AssetBundleParts == null || assetInfo.AssetBundleParts.Count == 0)
            {
                return;
            }
            
            foreach (var pathToPart in assetInfo.AssetBundleParts)
            {
                var requestManifest = new RequestUri(ProjectData.IsMobileVr()
                    ? Path.Combine(prefabObject.ResourcesPath, $"android_{pathToPart}.manifest")
                    : Path.Combine(prefabObject.ResourcesPath, $"{pathToPart}.manifest"));
                
                requestManifest.OnFinish += response =>
                {
                    var responseManifest = (ResponseUri) response;
                    
                    Hash128 bundleHash = Hash128.Compute(responseManifest.TextData);

                    if (GameStateData.LoadedAssetBundleParts.Contains(bundleHash))
                    {
                        return;
                    }

                    GameStateData.LoadedAssetBundleParts.Add(bundleHash);

                    var assetBundleCacheSettings = new CachedAssetBundle()
                    {
                        name = $"AssetBundlePartManifest_{prefabObject.Guid}_{pathToPart}",
                        hash = bundleHash
                    };

                    var assetBundleUri = ProjectData.IsMobileVr()
                        ? Path.Combine(prefabObject.ResourcesPath, $"android_{pathToPart}")
                        : Path.Combine(prefabObject.ResourcesPath, $"{pathToPart}");

                    var requestAsset = new RequestAsset(assetInfo.AssetName, assetBundleUri, assetBundleCacheSettings, new object[] {prefabObject, assetInfo});

                    requestAsset.OnFinish += responseAsset =>
                    {
                        Debug.Log($"Resources loaded for {assetInfo.AssetName}");
                    };
                };
            }
        }

        private void LoadPrefabObject(PrefabObject o)
        {
            var request = new RequestUri(o.ConfigResource, null, true);

            request.OnFinish += response =>
            {
                string json = ((ResponseUri) response).TextData;
                AssetInfo assetInfo = null;

                if (!LoadAssetInfo(json, ref assetInfo, o))
                {
                    return;
                }

                string mainAssembly = FindMainAssembly(assetInfo, o);

                foreach (string dllName in assetInfo.Assembly)
                {
                    if (dllName != mainAssembly)
                    {
                        LoadDll(o, dllName);
                    }
                }

                LoadAssetBundlePart(assetInfo, o);
                
                LoadDll(o, mainAssembly);

                LoadCustomAssetApi(assetInfo, o);
            };

            request.OnError += s =>
            {
                FeedBackText = $"Load object {o.Name.en} error!\n{s}.";
                Helper.ShowFatalErrorLoadObject(o, s);
            };
        }

        private void LoadDll(PrefabObject o, string dllName)
        {
            string dllCachePath = FileSystemUtils.GetFilesPath(ProjectData.IsMobileClient, "cache/dll/")
                                  + o.Name.en
                                  + o.Guid;

            var dllPath = $"{o.ResourcesPath}/{dllName}";

            new RequestUri(dllPath).OnFinish += response1 =>
            {
                var byteData = ((ResponseUri) response1).ByteData;

                bool error = !AddAssembly(dllCachePath, dllName, ref byteData);

                if (!error)
                {
                    return;
                }

                string message = ErrorHelper.GetErrorDescByCode(ErrorCode.LoadObjectError)
                               + "\n"
                               + o.GetLocalizedName();

                FeedBackText = message;
                Logger.Fatal(message);
                FeedBackText = message;
                RequestManager.Instance.StopRequestsWithError(message);
            };
        }

        private void LoadDll(SceneTemplatePrefab s, string dllName)
        {
            var dllCachePath = $"{FileSystemUtils.GetFilesPath(ProjectData.IsMobileClient, "cache/dll/")}{s.Guid}";
            
            var dllPath = $"{s.Resources}/{dllName}";

            new RequestUri(dllPath).OnFinish += response1 =>
            {
                var byteData = ((ResponseUri) response1).ByteData;

                bool error = !AddAssembly(dllCachePath, dllName, ref byteData);

                if (!error)
                {
                    return;
                }

                string message = ErrorHelper.GetErrorDescByCode(ErrorCode.LoadSceneError)
                                 + "\n"
                                 + s.GetLocalizedName();

                FeedBackText = message;
                Logger.Fatal(message);
                FeedBackText = message;
                RequestManager.Instance.StopRequestsWithError(message);
            };
        }

        public override void LoadSceneTemplate(int sceneTemplateId, bool isLoadingScene)
        {
            SceneTemplatePrefab sceneTemplatePrefab = GetSceneTemplatePrefab(sceneTemplateId);

            var requestManifest = new RequestUri(ProjectData.IsMobileVr()
                ? sceneTemplatePrefab.AndroidManifestResource
                : sceneTemplatePrefab.ManifestResource);

            requestManifest.OnFinish += response =>
            {
                var responseManifest = (ResponseUri) response;
                DownloadSceneTemplateFiles(sceneTemplatePrefab, responseManifest.ByteData, isLoadingScene);
            };

            requestManifest.OnError += s => { Helper.ShowErrorLoadScene(); };
        }

        public override void LoadProjectStructure(int projectId, Action<ProjectStructure> onFinish)
        {
            const string apiRoute = ApiRoutes.ProjectStructureRequest;
            string structureRequestString = string.Format(apiRoute, projectId);

            var structureRequest = new RequestApi(structureRequestString);

            Logger.Info("API request: " + structureRequestString);

            structureRequest.OnFinish += response =>
            {
                var structureResponse = (ResponseApi) response;
                Logger.Info(RequestApi.GetResponseInfoMessage(structureRequest, structureResponse));

                if (!Helper.IsResponseGood(structureResponse))
                {
                    onFinish.Invoke(null);
                    Helper.ProcessBadResponse(structureRequest, structureResponse);
                    return;
                }

                try
                {
                    var projectStructureJson = structureResponse.Data.ToString();
                    ProjectStructure = projectStructureJson.JsonDeserialize<ProjectStructure>();
                }
                catch (Exception e)
                {
                    Logger.Fatal(
                        RequestApi.GetResponseErrorMessage(structureRequest, structureResponse)
                        + Environment.NewLine
                        + $"Exception = {e}");
                    onFinish.Invoke(null);
                    return;
                }

                foreach (ServerData.Scene scene in ProjectStructure.Scenes)
                {
                    var logicRequest = new RequestUri(scene.LogicResource);

                    logicRequest.OnFinish += response1 =>
                    {
                        var logicResponse = (ResponseUri) response1;
                        Logger.Info(RequestApi.GetResponseInfoMessage(structureRequest, structureResponse));
                        scene.AssemblyBytes = logicResponse.ByteData;
                    };
                }

                onFinish.Invoke(ProjectStructure);
            };
        }


        private void LoadCustomAssetApi(AssetInfo assetInfo, PrefabObject o)
        {
            var requestManifest = new RequestUri(ProjectData.IsMobileVr()
                ? o.AndroidBundleManifest
                : o.BundleManifest);

            requestManifest.OnFinish += response =>
            {
                var responseManifest = (ResponseUri) response;

                Hash128 bundleHash = Hash128.Compute(responseManifest.TextData);
                
                var assetBundleCacheSettings = new CachedAssetBundle()
                {
                    name = $"Object_{o.Guid}",
                    hash = bundleHash
                };
                
                string assetName = assetInfo.AssetName;
                string assetBundleUri = ProjectData.IsMobileVr() ? o.AndroidBundleResource : o.BundleResource;

                var requestAsset = new RequestAsset(assetName, assetBundleUri, assetBundleCacheSettings, new object[] {o, assetInfo});

                requestAsset.OnFinish += responseAsset =>
                {
                    FeedBackText = $"{LanguageManager.Instance.GetTextValue("LOADING")} {o.GetLocalizedName()}...";
                    CountLoadedObjects++;

                    if (LoaderAdapter.OnDownLoadUpdate != null)
                    {
                        LoaderAdapter.OnDownLoadUpdate(CountLoadedObjects / (float) LoadObjectsCounter.loadObjectsCounter.PrefabsCount);
                    }

                    if (ProjectData.GameMode == GameMode.View)
                    {
                        CreatePrefabEntity(responseAsset, o);
                    }
                    else
                    {
                        var requestDownLoad = new RequestTexture(Settings.Instance.ApiHost + o.IconResource, true);

                        requestDownLoad.OnFinish += responseUri =>
                        {
                            var responseTexture = (ResponseTexture) responseUri;
                            Texture2D texture2D = responseTexture.Texture;

                            var sprite = Sprite.Create(texture2D, new Rect(0, 0, texture2D.width, texture2D.height), Vector2.zero);
                            CreatePrefabEntity(responseAsset, o, sprite);
                        };

                        requestDownLoad.OnError += s => { CreatePrefabEntity(responseAsset, o); };
                    }
                };

                requestAsset.OnError += s =>
                {
                    FeedBackText = $"Load object {o.Name.en} error!\n{s}.";
                    Helper.ShowFatalErrorLoadObject(o, s);
                };
            };
            
            requestManifest.OnError += s =>
            {
                FeedBackText = $"Load object {o.Name.en} error! Manifest not found.\n{s}.";
                Helper.ShowFatalErrorLoadObject(o, s);
            };
        }

        private void DownloadSceneTemplateFiles(SceneTemplatePrefab sceneTemplatePrefab, byte[] manifestOnServer, bool isLoadingScene)
        {
            Logger.Info($"Starting download scene template <{sceneTemplatePrefab.GetLocalizedName()}> assets");

            string pathToEnvironmentsStorage = FileSystemUtils.GetFilesPath(ProjectData.IsMobileClient, "cache/sceneTemplates/");
            
            string environmentDirectory = Path.Combine(pathToEnvironmentsStorage, sceneTemplatePrefab.Guid);

            FileSystemUtils.CreateDirectory(environmentDirectory);

            string manifestStorageFile = Path.Combine(environmentDirectory, ProjectData.IsMobileVr() ? "android_bundle.manifest" : "bundle.manifest");

            
            if (File.Exists(manifestStorageFile))
            {
                var manifestOnStorage = File.ReadAllBytes(manifestStorageFile);

                //TODO: Turn back caching
                if (Equal(manifestOnServer, manifestOnStorage))
                {
                    LoadSceneTemplateFromStorage(sceneTemplatePrefab, environmentDirectory, isLoadingScene);
                }
                else
                {
                    LoadSceneTemplateFromWeb(sceneTemplatePrefab, environmentDirectory, isLoadingScene);
                }
            }
            else
            {
                LoadSceneTemplateFromWeb(sceneTemplatePrefab, environmentDirectory, isLoadingScene);
            }
        }

        private static bool Equal(byte[] a, byte[] b)
        {
            if (a.Length != b.Length)
            {
                return false;
            }

            return !a.Where((t, i) => t != b[i]).Any();
        }

        private void LoadSceneTemplateFromWeb(SceneTemplatePrefab sceneTemplatePrefab, string environmentDirectory, bool isLoadingScene)
        {
            DateTime startLoadingTime = DateTime.Now;
            Logger.Info($"Loading scene template \"{sceneTemplatePrefab.GetLocalizedName()}\" from web");
            
            var sceneDataRequest = new RequestUri(sceneTemplatePrefab.ConfigResource);
            sceneDataRequest.OnFinish += response =>
            {
                string json = ((ResponseUri) response).TextData;
                var sceneData = json.JsonDeserialize<SceneData>();

                var environmentFiles = new List<string>()
                {
                    sceneTemplatePrefab.ConfigResource,
                    ProjectData.IsMobileVr() ? sceneTemplatePrefab.AndroidBundleResource : sceneTemplatePrefab.BundleResource,
                    sceneTemplatePrefab.IconResource,
                    ProjectData.IsMobileVr() ? sceneTemplatePrefab.AndroidManifestResource : sceneTemplatePrefab.ManifestResource,
                };
                
                environmentFiles.AddRange(sceneData.DllNames.Select(x => $"{sceneTemplatePrefab.Resources}/{x}"));

                var requestDownloads = new RequestDownLoad(environmentFiles,
                    environmentDirectory,
                    LoaderAdapter.OnLoadingUpdate,
                    this,
                    LanguageManager.Instance.GetTextValue("LOADING_FILE"));

                requestDownloads.OnFinish += response1 =>
                {
                    TimeSpan span = DateTime.Now - startLoadingTime;
                    Logger.Info($"Scene template \"{sceneTemplatePrefab.GetLocalizedName()}\" is loaded. Time = {span.Seconds} sec.");

                    var responseDownload = (ResponseDownLoad) response1;

                    string sceneName = sceneData.Name;
                    foreach (string filename in responseDownload.Filenames)
                    {
                        Logger.Info($"Downloaded for scene \"{sceneName}\": {filename}");
                    }
                    
                    var dllNames = sceneData.DllNames.Select(x => (string) x).ToArray();
                    sceneTemplatePrefab.HasScripts = dllNames.Any();
                    foreach (string dllName in dllNames)
                    {
                        LoadDll(sceneTemplatePrefab, dllName);
                    }

                    string assetName = ProjectData.IsMobileVr() ? $"android_{sceneData.AssetBundleLabel}" : sceneData.AssetBundleLabel;
                    var bundlePath = $"{environmentDirectory}/{assetName}";

                    Logger.Info($"Loading scene template \"{sceneName}\"...");

                    var requestLoadSceneFromFile = new RequestLoadSceneFromFile(sceneName, bundlePath, null, isLoadingScene);
                    requestLoadSceneFromFile.OnFinish += response2 =>
                    {
                        var responseAsset = (ResponseAsset) response2;
                        string scenePath = Path.GetFileNameWithoutExtension(responseAsset.Path);
                        
                        if (!isLoadingScene)
                        {
                            ProjectDataListener.Instance.LoadScene(scenePath);
                        }
                        else
                        {
                            ProjectDataListener.Instance.AddLoadingScene(RequiredProjectArguments.ProjectConfigurationId, scenePath);
                        }
                    };

                    requestLoadSceneFromFile.OnError += s => { ShowErrorLoadScene(); };
                };

                requestDownloads.OnError += s => { ShowErrorLoadScene(); };
            };

            sceneDataRequest.OnError += s => { ShowErrorLoadScene(); };
        }

        private void LoadSceneTemplateFromStorage(SceneTemplatePrefab sceneTemplatePrefab, string environmentDirectory, bool isLoadingScene)
        {
            string sceneName = sceneTemplatePrefab.GetLocalizedName();
            Logger.Info($"Loading scene template \"{sceneName}\" from storage: " + environmentDirectory);
            
            var sceneDataPath = $"{environmentDirectory}/bundle.json";
            
            var requestJson = new RequestFileRead(sceneDataPath);

            requestJson.OnFinish += responseJson =>
            {
                string json = ((ResponseFileRead) responseJson).TextData;
                var sceneData = json.JsonDeserialize<SceneData>();

                var dllNames = sceneData.DllNames.Select(x => (string) x).ToArray();
                sceneTemplatePrefab.HasScripts = dllNames.Any();
                foreach (string dllName in dllNames)
                {
                    LoadDll(sceneTemplatePrefab, dllName);
                }
                
                var sceneBundlePath = $"{environmentDirectory}/{(ProjectData.IsMobileVr() ? "android_bundle" : "bundle")}";
                LoadSceneBundle(sceneBundlePath);
            };

            requestJson.OnError += responseJson =>
            {
                var sceneBundlePath = $"{environmentDirectory}/{(ProjectData.IsMobileVr() ? "android_bundle" : "bundle")}";
                LoadSceneBundle(sceneBundlePath);
            };
            
            void LoadSceneBundle(string bundlePath)
            {
                var request = new RequestLoadSceneFromFile(sceneName, bundlePath, null, isLoadingScene);
                request.OnFinish += response1 =>
                {
                    var response = (ResponseAsset) response1;
                    string scenePath = Path.GetFileNameWithoutExtension(response.Path);

                    if (!isLoadingScene)
                    {
                        ProjectDataListener.Instance.LoadScene(scenePath);
                    }
                    else
                    {
                        ProjectDataListener.Instance.AddLoadingScene(RequiredProjectArguments.ProjectConfigurationId, scenePath);
                    }
                };

                request.OnError += s =>
                {
                    string message = ErrorHelper.GetErrorDescByCode(ErrorCode.EnvironmentNotFoundError);
                    CoreErrorManager.Error(new Exception(message));
                    LogManager.GetCurrentClassLogger().Fatal("Scene template is not loaded from storage! " + s);
                    LauncherErrorManager.Instance.Show(message);
                };
            }
        }
    }
}
