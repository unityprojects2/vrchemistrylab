using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using NLog;
using SmartLocalization;
using UnityEngine;
using Varwin.Data.ServerData;
using Varwin.Errors;
using Varwin.Public;
using Varwin.UI;
using Varwin.UI.VRErrorManager;
using Varwin.WWW;
using Logger = NLog.Logger;
using Object = UnityEngine.Object;

namespace Varwin.Data
{
    public abstract class BaseLoader
    {
        #region ABSTRACT METHODS
        /// <summary>
        /// Load list of objects
        /// </summary>
        /// <param name="objects">Prefab object data with resources links</param>
        public abstract void LoadObjects(List<PrefabObject> objects);
        
        /// <summary>
        /// Load list of objects
        /// </summary>
        /// <param name="resources">Resources data</param>
        public abstract void LoadResources(List<ResourceDto> resources);

        protected abstract void LoadTextureResource(ResourceDto resource);
        protected abstract void LoadMeshResource(ResourceDto resource);
        protected abstract void LoadTextResource(ResourceDto resource);
        protected abstract void LoadAudioResource(ResourceDto resource);
        protected abstract void LoadVideoResource(ResourceDto resource);

        protected abstract void LoadAssetBundlePart(AssetInfo assetInfo, PrefabObject o);
        
        /// <summary>
        /// Load scene asset
        /// </summary>
        /// <param name="sceneTemplateId">Scene template Id (Scene template prefab)</param>
        /// <param name="isLoadingScene">Load scene as loading scene</param>
        public abstract void LoadSceneTemplate(int sceneTemplateId, bool isLoadingScene);
        
        /// <summary>
        /// Load/Get world structure data
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="onFinish"></param>
        public abstract void LoadProjectStructure(int projectId, Action<ProjectStructure> onFinish);
        #endregion

        #region PROTECTED FIELDS
        /// <summary>
        /// Current class logger
        /// </summary>
        protected static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        
        /// <summary>
        /// Count of current loaded (spawn ready) prefabs (LoadObjectsCounter.cs)
        /// </summary>
        protected static GameEntity LoadObjectsCounter { get; private set; }
        
        /// <summary>
        /// Count of current loaded (spawn ready) prefabs (LoadObjectsCounter.cs)
        /// </summary>
        protected static GameEntity LoadResourcesCounter { get; private set; }
        
        /// <summary>
        /// Count of current loaded assets 
        /// </summary>
        protected int CountLoadedObjects;
        
        /// <summary>
        /// Count of current loaded assets 
        /// </summary>
        protected int CountLoadedResources;
        
        /// <summary>
        /// Current loading project structure
        /// </summary>
        protected ProjectStructure ProjectStructure { get; set; }

        #endregion

        #region PUPLIC PROPERIES

        /// <summary>
        /// Current loading required project arguments
        /// </summary>
        public RequiredProjectArguments RequiredProjectArguments { get; set; }

        #endregion

        
        /// <summary>
        /// Loader processes realtime feedback
        /// </summary>
        public string FeedBackText { get; set; }

        /// <summary>
        /// Write assembly to disk and load
        /// </summary>
        /// <param name="dllPath">Path to write dll assembly</param>
        /// <param name="dllName">Dll file name</param>
        /// <param name="byteData">Assembly bytes</param>
        protected bool AddAssembly(string dllPath, string dllName, ref byte[] byteData)
        {
            if (!Directory.Exists(dllPath))
            {
                Directory.CreateDirectory(dllPath);
            }

            string dllFileName = $"{dllPath}/{dllName}";
            
            try
            {
                File.WriteAllBytes(dllFileName, byteData);
            }
            catch (Exception e)
            {
                Logger.Fatal($"Can not write assembly {dllName} to cache\n{e}");
                FeedBackText = e.Message;
                return false;
            }           

            try
            {
                Assembly assembly = Assembly.Load(byteData);
                GameStateData.AddAssembly(assembly.ManifestModule.Name, dllFileName);

                foreach (Type exportedType in assembly.GetExportedTypes())
                {
                    if (exportedType.BaseType == typeof(VarwinObject))
                    {
                        Logger.Info($"<Color=Cyan>{exportedType} is loaded!</Color>");
                    }
                    else
                    {
                        Logger.Debug(exportedType + " is loaded!");
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                Logger.Fatal($"Can not load assembly {dllName}\n{e}");
                FeedBackText = e.Message;
                return false;
            }
            
        }

        /// <summary>
        /// Sort objects if already loaded
        /// </summary>
        /// <param name="objects"></param>
        /// <returns></returns>
        public List<PrefabObject> GetRequiredObjects(List<PrefabObject> objects)
        {
            var result = new List<PrefabObject>(objects);
            var alreadyLoaded = GameStateData.GetPrefabsData();
            var resultRemove = new List<PrefabObject>();

            foreach (PrefabObject o in result)
            {
                PrefabObject found = alreadyLoaded.Find(prefabObject => prefabObject.Id == o.Id);

                if (found != null)
                {
                    PrefabObject loaded = result.Find(prefabObject => prefabObject.Id == o.Id);
                    resultRemove.Add(loaded);
                    Debug.Log(o.Name.en + " is already loaded");
                }
                else
                {
                    Debug.Log(o.Name.en + " is required");
                }
                
                Debug.Log(o.Name.en + " was added to ProjectStructure.SceneObjects");
            }

            foreach (PrefabObject o in resultRemove)
            {
                result.Remove(o);
            }

            return result;
        }
        
        public List<ResourceDto> GetRequiredResources(IEnumerable<ResourceDto> resources)
        {
            var result = new List<ResourceDto>(resources);
            var alreadyLoaded = GameStateData.GetResourcesData();
            var resultRemove = new List<ResourceDto>();

            foreach (ResourceDto o in result)
            {
                ResourceDto found = alreadyLoaded.Find(resourceDto => resourceDto.Id == o.Id);

                if (found != null)
                {
                    var loaded = result.Find(resourceDto => resourceDto.Id == o.Id);
                    resultRemove.Add(loaded);
                    Debug.Log(o.Name + " is already loaded");
                }
                else
                {
                    Debug.Log(o.Name + " is required");
                }
                
                Debug.Log(o.Name + "was added to ProjectStructure.Resources");
            }

            foreach (ResourceDto o in resultRemove)
            {
                result.Remove(o);
            }

            return result;
        }

        protected static string FindMainAssembly(AssetInfo assetInfo, PrefabObject o)
        {
            try
            {
                string result = assetInfo.Assembly.Find(x => Regex.Match(x, $"^{o.Config.type.Split('.')[0]}").Success);
                return result;
            }
            catch 
            {
                Logger.Fatal("Can not find main assembly");
                return null;
            }
            
        }
        
        protected void ResetObjectsCounter(int count)
        {
            ProjectData.ObjectsAreLoaded = false;
            FeedBackText = LanguageManager.Instance.GetTextValue("LOADING") + " " + LanguageManager.Instance.GetTextValue("SCENE_OBJECTS");
            CountLoadedObjects = 0;
                
            if (LoadObjectsCounter != null && LoadObjectsCounter.hasLoadObjectsCounter)
            {
                LoadObjectsCounter.ReplaceLoadObjectsCounter(count, 0, false);
            }
            else
            {
                LoadObjectsCounter = Contexts.sharedInstance.game.CreateEntity();
                LoadObjectsCounter.AddLoadObjectsCounter(count, 0, false);
            }
        }
        
        protected void ResetResourcesCounter(int count)
        {
            ProjectData.ResourcesAreLoaded = false;
            FeedBackText = LanguageManager.Instance.GetTextValue("LOADING") + " " + LanguageManager.Instance.GetTextValue("SCENE_OBJECTS");
            CountLoadedResources = 0;
                
            if (LoadResourcesCounter != null && LoadResourcesCounter.hasLoadResourcesCounter)
            {
                LoadResourcesCounter.ReplaceLoadResourcesCounter(count, 0, false);
            }
            else
            {
                LoadResourcesCounter = Contexts.sharedInstance.game.CreateEntity();
                LoadResourcesCounter.AddLoadResourcesCounter(count, 0, false);
            }
        }

        protected bool LoadAssetInfo(string json, ref AssetInfo assetInfo, PrefabObject o)
        {
            var launcherErrorManager = LauncherErrorManager.Instance;
            var vrErrorManager = VRErrorManager.Instance;

            try
            {
                assetInfo = json.JsonDeserialize<AssetInfo>();

                if (assetInfo.AssetName != null)
                {
                    return true;
                }

                string message = $"Asset name can not be null. {o.Name.en} Bundle.json is not actual version!";
                FeedBackText = message;
                Logger.Fatal(message);
                RequestManager.Instance.StopRequestsWithError(message);
                string errorMsg = $"{o.Name.en} not actual!";
                
                if (launcherErrorManager)
                {    
                    launcherErrorManager.ShowFatal(errorMsg, "null asset");
                }

                if (vrErrorManager)
                {
                    vrErrorManager.ShowFatal(errorMsg, "null asset");
                }

                return false;
            }
            catch (Exception e)
            {
                string message = $"AssetInfo can not be loaded. {o.Name.en} Bundle.json is not actual version! Bundle.json = {json}";
                FeedBackText = message;
                Logger.Fatal(message);
                RequestManager.Instance.StopRequestsWithError(message);

                if (launcherErrorManager)
                {
                    launcherErrorManager.ShowFatal($"{o.Name.en} not actual!", e.ToString());
                }

                if (vrErrorManager)
                {
                    vrErrorManager.ShowFatal($"{o.Name.en} not actual!", e.StackTrace);
                }

                return false;
            }
        }

        protected void CreateResourceEntity(ResourceDto resource, object resourceValue)
        {
            if (GameStateData.ResourceDataIsLoaded(resource.Guid))
            {
                LoadResourcesCounter.loadResourcesCounter.ResoursesLoaded++;
                Logger.Info($"{resource.Name} was ignored, because it already loaded");
                return;
            }
            
            var resourceObject = new ResourceObject
            {
                Data = resource,
                Value = resourceValue
            };
            
            GameEntity entity = Contexts.sharedInstance.game.CreateEntity();
            entity.AddResource(resourceObject);
            
            GameStateData.AddResourceObject(resourceObject, resource);
            
            LoadResourcesCounter.loadResourcesCounter.ResoursesLoaded++;
            Logger.Info($"Resourse {resource.Name} is loaded");
            
            GCManager.Collect();

            if (resourceValue != null)
            {
                ResourceLoaded?.Invoke(resource, resourceValue);
            }
            else
            {
                ResourceUnloaded?.Invoke(resource);
            }
        }

        protected void UpdateResourceEntity(ResourceDto resource, object resourceValue)
        {
            ResourceObject resourceObject = GameStateData.GetResource(resource.Guid);

            if (resourceObject == null)
            {
                CreateResourceEntity(resource, resourceValue);
                return;
            }

            object oldValue = resourceObject.Value;
            resourceObject.Value = resourceValue;

            if (resourceValue != null)
            {
                ResourceLoaded?.Invoke(resource, resourceValue);
            }
            else if (oldValue != null)
            {
                ResourceUnloaded?.Invoke(resource);

                if (oldValue is Object oldValueObject)
                {
                    Object.Destroy(oldValueObject);
                }
            }
            
            GCManager.Collect();
        }
        
        protected void CreatePrefabEntity(IResponse response, PrefabObject o, Sprite icon = null)
        {
            var alreadyLoaded = GameStateData.GetPrefabData(o.Id);

            if (alreadyLoaded != null)
            {
                LoadObjectsCounter.loadObjectsCounter.PrefabsLoaded++;
                Logger.Info($"Object {o.Name.en} was ignored, because it already loaded");
                return;
            }
            
            var responseAsset = (ResponseAsset) response;
            var unityObject = responseAsset.Asset;
            var serverObject = (PrefabObject) responseAsset.UserData[0];
            
            GameEntity entity = Contexts.sharedInstance.game.CreateEntity();
            entity.AddServerObject(serverObject);
            
            GameStateData.AddPrefabGameObject(responseAsset.Asset, o);
            GameStateData.AddObjectIcon(serverObject.Id, icon);
            entity.AddIcon(icon);

            if (o.Embedded)
            {
                GameStateData.AddToEmbeddedList(serverObject.Id);
            }
            
            var gameObject = unityObject as GameObject;
            if (gameObject)
            {
                entity.AddGameObject(gameObject);
            }
            else
            {
                var message = $"Game object is null in asset {o.Name.en}";
                FeedBackText = message;
                Logger.Fatal(message);
                RequestManager.Instance.StopRequestsWithError(message);
                return;
            }
            
            LoadObjectsCounter.loadObjectsCounter.PrefabsLoaded++;
            Logger.Info($"{o.Name.en} is loaded");
            
            GCManager.Collect();
        }

        protected SceneTemplatePrefab GetSceneTemplatePrefab(int sceneTemplateId)
        {
            SceneTemplatePrefab sceneTemplate = ProjectStructure.SceneTemplates.GetProjectScene(sceneTemplateId);

            if (sceneTemplate == null)
            {
                string message = string.Format(ErrorHelper.GetErrorDescByCode(ErrorCode.EnvironmentNotFoundError), "???");
                FeedBackText = message;
                Logger.Error(message);
                RequestManager.Instance.StopRequestsWithError(message);

                return null;
            }
            else
            {
                string message = string.Format(LanguageManager.Instance.GetTextValue("LOADING_SCENE_TEMPLATE"), sceneTemplate.GetLocalizedName());
                Logger.Info(message);
                FeedBackText = message;
            }

            return sceneTemplate;
        }

        protected void ShowErrorLoadScene()
        {
            string message = ErrorHelper.GetErrorDescByCode(ErrorCode.LoadSceneError);

            FeedBackText = message;
            Logger.Fatal("Scene template is not loaded");
            RequestManager.Instance.StopRequestsWithError(message);
        }

        public void LoadResource(ResourceDto resource)
        {
            if (resource.IsModel())
            {
                LoadMeshResource(resource);
            }
            else if (resource.IsPicture())
            {
                if (resource.OnDemand && !resource.ForceLoad)
                {
                    UpdateResourceEntity(resource, null);
                }
                else
                {
                    ResourceObject resourceObject = GameStateData.GetResource(resource.Guid);
                    if (resourceObject?.Value == null)
                    {
                        LoadTextureResource(resource);
                    }
                }
            } 
            else if (resource.IsText())
            {
                LoadTextResource(resource);
            }
            else if (resource.IsAudio())
            {
                LoadAudioResource(resource);
            }
            else if (resource.IsVideo())
            {
                LoadVideoResource(resource);
            }
        }

        protected void LoadResourceDefaultErrorHandler(ResourceDto resource, string message)
        {
            string feedBack = LanguageManager.Instance.GetTextValue("LOAD_RESOURCE_ERROR") + " " + resource.Name;
            Logger.Fatal(feedBack + ". " + message);
            FeedBackText = feedBack;
            RequestManager.Instance.StopRequestsWithError(feedBack);
        }

        public event Action<ResourceDto, object> ResourceLoaded;
        public event Action<ResourceDto> ResourceUnloaded;
    }
}
