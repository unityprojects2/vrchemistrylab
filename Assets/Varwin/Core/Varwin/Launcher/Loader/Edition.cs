namespace Varwin
{
    public enum Edition
    {
        None,
        Starter,
        Professional,
        Business
    }
}