using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SmartLocalization;
using UnityEngine;
using Varwin.Data.AssetBundle;
using Varwin.Data.ServerData;
using Varwin.Errors;
using Varwin.UI;
using Varwin.WWW;

namespace Varwin.Data
{
    public class StorageLoader : BaseLoader
    {
        public override void LoadObjects(List<PrefabObject> objects)
        {
            ResetObjectsCounter(objects.Count);

            foreach (PrefabObject prefabObject in objects)
            {
                LoadObject(prefabObject);
            }
        }
        
        public override void LoadResources(List<ResourceDto> resources)
        {
            ResetResourcesCounter(resources.Count);
            
            foreach (ResourceDto resourceDto in resources)
            {
                LoadResource(resourceDto);
            }
        }

        protected override void LoadAssetBundlePart(AssetInfo assetInfo, PrefabObject prefabObject)
        {
            if (assetInfo?.AssetBundleParts == null || assetInfo.AssetBundleParts.Count == 0)
            {
                return;
            }
            
            foreach (var pathToPart in assetInfo.AssetBundleParts)
            {
                var requestManifest = new RequestUri(ProjectData.IsMobileVr()
                    ? Path.Combine(prefabObject.ResourcesPath, $"android_{pathToPart}.manifest")
                    : Path.Combine(prefabObject.ResourcesPath, $"{pathToPart}.manifest"));
                
                requestManifest.OnFinish += response =>
                {
                    var responseManifest = (ResponseUri) response;
                    
                    Hash128 bundleHash = Hash128.Compute(responseManifest.TextData);

                    if (GameStateData.LoadedAssetBundleParts.Contains(bundleHash))
                    {
                        return;
                    }

                    GameStateData.LoadedAssetBundleParts.Add(bundleHash);

                    var assetBundleUri = ProjectData.IsMobileVr()
                        ? Path.Combine(prefabObject.ResourcesPath, $"android_{pathToPart}")
                        : Path.Combine(prefabObject.ResourcesPath, $"{pathToPart}");

                    var requestAsset = new RequestLoadAssetFromFile(assetInfo.AssetName, assetBundleUri, new object[] {prefabObject, assetInfo});

                    requestAsset.OnFinish += responseAsset =>
                    {
                        Debug.Log($"Resources loaded for {assetInfo.AssetName}");
                    };
                };
            }
        }

        public override void LoadSceneTemplate(int sceneTemplateId, bool isLoadingScene)
        {
            SceneTemplatePrefab sceneTemplate = ProjectData.ProjectStructure.SceneTemplates.GetProjectScene(sceneTemplateId);
            Logger.Info($"Loading scene template \"{sceneTemplate.GetLocalizedName()}\" from tar file");
            
            var requestConfig = new RequestFileRead(sceneTemplate.ConfigResource);
            requestConfig.OnFinish += responseConfig =>
            {
                var sceneData = ((ResponseFileRead) responseConfig).TextData.JsonDeserialize<SceneData>();

                string sceneUri = Settings.Instance.StoragePath + (ProjectData.IsMobileClient
                    ? sceneTemplate.AndroidBundleResource
                    : sceneTemplate.BundleResource);
                
                var dllNames = sceneData.DllNames.Select(x => (string) x).ToArray();
                sceneTemplate.HasScripts = dllNames.Any();
                foreach (string dllName in dllNames)
                {
                    LoadDll(sceneTemplate, dllName);
                }

                var request = new RequestLoadSceneFromFile(sceneData.Name, sceneUri, null, isLoadingScene);
                request.OnFinish += response1 =>
                {
                    var responseAsset = (ResponseAsset) response1;
                    string scenePath = Path.GetFileNameWithoutExtension(responseAsset.Path);

                    if (!isLoadingScene)
                    {
                        ProjectDataListener.Instance.LoadScene(scenePath);
                    }
                    else
                    {
                        ProjectDataListener.Instance.AddLoadingScene(RequiredProjectArguments.ProjectConfigurationId, scenePath);
                    }
                };

                request.OnError += s => { Helper.ShowErrorLoadScene(); };
            };
        }

        public override void LoadProjectStructure(int projectId, Action<ProjectStructure> onFinish)
        {
            var request = new RequestFileRead("/index.json");

            request.OnFinish += response =>
            {
                string jsonWorldStructure = ((ResponseFileRead) response).TextData;
                ProjectStructure = jsonWorldStructure.JsonDeserialize<ProjectStructure>();

                foreach (var scene in ProjectStructure.Scenes)
                {
                    string logicFilePath = Settings.Instance.StoragePath + scene.LogicResource;

                    if (!File.Exists(logicFilePath))
                    {
                        continue;
                    }
                    
                    var logicRequest = new RequestFileRead(scene.LogicResource);

                    logicRequest.OnFinish += response1 =>
                    {
                        ResponseFileRead logicResponse = (ResponseFileRead) response1;
                        scene.AssemblyBytes = logicResponse.ByteData;
                    };
                }

                onFinish.Invoke(ProjectStructure);
            };

            request.OnError += s =>
            {
                LauncherErrorManager.Instance.ShowFatal(ErrorHelper.GetErrorDescByCode(ErrorCode.LoadSceneError), null);
                FeedBackText = $"Can't find project folder";
            };
        }

        private void LoadObject(PrefabObject o)
        {
            RequestFileRead request = new RequestFileRead(o.ConfigResource, null, true);

            request.OnFinish += response =>
            {
                string json = ((ResponseFileRead) response).TextData;
                AssetInfo assetInfo = null;

                if (!LoadAssetInfo(json, ref assetInfo, o))
                {
                    return;
                }

                string mainAssembly = FindMainAssembly(assetInfo, o);

                foreach (string dllName in assetInfo.Assembly)
                {
                    if (dllName != mainAssembly)
                    {
                        LoadDll(o, dllName);
                    }
                }
                
                LoadAssetBundlePart(assetInfo, o);

                LoadDll(o, mainAssembly);

                LoadAssetFromStorage(assetInfo, o);
            };

            request.OnError += s =>
            {
                FeedBackText = $"Load object {o.Name.en} error!\n{s}.";
                Helper.ShowFatalErrorLoadObject(o, s);
            };
        }

        protected override void LoadMeshResource(ResourceDto resource)
        {
            var path = $"file:///{Settings.Instance.StoragePath}{resource.Resources}";
            var request = new Request3dModel(path);

            request.OnFinish += response =>
            {
                var response3DModel = (Response3dModel) response;
                CreateResourceEntity(resource, response3DModel.GameObject);
            };

            request.OnError += message => LoadResourceDefaultErrorHandler(resource, message);
        }

        protected override void LoadTextureResource(ResourceDto resource)
        {
            var url = $"file:///{Settings.Instance.StoragePath}{resource.Path}";
            
            var requestTexture = new RequestTexture(url, true);

            requestTexture.OnFinish += response =>
            {
                var responseTexture = (ResponseTexture) response;
                UpdateResourceEntity(resource, responseTexture.Texture);
            };

            requestTexture.OnError += message => LoadResourceDefaultErrorHandler(resource, message);
        }
        
        protected override void LoadTextResource(ResourceDto resource)
        {
            var requestFileRead = new RequestFileRead(resource.Path, null, true);

            requestFileRead.OnFinish += response =>
            {
                var responseFileRead = (ResponseFileRead) response;
                CreateResourceEntity(resource, new TextAsset(responseFileRead.TextData));
            };
                
            requestFileRead.OnError += message => LoadResourceDefaultErrorHandler(resource, message);
        }

        protected override void LoadAudioResource(ResourceDto resource)
        {
            var path = $"file:///{Settings.Instance.StoragePath}{resource.Path}";
            var request = new RequestAudio(path);
            
            request.OnFinish += response =>
            {
                var responseAudio = (ResponseAudio) response;
                CreateResourceEntity(resource, responseAudio.AudioClip);
            };

            request.OnError += message => LoadResourceDefaultErrorHandler(resource, message);
        }
        
        protected override void LoadVideoResource(ResourceDto resource)
        {
            var path = $"file:///{Settings.Instance.StoragePath}{resource.Path}";
            var request = new RequestVideo(path);
            
            request.OnFinish += response =>
            {
                var responseVideo = (ResponseVideo) response;
                CreateResourceEntity(resource, responseVideo.VideoUrl);
            };

            request.OnError += message => LoadResourceDefaultErrorHandler(resource, message);
        }
        
        private void LoadDll(PrefabObject o, string dllName)
        {
            string dllCachePath = FileSystemUtils.GetFilesPath(ProjectData.IsMobileClient, "cache/dll/")
                                  + o.Name.en
                                  + o.Guid;

            string dllPath = o.ResourcesPath + "/" + dllName;

            new RequestFileRead(dllPath).OnFinish += response1 =>
            {
                var byteData = ((ResponseFileRead) response1).ByteData;

                bool error = !AddAssembly(dllCachePath, dllName, ref byteData);

                if (!error)
                {
                    return;
                }

                string message = ErrorHelper.GetErrorDescByCode(ErrorCode.LoadObjectError)
                                 + "\n"
                                 + o.GetLocalizedName();
                
                FeedBackText = message;
                Logger.Fatal(message);
                FeedBackText = message;
                RequestManager.Instance.StopRequestsWithError(message);
            };
        }

        private void LoadDll(SceneTemplatePrefab s, string dllName)
        {
            string dllCachePath = $"{FileSystemUtils.GetFilesPath(ProjectData.IsMobileClient, "cache/dll/")}{s.Guid}";
            
            string dllPath = $"{s.Resources}/{dllName}";

            new RequestFileRead(dllPath).OnFinish += response1 =>
            {
                var byteData = ((ResponseFileRead) response1).ByteData;

                bool error = !AddAssembly(dllCachePath, dllName, ref byteData);

                if (!error)
                {
                    return;
                }

                string message = ErrorHelper.GetErrorDescByCode(ErrorCode.LoadObjectError)
                                 + "\n"
                                 + s.GetLocalizedName();
                
                FeedBackText = message;
                Logger.Fatal(message);
                FeedBackText = message;
                RequestManager.Instance.StopRequestsWithError(message);
            };
        }

        private void LoadAssetFromStorage(AssetInfo assetInfo, PrefabObject o)
        {
            string assetName = assetInfo.AssetName;
            string assetBundleUri = ProjectData.IsMobileClient ? o.AndroidBundleResource : o.BundleResource;
            
            RequestLoadAssetFromFile requestAsset =
                new RequestLoadAssetFromFile(assetName, assetBundleUri, new object[] {o, assetInfo});

            requestAsset.OnFinish += response =>
            {
                FeedBackText = LanguageManager.Instance.GetTextValue("LOADING")
                               + " "
                               + o.GetLocalizedName()
                               + "...";
                CreatePrefabEntity(response, o);
            };

        }
    }
}
