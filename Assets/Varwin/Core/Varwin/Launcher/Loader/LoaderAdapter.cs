﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLog;
using Photon.Pun;
using SmartLocalization;
using UnityEngine;
using Varwin.Data;
using Varwin.Data.ServerData;
using Varwin.Errors;
using Varwin.Onboarding;
using Varwin.PlatformAdapter;
using Varwin.Public;
using Varwin.PUN;
using Varwin.UI;
using Logger = NLog.Logger;
using Object = UnityEngine.Object;

namespace Varwin
{
    public static class LoaderAdapter
    {
        public static BaseLoader Loader { get; private set; }

        public delegate void ProgressUpdate(float val);

        public static ProgressUpdate OnLoadingUpdate;
        public static ProgressUpdate OnDownLoadUpdate;
        public static Type LoaderType => Loader?.GetType();
        public static string FeedBackText => Loader.FeedBackText;
        private static RequiredProjectArguments _requiredProjectArguments;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public static bool IsConnectedToMultiplayer { get; set; }

        private static List<PrefabObject> UsingObjects { get; set; }

        private static bool Initialized { get; set; }

        private const string AutoLanguage = "auto";

        public static void Init(BaseLoader loader)
        {
            if (Initialized)
            {
                return;
            }
            
            Loader = loader;
            
            if (Settings.Instance.Multiplayer)
            {
                PhotonNetwork.AutomaticallySyncScene = true;
                PhotonNetwork.PhotonServerSettings.AppSettings.AppIdRealtime = Settings.Instance.PhotonAppId;

                if (string.IsNullOrEmpty(Settings.Instance.PhotonHost))
                {
                    return;
                }

                PhotonNetwork.PhotonServerSettings.AppSettings.Server = Settings.Instance.PhotonHost;
                PhotonNetwork.PhotonServerSettings.AppSettings.Port = Convert.ToInt32(Settings.Instance.PhotonPort);
            }
            else
            {
                PhotonNetwork.OfflineMode = true;
            }

            Initialized = true;

            LogManager.GetCurrentClassLogger().Info($"<Color=Magenta>Loader adapter with {loader.GetType().Name} initialized...</Color>");
        }
        
        public static void Reset()
        {
            Loader = null;
            Initialized = false;
        }

        public static void LoaderFeedBackText(string message)
        {
            Loader.FeedBackText = message;
        }

        public static void LoadResources(ResourceDto resource)
        {
            LoadResources(new List<ResourceDto> {resource});
        }

        public static void LoadResources(IEnumerable<ResourceDto> resources)
        {
            var requiredResources = Loader.GetRequiredResources(resources);
            Logger.Info("<Color=Olive><b>Load Resources started...</b></Color>");
            Loader.LoadResources(requiredResources);
        }

        public static void LoadPrefabObjects(List<PrefabObject> objects)
        {
            var requiredObjects = Loader.GetRequiredObjects(objects);
            Logger.Info("<Color=Olive><b>Load SceneObjects started...</b></Color>");
            Loader.LoadObjects(requiredObjects);
        }
 
        public static void LoadProjectStructure(int projectId, Action<ProjectStructure> onFinish)
        {
            Loader.LoadProjectStructure(projectId, onFinish);
        }

        private static void LoadSceneTemplate(int sceneTemplateId)
        {
            Logger.Info("<Color=Olive><b>Load Scene started...</b></Color>");
            Loader.LoadSceneTemplate(sceneTemplateId, false);
        }

        private static void CheckSceneId()
        {
            if (_requiredProjectArguments.SceneId != 0 || ProjectData.ProjectStructure == null)
            {
                return;
            }
            
            if (_requiredProjectArguments.ProjectConfigurationId == 0)
            {
                if (ProjectData.ProjectStructure.ProjectConfigurations.Count > 0)
                {
                    var sceneId = _requiredProjectArguments.SceneId = ProjectData.ProjectStructure.ProjectConfigurations[0].StartSceneId;
                    if (sceneId == 0)
                    {
                        sceneId = ProjectData.ProjectStructure.Scenes[0].Id;
                    }
                    
                    _requiredProjectArguments.SceneId = sceneId;
                    return;
                }
            }

            ProjectConfiguration configuration = ProjectData.ProjectStructure.ProjectConfigurations.GetProjectConfigurationByProjectScene(_requiredProjectArguments.ProjectConfigurationId);

            if (configuration == null)
            {
                return;
            }
            
            _requiredProjectArguments.SceneId = configuration.StartSceneId != 0 ? configuration.StartSceneId : ProjectData.ProjectStructure.Scenes[0].Id;
        }

        private static bool _isOtherScene = true;
        private static void LoadProjectScene()
        {
            if (ProjectData.ProjectStructure != null && _requiredProjectArguments.SceneId == 0)
            {
                CheckSceneId();
            }
            
            _isOtherScene = ProjectData.ProjectStructure == null || 
                            ProjectData.ProjectId != _requiredProjectArguments.ProjectId || 
                            ProjectData.SceneId != _requiredProjectArguments.SceneId ||
                            ProjectData.SceneId == _requiredProjectArguments.SceneId && ProjectData.IsRichSceneTemplate;
            
            bool isOtherMode = ProjectData.ProjectStructure != null && (ProjectData.GameMode != _requiredProjectArguments.GameMode);

            if (isOtherMode && !_isOtherScene)
            {
                if (ProjectData.GameMode != GameMode.Edit && _requiredProjectArguments.GameMode == GameMode.Edit)
                {
                    Helper.ReloadSceneObjects();
                }
                
                ProjectData.GameMode = _requiredProjectArguments.GameMode;
                ProjectDataListener.Instance.ReadyToGetNewMessages();

                return;
            }
            
            if (!_isOtherScene)
            {
                CheckSceneId();
            }
            
            Logger.Info($"<Color=Olive><b>Other scene: {_isOtherScene}</b></Color>");
            
            ProjectDataListener.Instance.BeforeSceneLoaded(_isOtherScene);
            ProjectData.SceneCleared += OnSceneCleared;
            ProjectData.SceneLoaded += OnBoarding;
            ProjectData.SceneLoaded += DisableAudioListenersOnScene;

            void OnSceneCleared()
            {
                ProjectData.SceneCleared -= OnSceneCleared;
                ProjectData.ObjectsLoaded += PrefabObjectsLoaded;

                if (ProjectData.ProjectId != _requiredProjectArguments.ProjectId || ProjectData.IsRichSceneTemplate)
                {
                    LoadProjectStructure(_requiredProjectArguments.ProjectId, OnLoadProjectStructure);
                    
                    void OnLoadProjectStructure(ProjectStructure projectStructure)
                    {
                        if (projectStructure == null)
                        {
                            return;
                        }

                        if (ProjectData.GameMode == GameMode.View)
                        {
                            ProjectConfiguration configuration = 
                                projectStructure.
                                ProjectConfigurations.
                                GetProjectConfigurationById(_requiredProjectArguments.ProjectConfigurationId);

                            if (!configuration.Lang.Equals(AutoLanguage))
                            {
                                LanguageManager.Instance.ChangeLanguage(configuration.Lang);
                            }
                        }

                        LoadLoadingScenes(projectStructure);
                       
                        _requiredProjectArguments.Guid = projectStructure.Guid;
                        Logger.Info("Checking license info...");

                        if (projectStructure.LicenseKey == null)
                        {
                            LauncherErrorManager.Instance.ShowFatal(ErrorHelper.GetErrorDescByCode(ErrorCode.LicenseKeyError), Environment.StackTrace);
                            Logger.Fatal("License information error");

                            return;
                        }
                        
                        Logger.Info($"License key provided: {projectStructure.LicenseKey}");

                        var license = ADec.GetProjectDecJson(projectStructure.LicenseKey).JsonDeserialize<License>();

                        if (license == null)
                        {
                            return;
                        }
                        
                        if (license.ExpiresAt.HasValue && DateTime.Compare(DateTime.Now, license.ExpiresAt.Value) >= 0)
                        {
                            license.EditionId = Edition.Starter;
                        }
                        
                        LicenseInfo.Value = license;

//                        if (LauncherErrorManager.Instance)
//                        {
//                            LauncherErrorManager.Instance.License(LicenseInfo.Value);
//                        }

                        Logger.Info("License is ok! Edition: " + LicenseInfo.Value.EditionId);

                        AuthorAttribution.ProjectAttribution =
                            AuthorAttribution.ParseProjectStructure(projectStructure);

                        ProjectData.ProjectStructure = projectStructure;
                        CheckSceneId();
                        SetMultiplayer(projectStructure.Multiplayer, _requiredProjectArguments);
                        LoadResourcesAndObjects(_requiredProjectArguments.SceneId);
                    }
                }
                else
                {
                    CheckSceneId();
                    SetMultiplayer(ProjectData.ProjectStructure.Multiplayer, _requiredProjectArguments);
                    LoadResourcesAndObjects(_requiredProjectArguments.SceneId);
                }
            }
        }

        private static void LoadLoadingScenes(ProjectStructure projectStructure)
        {
            foreach (ProjectConfiguration projectConfiguration in projectStructure.ProjectConfigurations)
            {
                if (projectConfiguration.LoadingSceneTemplateId == 0)
                {
                    continue;
                }

                if (!ProjectData.LoadingScenePaths.ContainsKey(projectConfiguration.LoadingSceneTemplateId))
                {
                    Loader.LoadSceneTemplate(projectConfiguration.LoadingSceneTemplateId, true);
                }
            }
        }

        private static void LoadResourcesAndObjects(int sceneId)
        {
            var usingResources = GetUsingResources(sceneId);
            LoadResources(usingResources);

            ProjectData.ResourcesLoaded += LoadObjects;

            void LoadObjects()
            {
                UsingObjects = GetUsingObjects(sceneId);
                LoadPrefabObjects(UsingObjects);
                ProjectData.ResourcesLoaded -= LoadObjects;
            }
        }

        private static void SetMultiplayer(bool multiplayer, RequiredProjectArguments requiredProjectArguments)
        {
            Settings.Instance.Multiplayer = multiplayer;

            if (Settings.Instance.Multiplayer)
            {
                PhotonConnector.Instance.Connect(requiredProjectArguments);
            }
            else
            {
                PhotonConnector.Instance.Disconnect();
            }
        }

        private static void OnBoarding()
        {
            if (!Settings.Instance.OnboardingMode || OnboardingManager.Instance != null)
            {
                return;
            }
            
            var tutorial = new GameObject("Tutorial Mode");
            var manager = tutorial.AddComponent<OnboardingManager>();
            manager.Init(_requiredProjectArguments.GameMode);
        }

        private static void DisableAudioListenersOnScene()
        {
            GameObject playerHead = InputAdapter.Instance.PlayerController.Nodes.Head.GameObject;
            var playerAudioListeners = playerHead.GetComponentsInChildren<AudioListener>();
            var audioListeners = Object.FindObjectsOfType<AudioListener>().Except(playerAudioListeners);
            foreach (AudioListener audioListener in audioListeners)
            {
                if (audioListener.GetComponentInParent<IPlayerController>() == null)
                {
                    audioListener.enabled = false;
                }
            }
        }

        private static void InitializeSceneTemplateObjects()
        {
            var sceneObjects = GetSceneTemplateObjects();
            SceneTemplatePrefab sceneTemplatePrefab = ProjectData.ProjectStructure.SceneTemplates.GetProjectScene(ProjectData.SceneTemplateId);
            ProjectData.IsRichSceneTemplate = sceneObjects.Count > 0 || sceneTemplatePrefab.HasScripts;
            
            foreach (var sceneObject in sceneObjects)
            {
                var spawn = new SpawnInitParams
                {
                    Name = sceneObject.Value,
                    IdScene = ProjectData.SceneId,
                    IdObject = 0,
                    SceneTemplateObject = true,
                };
            
                Helper.InitObject(0, spawn, sceneObject.Key, null);
            }
        }

        private static Dictionary<GameObject, string> GetSceneTemplateObjects()
        {
            var sceneObjects = new Dictionary<GameObject, string>();
        
            var descriptors = Object.FindObjectsOfType<VarwinObjectDescriptor>();
            foreach (VarwinObjectDescriptor descriptor in descriptors)
            {
                if (!sceneObjects.ContainsKey(descriptor.gameObject))
                {
                    sceneObjects.Add(descriptor.gameObject, descriptor.Name);
                }
            }
        
            var monoBehaviours = Object.FindObjectsOfType<MonoBehaviour>().Where(x => x is IVarwinInputAware);
            foreach (MonoBehaviour monoBehaviour in monoBehaviours)
            {
                if (monoBehaviour.GetComponentInParent<VarwinObjectDescriptor>())
                {
                    continue;
                }
            
                if (!sceneObjects.ContainsKey(monoBehaviour.gameObject))
                {
                    sceneObjects.Add(monoBehaviour.gameObject, monoBehaviour.name);
                }
            }

            return sceneObjects;
        }
        
        private static List<PrefabObject> GetUsingObjects(int sceneId)
        {
            var sceneObjects = GetSceneObjects(sceneId);
            var usingObjects = new List<PrefabObject>();

            foreach (PrefabObject prefabObject in ProjectData.ProjectStructure.Objects)
            {
                foreach (SceneObjectDto objectDto in sceneObjects)
                {
                    if (objectDto.ObjectId != prefabObject.Id)
                    {
                        continue;
                    }

                    if (!usingObjects.Contains(prefabObject))
                    {
                        usingObjects.Add(prefabObject);
                    }
                }
            }

            return usingObjects;
        }
        
        private static List<ResourceDto> GetUsingResources(int sceneId)
        {
            Data.ServerData.Scene currentScene = ProjectData.ProjectStructure.Scenes.FirstOrDefault(x => x.Id == sceneId);
            var onDemandedResourceGuids = currentScene?.Data?.OnDemandedResourceGuids;

            var usingResources = new List<ResourceDto>();

            if (ProjectData.ProjectStructure.Resources == null)
            {
                return usingResources;
            }

            var sceneObjects = GetSceneObjects(sceneId);
            
            foreach (ResourceDto projectResource in ProjectData.ProjectStructure.Resources)
            {
                foreach (SceneObjectDto sceneObjectDto in sceneObjects)
                {
                    if (sceneObjectDto.Resources == null)
                    {
                        continue;
                    }
                    
                    foreach (ResourceDto sceneObjectResource in sceneObjectDto.Resources)
                    {
                        if (sceneObjectResource.Id != projectResource.Id)
                        {
                            continue;
                        }

                        if (usingResources.Exists(o => o.Guid == sceneObjectResource.Guid))
                        {
                            continue;
                        }

                        if (onDemandedResourceGuids != null && onDemandedResourceGuids.Contains(sceneObjectResource.Guid))
                        {
                            sceneObjectResource.OnDemand = true;
                        }
                        
                        usingResources.Add(sceneObjectResource);
                    }
                }
            }

            return usingResources;
        }
        
        private static List<SceneObjectDto> GetSceneObjects(int sceneId)
        {
            Data.ServerData.Scene currentScene = ProjectData.ProjectStructure.Scenes.Find(scene => scene.Id == sceneId);
            var sceneObjects = new List<SceneObjectDto>(); 
            sceneObjects.AddRange(currentScene.SceneObjects);

            foreach (SceneObjectDto objectDto in currentScene.SceneObjects)
            {
                GetSceneObjectsHierarchy(objectDto, sceneObjects);
            }

            return sceneObjects;
        }

        private static void GetSceneObjectsHierarchy(SceneObjectDto objectDto, List<SceneObjectDto> result)
        {
            foreach (SceneObjectDto child in objectDto.SceneObjects)
            {
                result.Add(child);
                GetSceneObjectsHierarchy(child, result);
            }
        }

        private static void PrefabObjectsLoaded()
        {
            if (_isOtherScene)
            {
                if (ProjectData.ProjectStructure == null)
                {
                    return;
                }

                Data.ServerData.Scene sceneTemplate = ProjectData.ProjectStructure.Scenes.GetProjectScene(_requiredProjectArguments.SceneId);

                if (sceneTemplate == null)
                {
                    return;
                }
                
                int sceneTemplateId = sceneTemplate.SceneTemplateId;
                ProjectData.SceneLoaded += OnLoadScene;
                
                // ProjectData.SceneLoaded += InitializeSceneTemplateObjects;
                ProjectData.SceneLoaded += PlayerAnchorManager.StoreSettingsOnSceneLoad;
                LoadSceneTemplate(sceneTemplateId);
            
                void OnLoadScene()
                {
                    Helper.SpawnSceneObjects(_requiredProjectArguments.SceneId, ProjectData.SceneId);
                    ProjectData.UpdateSubscribes(_requiredProjectArguments);
                    ProjectData.SceneLoaded -= OnLoadScene;
                    
                    // ProjectData.SceneLoaded -= InitializeSceneTemplateObjects;
                }
            }

            else
            {
                ProjectData.SceneWasLoaded();
                Helper.SpawnSceneObjects(_requiredProjectArguments.SceneId, ProjectData.SceneId);
                ProjectData.UpdateSubscribes(_requiredProjectArguments);
            }
            
            ProjectData.ObjectsLoaded -= PrefabObjectsLoaded;
        }

        public static void LoadProject(LaunchArguments launchArguments)
        {
            _requiredProjectArguments.ProjectId = launchArguments.projectId;
            _requiredProjectArguments.SceneId = launchArguments.sceneId;
            _requiredProjectArguments.ProjectConfigurationId = launchArguments.projectConfigurationId;
            _requiredProjectArguments.GameMode = (GameMode) launchArguments.gm;
            _requiredProjectArguments.PlatformMode = (PlatformMode) launchArguments.platformMode;
            //Force it to be VR or Desktop from the beginning
            ProjectData.PlatformMode = (PlatformMode) launchArguments.platformMode;
            Loader.RequiredProjectArguments = _requiredProjectArguments;
            LoadProjectScene();
        }

        public static void LoadProject(int projectId, int sceneId, int projectConfigurationId)
        {
            _requiredProjectArguments.ProjectId = projectId;
            _requiredProjectArguments.SceneId = sceneId;
            _requiredProjectArguments.ProjectConfigurationId = projectConfigurationId;
            _requiredProjectArguments.GameMode = ProjectData.GameMode;
            _requiredProjectArguments.PlatformMode = ProjectData.PlatformMode;
            Loader.RequiredProjectArguments = _requiredProjectArguments;
            LoadProjectScene();
        }

        public static void LoadProject(ProjectConfiguration projectConfiguration)
        {
            _requiredProjectArguments.ProjectId = projectConfiguration.ProjectId;
            _requiredProjectArguments.SceneId = projectConfiguration.StartSceneId;
            _requiredProjectArguments.ProjectConfigurationId = projectConfiguration.Id;
            _requiredProjectArguments.GameMode = ProjectData.GameMode;
            _requiredProjectArguments.PlatformMode = (PlatformMode) projectConfiguration.PlatformMode;
            ProjectData.PlatformMode = (PlatformMode) projectConfiguration.PlatformMode;
            Loader.RequiredProjectArguments = _requiredProjectArguments;
            LoadProjectScene();
        }

        public static void LoadProjectConfiguration(int projectConfigurationId)
        {
            ProjectConfiguration projectConfiguration = ProjectData.ProjectStructure.ProjectConfigurations.Find(configuration => configuration.Id == projectConfigurationId);
            LoadProject(projectConfiguration);
        }
    }
}
