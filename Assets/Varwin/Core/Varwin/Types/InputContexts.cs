﻿using UnityEngine;
using Varwin.PlatformAdapter;

namespace Varwin
{
    public class UsingContext
    {
        public GameObject GameObject;
        public ControllerInteraction.ControllerHand Hand;
    }

    public class GrabingContext
    {
        public GameObject GameObject;
        public ControllerInteraction.ControllerHand Hand;
    }
}
