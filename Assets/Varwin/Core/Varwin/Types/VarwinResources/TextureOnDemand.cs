using System;
using UnityEngine;
using Varwin.Data;
using Varwin.Data.ServerData;
using Varwin.WWW;

namespace Varwin
{
    public class TextureOnDemand
    {
        private ResourceDto _resource;
        
        public ResourceDto Resource
        {
            get => _resource;
            set
            {
                if (_resource == value)
                {
                    return;
                }

                if (value == null)
                {
                    Texture = null;
                    OnUnloaded?.Invoke();
                }
                else
                {
                    Texture = (Texture) GameStateData.GetResourceValue(value.Guid);
                    if (Texture)
                    {
                        OnLoaded?.Invoke(Texture);
                    }
                    else
                    {
                        OnUnloaded?.Invoke();
                    }
                }
                
                _resource = value;
            }
        }
        
        public Texture Texture { get; private set; }

        public event Action<Texture> OnLoaded;
        public event Action OnUnloaded;

        public TextureOnDemand()
        {
            LoaderAdapter.Loader.ResourceLoaded += OnResourceLoaded;
            LoaderAdapter.Loader.ResourceUnloaded += OnResourceUnloaded;
            
            ProjectData.GameModeChanging += OnGameModeChanging;
        }

        public TextureOnDemand(ResourceDto resource) : this()
        {
            Resource = resource;
        }

        ~TextureOnDemand()
        {
            if (Texture)
            {
                UnityEngine.Object.Destroy(Texture);
            }
            
            LoaderAdapter.Loader.ResourceLoaded -= OnResourceLoaded;
            LoaderAdapter.Loader.ResourceUnloaded -= OnResourceUnloaded;
            
            ProjectData.GameModeChanging -= OnGameModeChanging;
        }

        private void OnGameModeChanging(GameMode newGameMode)
        {
            Unload();
        }

        public void Load()
        {
            if (Resource == null)
            {
                return;
            }
            
            Resource.ForceLoad = true;
            LoaderAdapter.Loader.LoadResource(Resource);
        }

        public void Unload()
        {
            if (Resource == null)
            {
                return;
            }
            
            Resource.ForceLoad = false;
            LoaderAdapter.Loader.LoadResource(Resource);
        }

        private void OnResourceLoaded(ResourceDto resource, object resourceValue)
        {
            if (Resource == null)
            {
                return;
            }
            
            if (resource != Resource)
            {
                return;
            }

            Texture = (Texture2D) resourceValue;
            
            OnLoaded?.Invoke(Texture);
        }

        private void OnResourceUnloaded(ResourceDto resource)
        {
            if (Resource == null)
            {
                return;
            }
            
            if (resource != Resource)
            {
                return;
            }

            if (Texture)
            {
                UnityEngine.Object.Destroy(Texture);
            }
            
            OnUnloaded?.Invoke();
        }

        public static implicit operator Texture(TextureOnDemand t) => t?.Texture;
        public static implicit operator TextureOnDemand(ResourceDto resourceDto) => new TextureOnDemand(resourceDto);

        public static implicit operator bool(TextureOnDemand t) => t != null;
    }
}