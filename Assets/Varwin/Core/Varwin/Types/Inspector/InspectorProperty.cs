using System.Reflection;
using UnityEngine;
using UnityEngine.UI;
using Varwin.Public;

namespace Varwin
{
    public class InspectorProperty
    {
        public string Name => $"{ComponentReference.Name}__{PropertyInfo.Name}";
        
        public ComponentReference ComponentReference;
        public PropertyInfo PropertyInfo;
        public I18n LocalizedName;
        public InspectorPropertyData Data;
        public bool IsResource => PropertyInfo.PropertyType == typeof(Sprite)
                                  || PropertyInfo.PropertyType == typeof(Texture)
                                  || PropertyInfo.PropertyType == typeof(Texture2D)
                                  || PropertyInfo.PropertyType == typeof(TextureOnDemand)
                                  || PropertyInfo.PropertyType == typeof(TextAsset)
                                  || PropertyInfo.PropertyType == typeof(GameObject);

        public bool OnDemand => PropertyInfo.PropertyType == typeof(TextureOnDemand);
    }
}