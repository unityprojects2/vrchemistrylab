namespace Varwin
{
    public class InspectorPropertyData
    {
        public string ComponentPropertyName { get; set; }
        public PropertyValue PropertyValue { get; set; }
    }

    public class PropertyValue
    {
        public string ResourceGuid;
        public object Value;

        public object GetPropertyRealValue() => Value ?? ResourceGuid;
        public string GetPropertyValueString() => Value != null ? Value.ToString() : ResourceGuid;
    }
    
    public class InspectorPropertyInfo
    {
        public int ControllerId { get; }
        public ObjectController Controller { get; }
        public InspectorProperty Property { get; }
        
        public object OldValue { get; set; }
        
        public object StartValue { get; set; }

        public InspectorPropertyInfo(ObjectController controller, InspectorProperty property)
        {
            ControllerId = controller.Id;
            Controller = controller;
            Property = property;
        }
    }
}
