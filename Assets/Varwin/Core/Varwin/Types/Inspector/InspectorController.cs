﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json.Linq;
using NLog;
using UnityEngine;
using Varwin.Core.Behaviours;
using Varwin.Data;
using Varwin.Data.ServerData;
using Varwin.Public;
using Varwin.WWW;
using Logger = NLog.Logger;
using Object = UnityEngine.Object;


namespace Varwin
{
    public class InspectorController
    {
        /// <summary>
        /// Event after changed property value
        /// </summary>
        public event Action<string, object> PropertyValueChanged;

        /// <summary>
        /// Properties with VarwinInspector
        /// </summary>
        public Dictionary<string, InspectorProperty> InspectorProperties { get; }
        
        /// <summary>
        /// Methods with VarwinInspector
        /// </summary>
        public Dictionary<string, InspectorMethod> InspectorMethods { get; }

        /// <summary>
        /// Set of Inspector Components types
        /// </summary>
        public HashSet<Type> InspectorComponentsTypes { get; }
        
        public List<string> ObjectBehaviours { get; }
        
        private Dictionary<string, InspectorProperty> SerializableProperties { get; }
        private Dictionary<string, ResourceObject> UsingResources { get; set; }
        private List<InspectorPropertyData> InspectorPropertiesData { get; }

        private readonly Dictionary<PropertyInfo, PropertyWrapper> _propertyWrappers = new Dictionary<PropertyInfo, PropertyWrapper>();
        private readonly VarwinObjectDescriptor _varwinObjectDescriptor;
        private readonly Dictionary<string, object> _propertyDefaults = new Dictionary<string, object>();
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly ObjectController _objectController;
        
        public InspectorController(ObjectController objectController, List<InspectorPropertyData> propertyDatas)
        {
            _objectController = objectController;
            
            _varwinObjectDescriptor = objectController.VarwinObjectDescriptor;
            SerializableProperties = new Dictionary<string, InspectorProperty>();
            InspectorProperties = new Dictionary<string, InspectorProperty>();
            InspectorMethods = new Dictionary<string, InspectorMethod>();
            InspectorComponentsTypes = new HashSet<Type>();
            ObjectBehaviours = new List<string>();
            
            ObjectBehaviours.AddRange(BehavioursCollection.GetBehaviours(objectController));
            
            if (_varwinObjectDescriptor && _varwinObjectDescriptor.AddBehavioursAtRuntime)
            {
                ObjectBehaviours.AddRange(BehavioursCollection.AddBehaviours(objectController));
                _varwinObjectDescriptor.Components.SetupRuntimeBehaviours(objectController.RootGameObject);
            }
            
            InspectorPropertiesData = new List<InspectorPropertyData>();
            if (propertyDatas != null)
            {
                foreach (var inspectorPropertyData in propertyDatas)
                {
                    InspectorPropertiesData.Add(new InspectorPropertyData
                    {
                        ComponentPropertyName = inspectorPropertyData.ComponentPropertyName, 
                        PropertyValue = new PropertyValue
                        {
                            ResourceGuid = inspectorPropertyData.PropertyValue?.ResourceGuid,
                            Value = inspectorPropertyData.PropertyValue?.Value
                        }
                    });
                }
            }
        }

        public void InitInspectorFields()
        {
            if (!_varwinObjectDescriptor || _varwinObjectDescriptor.Components == null || _varwinObjectDescriptor.Components.Count == 0)
            {
                return;
            }

            var varwinInspectors = _varwinObjectDescriptor.Components.Where(x => VarwinInspectorHelper.IsVarwinInspector(x.Component));
            
            foreach (ComponentReference varwinInspector in varwinInspectors)
            {
                var members = VarwinInspectorHelper.GetVarwinInspectorMembers(varwinInspector.Component);
                var properties = members.OfType<PropertyInfo>();
                var methods = members.OfType<MethodInfo>();
                
                foreach (PropertyInfo property in properties)
                {
                    try
                    {
                        string componentPropertyName = $"{varwinInspector.Name}__{property.Name}";

                        var varwinSerializableAttribute = property.GetCustomAttribute<VarwinSerializableAttribute>(true);
                        var varwinInspectorAttribute = property.GetCustomAttribute<VarwinInspectorAttribute>(true);

                        if (varwinSerializableAttribute == null)
                        {
                            continue;
                        }

                        if (_propertyWrappers.ContainsKey(property))
                        {
                            continue;
                        }
                        
                        _propertyWrappers.Add(property, ReflectionUtils.BuildPropertyWrapper(property));
                        
                        InspectorPropertyData data = null;

                        if (InspectorPropertiesData != null && InspectorPropertiesData.Count > 0)
                        {
                            data = InspectorPropertiesData.FirstOrDefault(x => x.ComponentPropertyName == componentPropertyName);
                        }
                        else
                        {
                            object varwinInspectorPropertyValue = _propertyWrappers[property].Getter(varwinInspector.Component);

                            if (varwinInspectorPropertyValue != null && !(varwinInspectorPropertyValue is Object))
                            {
                                data = new InspectorPropertyData
                                {
                                    ComponentPropertyName = property.Name,
                                    PropertyValue = new PropertyValue
                                    {
                                        Value = varwinInspectorPropertyValue
                                    }
                                };
                            }
                            else if (varwinInspectorPropertyValue == null && property.PropertyType == typeof(string))
                            {
                                data = new InspectorPropertyData
                                {
                                    ComponentPropertyName = property.Name,
                                    PropertyValue = new PropertyValue
                                    {
                                        Value = ""
                                    }
                                };
                            }
                        }
                        
                        var inspectorProperty = new InspectorProperty
                        {
                            ComponentReference = varwinInspector,
                            PropertyInfo = property,
                            LocalizedName = varwinInspectorAttribute?.LocalizedNames,
                            Data = data
                        };

                        SerializableProperties.Add(componentPropertyName, inspectorProperty);
                        if (varwinInspectorAttribute != null)
                        {
                            InspectorProperties.Add(componentPropertyName, inspectorProperty);
                            InspectorComponentsTypes.Add(inspectorProperty.ComponentReference.Type);
                        }
                    }
                    catch (Exception e)
                    {
                        LogManager.GetCurrentClassLogger()
                            .Error($"Exception was raised in property {property.Name} of class {varwinInspector.Component}: {e}");
                    }
                }
                
                foreach (MethodInfo method in methods)
                {
                    var varwinInspectorAttribute = method.GetCustomAttribute<VarwinInspectorAttribute>(true); 
                    var inspectorMethod = new InspectorMethod
                    {
                        MethodInfo = method,
                        ComponentReference = varwinInspector,
                        LocalizedName = varwinInspectorAttribute?.LocalizedNames
                    };
                    
                    if (varwinInspectorAttribute != null)
                    {
                        InspectorMethods.Add($"{varwinInspector.Name}__{method.Name}", inspectorMethod);
                        InspectorComponentsTypes.Add(inspectorMethod.ComponentReference.Type);
                    }
                }
            }
        }

        public void InitResources()
        {
            UsingResources = new Dictionary<string, ResourceObject>();

            var usingResourcesGuid = new List<string>();

            foreach (InspectorPropertyData propertyInfo in InspectorPropertiesData)
            {
                string resourceGuid = propertyInfo.PropertyValue.ResourceGuid;

                if (string.IsNullOrEmpty(resourceGuid))
                {
                    continue;
                }

                usingResourcesGuid.Add(resourceGuid);
            }

            foreach (string guid in usingResourcesGuid)
            {
                ResourceObject resource = GameStateData.GetResource(guid);
                if (!UsingResources.ContainsKey(guid))
                {
                    UsingResources.Add(guid, resource);
                }
            }

            RequestManager.Instance.StartCoroutine(SetPropertiesWithDelay());
        }


        public object GetInspectorPropertyValue(string componentPropertyName)
        {
            var serializablePropertyPair = SerializableProperties.FirstOrDefault(x => x.Key == componentPropertyName);
            
            InspectorProperty serializableProperty = serializablePropertyPair.Value;

            if (serializableProperty.Data == null)
            {
                return null;
            }
            
            return serializableProperty.IsResource
                ? serializableProperty.Data.PropertyValue.ResourceGuid
                : serializableProperty.Data.PropertyValue.Value;
        }

        public object GetInspectorPropertyRealValue(string componentPropertyName)
        {
            KeyValuePair<string, InspectorProperty> serializablePropertyPair = SerializableProperties.FirstOrDefault(x => x.Key == componentPropertyName);
            InspectorProperty serializableProperty = serializablePropertyPair.Value;

            return _propertyWrappers[serializableProperty.PropertyInfo].Getter(serializableProperty.ComponentReference.Component);
        }

        public T GetInspectorPropertyValue<T>(string componentPropertyName)
        {
            var serializableProperty = SerializableProperties.FirstOrDefault(x => x.Key == componentPropertyName);
            return (T) _propertyWrappers[serializableProperty.Value.PropertyInfo].Getter(serializableProperty.Value.ComponentReference.Component);
        }

        public void SetInspectorPropertyValue(string componentPropertyName, object value)
        {
            bool existingProperty = false;

            foreach (InspectorPropertyData resourcePropertyData in InspectorPropertiesData)
            {
                if (resourcePropertyData.ComponentPropertyName != componentPropertyName)
                {
                    continue;
                }

                resourcePropertyData.PropertyValue.Value = value;
                resourcePropertyData.PropertyValue.ResourceGuid = null;
                existingProperty = true;
            }

            if (!existingProperty)
            {
                InspectorPropertiesData.Add(new InspectorPropertyData
                {
                    ComponentPropertyName = componentPropertyName,
                    PropertyValue = new PropertyValue
                    {
                        ResourceGuid = null, 
                        Value = value
                    }
                });
            }

            SetPropertyValue(componentPropertyName, value);
        }

        public void SetInspectorPropertyResourceValue(string componentPropertyName, string resourceGuid)
        {
            if (string.IsNullOrEmpty(resourceGuid))
            {
                RemoveUsingResource(componentPropertyName);
                return;
            }
            
            ResourceObject resourceObject = GameStateData.GetResource(resourceGuid);

            if (resourceObject == null)
            {
                void GetResourceCallback(ResourceDto resourceData)
                {
                    if (string.IsNullOrEmpty(resourceData.Guid))
                    {
                        Debug.LogError($"Resource with {resourceGuid} not found", _objectController.VarwinObjectDescriptor);
                        return;
                    }
                    
                    InspectorProperty inspectorProperty = FindInspectorProperty(componentPropertyName);

                    if (inspectorProperty.OnDemand)
                    {
                        resourceData.OnDemand = true;
                    }

                    LoaderAdapter.LoadResources(resourceData);
                    ProjectData.ResourcesLoaded += SetValue;
                }
                
                API.GetResourceByGuid(resourceGuid, GetResourceCallback);
            }
            else
            {
                SetValue();
            }
            
            void SetValue()
            {
                ProjectData.ResourcesLoaded -= SetValue;

                if (!UsingResources.ContainsKey(resourceGuid))
                {
                    resourceObject = GameStateData.GetResource(resourceGuid);
                    UsingResources.Add(resourceGuid, resourceObject);
                }

                bool existingProperty = false;

                foreach (InspectorPropertyData resourcePropertyData in InspectorPropertiesData)
                {
                    if (resourcePropertyData.ComponentPropertyName != componentPropertyName)
                    {
                        continue;
                    }

                    if (resourcePropertyData.PropertyValue.ResourceGuid == null)
                    {
                        continue;
                    }

                    if (resourcePropertyData.PropertyValue.ResourceGuid == resourceGuid)
                    {
                        //Don't do anything
                        //Property already set
                        return;
                    }

                    resourcePropertyData.PropertyValue.ResourceGuid = resourceGuid;
                    resourcePropertyData.PropertyValue.Value = null;
                    existingProperty = true;
                }

                if (!existingProperty)
                {
                    InspectorPropertiesData.Add(new InspectorPropertyData
                    {
                        ComponentPropertyName = componentPropertyName,
                        PropertyValue = new PropertyValue
                        {
                            ResourceGuid = resourceGuid, 
                            Value = null
                        }
                    });
                }

                SetPropertyResourceValue(componentPropertyName, resourceGuid);
            }
        }

        private void SetPropertyValue(string componentPropertyName, object value)
        {
            InspectorProperty inspectorProperty = FindInspectorProperty(componentPropertyName);
            
            if (inspectorProperty == null)
            {
                return;
            }

            inspectorProperty.Data = new InspectorPropertyData
            {
                ComponentPropertyName = inspectorProperty.Name,
                PropertyValue = new PropertyValue
                {
                    ResourceGuid = null,
                    Value = value
                }
            };

            var data = InspectorPropertiesData.FirstOrDefault(x => x.ComponentPropertyName == componentPropertyName);
            if (data != null)
            {
                data.PropertyValue = inspectorProperty.Data.PropertyValue;
            }
            else
            {
                InspectorPropertiesData.Add(inspectorProperty.Data);
            }

            SetPropertyValueBase(inspectorProperty.ComponentReference, inspectorProperty.PropertyInfo, value);
            
            PropertyValueChanged?.Invoke(componentPropertyName, value);
        }
        
        private void SetPropertyResourceValue(string componentPropertyName, string resourceGuid)
        {
            InspectorProperty inspectorProperty = FindInspectorProperty(componentPropertyName);

            if (inspectorProperty == null)
            {
                return;
            }

            object value = UsingResources[resourceGuid].Value;

            inspectorProperty.Data = new InspectorPropertyData
            {
                ComponentPropertyName = inspectorProperty.Name,
                PropertyValue = new PropertyValue
                {
                    ResourceGuid = resourceGuid,
                    Value = null
                }
            };

            var data = InspectorPropertiesData.FirstOrDefault(x => x.ComponentPropertyName == componentPropertyName);
            if (data != null)
            {
                data.PropertyValue = inspectorProperty.Data.PropertyValue;
            }
            else
            {
                InspectorPropertiesData.Add(inspectorProperty.Data);
            }

            SetPropertyValueBase(inspectorProperty.ComponentReference, inspectorProperty.PropertyInfo, value);
            
            PropertyValueChanged?.Invoke(componentPropertyName, resourceGuid);
        }
        
        public List<ResourceDto> GetUsingResourcesData()
        {
            ClearNotUsingResources();
            return UsingResources.Values.Select(resourcesValue => resourcesValue.Data).ToList();
        }

        public List<string> GetOnDemandedResourceGuids()
        {
            return SerializableProperties
                .Where(x => x.Value != null)
                .Where(x => x.Value.OnDemand)
                .Where(x => x.Value.Data?.PropertyValue?.ResourceGuid != null)
                .Select(x => x.Value.Data.PropertyValue.ResourceGuid)
                .ToList();
        }
        
        public List<InspectorPropertyData> GetInspectorPropertiesData()
        {
            var serializables = SerializableProperties.Where(x => !InspectorProperties.ContainsKey(x.Key));
            
            foreach (var serializable in serializables)
            {
                InspectorPropertyData data = serializable.Value.Data ?? new InspectorPropertyData
                {
                    ComponentPropertyName = serializable.Key.Substring(serializable.Key.IndexOf("__") + 2),
                    PropertyValue = new PropertyValue
                    {
                        ResourceGuid = null,
                        Value = _propertyWrappers[serializable.Value.PropertyInfo].Getter(serializable.Value.ComponentReference.Component)
                    }
                };
                
                PropertyValue value = data.PropertyValue;
                
                InspectorPropertyData item = InspectorPropertiesData.FirstOrDefault(x => x.ComponentPropertyName == serializable.Key);
                if (item == null)
                {
                    item = new InspectorPropertyData
                    {
                        ComponentPropertyName = serializable.Key,
                        PropertyValue = value
                    };
                    InspectorPropertiesData.Add(item);
                }
                else
                {
                    item.PropertyValue = value;
                }
            }

            foreach (var serializableProperty in SerializableProperties)
            {
                if (serializableProperty.Value?.Data == null)
                {
                    continue;
                }
                
                if (InspectorPropertiesData.FirstOrDefault(x => x.ComponentPropertyName == serializableProperty.Value.Name) != null)
                {
                    continue;
                }
                
                InspectorPropertiesData.Add(new InspectorPropertyData
                {
                    ComponentPropertyName = serializableProperty.Value.Name,
                    PropertyValue = serializableProperty.Value.Data.PropertyValue,
                });
            }

            foreach (InspectorPropertyData inspectorPropertyData in InspectorPropertiesData)
            {
                if (inspectorPropertyData.PropertyValue.Value is JObject)
                {
                    continue;
                }
                
                if (!(inspectorPropertyData.PropertyValue.Value is ValueType || inspectorPropertyData.PropertyValue.Value is string))
                {
                    inspectorPropertyData.PropertyValue.Value = null;
                }
            }
            
            return InspectorPropertiesData;
        } 
        
        private void RemoveUsingResource(string componentPropertyName)
        {
            InspectorPropertyData propertyDataToRemove =
                InspectorPropertiesData.FirstOrDefault(propertyData => propertyData.ComponentPropertyName == componentPropertyName);

            if (propertyDataToRemove?.PropertyValue.ResourceGuid == null)
            {
                return;
            }

            if (UsingResources.ContainsKey(propertyDataToRemove.PropertyValue.ResourceGuid))
            {
                UsingResources.Remove(propertyDataToRemove.PropertyValue.ResourceGuid);
            }

            InspectorPropertiesData.Remove(propertyDataToRemove);

            if (_propertyDefaults.ContainsKey(componentPropertyName))
            {
                SetPropertyValue(componentPropertyName, _propertyDefaults[componentPropertyName]);
            }
        }
        
        private void ClearNotUsingResources()
        {
            var resourcesToRemove = new List<string>();

            foreach (ResourceObject resourceObject in UsingResources.Values)
            {
                string resourceGuid = resourceObject.Data.Guid;

                bool isUsing = InspectorPropertiesData.Any(x => x.PropertyValue.ResourceGuid == resourceGuid);

                if (!isUsing)
                {
                    resourcesToRemove.Add(resourceGuid);
                }
            }

            foreach (string resource in resourcesToRemove)
            {
                UsingResources.Remove(resource);
            }
        }

        private IEnumerator SetPropertiesWithDelay()
        {
            if (InspectorPropertiesData == null)
            {
                yield break;
            }

            foreach (InspectorPropertyData propertyData in InspectorPropertiesData)
            {
                if (propertyData.PropertyValue.ResourceGuid != null)
                {
                    SetPropertyResourceValue(propertyData.ComponentPropertyName, propertyData.PropertyValue.ResourceGuid);

                    continue;
                }

                if (propertyData.PropertyValue.Value != null)
                {
                    SetPropertyValue(propertyData.ComponentPropertyName, propertyData.PropertyValue.Value);

                    continue;
                }
            }
        }

        private void SetPropertyValueBase(ComponentReference componentReference, PropertyInfo propertyInfo, object value)
        {
            if (componentReference == null || !componentReference.Component || propertyInfo == null)
            {
                return;
            }

            if (!_propertyDefaults.ContainsKey($"{componentReference.Name}__{propertyInfo.Name}"))
            {
                object currentValue = _propertyWrappers[propertyInfo].Getter(componentReference.Component);
                _propertyDefaults.Add($"{componentReference.Name}__{propertyInfo.Name}", currentValue);
            }

            try
            {
                if (propertyInfo.PropertyType == typeof(GameObject))
                {
                    if (value is GameObject prefab && prefab)
                    {
                        var go = Object.Instantiate(prefab);
                        go.SetActive(true);
                        _propertyWrappers[propertyInfo].Setter(componentReference.Component, go);
                    }
                    else
                    {
                        _propertyWrappers[propertyInfo].Setter(componentReference.Component, null);
                    }
                }
                else if (propertyInfo.PropertyType == typeof(Texture) || propertyInfo.PropertyType == typeof(TextAsset))
                {
                    _propertyWrappers[propertyInfo].Setter(componentReference.Component, value);
                }
                else if (propertyInfo.PropertyType == typeof(Sprite))
                {
                    if (value is Texture2D texture && texture)
                    {
                        var sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), 0.5f * Vector2.one, 100f);
                        _propertyWrappers[propertyInfo].Setter(componentReference.Component, sprite);
                    }
                    else if (value is Sprite sprite && sprite)
                    {
                        _propertyWrappers[propertyInfo].Setter(componentReference.Component, sprite);
                    }
                    else
                    {
                        _propertyWrappers[propertyInfo].Setter(componentReference.Component, null);
                    }
                }
                else if (propertyInfo.PropertyType == typeof(TextureOnDemand))
                {
                    object oldValue = _propertyWrappers[propertyInfo].Getter(componentReference.Component);
                    
                    ResourceDto resourceDto = GetComponentPropertyResource(componentReference, propertyInfo); 
                    if (oldValue != null && oldValue is TextureOnDemand textureOnDemand)
                    {
                        textureOnDemand.Resource = resourceDto;
                    }
                    else if (resourceDto != null)
                    {
                        _propertyWrappers[propertyInfo].Setter(componentReference.Component, new TextureOnDemand(resourceDto));
                    }
                }
                else if (propertyInfo.PropertyType == typeof(AudioClip))
                {
                    if (value is AudioClip audioClip && audioClip)
                    {
                        _propertyWrappers[propertyInfo].Setter(componentReference.Component, audioClip);
                    }
                    else
                    {
                        _propertyWrappers[propertyInfo].Setter(componentReference.Component, null);
                    }
                }
                else if (propertyInfo.PropertyType == typeof(VarwinVideoClip))
                {
                    if (value is string videoClipUrl)
                    {
                        _propertyWrappers[propertyInfo].Setter(componentReference.Component, new VarwinVideoClip(videoClipUrl));
                    }
                    else
                    {
                        _propertyWrappers[propertyInfo].Setter(componentReference.Component, null);
                    }
                }
                else
                {
                    Converter.CastValue(propertyInfo, value, componentReference.Component);
                }
            }
            catch (Exception e)
            {
                Logger
                    .Error(
                        $"{componentReference.Component.name}: Can not set value {value} to property {propertyInfo.Name}. Error = {e.Message}");
            }
        }

        private InspectorProperty FindInspectorProperty(string componentPropertyName)
        {
            return SerializableProperties.ContainsKey(componentPropertyName) ? SerializableProperties[componentPropertyName] : null;
        }

        private ResourceDto GetComponentPropertyResource(ComponentReference componentReference, PropertyInfo propertyInfo)
        {
            PropertyValue propertyValue = SerializableProperties[$"{componentReference.Name}__{propertyInfo.Name}"].Data.PropertyValue;
            if (propertyValue == null)
            {
                return null;
            }
            
            string resourceGuid = propertyValue.ResourceGuid;
            return string.IsNullOrEmpty(resourceGuid) ? null : UsingResources[resourceGuid].Data;
        }

        
        public static implicit operator bool(InspectorController inspectorController) => inspectorController != null;
        
        public override string ToString()
        {
            return _objectController ? $"InspectorController ({_varwinObjectDescriptor.Name}" : base.ToString();
        }
    }
}