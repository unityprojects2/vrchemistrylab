﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DesperateDevs.Utils;
using Entitas.VisualDebugging.Unity;
using Newtonsoft.Json;
using NLog;
using Photon.Pun;
using UnityEngine;
using Varwin.Core.Behaviours;
using Varwin.Core.Behaviours.ConstructorLib;
using Varwin.Data;
using Varwin.Data.ServerData;
using Varwin.Models.Data;
using Varwin.Public;
using Varwin.UI.ObjectManager;
using Varwin.PlatformAdapter;
using Varwin.PUN;
using Varwin.WWW;
using Logger = NLog.Logger;
using Object = UnityEngine.Object;

#pragma warning disable 618

namespace Varwin
{
    /// <summary>
    /// Base class for all objects
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public sealed class ObjectController
    {
        #region PRIVATE VARS

        private static Logger Logger = LogManager.GetCurrentClassLogger();
        
        private static readonly HashSet<string> IgnoredBehaviours = new HashSet<string>
        {
            "Interactable",
            "Detachable",
            "VelocityEstimator",
            "SteamVRBooleanAction",
            "SteamVRInteractableObject",

            "HighlightEffect",
            "VarwinHighlightEffect",
            "DefaultHighlighter",
        };
        
        private readonly InspectorController InspectorController;
        private readonly HierarchyController HierarchyController;
        
        private readonly Dictionary<int, InputController> _inputControllers = new Dictionary<int, InputController>();
        private readonly Dictionary<Transform, bool> _rigidBodyKinematicsDefaults = new Dictionary<Transform, bool>();

        private readonly List<GameModeSwitchController> _gameModeSwitchControllers = new List<GameModeSwitchController>();
        private readonly List<PlatformModeSwitchController> _platformModeSwitchControllers = new List<PlatformModeSwitchController>();

        private readonly List<ColliderController> _colliderControllers = new List<ColliderController>();
        private readonly List<ObjectTransform> _objectTransforms = new List<ObjectTransform>();
        private readonly List<Rigidbody> _rigidbodies = new List<Rigidbody>();
        
        private Contexts _context;
        private I18n _localizedNames;

        private JointBehaviour _jointBehaviour;
        
        private bool _activeInHierarchy;
        private bool _activeSelf = true;
        private ActivityRpc _activityRpc;
        
        #endregion

        #region PUBLIC VARS
        
        public readonly VarwinObjectDescriptor VarwinObjectDescriptor;
        
        /// <summary>
        /// Properties with VarwinInspector
        /// </summary>
        public Dictionary<string, InspectorProperty> InspectorProperties => InspectorController.InspectorProperties;

        /// <summary>
        /// Methods with VarwinInspector
        /// </summary>
        public Dictionary<string, InspectorMethod> InspectorMethods => InspectorController.InspectorMethods;

        public HashSet<Type> InspectorComponentsTypes => InspectorController.InspectorComponentsTypes;

        /// <summary>
        /// Event after changed property value
        /// </summary>
        public event Action<string, object> PropertyValueChanged;

        // ReSharper disable once InconsistentNaming
        /// <summary>
        /// Link to gameObject with IWrapperAware
        /// </summary>
        public GameObject gameObject { get; private set; }

        /// <summary>
        /// Link to root gameObject
        /// </summary>
        public GameObject RootGameObject { get; private set; }


        // ReSharper disable once InconsistentNaming
        /// <summary>
        /// Link to photonView
        /// </summary>
        public PhotonView photonView { get; private set; }


        /// <summary>
        /// ECS Entity link
        /// </summary>
        public GameEntity Entity;

        /// <summary>
        /// Object Name
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Id inside group
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// Scene Id 
        /// </summary>
        public int IdScene { get; private set; }

        /// <summary>
        /// Id object inside server
        /// </summary>
        public int IdServer { get; set; }

        /// <summary>
        /// Object Type Id. Used to save.
        /// </summary>
        public int IdObject { get; private set; }

        
        public bool IsEmbedded { get; private set; }
        
        /// <summary>
        /// Is Scene Template Object
        /// </summary>
        public bool IsSceneTemplateObject { get; private set; }
        
        /// <summary>
        /// Multiplayer object option
        /// </summary>
        public Collaboration Collaboration { get; private set; }

        public bool IsPlayerObject { get; private set; }

        public WrappersCollection WrappersCollection { get; private set; }
        
        public event Action<ObjectController, bool> ActivityChanged; 
        public bool ActiveInHierarchy
        {
            get => _activeInHierarchy;
            private set
            {
                _activeInHierarchy = value;
                
                RootGameObject.transform.parent = _activeInHierarchy ? null : Parent?.RootGameObject.transform;
                RootGameObject.SetActive(_activeInHierarchy);

                if (_activeInHierarchy)
                {
                    foreach (InputController controller in _inputControllers.Values)
                    {
                        controller.GameModeChanged(ProjectData.GameMode);
                    }
                }

                ActivityChanged?.Invoke(this, _activeInHierarchy);
            }
        }
        
        public bool ActiveSelf
        {
            get => _activeSelf;
            private set
            {
                _activeSelf = value;

                if (!LockChildren && ProjectData.IsPlayMode)
                {
                    ActiveInHierarchy = _activeSelf;
                    return;
                }

                if (LockParent == this || Parent && Parent.ActiveInHierarchy)
                {
                    ActiveInHierarchy = _activeSelf;

                    foreach (var child in Descendants)
                    {
                        if (ActiveInHierarchy)
                        {
                            child.ActiveInHierarchy = child.Parent.ActiveInHierarchy && child.ActiveSelf;
                        }
                        else
                        {
                            child.ActiveInHierarchy = ActiveInHierarchy;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Lock children transforms
        /// </summary>
        public bool LockChildren
        {
            get => HierarchyController.LockChildren;
            set => HierarchyController.LockChildren = value;
        }

        /// <summary>
        /// Enabled this allows to select objects 
        /// </summary>
        public bool SelectableInEditor { get; set; }

        /// <summary>
        /// Enable this object instance in Blockly Editor
        /// </summary>
        public bool EnableInLogicEditor { get; set; }
        
        /// <summary>
        /// Sort order index
        /// </summary>
        public int Index
        {
            get => HierarchyController.Index;
            set => HierarchyController.Index = value;
        }
        
        public ObjectController Parent => HierarchyController?.Parent?.ObjectController;
        
        public ObjectController LockParent => HierarchyController.LockParent.ObjectController;

        public List<ObjectController> Children => HierarchyController.Children.Select(x => x.ObjectController).ToList();

        public List<ObjectController> Descendants => HierarchyController.Descendants.Select(x => x.ObjectController).ToList();

        public int ParentId => HierarchyController.ParentId;

        public bool HierarchyExpandedState
        {
            get => HierarchyController.TreeExpandedState;
            set => HierarchyController.TreeExpandedState = value;
        }

        public event Action ParentChanged;

        /// <summary>
        /// UI show object Id 
        /// </summary>
        public UIID Uiid;

        /// <summary>
        /// UI show object management
        /// </summary>
        public UIObject UiObject;

        public bool IsSelectedInEditor { get; set; }

        #endregion

        
        public ObjectController(InitObjectParams initObjectParams)
        {
            _context = Contexts.sharedInstance;
            Entity = _context.game.CreateEntity();
            _localizedNames = initObjectParams.LocalizedNames;
            IsEmbedded = initObjectParams.Embedded;
            IsSceneTemplateObject = initObjectParams.SceneTemplateObject;
            
            RootGameObject = initObjectParams.RootGameObject;
            gameObject = initObjectParams.Asset;
            photonView = initObjectParams.Photonview;
            VarwinObjectDescriptor = gameObject.GetComponent<VarwinObjectDescriptor>();

            HierarchyController = new HierarchyController(this);
            
            Helper.AddPhotonTransformView(gameObject, photonView);
            Id = initObjectParams.Id;
            Index = initObjectParams.Index;
            IdObject = initObjectParams.IdObject;
            IdScene = initObjectParams.IdScene;
            IdServer = initObjectParams.IdServer;
            Name = initObjectParams.Name;
            Collaboration = initObjectParams.Collaboration;
            WrappersCollection = initObjectParams.WrappersCollection; 
            LockChildren = initObjectParams.LockChildren;
            SelectableInEditor = !initObjectParams.DisableSelectabilityInEditor;
            EnableInLogicEditor = !initObjectParams.DisableSceneLogic;
            ActiveSelf = !initObjectParams.IsDisabled;
            ActiveInHierarchy = !initObjectParams.IsDisabledInHierarchy;
           
            SetParent(initObjectParams.Parent, Index, true);
            
            int instanceId = Id;
            this.RegisterMeInScene(ref instanceId, Name);

            Id = instanceId;
            SetName(Name);

            RootGameObject.AddComponent<ObjectBehaviourWrapper>().OwdObjectController = this;

            AddWrapper();
            
            InspectorController = new InspectorController(this, initObjectParams.ResourcesPropertyData);
            InspectorController.PropertyValueChanged += OnPropertyValueChanged;
            
            if (Settings.Instance.Multiplayer)
            {
                _activityRpc = gameObject.AddComponent<ActivityRpc>();
                _activityRpc.Init(this);
                photonView.ObservedComponents.Add(_activityRpc);
                Entity.AddPhotonView(photonView);
            }

            AddBehaviours();

            SaveKinematics();

            Entity.AddId(Id);
            Entity.AddIdServer(IdServer);
            Entity.AddIdObject(IdObject);
            Entity.AddRootGameObject(RootGameObject);
            Entity.AddGameObject(gameObject);

            InitPhysics();
            InitColliders();
            InitAnimations();
            InitCollaboration();

#if VARWINCLIENT
            InspectorController.InitInspectorFields();
            InspectorController.InitResources();
#endif

            ApplyGameMode(ProjectData.GameMode, ProjectData.GameMode);
            RequestManager.Instance.StartCoroutine(ExecuteSwitchGameModeDelayedCoroutine());
            RequestManager.Instance.StartCoroutine(ExecuteSwitchPlatformModeDelayedCoroutine());

            InitUiId();
            Create();

            ParentChanged += UpdateActivityOnParentChanged;
        }

        #region Activity

        public void SetActive(bool isActive)
        {
            ActiveSelf = isActive;
            
            if (ProjectData.IsMultiplayerAndMasterClient)
            {
                photonView.RPC(nameof(_activityRpc.SetActive), RpcTarget.Others, isActive);
            }
        }

        private void UpdateActivityOnParentChanged()
        {
            if (Parent == null)
            {
                ActiveInHierarchy = ActiveSelf;
                foreach (var child in Children)
                {
                    child.UpdateActivityOnParentChanged();
                }
                return;
            }

            if (Parent.ActiveInHierarchy)
            {
                // set active self to the same value to update descendants activity if needed
                ActiveSelf = _activeSelf;
                return;
            }
            
            ActiveInHierarchy = Parent.ActiveInHierarchy;
            foreach (var child in Descendants)
            {
                child.ActiveInHierarchy = Parent.ActiveInHierarchy;
            }
        }

        #endregion Activity
        
        #region Init

        private void InitCollaboration()
        {
            IPlayerObject varwinObject = gameObject.GetComponent<IPlayerObject>();

            if (varwinObject == null)
            {
                return;
            }

            IsPlayerObject = true;

            GameStateData.SetPlayerObject(GameStateData.GetPrefabData(IdObject));

            if (Settings.Instance.Multiplayer)
            {
                return;
            }

            varwinObject.SetNodes(new PlayerNodes
            {
                Head = InputAdapter.Instance.PlayerController.Nodes.Head.Transform,
                RightHand = InputAdapter.Instance.PlayerController.Nodes.RightHand?.Transform,
                LeftHand = InputAdapter.Instance.PlayerController.Nodes.LeftHand?.Transform
            });

            varwinObject.SetPlayerInfo(new PlayerInfo {NickName = Environment.UserName, UserId = ""});


            if (!photonView)
            {
                return;
            }

            if (!Settings.Instance.Multiplayer)
            {
                return;
            }

            if (!PhotonNetwork.IsMasterClient)
            {
                return;
            }

            if (Collaboration == Collaboration.SinglePlayer)
            {
                photonView.TransferOwnership(0);
            }
        }

        private void InitAnimations()
        {
            if (!photonView)
            {
                return;
            }

            var animators = RootGameObject.GetComponentsInChildren<Animator>();

            foreach (Animator animator in animators)
            {
                var animatorView = animator.gameObject.AddComponent<PhotonAnimatorView>();
                photonView.ObservedComponents.Add(animatorView);
            }
        }
        
        private void InitColliders()
        {
            var collider = gameObject.GetComponentInChildren<Collider>();

            if (collider)
            {
                Entity.AddCollider(collider);
            }
        }

        private void InitPhysics()
        {
            if (!photonView)
            {
                return;
            }

            Rigidbody rigidbody = gameObject.GetComponent<Rigidbody>();

            if (rigidbody)
            {
                Entity.AddRigidbody(rigidbody);
            }

            if (!Settings.Instance.Multiplayer)
            {
                return;
            }

            var rigidBody = RootGameObject.GetComponentsInChildren<Rigidbody>();

            foreach (Rigidbody body in rigidBody)
            {
                PhotonRigidbodyView rigidbodyView = body.gameObject.AddComponent<PhotonRigidbodyView>();
                rigidbodyView.m_TeleportEnabled = true;
                rigidbodyView.m_SynchronizeVelocity = true;
                rigidbodyView.m_SynchronizeAngularVelocity = true;
                photonView.ObservedComponents.Add(rigidbodyView);
            }
        }
                
        public void InitUiId()
        {
            if (IsEmbedded || IsSceneTemplateObject || ProjectData.GameMode == GameMode.View || ProjectData.PlatformMode == PlatformMode.Desktop)
            {
                return;
            }
            
            try
            {
                GameObject uiid = Object.Instantiate(GameObjects.Instance.UIID);
                uiid.GetComponent<UIID>().Init(RootGameObject.transform, this);
                if (Entity.hasUIID)
                {
                    Entity.ReplaceUIID(uiid);
                }
                else
                {
                    Entity.AddUIID(uiid);
                }

                GameObject uiObject = Object.Instantiate(GameObjects.Instance.UIObject);
                uiObject.GetComponent<UIObject>().Init(RootGameObject.transform, this);
                if (Entity.hasUIObject)
                {
                    Entity.ReplaceUIObject(uiObject);
                }
                else
                {
                    Entity.AddUIObject(uiObject);
                }
                
                EnableUI(ProjectData.GameMode == GameMode.Edit);
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
            }
        }
        
        private void Create()
        {
            var monoBehaviours = RootGameObject.GetComponentsInChildren<MonoBehaviour>().ToList();
            monoBehaviours.RemoveAll(x => !x || IgnoredBehaviours.Contains(x.GetType().Name));
            
            foreach (MonoBehaviour monoBehaviour in monoBehaviours)
            {
                if (!monoBehaviour)
                {
                    continue;
                }

                MethodInfo method = monoBehaviour.GetType()
                    .GetMethod("Create", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);

                if (method == null || method.GetParameters().Length != 0)
                {
                    continue;
                }

                try
                {
                    method.Invoke(monoBehaviour, null);
                }
                catch (Exception e)
                {
                    Debug.LogError($"Caught error when trying to invoke Create method in object {Name}:{e.Message}\n{e.StackTrace}");
                }
            }
        }

        public void SetServerId(int id)
        {
            IdServer = id;
            Entity.ReplaceIdServer(id);
        }

        public void SetName(string newName)
        {
            Name = newName;
            Entity.ReplaceName(newName);
        }
        #endregion

        #region Add

        public void AddObservedComponent(Component varwinObjectSpawnSync)
        {
            photonView.ObservedComponents.Add(varwinObjectSpawnSync);
        }
        
        private void AddBehaviours()
        {
            var behaviours = RootGameObject.GetComponentsInChildren<MonoBehaviour>(true);
            bool haveRootInputControl = false;
            bool haveJoints = false;

            if (behaviours.Any(x => x is JointPoint) && !behaviours.Any(x => x is InteractableObjectBehaviour))
            {
                var interactableBehaviour = RootGameObject.AddComponent<InteractableObjectBehaviour>();

                interactableBehaviour.SetIsGrabbable(behaviours.Any(x => x is IGrabStartAware) ||
                                                     behaviours.Any(x => x is IGrabEndAware));

                interactableBehaviour.SetIsUsable(behaviours.Any(x => x is IUseStartAware) ||
                                                  behaviours.Any(x => x is IUseEndAware));

                interactableBehaviour.SetIsTouchable(behaviours.Any(x => x is ITouchStartAware) ||
                                                     behaviours.Any(x => x is ITouchEndAware));
                
                behaviours.Append(interactableBehaviour);
            }
            
            foreach (MonoBehaviour behaviour in behaviours)
            {
                if (!behaviour)
                {
                    continue;
                }

                bool haveInputControlInObject = false;
                
                var idComponent = behaviour.gameObject.GetComponent<ObjectId>();
                
                if (idComponent)
                {
                    AddInputControl(behaviour, idComponent, out haveInputControlInObject);
                }

                if (behaviour is JointPoint)
                {
                    haveJoints = true;
                }

                if (haveInputControlInObject)
                {
                    haveRootInputControl = true;
                }

                AddGameModeSwitchControls(behaviour);
                AddCollidersAware(behaviour);
                if (idComponent)
                {
                    AddObjectTransforms(behaviour);
                }
            }
           
            if (haveJoints)
            {
                AddJointBehaviour();
            }

            if (Settings.Instance.Spectator)
            {
                return;
            }

            if (!haveRootInputControl)
            {
                InputController inputController = new InputController(this, RootGameObject, true);
                _inputControllers.Add(-1, inputController);
            }
            
            Entity.AddInputControls(_inputControllers);
        }
        
        private void AddJointBehaviour()
        {
            if (RootGameObject.GetComponent<JointBehaviour>())
            {
                return;
            }

            _jointBehaviour = RootGameObject.AddComponent<JointBehaviour>();
            _jointBehaviour.Init();
        }
        
        public void AddWrapper()
        {
            Wrapper wrapper;
            
            if (IsSceneTemplateObject)
            {
                wrapper = new NullWrapper(RootGameObject);
            }
            else
            {
                IWrapperAware wrapperAware = RootGameObject.GetComponentInChildren<IWrapperAware>();

                if (wrapperAware == null)
                {
                    return;
                }

                wrapper = wrapperAware.Wrapper();
            }

            WrappersCollection.Add(Id, wrapper);
            Entity.AddWrapper(wrapper);
            wrapper.InitEntity(Entity);
            wrapper.InitObjectController(this);
        }

        private void AddObjectTransforms(MonoBehaviour child)
        {
            if (child.GetType().ImplementsInterface<ISaveTransformAware>())
            {
                GameObject go = child.gameObject;
                var objectTransform = go.AddComponent<ObjectTransform>();
                _objectTransforms.Add(objectTransform);
            }
        }

        private void AddCollidersAware(MonoBehaviour child)
        {
            if (!child.GetType().ImplementsInterface<IColliderAware>())
            {
                return;
            }

            var colliderAware = (IColliderAware) child;
            _colliderControllers.Add(new ColliderController(colliderAware, child.gameObject));
        }
        
        private void AddGameModeSwitchControls(MonoBehaviour child)
        {
            if (!child.GetType().ImplementsInterface<ISwitchModeSubscriber>())
            {
                return;
            }

            var switchMode = (ISwitchModeSubscriber) child;
            _gameModeSwitchControllers.Add(new GameModeSwitchController(switchMode));
        }
        
        private void AddPlatformModeSwitchControls(MonoBehaviour child)
        {
            if (!child.GetType().ImplementsInterface<ISwitchPlatformModeSubscriber>())
            {
                return;
            }

            var switchMode = (ISwitchPlatformModeSubscriber) child;
            _platformModeSwitchControllers.Add(new PlatformModeSwitchController(switchMode));
        }
        
        public void AddInputControl(MonoBehaviour behaviour, ObjectId idComponent, out bool haveInputControlInObject)
        {
            haveInputControlInObject = false;

            if (Settings.Instance.Spectator)
            {
                return;
            }
            
            if (!behaviour.GetType().ImplementsInterface<IVarwinInputAware>())
            {
                return;
            }

            int id = idComponent.Id;
            GameObject go = behaviour.gameObject;
            bool root = go == RootGameObject;

            if (go == gameObject)
            {
                haveInputControlInObject = true;
            }

            if (!_inputControllers.ContainsKey(id))
            {
                _inputControllers.Add(id, new InputController(this, go, root));
            }
        }

        #endregion

        #region Get

        public static List<HierarchyController> GetRootObjectsInScene()
        {
            return GameStateData.GetRootObjectsScene().Select(x => x.HierarchyController).ToList();
        }

        public Dictionary<int, TransformDT> GetTransforms()
        {
            var transforms = new Dictionary<int, TransformDT>();

            foreach (InputController controllersValue in _inputControllers.Values)
            {
                TransformDto transform = controllersValue.GetTransform();

                if (transform == null)
                {
                    continue;
                }

                if (!transforms.Keys.Contains(transform.Id))
                {
                    transforms.Add(transform.Id, transform.Transform);
                }
            }

            foreach (ObjectTransform objectTransform in _objectTransforms)
            {
                TransformDto transform = objectTransform.GetTransform();

                if (transform == null)
                {
                    continue;
                }

                if (!transforms.Keys.Contains(transform.Id))
                {
                    transforms.Add(transform.Id, transform.Transform);
                }
            }

            return transforms;
        }

        public JointData GetJointData()
        {
            if (!RootGameObject)
            {
                return null;
            }
            
            JointBehaviour jointBehaviour = RootGameObject.GetComponent<JointBehaviour>();

            if (!jointBehaviour)
            {
                return null;
            }

            JointData jointData = new JointData {JointConnectionsData = new Dictionary<int, JointConnectionsData>()};

            foreach (JointPoint jointPoint in jointBehaviour.JointPoints)
            {
                if (jointPoint.IsFree)
                {
                    continue;
                }

                ObjectId objectId = jointPoint.gameObject.GetComponent<ObjectId>();

                if (!objectId)
                {
                    Logger
                        .Error($"Joint point {jointPoint.gameObject} have no object id");

                    continue;
                }

                ObjectId connectedJointPointObjectId =
                    jointPoint.ConnectedJointPoint.gameObject.GetComponent<ObjectId>();

                if (!connectedJointPointObjectId)
                {
                    Logger
                        .Error(
                            $"Connected joint point {jointPoint.ConnectedJointPoint.gameObject} have no object id");

                    continue;
                }

                int jointPointId = objectId.Id;

                int connectedObjectInstanceId =
                    jointPoint.ConnectedJointPoint.JointBehaviour.Wrapper.GetInstanceId();
                int connectedObjectJointPointId = connectedJointPointObjectId.Id;

                jointData.JointConnectionsData.Add(jointPointId,
                    new JointConnectionsData
                    {
                        ConnectedObjectInstanceId = connectedObjectInstanceId,
                        ConnectedObjectJointPointId = connectedObjectJointPointId,
                        ForceLocked = !jointPoint.CanBeDisconnected
                    });
            }

            return jointData;
        }

        public SpawnInitParams GetSpawnInitParams()
        {
            var transforms = new Dictionary<int, TransformDT>();
            JointData jointData = GetJointData();
            var inspectorPropertiesData = InspectorController.GetInspectorPropertiesData();
            var objectsIds = RootGameObject.GetComponentsInChildren<ObjectId>();

            foreach (ObjectId objectId in objectsIds)
            {
                if (transforms.ContainsKey(objectId.Id))
                {
                    continue;
                }

                TransformDT transformDt = objectId.gameObject == gameObject
                    ? GetAffectedTransform().ToTransformDT()
                    : objectId.gameObject.transform.ToTransformDT();

                transforms.Add(objectId.Id, transformDt);
            }

            var spawn = new SpawnInitParams
            {
                ParentId = Parent?.Id ?? 0,
                IdScene = ProjectData.SceneId,
                IdInstance = Id,
                IdObject = IdObject,
                IdServer = IdServer,
                Name = Name,
                Joints = jointData,
                Transforms = transforms,
                InspectorPropertiesData = inspectorPropertiesData,
                LockChildren = LockChildren,
                DisableSelectabilityInEditor = !SelectableInEditor,
                DisableSceneLogic = !EnableInLogicEditor,
                IsDisabled = !ActiveSelf,
                IsDisabledInHierarchy = !ActiveInHierarchy,
                Index = Index
            };

            return spawn;
        }

        public List<SpawnInitParams> GetDescendedSpawnInitParams()
        {
            var result = new List<SpawnInitParams> {GetSpawnInitParams()};
            result.AddRange(Descendants.Select(child => child.GetSpawnInitParams()));
            return result;
        }

        public string GetLocalizedName()
        {
            if (_localizedNames == null)
            {
                return "name unknown";
            }

            if (Settings.Instance.Language == "ru")
            {
                if (!string.IsNullOrEmpty(_localizedNames.ru))
                {
                    return _localizedNames.ru;
                }

                return _localizedNames.en;
            }
            
            if (Settings.Instance.Language == "en")
            {
                return _localizedNames.en;
            }

            return _localizedNames.en;
        }

        public I18n GetLocalizedNames()
        {
            return _localizedNames ?? null;
        }
        
        public Transform GetAffectedTransform()
        {
            return HierarchyController.GetAffectedTransform();
        }

        public void UpdateTransforms()
        {
            if (ActiveInHierarchy)
            {
                return;
            }
            
            HierarchyController.UpdateTransformManually();
        }
        
        #endregion

        #region Switch Mode
        
        public void ApplyGameMode(GameMode newMode, GameMode oldMode)
        {
            if (!gameObject)
            {
                return;
            }
            
            foreach (InputController controller in _inputControllers.Values)
            {
                controller.GameModeChanged(newMode);
            }

            if (newMode == GameMode.Edit)
            {
                SetKinematicsOn();
            }
            else if ((newMode == GameMode.Preview || newMode == GameMode.View) &&
                     (oldMode == GameMode.Edit || oldMode == GameMode.Undefined || oldMode == newMode))
            {
                var interactableBehaviour = gameObject.GetComponent<InteractableBehaviour>();
                if (!ActiveInHierarchy && interactableBehaviour)
                {
                    interactableBehaviour.TrySetPhysicsSettings();
                }

                HierarchyController.UpdateConstraintsForPlayMode();
                
                if (interactableBehaviour)
                {
                    SaveKinematics();
                }

                SetKinematicsDefaults();
            }
        }

        public void ApplyPlatformMode(PlatformMode newMode, PlatformMode oldMode)
        {
            if (oldMode == PlatformMode.Vr)
            {
                if (Uiid && Uiid.gameObject)
                {
                    Object.Destroy(Uiid.gameObject);
                }

                if (UiObject && UiObject.gameObject)
                {
                    Object.Destroy(UiObject.gameObject);
                }
            }
            else
            {
                InitUiId();
            }
            
            InitCollaboration();
            bool wrapperIsEnabled = WrappersCollection.Get(Id).Enabled;
            foreach (InputController controller in _inputControllers.Values)
            {
                controller.PlatformModeChanged(newMode);
                if (wrapperIsEnabled)
                {
                    controller.EnableViewInput();
                }
                else
                {
                    controller.DisableViewInput();
                }
            }
        }

        public void ExecuteSwitchGameModeOnObject(GameMode newMode, GameMode oldMode)
        {
            if (!gameObject || !gameObject.activeInHierarchy)
            {
                return;
            }

            foreach (var gameModeSwitchController in _gameModeSwitchControllers)
            {
                try
                {
                    gameModeSwitchController.SwitchGameMode(newMode, oldMode);
                }
                catch (Exception e)
                {
                    Debug.LogError($"Can not apply game mode for {Name}.\n{e}");
                }
            }
        }

        public void ExecuteSwitchPlatformModeOnObject(PlatformMode newMode, PlatformMode oldMode)
        {
            if (!gameObject || !gameObject.activeInHierarchy)
            {
                return;
            }
            
            foreach (var platformModeSwitchController in _platformModeSwitchControllers)
            {
                try
                {
                    platformModeSwitchController.SwitchPlatformMode(newMode, oldMode);
                }
                catch (Exception e)
                {
                    Debug.LogError($"Can not apply platform mode for {Name}.\n{e}");
                }
            }
        }

        public IEnumerator ExecuteSwitchGameModeDelayedCoroutine()
        {
            yield return null;
            ExecuteSwitchGameModeOnObject(ProjectData.GameMode, ProjectData.GameMode);
        }
        
        public IEnumerator ExecuteSwitchPlatformModeDelayedCoroutine()
        {
            yield return null;
            ExecuteSwitchPlatformModeOnObject(ProjectData.PlatformMode, ProjectData.PlatformMode);
        }

        #endregion
        
        public void SaveKinematics()
        {
            var rigidBodies = RootGameObject.GetComponentsInChildren<Rigidbody>();

            foreach (Rigidbody rigidbody in rigidBodies)
            {
                if (!_rigidBodyKinematicsDefaults.ContainsKey(rigidbody.transform))
                {
                    _rigidBodyKinematicsDefaults.Add(rigidbody.transform, rigidbody.isKinematic);
                }
                else
                {
                    _rigidBodyKinematicsDefaults[rigidbody.transform] = rigidbody.isKinematic;
                }
            }
        }

        public void SetKinematicsDefaults()
        {
            foreach (var rigidbody in _rigidBodyKinematicsDefaults)
            {
                rigidbody.Key.GetComponent<Rigidbody>().isKinematic = rigidbody.Value;
            }

            foreach (Rigidbody rigidbody in _rigidbodies)
            {
                rigidbody.useGravity = true;
            }
            _rigidbodies.Clear();
        }

        public void SetKinematicsOn()
        {
            foreach (Transform transform in _rigidBodyKinematicsDefaults.Keys)
            {
                if (!transform)
                {
                    Logger.Error("Transform was lost");
                    continue;
                }

                Rigidbody body = transform.GetComponent<Rigidbody>();

                if (!body)
                {
                    continue;
                }

                if (ProjectData.GameMode == GameMode.Edit
                    && !body.isKinematic
                    && transform.GetComponent<JointBehaviour>())
                {
                    body.isKinematic = false;
                    body.useGravity = false;
                    _rigidbodies.Add(body);
                }
                else
                {
                    body.isKinematic = true;
                }
            }
        }
        
        public void CopyKinematicsFrom(Rigidbody rigidbodyToCopy)
        {
            foreach (Transform transform in _rigidBodyKinematicsDefaults.Keys)
            {
                if (!transform)
                {
                    Logger.Error("Transform was lost");
                    continue;
                }

                Rigidbody body = transform.GetComponent<Rigidbody>();

                if (!body)
                {
                    continue;
                }

                body.isKinematic = rigidbodyToCopy.isKinematic;
                body.useGravity = rigidbodyToCopy.useGravity;
            }
            
            SaveKinematics();
        }
        
        public void Delete()
        {
            this.OnDeletingObject();
            
            if (RootGameObject)
            {
                JointBehaviour jointBehaviour = RootGameObject.GetComponent<JointBehaviour>();

                if (jointBehaviour)
                {
                    jointBehaviour.UnLockAndDisconnectPoints();
                }
            }

            RemoveParent();

            WrappersCollection.Remove(Id);

            foreach (ColliderController colliderController in _colliderControllers)
            {
                colliderController.Destroy();
            }

            foreach (InputController inputController in _inputControllers.Values)
            {
                inputController.Destroy();
            }

            try
            {
                Entity?.Destroy();
            }
            catch (Exception e)
            {
                Debug.LogError("Can not destroy object Entity! Message = " + e.Message);
            }

            if (Uiid)
            {
                Object.Destroy(Uiid.gameObject);
            }

            if (UiObject)
            {
                Object.Destroy(UiObject.gameObject);
            }

            HierarchyController.DestroyGhostObject();

            RootGameObject.DestroyGameObject();
            ProjectData.ObjectsAreChanged = true;
            this.Dispose();
        }

        public void EnableUI(bool isEnabled)
        {
            UiObject.gameObject.SetActive(isEnabled);
            Uiid.gameObject.SetActive(isEnabled);
        }

        #region Inspector Property

        public void OnPropertyValueChanged(string componentPropertyName, object value)
        {
            PropertyValueChanged?.Invoke(componentPropertyName, value);
        }

        public List<string> GetObjectBehaviours()
        {
            return InspectorController.ObjectBehaviours;
        }

        public List<ResourceDto> GetUsingResourcesData()
        {
            return InspectorController.GetUsingResourcesData();
        }

        public List<string> GetOnDemandedResourceGuids()
        {
            return InspectorController.GetOnDemandedResourceGuids();
        }

        public List<InspectorPropertyData> GetInspectorPropertiesData()
        {
            return InspectorController.GetInspectorPropertiesData();
        }

        public void SetInspectorPropertyValue(string propertyName, object value, bool isResource)
        {
            if (isResource)
            {
                InspectorController.SetInspectorPropertyResourceValue(propertyName, (string) value);
            }
            else
            {
                InspectorController.SetInspectorPropertyValue(propertyName, value);
            }
        }

        #endregion
        
        public void OnEditorSelect()
        {
            IsSelectedInEditor = true;
        }
        
        public void OnEditorUnselect()
        {
            IsSelectedInEditor = false;
        }

        #region Hierarchy Controller Wrappers

        public void SetParent(ObjectController parent, int index, bool keepOriginalScale = false)
        {
            HierarchyController.SetParent(parent?.HierarchyController, index, keepOriginalScale);
        }

        public void RemoveParent()
        {
            HierarchyController.RemoveParent();
        }

        public void OnGrabStart()
        {
            HierarchyController.OnGrabStart();
        }

        public void OnGrabEnd()
        {
            HierarchyController.OnGrabEnd();
        }

        public ObjectController GetRootParent()
        {
            return HierarchyController.GetRootParent().ObjectController;
        }
        
        public void InvokeParentChangedEvent()
        {
            ParentChanged?.Invoke();
        }

        #endregion

        public static implicit operator bool(ObjectController objectController) => objectController != null;
        
        public override string ToString()
        {
            return $"ObjectController ({Name})";
        }
    }
}
