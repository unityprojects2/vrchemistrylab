﻿using System;
using NLog;
using Varwin.Errors;
using Varwin.PUN;
using Varwin.UI;
using Varwin.WWW;

namespace Varwin.Types
{
    public class LogicInstance
    {
        public ILogic Logic { get; private set; }

        private readonly int _sceneId;
        private WrappersCollection _myItems;
        private bool _initialized;

        public int SceneId => _sceneId;

        public LogicInstance(int sceneId)
        {
            _sceneId = sceneId;
            GameStateData.SetLogic(this);
        }

        public void UpdateGroupLogic(Type newLogic)
        {
            LogicUtils.RemoveAllEventHandlers();
            
            if (newLogic == null)
            {
                LogManager.GetCurrentClassLogger().Info($"Scene {_sceneId} logic is empty!");
                Clear();

                return;
            }

            _myItems = GameStateData.GetWrapperCollection();

            var logic = Activator.CreateInstance(newLogic) as ILogic;

            if (logic == null)
            {
                string msg = ErrorHelper.GetErrorDescByCode(ErrorCode.LogicInitError);
                PopupWindowManager.ShowPopup(msg, true);

                LogManager.GetCurrentClassLogger()
                    .Error($"Initialize scene logic error! SceneId = {_sceneId}. Message: Logic is null!");

                CoreErrorManager.Error(new Exception(msg));

                return;
            }

            Logic = logic;

            try
            {
                PhotonConnector.Instance.SetMasterClientReady();
                LogManager.GetCurrentClassLogger().Info($"Scene {_sceneId} logic initialize started...");
                InitializeLogic();
                LogManager.GetCurrentClassLogger().Info($"Scene {_sceneId} logic initialize successful");
            }
            catch (Exception e)
            {
                CoreErrorManager.Error(e);
                ShowLogicExceptionError(ErrorCode.LogicInitError, "Initialize scene logic error!", e);
                Logic = null;
            }
        }

        public void InitializeLogic()
        {
            _initialized = false;
            Logic.SetCollection(_myItems);
            Logic.Events();
            Logic.Initialize();
            _initialized = true;
        }

        public void ExecuteLogic()
        {
            if (Logic == null || !_initialized)
            {
                return;
            }

            try
            {
                Logic.Update();
            }
            catch (Exception e)
            {
                ShowLogicExceptionError(Errors.ErrorCode.LogicExecuteError, "Execute scene logic error!", e);
                Logic = null;
            }
        }

        private void ShowLogicExceptionError(int errorCode, string errorMessage, Exception exception)
        {
            LogicException logicException = new LogicException(this, errorMessage, exception);
            LogManager.GetCurrentClassLogger().Error($"{errorMessage} {logicException.GetStackFrameString()}");
            CoreErrorManager.Error(logicException);
            PopupWindowManager.ShowPopup(ErrorHelper.GetErrorDescByCode(errorCode), true);
            AMQPClient.SendRuntimeErrorMessage(logicException);
        }

        public void Clear()
        {
            LogicUtils.RemoveAllEventHandlers();
            Logic = null;
        }
    }
}
