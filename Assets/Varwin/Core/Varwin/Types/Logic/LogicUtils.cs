using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using UnityEngine;
using Varwin.Public;

namespace Varwin
{
    public static class LogicUtils
    {
#if !NET_STANDARD_2_0
        private static readonly Dictionary<string, Delegate> RegisteredDelegates = new Dictionary<string, Delegate>();
        private static Dictionary<Delegate, DelegateData> RegisteredDelegatesData = new Dictionary<Delegate, DelegateData>(); 

        public static void AddEventHandler(this ILogic logic, object target, string eventName, string methodName)
        {
            SetEventHandler(EventHandlerAction.Add, logic, target, eventName, methodName);
        }
        
        public static void RemoveEventHandler(this ILogic logic, object target, string eventName, string methodName)
        {
            SetEventHandler(EventHandlerAction.Remove, logic, target, eventName, methodName);
        }

        private static void SetEventHandler(EventHandlerAction action, ILogic logic, object target, string eventName, string methodName)
        {
            if (target is DynamicCollection<dynamic> behaviourColl)
            {
                foreach (var behaviour in behaviourColl.Collection)
                {
                    string name = (behaviour as MonoBehaviour)?.gameObject.GetWrapper().GetName();
                    SetEventHandler(action, logic, behaviour, eventName, methodName, name);
                }
            }
            else if (target is DynamicCollection<Wrapper> wrappers)
            {
                foreach (var wrapper in wrappers.Collection)
                {
                    SetEventHandler(action, logic, wrapper, eventName, methodName, wrapper.GetName());
                }
            }
            else
            {
                SetEventHandler(action, logic, target, eventName, methodName, "alone");
            }
        }
        
        public static void RemoveAllEventHandlers()
        {
            foreach (var registeredDelegateData in RegisteredDelegatesData)
            {
                DelegateData data = registeredDelegateData.Value;
                data.EventInfo.RemoveEventHandler(data.Target, registeredDelegateData.Key);
            }

            RegisteredDelegates.Clear();
            RegisteredDelegatesData.Clear();
            
            GCManager.Collect();
        }

        private static void SetEventHandler(EventHandlerAction eventHandlerAction, ILogic logic, object target, string eventName, string methodName, string wrapperName)
        {
            string prefix = eventHandlerAction == EventHandlerAction.Add ? "AddEventHandler" : "RemoveEventHandler";
            
            // Get Target Object EventInfo
            
            if (target == null)
            {
                var message = $"{prefix}: Target object can't be null (Event: \"{eventName}\"; Method: \"{methodName}\")";
                throw new Exception(message);
            }
            
            Type targetType = target.GetType();
            EventInfo eventInfo = targetType.GetEvent(eventName);
            
            if (eventInfo == null)
            {
                var message = $"{prefix}: Not found event with name \"{eventName}\" (TargetType: \"{targetType.FullName}\"; Method: \"{methodName}\")";
                throw new Exception(message);
            }

            // Get Logic MethodInfo
            
            if (logic == null)
            {
                var message = $"{prefix}: Logic instance can't be null (Event: \"{eventName}\"; Method: \"{methodName}\")";
                throw new Exception(message);
            }
            
            Type logicType = logic.GetType();
            MethodInfo methodInfo = logicType.GetMethod(methodName, BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic);
            
            if (methodInfo == null)
            {
                var message = $"{prefix}: Not found method with name \"{methodName}\" (TargetType: \"{targetType.FullName}\"; Event: \"{eventName}\")";
                throw new Exception(message);
            }
            
            // Get Delegate Method

            var delegateKey = $"{target}__{wrapperName}_{eventName}__{methodName}";
            Delegate delegatedMethod = GetOrCreateDelegate(delegateKey, logic, target, eventInfo, eventName, methodInfo, methodName);

            if (delegatedMethod == null)
            {
                var message = $"{prefix}: Can't create delegate (TargetType: \"{targetType.FullName}\"; Event: \"{eventName}\"; EventHandlerType: \"{eventInfo.EventHandlerType}\"; Method: \"{methodName}\")";
                throw new Exception(message);
            }

            // Add or Remove EventHandler
            
            if (eventHandlerAction == EventHandlerAction.Add)
            {
                eventInfo.AddEventHandler(target, delegatedMethod);
                RegisteredDelegatesData.Add(delegatedMethod, new DelegateData
                {
                    Target = target,
                    EventInfo = eventInfo
                });
            }
            else
            {
                eventInfo.RemoveEventHandler(target, delegatedMethod);
                RegisteredDelegates.Remove(delegateKey);
                RegisteredDelegatesData.Remove(delegatedMethod);
            }
        }
        
        private static Delegate GetOrCreateDelegate(string delegateKey, ILogic logic, object target, EventInfo eventInfo, string eventName, MethodInfo methodInfo, string methodName)
        {
            Delegate delegatedMethod;
            if (RegisteredDelegates.ContainsKey(delegateKey))
            {
                delegatedMethod = RegisteredDelegates[delegateKey];
            }
            else
            {
                delegatedMethod = CreateDelegate(logic, target, eventInfo, methodInfo);
                RegisteredDelegates.Add(delegateKey, delegatedMethod);
            }
            
            return delegatedMethod;
        }

        private static Delegate CreateDelegate(ILogic logic, object target, EventInfo eventInfo, MethodInfo methodInfo)
        {
            var eventTypes = eventInfo.EventHandlerType.GetMethod("Invoke").GetParameters().Select(param => param.ParameterType);
            var methodTypes = methodInfo.GetParameters().Select(param => param.ParameterType);
           
            if (eventTypes.Count() == methodTypes.Count())
            {
                return Delegate.CreateDelegate(eventInfo.EventHandlerType, logic, methodInfo);
            }
     
            var paramList = eventTypes.Select(Expression.Parameter).ToList();
            
            var logicConstant = Expression.Constant(logic);

            if (target is MonoBehaviour monoBehaviour)
            {
                target = monoBehaviour.gameObject.GetWrapper();
            }
            
            var objectConstant = Expression.Constant(target);
            
            BlockExpression blockExpr = Expression.Block(
                paramList,
                Expression.RuntimeVariables(paramList),
                Expression.Call(logicConstant, methodInfo, paramList.Concat(new List<Expression> {objectConstant}))
            );
            
            return Expression.Lambda(eventInfo.EventHandlerType, blockExpr, paramList).Compile();
        }
        
        private enum EventHandlerAction
        {
            Add,
            Remove
        }

        private class DelegateData
        {
            public EventInfo EventInfo;
            public object Target;
        }
#else
        public static void RemoveAllEventHandlers()
        {
            
        }
#endif
    }
}