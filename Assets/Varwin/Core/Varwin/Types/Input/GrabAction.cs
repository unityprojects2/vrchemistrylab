using System;
using System.Collections.Generic;
using System.Linq;
using Photon.Pun;
using UnityEngine;
using Varwin.Commands;
using Varwin.Data.ServerData;
using Varwin.Models.Data;
using Varwin.Public;
using Varwin.PlatformAdapter;

namespace Varwin
{
    public class GrabAction : InputAction
    {
        private readonly List<IGrabStartAware> _grabStartList;
        private readonly List<IGrabEndAware> _grabEndList;
        private readonly IGrabPointAware _grabPoint;
        private Action _onEditorGrabStart = delegate { };
        private Action _onEditorGrabEnd = delegate { };
        private TransformDT _saveTransform;
        private JointData _saveJointData;
        private TransformDT _onInitTransform;
        private GameObject _gameObject;
        private PlayerAppearance.InteractControllerAppearance grab;
        
        public GrabAction(
            ObjectController objectController,
            GameObject gameObject, ObjectInteraction.InteractObject interactObject, InputController inputController) : 
            base(objectController, gameObject, interactObject, inputController)
        {
            _grabStartList = GameObject.GetComponents<IGrabStartAware>().ToList();

            if (_grabStartList.Count > 0)
            {
                AddGrabStartBehaviour();
            }

            _grabEndList = GameObject.GetComponents<IGrabEndAware>().ToList();

            if (_grabEndList.Count > 0)
            {
                AddGrabEndBehaviour();
            }

            _grabPoint = GameObject.GetComponent<IGrabPointAware>();
            _gameObject = gameObject;

            _onEditorGrabStart += OnGrabInit;
            _onEditorGrabEnd += OnUngrabInit;
            
            ProjectData.PlatformModeChanging += mode =>
            {
                grab?.DestroyComponent();
            };
            
            if (_grabPoint == null)
            {
                return;
            }

            if (!_gameObject.GetComponent<GrabSettings>())
            {
                _gameObject.AddComponent<GrabSettings>()
                    .Init(_grabPoint.GetLeftGrabPoint(), _grabPoint.GetRightGrabPoint());
            }
        }

        #region OVERRIDES
        
        public override void DisableViewInput()
        {
            InteractObject.isGrabbable = false;

            if (_grabStartList.Count > 0)
            {
                InteractObject.InteractableObjectGrabbed -= OnGrabStartVoid;
            }

            if (_grabEndList.Count > 0)
            {
                InteractObject.InteractableObjectUngrabbed -= OnGrabEndVoid;
            }
        }

        public override void EnableViewInput()
        {
            var interactableObjectBehaviour = GameObject.GetComponent<InteractableObjectBehaviour>();
            
            bool canGrab;

            if (interactableObjectBehaviour)
            {
                canGrab = interactableObjectBehaviour.IsGrabbable;
            }
            else
            {
                canGrab = _grabStartList.Count > 0 || _grabEndList.Count > 0;
            }

            InteractObject.isGrabbable = canGrab;

            if (!canGrab)
            {
                return;
            }

            if (_grabStartList.Count > 0)
            {
                InteractObject.InteractableObjectGrabbed -= OnGrabStartVoid;
                InteractObject.InteractableObjectGrabbed += OnGrabStartVoid;
            }

            if (_grabEndList.Count > 0)
            {
                InteractObject.InteractableObjectUngrabbed -= OnGrabEndVoid;
                InteractObject.InteractableObjectUngrabbed += OnGrabEndVoid;
            }
        }

        protected override void EnableEditorInput()
        {
            if (!IsRootGameObject)
            {
                return;
            }

            InteractObject.isGrabbable = true;

            grab = InputAdapter.Instance.PlayerAppearance.ControllerAppearance.GetFrom(GameObject)
                ?? InputAdapter.Instance.PlayerAppearance.ControllerAppearance.AddTo(GameObject);
            
            grab.HideControllerOnGrab = true;

            InteractObject.SwapControllersFlag = true;

            InteractObject.grabOverrideButton = ControllerInput.ButtonAlias.GripPress;
            InteractObject.InteractableObjectGrabbed += EditorGrabbed;
            InteractObject.InteractableObjectUngrabbed += EditorUngrabbed;

            JointBehaviour jointBehaviour = GameObject.GetComponent<JointBehaviour>();

            if (jointBehaviour == null)
            {
                return;
            }

            _onEditorGrabStart += jointBehaviour.OnGrabStart;
            _onEditorGrabEnd += jointBehaviour.OnGrabEnd;
        }

        protected override void DisableEditorInput()
        {
            if (_grabStartList.Count == 0 && _grabEndList.Count == 0)
            {
                grab = InputAdapter.Instance.PlayerAppearance.ControllerAppearance.GetFrom(GameObject);
                grab?.DestroyComponent();
            }

            InteractObject.isGrabbable = false;
            InteractObject.InteractableObjectGrabbed -= EditorGrabbed;
            InteractObject.InteractableObjectUngrabbed -= EditorUngrabbed;
        }

        #endregion

        private void AddGrabStartBehaviour()
        {
            InitGrabAction();
            InteractObject.grabOverrideButton = ControllerInput.ButtonAlias.GripPress;
        }

        private void AddGrabEndBehaviour()
        {
            InitGrabAction();
            InteractObject.grabOverrideButton = ControllerInput.ButtonAlias.GripPress;
        }

        private void InitGrabAction()
        {
            InteractObject.isGrabbable = true;

            PlayerAppearance.InteractControllerAppearance grab =
                InputAdapter.Instance.PlayerAppearance.ControllerAppearance.GetFrom(GameObject)
                ?? InputAdapter.Instance.PlayerAppearance.ControllerAppearance.AddTo(GameObject);

            grab.HideControllerOnGrab = true;

            InteractObject.SwapControllersFlag = true;
        }
        
        private void OnGrabStartVoid(
            object sender,
            ObjectInteraction.InteractableObjectEventArgs interactableObjectEventArgs)
        {
            if (Settings.Instance.Multiplayer)
            {
                if (ObjectController.Collaboration == Collaboration.AllPlayers)
                {
                    ChangeOwner();
                    InputController.photonView.RPC("OnGrabStartRpc", RpcTarget.MasterClient,
                        (int) interactableObjectEventArgs.Hand, (int) ProjectData.PlatformMode);
                    
                    if (!Settings.Instance.Education)
                    {
                        InputController.photonView.RPC("SetKinematicsOnRpc", RpcTarget.Others);
                    }
                }

                if (ObjectController.Collaboration == Collaboration.SinglePlayer)
                {
                    GrabStart(interactableObjectEventArgs.Hand);
                }
            }
            else
            {
                GrabStart(interactableObjectEventArgs.Hand);
            }
        }

        private void OnGrabEndVoid(
            object sender,
            ObjectInteraction.InteractableObjectEventArgs interactableObjectEventArgs)
        {
            if (Settings.Instance.Multiplayer)
            {
                if (ObjectController.Collaboration == Collaboration.AllPlayers)
                {
                    ChangeOwner();
                    InputController.photonView.RPC("OnGrabEndRpc", RpcTarget.MasterClient,
                        (int) ProjectData.PlatformMode);

                    if (!Settings.Instance.Education)
                    {
                        InputController.photonView.RPC("SetKinematicsDefaultsRpc", RpcTarget.Others);
                    }
                }

                if (ObjectController.Collaboration == Collaboration.SinglePlayer)
                {
                    GrabEnd();
                }
            }
            else
            {
                GrabEnd();
            }
        }

        private void EditorGrabbed(
            object sender,
            ObjectInteraction.InteractableObjectEventArgs interactableObjectEventArgs)
        {
            if (Settings.Instance.Multiplayer)
            {
                ChangeOwner();
                //ObjectController.photonView.RPC("OnGrabbedRpc", RpcTarget.All);
            }
            else
            {
                _onEditorGrabStart();
            }
        }

        private void EditorUngrabbed(
            object sender,
            ObjectInteraction.InteractableObjectEventArgs interactableObjectEventArgs)
        {
            if (Settings.Instance.Multiplayer)
            {
                //ObjectController.photonView.RPC("OnUngrabbedRpc", RpcTarget.All);
            }
            else
            {
                _onEditorGrabEnd();
            }
        }

        private void OnGrabInit()
        {
            _saveTransform = GameObject.transform.ToTransformDT();

            _onInitTransform = ObjectController.gameObject.transform.ToTransformDT();

            if (ObjectController != null)
            {
                _saveJointData = ObjectController.GetJointData();
            }
        }

        private void OnUngrabInit()
        {
            var newTransform = GameObject.transform.ToTransformDT();
            
            var objectControllers = new List<ObjectController>();
            var previousTransformData = new Dictionary<int, TransformDT>();
            var newTransformData = new Dictionary<int, TransformDT>();
            
            objectControllers.Add(ObjectController);
            previousTransformData.Add(ObjectController.Id, _saveTransform);
            newTransformData.Add(ObjectController.Id, newTransform);

            new ModifyCommand(objectControllers,
                previousTransformData,
                newTransformData,
                _saveJointData);

            ProjectData.ObjectsAreChanged = true;
        }

        public void ReturnPosition()
        {
            _onInitTransform?.ToTransformUnity(GameObject.transform);
        }

        public void GrabStart(ControllerInteraction.ControllerHand hand)
        {
            GrabingContext context = new GrabingContext
            {
                GameObject = InteractObject.GetUsingObject(), Hand = hand
            }; 
            
            ObjectController.OnGrabStart();
            
            foreach (IGrabStartAware startAware in _grabStartList)
            {
                startAware?.OnGrabStart(context);
            }
        }

        public void GrabEnd()
        {
            ObjectController.OnGrabEnd();
            
            foreach (IGrabEndAware grabEndAware in _grabEndList)
            {
                grabEndAware?.OnGrabEnd();
            }
        }
    }
}
