using Photon.Pun;
using UnityEngine;
using Varwin.PlatformAdapter;
using Varwin.WWW;

namespace Varwin
{
    public class InputAction
    {
        public readonly ObjectInteraction.InteractObject InteractObject;
        protected readonly ObjectController ObjectController;
        protected readonly GameObject GameObject;
        protected readonly InputController InputController;
        protected bool IsRootGameObject => GameObject == ObjectController.RootGameObject;

        protected InputAction(ObjectController objectController, GameObject gameObject, ObjectInteraction.InteractObject interactObject, InputController inputController)
        {
            ObjectController = objectController;
            GameObject = gameObject;
            InteractObject = interactObject;
            InputController = inputController;
        }

        public virtual void DisableViewInput()
        {
            
        }

        public virtual void EnableViewInput()
        {
            
        }

        protected virtual void DisableEditorInput()
        {
            
        }

        protected virtual void EnableEditorInput()
        {
            
        }
        
        public void GameModeChanged(GameMode newGameMode)
        {
            if (newGameMode == GameMode.Edit)
            {
                DisableViewInput();
                EnableEditorInput();
            }
            else
            {
                DisableEditorInput();
                EnableViewInput();
            }
        }
        
        public void PlatformModeChanged(PlatformMode newPlatformMode)
        {
            GameModeChanged(ProjectData.GameMode);
        }

        protected void ChangeOwner()
        {
            if (!Settings.Instance.Multiplayer)
            {
                return;
            }

            var photonViews = ObjectController.RootGameObject.GetComponentsInChildren<PhotonView>();

            foreach (PhotonView photonView in photonViews)
            {
                if (photonView.Owner != null && photonView.Owner.UserId == PhotonNetwork.LocalPlayer.UserId)
                {
                    continue;
                }

                ObjectId objectIdComponent = photonView.GetComponent<ObjectId>();
                
                if (objectIdComponent)
                {
                    int objectId = objectIdComponent.Id;
                    PreviousOwnerDropObject(objectId);
                }
                
                Debug.Log($"Transfer owner ship for {photonView.gameObject.name} started! ");

                RequestThread requestThread = new RequestThread(
                    () =>
                    {
                        photonView.TransferOwnership(PhotonNetwork.LocalPlayer);
                    }
                );
            }
        }
        
        private void PreviousOwnerDropObject(int objectId)
        {
            if (InputController.photonView == null && InputController.photonView.Owner == null)
            {
                return;
            }
            
            string previousOwnerUserIs = InputController.photonView.Owner.UserId;
            if (!Settings.Instance.Education)
            {
                InputController.photonView.RPC("StopGrabRpc", RpcTarget.All, previousOwnerUserIs, objectId);
            }
        }
    }
}