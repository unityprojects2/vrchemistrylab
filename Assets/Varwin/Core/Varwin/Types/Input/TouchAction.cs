using System.Collections.Generic;
using System.Linq;
using Photon.Pun;
using UnityEngine;
using Varwin.Public;
using Varwin.PlatformAdapter;

namespace Varwin
{
    public class TouchAction: InputAction
    {
        private readonly List<ITouchStartAware> _touchStartList;
        private readonly List<ITouchEndAware> _touchEndList;
        

        public TouchAction(ObjectController objectController, GameObject gameObject, ObjectInteraction.InteractObject interactObject, InputController inputController) : base(objectController, gameObject, interactObject, inputController)
        {
            _touchStartList = GameObject.GetComponents<ITouchStartAware>().ToList();

            if (_touchStartList.Count > 0)
            {
                AddTouchStartAction();
            }

            _touchEndList = GameObject.GetComponents<ITouchEndAware>().ToList();

            if (_touchEndList.Count > 0)
            {
                AddTouchEndAction();
            }
        }

        #region OVERRIDES
        
        public override void DisableViewInput()
        {
            InteractObject.isTouchable = false;
            
            if (_touchStartList.Count > 0)
            {
                InteractObject.InteractableObjectTouched -= OnTouchStartVoid;
            }

            if (_touchEndList.Count > 0)
            {
                InteractObject.InteractableObjectUntouched -= OnTouchEndVoid;
            }
        }

        public override void EnableViewInput()
        {
            var interactableObjectBehaviour = GameObject.GetComponent<InteractableObjectBehaviour>();

            bool canTouch;

            if (interactableObjectBehaviour)
            {
                canTouch = interactableObjectBehaviour.IsTouchable;
            }
            else
            {
                canTouch = _touchStartList.Count > 0 || _touchEndList.Count > 0;
            }

            InteractObject.isTouchable = canTouch;
            
            if (!canTouch)
            {
                return;
            }

            if (_touchStartList.Count > 0)
            {
                InteractObject.InteractableObjectTouched -= OnTouchStartVoid;
                InteractObject.InteractableObjectTouched += OnTouchStartVoid;
            }

            if (_touchEndList.Count > 0)
            {
                InteractObject.InteractableObjectUntouched -= OnTouchEndVoid;
                InteractObject.InteractableObjectUntouched += OnTouchEndVoid;
            }
        }

        protected override void DisableEditorInput()
        {
             
        }

        protected override void EnableEditorInput()
        {
              
        }
        
        #endregion

        private void AddTouchStartAction()
        {
            InteractObject.isTouchable = true;
            InteractObject.InteractableObjectTouched += OnTouchStartVoid;
        }

        private void AddTouchEndAction()
        {
            InteractObject.isTouchable = true;
            InteractObject.InteractableObjectUntouched += OnTouchEndVoid;
        }

        private void OnTouchStartVoid(
            object sender,
            ObjectInteraction.InteractableObjectEventArgs interactableObjectEventArgs)
        {
            if (Settings.Instance.Multiplayer)
            {
                if (ObjectController.Collaboration == Collaboration.AllPlayers)
                {
                    ChangeOwner();
                    InputController.photonView.RPC("OnTouchStartRpc", RpcTarget.MasterClient);
                }

                if (ObjectController.Collaboration == Collaboration.SinglePlayer)
                {
                    TouchStart();
                }
            }
            else
            {
                TouchStart();
            }
        }
        private void OnTouchEndVoid(
            object sender,
            ObjectInteraction.InteractableObjectEventArgs interactableObjectEventArgs)
        {
            if (Settings.Instance.Multiplayer)
            {
                ChangeOwner();
                InputController.photonView.RPC("OnTouchEndRpc", RpcTarget.MasterClient);
            }
            else
            {
                TouchEnd();
            }
        }

        public void TouchEnd()
        {
            foreach (ITouchEndAware touchEndAware in _touchEndList)
            {
                touchEndAware?.OnTouchEnd();
            }
        }

        public void TouchStart()
        {
            foreach (ITouchStartAware touchStartAware in _touchStartList)
            {
                touchStartAware?.OnTouchStart();
            }
        }
    }
}