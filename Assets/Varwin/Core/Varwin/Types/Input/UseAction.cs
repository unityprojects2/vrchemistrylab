using System.Collections.Generic;
using System.Linq;
using Photon.Pun;
using UnityEngine;
using Varwin.Public;
using Varwin.PlatformAdapter;

namespace Varwin
{
    public class UseAction : InputAction
    {
        private readonly List<IUseStartAware> _useStartList;
        private readonly List<IUseEndAware> _useEndList;

        public UseAction(ObjectController objectController, GameObject gameObject, ObjectInteraction.InteractObject interactObject, InputController inputController) : 
            base(objectController, gameObject, interactObject, inputController)
        {
            _useStartList = GameObject.GetComponents<IUseStartAware>().ToList();

            if (_useStartList.Count > 0)
            {
                AddUseStartBehaviour();
            }

            _useEndList = GameObject.GetComponents<IUseEndAware>().ToList();

            if (_useEndList.Count > 0)
            {
                AddUseEndBehaviour();
            }

        }

        #region OVERRIDES

        public override void DisableViewInput()
        {
            InteractObject.isUsable = false;

            if (_useStartList.Count > 0)
            {
                InteractObject.InteractableObjectUsed -= OnUseStartVoid;
            }

            if (_useEndList.Count > 0)
            {
                InteractObject.InteractableObjectUnused -= OnUseEndVoid;
            }
        }

        public override void EnableViewInput()
        {
            var interactableObjectBehaviour = GameObject.GetComponent<InteractableObjectBehaviour>();

            bool canUse;

            if (interactableObjectBehaviour)
            {
                canUse = interactableObjectBehaviour.IsUsable;
            }
            else
            {
                canUse = _useStartList.Count > 0 || _useEndList.Count > 0;
            }

            InteractObject.isUsable = canUse;

            if (!canUse)
            {
                return;
            }

            if (_useStartList.Count > 0)
            {
                InteractObject.InteractableObjectUsed -= OnUseStartVoid;
                InteractObject.InteractableObjectUsed += OnUseStartVoid;
            }

            if (_useEndList.Count > 0)
            {
                InteractObject.InteractableObjectUnused -= OnUseEndVoid;
                InteractObject.InteractableObjectUnused += OnUseEndVoid;
            }
        }

        protected override void EnableEditorInput()
        {
             
        }

        protected override void DisableEditorInput()
        {
             
        }

        #endregion
        

        private void AddUseStartBehaviour()
        {
            InteractObject.isUsable = true;
            InteractObject.useOverrideButton = ControllerInput.ButtonAlias.TriggerPress;
        }

        private void AddUseEndBehaviour()
        {
            InteractObject.isUsable = true;
            InteractObject.useOverrideButton = ControllerInput.ButtonAlias.TriggerPress;
        }

        private void OnUseStartVoid(
            object sender,
            ObjectInteraction.InteractableObjectEventArgs interactableObjectEventArgs)
        {
            if (Settings.Instance.Multiplayer)
            {
                if (ObjectController.Collaboration == Collaboration.AllPlayers)
                {
                    ChangeOwner();
                    InputController.photonView.RPC("OnUseStartRpc", RpcTarget.MasterClient, (int) interactableObjectEventArgs.Hand);
                }

                if (ObjectController.Collaboration == Collaboration.SinglePlayer)
                {
                    UseStart(interactableObjectEventArgs.Hand);
                }
            }
            else
            {
                UseStart(interactableObjectEventArgs.Hand);
            }
        }

        private void OnUseEndVoid(
            object sender,
            ObjectInteraction.InteractableObjectEventArgs interactableObjectEventArgs)
        {
            if (Settings.Instance.Multiplayer)
            {
                if (ObjectController.Collaboration == Collaboration.AllPlayers)
                {
                    ChangeOwner();
                    InputController.photonView.RPC("OnUseEndRpc", RpcTarget.MasterClient);
                }

                if (ObjectController.Collaboration == Collaboration.SinglePlayer)
                {
                    UseEnd();
                }

            }
            else
            {
                UseEnd();
            }
        }

        public void UseEnd()
        {
            foreach (IUseEndAware useEndAware in _useEndList)
            {
                useEndAware?.OnUseEnd();
            }
        }
        
        public void UseStart(ControllerInteraction.ControllerHand hand)
        {
            UsingContext context = new UsingContext {GameObject = InteractObject.GetUsingObject(), Hand = hand};

            foreach (IUseStartAware useStartAware in _useStartList)
            {
                useStartAware?.OnUseStart(context);
            }
        }
    }
}