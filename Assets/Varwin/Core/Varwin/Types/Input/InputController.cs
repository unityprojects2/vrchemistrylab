﻿using System;
using System.Collections.Generic;
using NLog;
using Photon.Pun;
using UnityEngine;
using Object = UnityEngine.Object;
using Varwin.Models.Data;
using Varwin.ObjectsInteractions;
using Varwin.Public;
using Varwin.PlatformAdapter;
using Varwin.PUN;
using Logger = NLog.Logger;

#pragma warning disable 618

namespace Varwin
{
    public class InputController
    {
        public ObjectController ObjectController => _objectController;
        public ControllerInput.ControllerEvents ControllerEvents;
        
        public PhotonView photonView;

        public bool IsRoot => _isRoot;

        private RpcInputControllerMethods _rpc;
        private IHighlightAware _highlight;

        // ReSharper disable once NotAccessedField.Local
        private IHapticsAware _haptics;
        private readonly List<InputAction> _inputActions = new List<InputAction>();
        private readonly bool _isRoot;
        private readonly ObjectController _objectController;
        private CollisionController _collisionController;
        private readonly GameObject _gameObject;
        private ObjectInteraction.InteractObject _interactObject;
        private PlayerAppearance.InteractControllerAppearance _controllerAppearance;

        private GameObject _highlightOverriderGameObject;
        private VarwinHighlightEffect _highlighter;

        public HightLightConfig DefaultHighlightConfig;
        private readonly HightLightConfig _useHighlightConfig = DefaultHighlightConfigs.UseHighlight;

        private bool _highlightEnabled;
        
        private GrabSettings _grabSettings;

        private static Logger Logger = LogManager.GetCurrentClassLogger();

        public InputController(
            ObjectController objectController,
            GameObject gameObject,
            bool isRoot = false)
        {
            _objectController = objectController;
            _gameObject = gameObject;

            bool isRuntimeInstanced = false;
            _interactObject = InputAdapter.Instance.ObjectInteraction.Object.GetFrom(_gameObject);
            if (_interactObject == null)
            {
                _interactObject = InputAdapter.Instance.ObjectInteraction.Object.AddTo(_gameObject);
            }
            else
            {
                isRuntimeInstanced = true;
            }

            _isRoot = isRoot;
            Init();
            _interactObject.InteractableObjectGrabbed += OnAnyGrabStart;
            _interactObject.InteractableObjectUngrabbed += OnAnyGrabEnd;

            _controllerAppearance = InputAdapter.Instance.PlayerAppearance.ControllerAppearance.GetFrom(_gameObject);

            if (isRuntimeInstanced)
            {
                GameModeChanged(ProjectData.GameMode);
            }
        }

        public void ReturnPosition()
        {
            foreach (InputAction inputAction in _inputActions)
            {
                if (inputAction is GrabAction action)
                {
                    action.ReturnPosition();
                }
            }
        }

        public void Destroy()
        {
            if (_interactObject.IsGrabbed() || _interactObject.IsUsing())
            {
                _interactObject.ForceStopInteracting();
            }
            Object.Destroy(_gameObject);
        }
        
        public void EnableMultiplayer(int viewId)
        {
            if (!Settings.Instance.Multiplayer)
            {
                return;
            }

            photonView = _gameObject.GetComponent<PhotonView>();
            if (!photonView)
            {
                photonView = _gameObject.AddComponent<PhotonView>();
            }
            
            photonView.ViewID = viewId;

            if (photonView.ObservedComponents == null)
            {
                photonView.ObservedComponents = new List<Component>();
            }
           
            if (!photonView.ObservedComponents.Contains(_rpc))
            {
                photonView.ObservedComponents.Add(_rpc);
            }
            
            Helper.AddPhotonTransformView(_gameObject, photonView);

            if (!PhotonNetwork.IsMasterClient)
            {
                return;
            }
            
            if (_objectController.Collaboration == Collaboration.SinglePlayer)
            {
                photonView.TransferOwnership(0);
            }
        }

        private static void RemoveCollisionController(CollisionController controller)
        {
            controller.enabled = false;
            Object.Destroy(controller);
        }

        private bool IsObjectInteractable()
        {
            var behaviour = _gameObject.GetComponent<InteractableObjectBehaviour>();
            
            if (behaviour)
            {
                return behaviour.IsInteractable;
            }

            return _gameObject.GetComponent<IVarwinInputAware>() != null;
        }

        private void Init()
        {
            InitInputActions();

            var highlightAware = _gameObject.GetComponent<IHighlightAware>();

            if (Settings.Instance.HighlightEnabled && highlightAware == null)
            {
                highlightAware = _gameObject.AddComponent<DefaultHighlighter>();
            }

            var highlightOverrider = _gameObject.GetComponent<HighlightOverrider>();

            if (highlightOverrider)
            {
                _highlightOverriderGameObject = highlightOverrider.ObjectToHightlight;
            }

            if (highlightAware != null)
            {
                _highlight = highlightAware;
                AddHighLighter(highlightAware);
            }

            if (Settings.Instance.HighlightEnabled && IsObjectInteractable())
            {
                EnableHighlight();
            }

            var hapticsAware = _gameObject.GetComponent<IHapticsAware>();

            if (hapticsAware == null
                && (Settings.Instance.TouchHapticsEnabled
                    || Settings.Instance.GrabHapticsEnabled
                    || Settings.Instance.UseHapticsEnabled))
            {
                hapticsAware = _gameObject.AddComponent<DefaultHaptics>();
            }

            if (hapticsAware != null)
            {
                AddHaptics(hapticsAware);
                _haptics = hapticsAware;
            }

            if (!Settings.Instance.Multiplayer)
            {
                return;
            }

            if (!_rpc)
            {
                _rpc = _gameObject.AddComponent<RpcInputControllerMethods>();
                _rpc.Init(this);
            }
        }

        private void InitInputActions()
        {
            if (_inputActions.Count > 0)
            {
                _inputActions.Clear();
            }

            try
            {
                _inputActions.Add(new UseAction(_objectController,
                    _gameObject,
                    _interactObject,
                    this));

                _inputActions.Add(new GrabAction(_objectController,
                    _gameObject,
                    _interactObject,
                    this));

                _inputActions.Add(new TouchAction(_objectController,
                    _gameObject,
                    _interactObject,
                    this));

                _inputActions.Add(new PointerAction(_objectController,
                    _gameObject,
                    _interactObject,
                    this));

                _grabSettings = _gameObject.GetComponent<GrabSettings>();
            }
            catch (Exception e)
            {
                Debug.LogError($"Can't create input actions for {_gameObject.name} Error: {e.Message}");
            }
        }

        private void OnAnyGrabStart(object sender, ObjectInteraction.InteractableObjectEventArgs e)
        {
            if (!_isRoot)
            {
                return;
            }

            if (!_collisionController && _interactObject.ValidDrop == ObjectInteraction.InteractObject.ValidDropTypes.DropAnywhere)
            {
                _collisionController = _gameObject.GetComponent<CollisionController>();

                if (!_collisionController)
                {
                    _collisionController = _gameObject.AddComponent<CollisionController>();
                    _collisionController.InitializeController(this);
                }
            }

            //OOF; Now I regret this a little bit
            var hc = InputAdapter.Instance.PlayerController.Nodes.GetControllerReference(e.Hand);

            GameObject grabbingObject = hc.Controller.GetGrabbedObject();

            if (grabbingObject)
            {
                hc.Controller.AddColliders(grabbingObject.GetComponentsInChildren<Collider>());
            }

            GameObject vioGrabbingObject = _interactObject.GetGrabbingObject();

            if (vioGrabbingObject)
            {
                ControllerEvents = InputAdapter.Instance.ControllerInput.ControllerEventFactory.GetFrom(_interactObject.GetGrabbingObject());
            }
        }

        private void OnAnyGrabEnd(object sender, ObjectInteraction.InteractableObjectEventArgs e)
        {
            if (!_isRoot)
            {
                return;
            }

            var behaviour = _gameObject.GetComponent<JointBehaviour>();

            if (_collisionController) //if there is a jointBehaviour on this go it'll manage the collisionController on its own 
            {
                if (!behaviour)
                {
                    RemoveCollisionController(_collisionController);
                }
            } 

            InputAdapter.Instance.PlayerController.Nodes.GetControllerReference(e.Hand).Controller.AddColliders(null);
        }

        private void EnableHighlight()
        {
            if (_highlight == null)
            {
                return;
            }

            _interactObject.InteractableObjectTouched += HighlightObject;
            _interactObject.InteractableObjectUntouched += UnhighlightObject;
            _interactObject.InteractableObjectGrabbed += UnhighlightObject;
            //Wrong behavior of the highlight
            //Now we use default highlight from steam
            //For Collision controller use varwin highlight effect
            _interactObject.InteractableObjectUsed += HighlightObjectOnUseStart;
            _interactObject.InteractableObjectUnused += HighlightObjectOnUseEnd;

            _highlightEnabled = true;
        }

        // ReSharper disable once UnusedMember.Local
        private void DisableHighlight()
        {
            if (_highlight == null)
            {
                return;
            }

            UnhighlightObject(null, new ObjectInteraction.InteractableObjectEventArgs());
            _interactObject.InteractableObjectTouched -= HighlightObject;
            _interactObject.InteractableObjectUntouched -= UnhighlightObject;
            _interactObject.InteractableObjectGrabbed -= UnhighlightObject;
            //Wrong behavior of the highlight
            //Now we use default highlight from steam
            //For Collision controller use varwin highlight effect
            _interactObject.InteractableObjectUsed -= HighlightObjectOnUseStart;
            _interactObject.InteractableObjectUnused -= HighlightObjectOnUseEnd;

            _highlightEnabled = false;
        }

        public void EnableDrop()
        {
            _interactObject.ValidDrop = ObjectInteraction.InteractObject.ValidDropTypes.DropAnywhere;
        }

        public void DisableDrop()
        {
            _interactObject.ValidDrop = ObjectInteraction.InteractObject.ValidDropTypes.NoDrop;
        }

        public bool IsDropEnabled() => _interactObject.ValidDrop == ObjectInteraction.InteractObject.ValidDropTypes.DropAnywhere;

        public void ForceDropIfNeeded()
        {
            if (ProjectData.PlatformMode != PlatformMode.Vr)
            {
                return;
            }

            if (ControllerEvents == null)
            {
                return;
            }

            if (_grabSettings && _grabSettings.GrabType == GrabSettings.GrabTypes.Toggle)
            {
                if (!ControllerEvents.GetBoolInputActionState("GrabToggle"))
                {
                    _interactObject.ForceStopInteracting();
                }
            }
            else if (!ControllerEvents.IsButtonPressed(ControllerInput.ButtonAlias.GripPress))
            {
                _interactObject.ForceStopInteracting();
            }
        }

        public void DropGrabbedObject()
        {
            _interactObject.ForceStopInteracting();
        }

        public bool IsGrabbed() => _interactObject.IsGrabbed();

        public void DropGrabbedObjectAndDeactivate()
        {
            _interactObject.DropGrabbedObjectAndDeactivate();
        }

        private void AddHighLighter(IHighlightAware highlight)
        {
            DefaultHighlightConfig = highlight.HighlightConfig();

            _highlighter = _highlightOverriderGameObject
                ? _highlightOverriderGameObject.GetComponent<VarwinHighlightEffect>()
                : _gameObject.GetComponent<VarwinHighlightEffect>();

            if (!_highlighter)
            {
                _highlighter = _highlightOverriderGameObject
                    ? _highlightOverriderGameObject.AddComponent<VarwinHighlightEffect>()
                    : _gameObject.AddComponent<VarwinHighlightEffect>();
            }

            try
            {
                _highlighter.Configuration = DefaultHighlightConfig;
            }
            catch (Exception)
            {
                Logger.Error($"Can not add highlight to game object = {_gameObject}");
            }
        }

        private void AddHaptics(IHapticsAware haptics)
        {
            HapticsConfig onUse = haptics.HapticsOnUse();
            HapticsConfig onTouch = haptics.HapticsOnTouch();
            HapticsConfig onGrab = haptics.HapticsOnGrab();

            ObjectInteraction.InteractHaptics interactHaptic =
                InputAdapter.Instance.ObjectInteraction.Haptics.GetFrom(_gameObject)
                ?? InputAdapter.Instance.ObjectInteraction.Haptics.AddTo(_gameObject);

            if (onUse != null)
            {
                interactHaptic.StrengthOnUse = onUse.Strength;
                interactHaptic.IntervalOnUse = onUse.Interval;
                interactHaptic.DurationOnUse = onUse.Duration;
            }
            else
            {
                interactHaptic.StrengthOnUse = 0;
                interactHaptic.DurationOnUse = 0;
            }

            if (onTouch != null)
            {
                interactHaptic.StrengthOnTouch = onTouch.Strength;
                interactHaptic.IntervalOnTouch = onTouch.Interval;
                interactHaptic.DurationOnTouch = onTouch.Duration;
            }
            else
            {
                interactHaptic.StrengthOnTouch = 0;
                interactHaptic.DurationOnTouch = 0;
            }

            if (onGrab != null)
            {
                interactHaptic.StrengthOnGrab = onGrab.Strength;
                interactHaptic.IntervalOnGrab = onGrab.Interval;
                interactHaptic.DurationOnGrab = onGrab.Duration;
            }
            else
            {
                interactHaptic.StrengthOnGrab = 0;
                interactHaptic.DurationOnGrab = 0;
            }
        }

        public void Vibrate(GameObject controllerObject, float strength, float duration, float interval)
        {
            if (!_interactObject.IsGrabbed() && !_interactObject.IsUsing())
            {
                Debug.LogWarning("Can't vibrate with object not grabbed or in use");
                return;
            }

            PlayerController.PlayerNodes.ControllerNode playerController =
                InputAdapter.Instance.PlayerController.Nodes.GetControllerReference(controllerObject);

            if (playerController == null)
            {
                Debug.LogWarning("Can't vibrate: " + controllerObject + " is not a controller");
                return;
            }

            playerController.Controller.TriggerHapticPulse(strength, duration, interval);
        }

        private void UnhighlightObject(object sender, ObjectInteraction.InteractableObjectEventArgs e)
        {
            SetupHighlightWithConfig(false, DefaultHighlightConfig);
        }

        private void HighlightObject(object sender, ObjectInteraction.InteractableObjectEventArgs e)
        {
            SetupHighlightWithConfig(true, DefaultHighlightConfig);
        }

        private void HighlightObjectOnUseStart(object sender, ObjectInteraction.InteractableObjectEventArgs e)
        {
            if (_interactObject.IsGrabbed())
            {
                return;
            }
            
            SetupHighlightWithConfig(true, _useHighlightConfig);
        }
        
        private void HighlightObjectOnUseEnd(object sender, ObjectInteraction.InteractableObjectEventArgs e)
        {
            var hc = InputAdapter.Instance.PlayerController.Nodes.GetControllerReference(e.Hand);
            if (_interactObject.IsGrabbed())
            {
                return;
            }
            
            SetupHighlightWithConfig(!hc.Controller.GetGrabbedObject(), DefaultHighlightConfig);
        }

        private void SetupHighlightWithConfig(bool isEnabled, HightLightConfig config)
        {
            if (!_gameObject || !_highlighter)
            {
                return;
            }
            
            _highlighter.SetHighlightEnabled(isEnabled);
            
            if (isEnabled)
            {
                _highlighter.SetConfiguration(config);
            }
        }

        public TransformDto GetTransform()
        {
            var objectId = _gameObject.GetComponent<ObjectId>();
            
            if (!_gameObject || !objectId)
            {
                return null;
            }

            Transform transform = _objectController.gameObject == _gameObject
                ? _objectController.GetAffectedTransform()
                : _gameObject.transform;

            return new TransformDto {Id = objectId.Id, Transform = transform.ToTransformDT()};
        }

        public bool IsConnectedToGameObject(GameObject go) => go == _gameObject;

        public void DisableViewInput()
        {
            foreach (InputAction inputAction in _inputActions)
            {
                inputAction.DisableViewInput();
            }
        }

        public void EnableViewInput()
        {
            foreach (InputAction inputAction in _inputActions)
            {
                inputAction.EnableViewInput();
            }
        }

        public void GameModeChanged(GameMode newGameMode)
        {
            if (_interactObject.IsGrabbed() || _interactObject.IsUsing())
            {
                _interactObject.ForceStopInteracting();
            }

            foreach (InputAction inputAction in _inputActions)
            {
                inputAction.GameModeChanged(newGameMode);
            }
            
            if (!_highlightEnabled && IsObjectInteractable())
            {
                EnableHighlight();
            }
            else if (_highlightEnabled && !IsObjectInteractable())
            {
                DisableHighlight();
            }
        }

        public void PlatformModeChanged(PlatformMode newPlatformMode)
        {
            if (_interactObject.IsGrabbed() || _interactObject.IsUsing())
            {
                _interactObject.ForceStopInteracting();
            }
            _interactObject.DestroyComponent();
            _controllerAppearance?.DestroyComponent();
            _interactObject = InputAdapter.Instance.ObjectInteraction.Object.AddTo(_gameObject);
            Init();
            _interactObject.InteractableObjectGrabbed += OnAnyGrabStart;
            _interactObject.InteractableObjectUngrabbed += OnAnyGrabEnd;
            
            _controllerAppearance = InputAdapter.Instance.PlayerAppearance.ControllerAppearance.GetFrom(_gameObject);
            
            foreach (InputAction inputAction in _inputActions)
            {
                inputAction.PlatformModeChanged(newPlatformMode);
            }
        }

        public void EnableViewUsing()
        {
            if (!_highlightEnabled)
            {
                EnableHighlight();
            }

            foreach (InputAction inputAction in _inputActions)
            {
                if (inputAction is UseAction)
                {
                    inputAction.EnableViewInput();
                }
            }
        }

        public void DisableViewUsing()
        {
            foreach (InputAction inputAction in _inputActions)
            {
                if (inputAction is UseAction)
                {
                    inputAction.DisableViewInput();
                }
            }
        }

        public void EnableViewGrab()
        {
            if (!_highlightEnabled)
            {
                EnableHighlight();
            }
            
            foreach (InputAction inputAction in _inputActions)
            {
                if (inputAction is GrabAction)
                {
                    inputAction.EnableViewInput();
                }
            }
        }

        public void DisableViewGrab()
        {
            foreach (InputAction inputAction in _inputActions)
            {
                if (inputAction is GrabAction)
                {
                    inputAction.DisableViewInput();
                }
            }
        }

        public void EnableViewTouch()
        {
            if (!_highlightEnabled)
            {
                EnableHighlight();
            }
            
            foreach (InputAction inputAction in _inputActions)
            {
                if (inputAction is TouchAction)
                {
                    inputAction.EnableViewInput();
                }
            }
        }

        public void DisableViewTouch()
        {
            foreach (InputAction inputAction in _inputActions)
            {
                if (inputAction is TouchAction)
                {
                    inputAction.DisableViewInput();
                }
            }
        }

        public void UseStart(ControllerInteraction.ControllerHand hand)
        {
            foreach (InputAction inputAction in _inputActions)
            {
                if (inputAction is UseAction action)
                {
                    action.UseStart(hand);
                }
            }
        }

        public void UseEnd()
        {
            foreach (InputAction inputAction in _inputActions)
            {
                if (inputAction is UseAction action)
                {
                    action.UseEnd();
                }
            }
        }

        public void GrabStart(ControllerInteraction.ControllerHand hand)
        {
            foreach (InputAction inputAction in _inputActions)
            {
                if (inputAction is GrabAction action)
                {
                    action.GrabStart(hand);
                }
            }
        }
        
        public void GrabEnd()
        {
            foreach (InputAction inputAction in _inputActions)
            {
                if (inputAction is GrabAction action)
                {
                    action.GrabEnd();
                }
            }
        }
        
        public void TouchStart()
        {
            foreach (InputAction inputAction in _inputActions)
            {
                if (inputAction is TouchAction action)
                {
                    action.TouchStart();
                }
            }
        }
        
        public void TouchEnd()
        {
            foreach (InputAction inputAction in _inputActions)
            {
                if (inputAction is TouchAction action)
                {
                    action.TouchEnd();
                }
            }
        }
        
        public void SetKinematicsOn()
        {
            _objectController.SaveKinematics();
            _objectController.SetKinematicsOn();
        }
        
        public void SetKinematicsDefaults()
        {
            _objectController.SetKinematicsDefaults();
        }
    }
}

public class TransformDto
{
    public int Id;
    public TransformDT Transform;
}
