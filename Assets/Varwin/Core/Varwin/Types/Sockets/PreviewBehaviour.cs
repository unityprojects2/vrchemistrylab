using UnityEngine;
using Varwin.SocketLibrary.Extension;

namespace Varwin.SocketLibrary
{
    /// <summary>
    /// Класс, реализующий логику превью цепей.
    /// </summary>
    public class PreviewBehaviour : MonoBehaviour
    {
        /// <summary>
        /// Объект-контроллер соединений.
        /// </summary>
        private SocketController _sourceSocketController;

        /// <summary>
        /// Инициализация контроллера превью.
        /// </summary>
        /// <param name="socketController">Контроллер соединений.</param>
        public void Init(SocketController socketController)
        {
            _sourceSocketController = socketController;
            gameObject.SetActive(false);
        }

        /// <summary>
        /// Отображение превью цепи.
        /// </summary>
        /// <param name="socketPoint">Розетка.</param>
        /// <param name="plugPoint">Вилка.</param>
        public void Show(SocketPoint socketPoint, PlugPoint plugPoint)
        {
            gameObject.TransformToSocket(socketPoint, plugPoint);
            DrawTreePreview(_sourceSocketController.ConnectionGraphBehaviour.HeadOfTree);
        }

        /// <summary>
        /// Отобразить превью дерева объектов.
        /// </summary>
        /// <param name="elementOfTree">Первый элемент дерева.</param>
        private void DrawTreePreview(ElementOfTree elementOfTree)
        {
            elementOfTree.SelfObject.PreviewBehaviour.gameObject.SetActive(true);
            
            foreach (var child in elementOfTree.Childs)
            {
                var position =
                    elementOfTree.SelfObject.PreviewBehaviour.transform.TransformPoint(child.ConnectionPositionOffset);
                
                var rotation = elementOfTree.SelfObject.PreviewBehaviour.transform.rotation * child.ConnectionRotationOffset;

                child.SelfObject.PreviewBehaviour.transform.position = position;
                child.SelfObject.PreviewBehaviour.transform.rotation = rotation;
                
                DrawTreePreview(child);
            }
        }
        
        /// <summary>
        /// Скрыть превью дерева.
        /// </summary>
        /// <param name="elementOfTree">Первый элемент дерева.</param>
        private void HideTreePreview(ElementOfTree elementOfTree)
        {
            elementOfTree.SelfObject.PreviewBehaviour.gameObject.SetActive(false);
            
            foreach (var child in elementOfTree.Childs)
            {
                HideTreePreview(child);
            }
        }

        /// <summary>
        /// Скрыть превью дерева.
        /// </summary>
        public void Hide()
        {
            HideTreePreview(_sourceSocketController.ConnectionGraphBehaviour.HeadOfTree);
        }
    }
}