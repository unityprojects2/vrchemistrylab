﻿using UnityEngine;
using Varwin.Public;

namespace Varwin.SocketLibrary.VarwinObjects
{
    /// <summary>
    /// Представление точки соединения в Varwin'e.
    /// </summary>
    public abstract class JointPointVarwinObject : VarwinObject, ISwitchModeSubscriber
    {
        /// <summary>
        /// Делегат события.
        /// </summary>
        public delegate void JointPointEventHandler(Wrapper point);

        /// <summary>
        /// Событие, вызываемое при соединении точки.
        /// </summary>
        [Event(English: "On connect", Russian: "При соединении")]
        public event JointPointEventHandler OnConnect;

        /// <summary>
        /// Событие, вызываемое при отсоединении точки.
        /// </summary>
        [Event(English: "On disconnect", Russian: "При отсоединении")]
        public event JointPointEventHandler OnDisconnect;

        /// <summary>
        /// Событие, вызываемое при появлении превью.
        /// </summary>
        [Event(English: "On preview show", Russian: "При появлении превью")]
        public event JointPointEventHandler OnPreviewShow;

        /// <summary>
        /// Событие, вызываемое при скрытии превью.
        /// </summary>
        [Event(English: "On preview hide", Russian: "При скрытии превью")]
        public event JointPointEventHandler OnPreviewHide;

        /// <summary>
        /// Может ли соединяться.
        /// </summary>
        [Variable(English: "Can connect", Russian: "Может ли соединяться")]
        [VarwinInspector(English: "Can connect", Russian: "Может ли соединяться")]
        public bool CanConnect
        {
            get => JointPoint.CanConnect;
            set => JointPoint.CanConnect = value;
        }

        /// <summary>
        /// Точка для создания соединения.
        /// </summary>
        public JointPoint JointPoint { get; protected set; }

        /// <summary>
        /// Иконка точки.
        /// </summary>
        public GameObject IconGameObject;

        /// <summary>
        /// Ключ точки.
        /// </summary>
        [VarwinInspector(English: "Key", Russian: "Ключ подключения")]
        public string Key
        {
            get => GetKey();
            set => SetKey(value);
        }

        /// <summary>
        /// Иницализация точки. 
        /// </summary>
        private void Awake()
        {
            JointPoint = InitPoint();
            JointPoint.OnConnect += JointPointOnConnect;
            JointPoint.OnDisconnect += JointPointOnDisconnect;
            JointPoint.OnPreviewShow += JointPointOnPreviewShow;
            JointPoint.OnPreviewHide += JointPointOnPreviewHide;
        }

        /// <summary>
        /// При появлении превью точки.
        /// </summary>
        /// <param name="plugPoint">Вилка.</param>
        /// <param name="socketPoint">Розетка.</param>
        private void JointPointOnPreviewShow(JointPoint plugPoint, SocketPoint socketPoint)
        {
            OnPreviewShow?.Invoke(plugPoint == JointPoint
                ? socketPoint.gameObject.GetWrapper()
                : plugPoint.gameObject.GetWrapper());
        }

        /// <summary>
        /// При скрытии превью точки.
        /// </summary>
        /// <param name="plugPoint">Вилка.</param>
        /// <param name="socketPoint">Розетка.</param>
        private void JointPointOnPreviewHide(PlugPoint plugPoint, SocketPoint socketPoint)
        {
            OnPreviewHide?.Invoke(plugPoint == JointPoint
                ? socketPoint.gameObject.GetWrapper()
                : plugPoint.gameObject.GetWrapper());
        }

        /// <summary>
        /// При отсоединении точки.
        /// </summary>
        /// <param name="plugPoint">Вилка.</param>
        /// <param name="socketPoint">Розетка.</param>
        private void JointPointOnDisconnect(PlugPoint plugPoint, SocketPoint socketPoint)
        {
            OnDisconnect?.Invoke(plugPoint == JointPoint
                ? socketPoint.gameObject.GetWrapper()
                : plugPoint.gameObject.GetWrapper());
        }

        /// <summary>
        /// При присоединении точки.
        /// </summary>
        /// <param name="plugPoint">Вилка.</param>
        /// <param name="socketPoint">Розетка.</param>
        private void JointPointOnConnect(PlugPoint plugPoint, SocketPoint socketPoint)
        {
            OnConnect?.Invoke(plugPoint == JointPoint
                ? socketPoint.gameObject.GetWrapper()
                : plugPoint.gameObject.GetWrapper());
        }

        /// <summary>
        /// Инициализация основных методов связной точки.
        /// </summary>
        private void Start()
        {
            var objectController = gameObject.GetWrapper().GetObjectController();
            if (objectController == null)
            {
                Debug.Log("Object controller not found");
                return;
            }

            JointPoint.gameObject.AddComponent<ObjectId>().Id = gameObject.GetWrapper().GetInstanceId();
            UpdatePoint();
        }

        /// <summary>
        /// Обновление параметров точки.
        /// </summary>
        private void UpdatePoint()
        {
            var objectController = gameObject.GetWrapper().GetObjectController();

            if (objectController == null)
            {
                return;
            }

            JointPoint.gameObject.SetActive(objectController.Parent != null);

            if (objectController.Parent == null)
            {
                return;
            }

            UpdateTransform();
            JointPoint.transform.parent = objectController.Parent.gameObject.transform;
            
            Debug.Log("PointUpdated!");
        }

        /// <summary>
        /// Обновление точки в редакторе.
        /// </summary>
        private void Update()
        {
            if (ProjectData.GameMode != GameMode.Edit)
            {
                return;
            }

            UpdateTransform();
        }

        /// <summary>
        /// Обновление трансформа связной точки.
        /// </summary>
        private void UpdateTransform()
        {
            var oldParent = JointPoint.transform.parent;
            JointPoint.transform.parent = null;
            JointPoint.transform.localScale = transform.lossyScale;
            JointPoint.transform.position = transform.position;
            JointPoint.transform.rotation = transform.rotation;
            JointPoint.transform.parent = oldParent;
        }

        /// <summary>
        /// Инициализация точки.
        /// </summary>
        /// <returns>Точка для соединения.</returns>
        protected abstract JointPoint InitPoint();

        /// <summary>
        /// Получение значения ключа.
        /// </summary>
        /// <param name="value">Значение ключа.</param>
        protected abstract string GetKey();

        /// <summary>
        /// Установление значения ключа.
        /// </summary>
        /// <param name="value">Значение ключа.</param>
        protected abstract void SetKey(string value);

        /// <summary>
        /// Отключение иконки при переходе из едита.
        /// </summary>
        /// <param name="newMode">Новый режим.</param>
        /// <param name="oldMode">Старый режим.</param>
        public void OnSwitchMode(GameMode newMode, GameMode oldMode)
        {
            IconGameObject.SetActive(newMode == GameMode.Edit);
            GetComponent<Collider>().enabled = newMode == GameMode.Edit;
            UpdatePoint();
        }

        /// <summary>
        /// Отключить точку.
        /// </summary>
        [Action(English: "Disconnect", Russian: "Отключить")]
        public void Disconnect()
        {
            SocketController.Disconnect(JointPoint);
        }

        /// <summary>
        /// Удаление связной точки.
        /// </summary>
        private void OnDestroy()
        {
            if (JointPoint && JointPoint.gameObject)
            {
                Destroy(JointPoint.gameObject);
            }
        }
    }
}