﻿using UnityEngine;

namespace Varwin.SocketLibrary.VarwinObjects
{
    public abstract class SocketPointVarwinObject : JointPointVarwinObject
    {
        /// <summary>
        /// Инициализация розетки.
        /// </summary>
        /// <returns>Розетка.</returns>
        protected override JointPoint InitPoint()
        {
            var obj = new GameObject("SocketPoint");
            var collider = obj.AddComponent<SphereCollider>();
            collider.gameObject.layer = 2;
            collider.isTrigger = true;

            return obj.AddComponent<SocketPoint>();
        }

        /// <summary>
        /// Получение ключа для подключения.
        /// </summary>
        /// <returns>Ключ для подключения.</returns>
        protected override string GetKey()
        {
            var socketPoint = (SocketPoint) JointPoint;
            return socketPoint.AvailableKeys == null || socketPoint.AvailableKeys.Length == 0
                ? ""
                : socketPoint.AvailableKeys[0];
        }

        /// <summary>
        /// Задание ключа для подключения.
        /// </summary>
        /// <param name="value">Ключ для подключения.</param>
        protected override void SetKey(string value)
        {
            var socketPoint = (SocketPoint) JointPoint;
            if (socketPoint.AvailableKeys == null || socketPoint.AvailableKeys.Length == 0)
            {
                socketPoint.AvailableKeys = new[] {value};
            }
            else
            {
                socketPoint.AvailableKeys[0] = value;
            }
        }

        /// <summary>
        /// Подключить.
        /// </summary>
        /// <param name="wrapperPoint">Враппер точки.</param>
        [Action(English: "Connect", Russian: "Подключить")]
        public void Connect(Wrapper wrapperPoint)
        {
            var jointPoint = wrapperPoint.GetGameObject().GetComponent<PlugPointVarwinObject>();
            if (!jointPoint)
            {
                return;
            }

            var plugPoint = (PlugPoint) (jointPoint.JointPoint);
            var socketPoint = (SocketPoint) JointPoint;

            if (!socketPoint || !plugPoint)
            {
                return;
            }

            SocketController.Connect(plugPoint, socketPoint);
        }

        /// <summary>
        /// Подключить с анимацией.
        /// </summary>
        /// <param name="wrapperPoint">Враппер точки.</param>
        [Action(English: "Connect with animation", Russian: "Подключить с анимацией")]
        public void ConnectWithAnimation(Wrapper wrapperPoint, float time)
        {
            var jointPoint = wrapperPoint.GetGameObject().GetComponent<PlugPointVarwinObject>();
            if (!jointPoint)
            {
                return;
            }

            var plugPoint = (PlugPoint) (jointPoint.JointPoint);
            var socketPoint = (SocketPoint) JointPoint;
            
            if (!socketPoint || !plugPoint)
            {
                return;
            }
            
            var clampedTime = Mathf.Clamp(time, 0, Mathf.Infinity);
            plugPoint.SocketController.ConnectWithAnimation(plugPoint, socketPoint, clampedTime);
        }
    }
}