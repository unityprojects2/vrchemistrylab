﻿using UnityEngine;

namespace Varwin.SocketLibrary.VarwinObjects
{
    /// <summary>
    /// Вилка для Varwin'a.
    /// </summary>
    public abstract class PlugPointVarwinObject : JointPointVarwinObject
    {
        /// <summary>
        /// Может ли быть разъединен.
        /// </summary>
        [Variable(English: "Can disconnect", Russian: "Может ли быть разъединен")]
        [VarwinInspector(English: "Can disconnect", Russian: "Может ли быть разъединен")]
        public bool CanDisconnect
        {
            get => ((PlugPoint) JointPoint).CanDisconnect;
            set => ((PlugPoint) JointPoint).CanDisconnect = value;
        }

        /// <summary>
        /// Инициализация точки.
        /// </summary>
        /// <returns>Вилка.</returns>
        protected override JointPoint InitPoint()
        {
            var obj = new GameObject("PlugPoint");
            var collider = obj.AddComponent<SphereCollider>();
            collider.gameObject.layer = 2;
            collider.isTrigger = true;

            return obj.AddComponent<PlugPoint>();
        }

        /// <summary>
        /// Получение ключа для подключения.
        /// </summary>
        /// <returns>Ключ для подключения.</returns>
        protected override string GetKey()
        {
            return ((PlugPoint) JointPoint).Key;
        }

        /// <summary>
        /// Задание ключа для подключения.
        /// </summary>
        /// <param name="value">Ключ для подключения.</param>
        protected override void SetKey(string value)
        {
            ((PlugPoint) JointPoint).Key = value;
        }

        /// <summary>
        /// Подключить.
        /// </summary>
        /// <param name="wrapperPoint">Враппер точки.</param>
        [Action(English:"Connect", Russian:"Подключить")]
        public void Connect(Wrapper wrapperPoint)
        {
            var jointPoint = wrapperPoint.GetGameObject().GetComponent<SocketPointVarwinObject>();
            if (!jointPoint)
            {
                return;
            }

            var socketPoint = (SocketPoint) jointPoint.JointPoint;
            var plugPoint = (PlugPoint) JointPoint;
            if (!plugPoint || !socketPoint)
            {
                return;
            }

            SocketController.Connect(plugPoint, socketPoint);
        }

        /// <summary>
        /// Подключить с анимацией.
        /// </summary>
        /// <param name="wrapperPoint">Враппер точки.</param>
        [Action(English:"Connect with animation", Russian:"Подключить с анимацией")]
        public void ConnectWithAnimation(Wrapper wrapperPoint, float time)
        {
            var jointPoint = wrapperPoint.GetGameObject().GetComponent<SocketPointVarwinObject>();
            if (!jointPoint)
            {
                return;
            }

            var socketPoint = (SocketPoint) jointPoint.JointPoint;
            var plugPoint = (PlugPoint) JointPoint;
            if (!plugPoint || !socketPoint)
            {
                return;
            }

            var clampedTime = Mathf.Clamp(time, 0, Mathf.Infinity);
            plugPoint.SocketController.ConnectWithAnimation(plugPoint, socketPoint, clampedTime);
        }
    }
}