﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Varwin.SocketLibrary.Extension
{
    /// <summary>
    /// Проще говоря дерево соединений.
    /// </summary>
    public class ConnectionGraphBehaviour : MonoBehaviour
    {
        /// <summary>
        /// Базовый контроллер, которому принадлежит дерево.
        /// </summary>
        private SocketController _baseController;
        
        /// <summary>
        /// Голова дерева.
        /// </summary>
        public ElementOfTree HeadOfTree { get; private set; }

        /// <summary>
        /// Инцииализация дерева.
        /// </summary>
        /// <param name="socketController">Контроллер соединений.</param>
        /// <returns>Дерево соединений.</returns>
        public static ConnectionGraphBehaviour InitTree(SocketController socketController)
        {
            var tree = new GameObject("ConnectionTree").AddComponent<ConnectionGraphBehaviour>();
            tree._baseController = socketController;
            tree.transform.parent = socketController.transform;

            tree.UpdateTree();

            return tree;
        }

        /// <summary>
        /// Включает ли в дочерние объекты данный контроллер.
        /// </summary>
        /// <param name="socketController">Контроллер.</param>
        /// <returns>Истина, если имеется.</returns>
        public bool HasChild(SocketController socketController)
        {
            var has = false;

            ForEach(a => has |= a == socketController);
            return has;
        }

        /// <summary>
        /// Обновление всех деревьев графа.
        /// </summary>
        public void UpdateAllTrees()
        {
            ForEach(a => a.ConnectionGraphBehaviour?.UpdateTree());
        }

        /// <summary>
        /// Обновление дерева.
        /// </summary>
        private void UpdateTree()
        {
            if (!_baseController)
            {
                return;
            }

            HeadOfTree = RecursiveAppendingTree(_baseController);
        }

        #region Различные необходимые операции для работы с деревом.

        /// <summary>
        /// Цикл по каждому элементу дерева.
        /// </summary>
        /// <param name="eventForController">Метод, выполняемый на каждом элементе.</param>
        public void ForEach(Action<SocketController> eventForController)
        {
            RecursiveEvent(arg => eventForController?.Invoke(arg.SelfObject), HeadOfTree);
        }

        /// <summary>
        /// Цикл по каждому элементу дерева.
        /// </summary>
        /// <param name="eventForElementOfTree">Метод, выполняемый на каждом элементе.</param>
        public void ForEach(Action<ElementOfTree> eventForElementOfTree)
        {
            RecursiveEvent(eventForElementOfTree, HeadOfTree);
        }

        /// <summary>
        /// Поднята ли связка.
        /// </summary>
        /// <returns>Истина, если поднята.</returns>
        public bool IsGrabbed()
        {
            var isGrabbed = false;

            ForEach(a => isGrabbed |= a.IsLocalGrabbed);

            return isGrabbed;
        }

        /// <summary>
        /// Вызов рекурсивного метода по дереву.
        /// </summary>
        /// <param name="eventForController">Метод, вызываемый для каждого элемента дерева.</param>
        /// <param name="elementOfTree">Голова дерева.</param>
        private void RecursiveEvent(Action<ElementOfTree> eventForController, ElementOfTree elementOfTree)
        {
            if (elementOfTree == null)
            {
                return;
            }

            eventForController?.Invoke(elementOfTree);

            foreach (var child in elementOfTree.Childs)
            {
                RecursiveEvent(eventForController, child);
            }
        }

        #endregion

        /// <summary>
        /// Рекурсивное заполнение дерева.
        /// </summary>
        /// <param name="socketController">Ссылка на базовый контроллер.</param>
        /// <param name="pullControllers">Пулл уже обработанных контроллеров.</param>
        /// <returns>Голова дерева.</returns>
        private ElementOfTree RecursiveAppendingTree(SocketController socketController, HashSet<SocketController> pullControllers = null)
        {
            if (socketController == null)
            {
                return null;
            }
            
            if (pullControllers == null)
            {
                pullControllers = new HashSet<SocketController>();
            }

            if (pullControllers.Contains(socketController))
            {
                return null;
            }

            var child = new ElementOfTree(socketController);

            pullControllers.Add(socketController);

            if (socketController.JointPoints == null)
            {
                return child;
            }
            
            foreach (var jointPoint in socketController.JointPoints)
            {
                if (jointPoint.IsFree)
                {
                    continue;
                }

                var connectedChild = RecursiveAppendingTree(jointPoint.ConnectedPoint.SocketController, pullControllers);

                if (connectedChild == null)
                {
                    continue;
                }

                if (jointPoint is PlugPoint point)
                {
                    point.SocketController.gameObject.TransformToSocket((SocketPoint) point.ConnectedPoint, point);
                }

                if (jointPoint is SocketPoint socketPoint)
                {
                    socketPoint.ConnectedPoint.SocketController.gameObject.TransformToSocket(socketPoint, (PlugPoint) socketPoint.ConnectedPoint);
                }

                connectedChild.ConnectionPositionOffset = socketController.transform.InverseTransformPoint(connectedChild.SelfObject.transform.position);
                connectedChild.ConnectionRotationOffset = Quaternion.Inverse(socketController.transform.rotation) * connectedChild.SelfObject.transform.rotation;

                child.Childs.Add(connectedChild);
            }

            return child;
        }

        /// <summary>
        /// При взятии в руку.
        /// </summary>
        public void OnGrabStart()
        {
            ForEach(a => { a.CollisionProvider.OnGrabStart(); });
        }

        /// <summary>
        /// При отпускании.
        /// </summary>
        public void OnGrabEnd()
        {
            if (IsGrabbed())
            {
                return;
            }
            
            ForEach(a =>
            {
                a.CollisionProvider.OnGrabEnd();
                a.ConnectIfPossible();
            });
        }

        /// <summary>
        /// Отрисовка в Unity дерева.
        /// </summary>
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.blue;
            DrawTree(HeadOfTree);
        }

        /// <summary>
        /// Отрисовка дерева.
        /// </summary>
        /// <param name="head"></param>
        private void DrawTree(ElementOfTree head)
        {
            Gizmos.DrawSphere(head.SelfObject.transform.position, 0.1f);
            foreach (var elementOfTree in head.Childs)
            {
                Gizmos.DrawLine(head.SelfObject.transform.position, elementOfTree.SelfObject.transform.position);
                DrawTree(elementOfTree);
            }
        }

        /// <summary>
        /// Фикс трансформации объекта при взятии в руку.
        /// </summary>
        private void LateUpdate()
        {
            if (HeadOfTree == null)
            {
                return;
            }

            if (!HeadOfTree.SelfObject.IsLocalGrabbed)
            {
                return;
            }

            if (ProjectData.PlatformMode != PlatformMode.Vr)
            {
                TransformChild(HeadOfTree);
            }
        }

        /// <summary>
        /// Фикс трансформаций при взятии в руку в VR.
        /// </summary>
        private void FixedUpdate()
        {
            if (HeadOfTree == null)
            {
                return;
            }

            if (!HeadOfTree.SelfObject.IsLocalGrabbed)
            {
                return;
            }

            if (ProjectData.PlatformMode == PlatformMode.Vr)
            {
                TransformChildByRigidbody(HeadOfTree, HeadOfTree.SelfObject.Rigidbody);
            }
        }

        /// <summary>
        /// Трансформация объектов дерева в VR.
        /// </summary>
        /// <param name="headOfTree">Голова списка.</param>
        /// <param name="mainRigidbody">Взятый объект.</param>
        private void TransformChildByRigidbody(ElementOfTree headOfTree, Rigidbody mainRigidbody)
        {
            mainRigidbody.ResetInertiaTensor();
            foreach (var element in headOfTree.Childs)
            {
                element.SelfObject.transform.position = headOfTree.SelfObject.transform.TransformPoint(element.ConnectionPositionOffset);
                element.SelfObject.transform.rotation = headOfTree.SelfObject.transform.rotation * element.ConnectionRotationOffset;
                element.SelfObject.Rigidbody.velocity = mainRigidbody.GetPointVelocity(mainRigidbody.transform.TransformPoint(element.ConnectionPositionOffset));
                
                TransformChildByRigidbody(element, mainRigidbody);
            }
        }

        /// <summary>
        /// Трансформация объектов дерева в DP.
        /// </summary>
        /// <param name="headOfTree">Голова дерева.</param>
        private void TransformChild(ElementOfTree headOfTree)
        {
            foreach (var element in headOfTree.Childs)
            {
                element.SelfObject.Rigidbody.velocity = Vector3.zero;
                element.SelfObject.Rigidbody.angularVelocity = Vector3.zero;

                element.SelfObject.transform.position = headOfTree.SelfObject.transform.TransformPoint(element.ConnectionPositionOffset);
                element.SelfObject.transform.rotation = headOfTree.SelfObject.transform.rotation * element.ConnectionRotationOffset;

                TransformChild(element);
            }
        }
    }
}