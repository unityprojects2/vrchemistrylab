﻿using UnityEngine;
using Varwin.ObjectsInteractions;
using Varwin.Public;

namespace Varwin.SocketLibrary
{
    /// <summary>
    /// Костыль, нужен для обхода ограничений CollisionController'a.
    /// </summary>
    public class CollisionProvider : MonoBehaviour
    {
        /// <summary>
        /// Joint 1.0-связка.
        /// </summary>
        public ChainedJointController ChainedJointController;

        /// <summary>
        /// Обработчик коллизий.
        /// </summary>
        private CollisionController _collisionController;

        /// <summary>
        /// Обработчик коллизий.
        /// </summary>
        public CollisionController CollisionController => GetCollisionController();

        /// <summary>
        /// Подписанный контроллер.
        /// </summary>
        private ChainedJointController _subscribedController;

        /// <summary>
        /// Инициализация контроллера цепи 1.0.
        /// </summary>
        private void Awake()
        {
            ChainedJointController = ChainedJointController.CreateNewChainedController();
            ChainedJointController.transform.parent = transform;
        }

        /// <summary>
        /// При взятии в руку.
        /// </summary>
        public void OnGrabStart()
        {
            CollisionController.enabled = true;
        }

        /// <summary>
        /// При взятии отдельного элемента цепи.
        /// </summary>
        /// <param name="socketController">Контроллер соединений.</param>
        public void OnLocalGrabStart(SocketController socketController)
        {
            socketController.ConnectionGraphBehaviour.ForEach(a =>
            {
                var provider = (CollisionProvider) (a.CollisionProvider);
                provider.SubscribeNewController(ChainedJointController);
            });
        }

        /// <summary>
        /// При отпускании.
        /// </summary>
        public void OnGrabEnd()
        {
            if (_collisionController)
            {
                Destroy(CollisionController);
            }
        }

        /// <summary>
        /// Получение контроллера коллизий.
        /// </summary>
        /// <returns>Обработчик коллизий.</returns>
        public CollisionController GetCollisionController()
        {
            if (!_collisionController)
            {
                _collisionController = GetComponentInParent<CollisionController>();
            }

            if (!_collisionController)
            {
                _collisionController = gameObject.GetWrapper().GetGameObject().AddComponent<CollisionController>();
                _collisionController.InitializeController(gameObject.GetRootInputController());
            }

            return _collisionController;
        }

        /// <summary>
        /// Подписать на новый контроллер 1.0.
        /// </summary>
        /// <param name="chainedJointController">Контроллер цепи.</param>
        public void SubscribeNewController(ChainedJointController chainedJointController)
        {
            if (!_collisionController)
            {
                return;
            }

            if (_subscribedController)
            {
                _collisionController.UnsubscribeFromJointControllerEvents(chainedJointController);
            }

            _subscribedController = chainedJointController;

            if (chainedJointController)
            {
                _collisionController.SubscribeToJointControllerEvents(chainedJointController);
            }
        }
    }
}