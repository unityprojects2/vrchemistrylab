﻿using System.Linq;
using UnityEngine;

namespace Varwin.SocketLibrary
{
    /// <summary>
    /// Класс, описывающий работу вилки.
    /// </summary>
    public class PlugPoint : JointPoint
    {
        /// <summary>
        /// Ключ подключения.
        /// </summary>
        public string Key = "key";

        /// <summary>
        /// Физический joint.
        /// </summary>
        public Joint Joint { get; internal set; }

        /// <summary>
        /// Может ли точка быть разъединена.
        /// </summary>
        public bool CanDisconnect = true;

        /// <summary>
        /// Инициализация точки.
        /// </summary>
        private void Awake()
        {
            SocketController?.AddJointPoint(this);
            
            if (!InstancedPlugPoints.Contains(this))
            {
                InstancedPlugPoints.Add(this);
            }

            gameObject.layer = 2;
        }

        /// <summary>
        /// Удаление точки.
        /// </summary>
        private void OnDestroy()
        {
            if (InstancedPlugPoints.Contains(this))
            {
                InstancedPlugPoints.Remove(this);
            }

            if (Joint)
            {
                Destroy(Joint);
            }
        }

        /// <summary>
        /// При столкновении с коллайдером.
        /// </summary>
        /// <param name="other">Другой коллайдер.</param>
        private void OnTriggerEnter(Collider other)
        {
            OnTrigger(other);
        }

        /// <summary>
        /// При нахождении в столкновении.
        /// </summary>
        /// <param name="other">Другой коллайдер.</param>
        private void OnTriggerStay(Collider other)
        {
            OnTrigger(other);
        }

        /// <summary>
        /// При триггере.
        /// </summary>
        /// <param name="other">Другой коллайдер.</param>
        private void OnTrigger(Collider other)
        {
            var socket = other.GetComponent<SocketPoint>();

            if (!socket || !SocketController.CanConnecting(socket, this))
            {
                return;
            }
            
            if (!socket.SocketController.IsGrabbed && !SocketController.IsGrabbed)
            {
                return;
            }
            
            SocketController.OnPlugPointTriggerEnter(this, socket);
        }

        /// <summary>
        /// При выходе из коллизии.
        /// </summary>
        /// <param name="other">Другой коллайдер.</param>
        private void OnTriggerExit(Collider other)
        {
            var socket = other.GetComponent<SocketPoint>();

            if (!socket || !socket.AvailableKeys.Contains(Key) || !socket.IsFree || !IsFree)
            {
                return;
            }

            if (!socket.SocketController.IsGrabbed && !SocketController.IsGrabbed)
            {
                return;
            }

            SocketController.OnPlugPointTriggerExit(this, socket);
        }
    }
}