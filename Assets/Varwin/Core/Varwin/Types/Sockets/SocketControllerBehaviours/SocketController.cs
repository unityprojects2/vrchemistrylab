﻿using System.Collections.Generic;
using System.Linq;
using Varwin.SocketLibrary.Extension;

namespace Varwin.SocketLibrary
{
    /// <summary>
    /// Класс, реализующий логику контроллера соединений одного объекта.
    /// </summary>
    public partial class SocketController : BaseComponentsBehaviour
    {
        /// <summary>
        /// Граф соединений.
        /// </summary>
        private ConnectionGraphBehaviour _connectionGraphBehaviour;

        /// <summary>
        /// Граф соединений.
        /// </summary>
        public ConnectionGraphBehaviour ConnectionGraphBehaviour => GetConnectingGraphBehaviour();

        /// <summary>
        /// Описание события контроллера соединений.
        /// </summary>
        /// <param name="socketPoint">Вилка.</param>
        /// <param name="plugPoint">Розетка.</param>
        public delegate void SocketHandler(SocketPoint socketPoint, PlugPoint plugPoint);

        /// <summary>
        /// Событие, вызываемое при соединении объектов.
        /// </summary>
        public event SocketHandler OnConnect;

        /// <summary>
        /// Событие, вызываемое при разъединении объектов.
        /// </summary>
        public event SocketHandler OnDisconnect;

        /// <summary>
        /// Список точек - вилок.
        /// </summary>
        public List<PlugPoint> PlugPoints => GetPlugPoints();

        /// <summary>
        /// Список точек - розеток.
        /// </summary>
        public List<SocketPoint> SocketPoints => GetSocketPoints();

        /// <summary>
        /// Список точек для соединения.
        /// </summary>
        public List<JointPoint> JointPoints { get; private set; }

        /// <summary>
        /// Контроллер превью.
        /// </summary>
        public PreviewBehaviour PreviewBehaviour { get; private set; }

        /// <summary>
        /// Предварительная вилка для подключения.
        /// </summary>
        private PlugPoint _previewPlugPoint;

        /// <summary>
        /// Предварительная розетка для подключения.
        /// </summary>
        private SocketPoint _previewSocketPoint;

        /// <summary>
        /// Взят ли объект в руку с учетом цепи.
        /// </summary>
        public bool IsGrabbed => ConnectionGraphBehaviour.IsGrabbed();

        /// <summary>
        /// Взят ли объект в руку.
        /// </summary>
        public bool IsLocalGrabbed => InteractableObjectBehaviour && InteractableObjectBehaviour.IsGrabbed;

        /// <summary>
        /// Инициализация.
        /// </summary>
        private void Awake()
        {
            InitPreview();
            InitEvents();
        }

        /// <summary>
        /// Инициализация событий.
        /// </summary>
        private void InitEvents()
        {
            InteractableObjectBehaviour?.OnGrabStarted.AddListener(OnGrabStart);
            InteractableObjectBehaviour?.OnGrabEnded.AddListener(OnGrabEnd);
        }

        /// <summary>
        /// Метод, вызываемый при броске объекта.
        /// </summary>
        private void OnGrabEnd()
        {
            ConnectionGraphBehaviour.OnGrabEnd();
        }

        /// <summary>
        /// Подключить, если имеются обе точки.
        /// </summary>
        public void ConnectIfPossible()
        {
            if (!_previewPlugPoint || !_previewSocketPoint)
            {
                return;
            }

            Connect(_previewPlugPoint, _previewSocketPoint);
        }

        /// <summary>
        /// Метод, вызываемый при поднятии объекта.
        /// </summary>
        private void OnGrabStart()
        {
            DisconnectIfPossible();
            ConnectionGraphBehaviour.OnGrabStart();
            
            CollisionProvider.OnLocalGrabStart(this);
        }

        /// <summary>
        /// Отключить, если возможно.
        /// </summary>
        public void DisconnectIfPossible()
        {
            if (PlugPoints == null)
            {
                return;
            }

            foreach (var plugPoint in PlugPoints.Where(plugPoint => plugPoint.CanDisconnect))
            {
                Disconnect(plugPoint);
            }
        }

        /// <summary>
        /// Инициализация превью.
        /// </summary>
        private void InitPreview()
        {
            PreviewBehaviour = gameObject.GetPreviewObject().AddComponent<PreviewBehaviour>();
            PreviewBehaviour.Init(this);
        }

        /// <summary>
        /// При поднесении одного объекта к другому.
        /// </summary>
        /// <param name="plugPoint">Вилка.</param>
        /// <param name="socketPoint">Розетка.</param>
        public void OnPlugPointTriggerEnter(PlugPoint plugPoint, SocketPoint socketPoint)
        {
            if (!CanConnecting(socketPoint, plugPoint))
            {
                return;
            }

            _previewPlugPoint = plugPoint;
            
            plugPoint.CandidateJointPoint = socketPoint;
            socketPoint.CandidateJointPoint = plugPoint;
            
            _previewSocketPoint = socketPoint;

            socketPoint.SocketController._previewPlugPoint = plugPoint;
            socketPoint.SocketController._previewSocketPoint = socketPoint;

            ConnectionGraphBehaviour.ForEach(controller => controller.SetJoinHighlight());
            
            plugPoint.InvokeOnPreviewShow(plugPoint, socketPoint);
            socketPoint.InvokeOnPreviewShow(plugPoint, socketPoint);
        }

        /// <summary>
        /// При прекращении коллизии точек.
        /// </summary>
        /// <param name="plugPoint">Вилка.</param>
        /// <param name="socketPoint">Розетка.</param>
        public void OnPlugPointTriggerExit(PlugPoint plugPoint, SocketPoint socketPoint)
        {
            plugPoint.SocketController.ResetState();
            socketPoint.SocketController.ResetState();
            
            plugPoint.InvokeOnPreviewHide(plugPoint, socketPoint);
            socketPoint.InvokeOnPreviewHide(plugPoint, socketPoint);
        }

        /// <summary>
        /// Сброс состояний.
        /// </summary>
        public void ResetState()
        {
            _previewPlugPoint?.InvokeOnPreviewHide(_previewPlugPoint, _previewSocketPoint);
            _previewSocketPoint?.InvokeOnPreviewHide(_previewPlugPoint, _previewSocketPoint);
            
            _previewPlugPoint = null;
            _previewSocketPoint = null;
            PreviewBehaviour.Hide();
            ConnectionGraphBehaviour.ForEach(controller => controller.ResetHighlight());

            // Фикс дополнительных превью (между двух объектов).
            foreach (var jointPoint in JointPoints)
            {
                if (jointPoint.CandidateJointPoint && jointPoint.CandidateJointPoint.SocketController)
                {
                    var candidate = jointPoint.CandidateJointPoint;
                    jointPoint.CandidateJointPoint = null;
                    candidate.SocketController.ResetState();
                }
            }
        }

        /// <summary>
        /// Получение графа соединений.
        /// </summary>
        /// <returns>Граф соединений.</returns>
        private ConnectionGraphBehaviour GetConnectingGraphBehaviour()
        {
            if (!_connectionGraphBehaviour)
            {
                _connectionGraphBehaviour = ConnectionGraphBehaviour.InitTree(this);
            }

            return _connectionGraphBehaviour;
        }

        /// <summary>
        /// Добавление точки в контроллер.
        /// </summary>
        /// <param name="jointPoint">Точка для соединения.</param>
        public void AddJointPoint(JointPoint jointPoint)
        {
            if (!jointPoint)
            {
                return;
            }

            if (JointPoints == null)
            {
                JointPoints = new List<JointPoint>();
            }

            JointPoints.Add(jointPoint);
        }

        /// <summary>
        /// Удаление точки из списка.
        /// </summary>
        /// <param name="jointPoint">Точка для соединения.</param>
        public void RemoveJointPoint(JointPoint jointPoint)
        {
            JointPoints?.Remove(jointPoint);

            if (JointPoints?.Count == 0)
            {
                Destroy(gameObject);
            }
        }
        
        /// <summary>
        /// Получение списка розеток.
        /// </summary>
        /// <returns>Список розеток.</returns>
        private List<SocketPoint> GetSocketPoints()
        {
            return JointPoints?.FindAll(a => a is SocketPoint).ConvertAll(a => a as SocketPoint);
        }

        /// <summary>
        /// Получение списка вилок.
        /// </summary>
        /// <returns>Список вилок.</returns>
        private List<PlugPoint> GetPlugPoints()
        {
            return JointPoints?.FindAll(a => a is PlugPoint).ConvertAll(a => a as PlugPoint);
        }

        /// <summary>
        /// Рендеринг превью, если таковое возможно. Также разрешаем возможность отпустить, если хайлайтится.
        /// </summary>
        private void LateUpdate()
        {
            if (!IsGrabbed)
            {
                return;
            }

            if (_previewPlugPoint && _previewSocketPoint)
            {
                _collisionProviderBase?.CollisionController.ForcedUnblock();
                
                _previewPlugPoint.SocketController.SetJoinHighlight();
                _previewSocketPoint.SocketController.SetJoinHighlight();
                
                PreviewBehaviour.Show(_previewSocketPoint, _previewPlugPoint);
            }
        }
    }
}