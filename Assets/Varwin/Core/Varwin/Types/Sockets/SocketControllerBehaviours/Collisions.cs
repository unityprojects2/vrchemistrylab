﻿using UnityEngine;

namespace Varwin.SocketLibrary
{
    /// <summary>
    /// Часть контроллера, отвечающая за коллизии.
    /// </summary>
    public partial class SocketController
    {
        /// <summary>
        /// Тип провайдера коллизий.
        /// </summary>
        private CollisionProvider _collisionProviderBase;

        /// <summary>
        /// Тип провайдера коллизий.
        /// </summary>
        public CollisionProvider CollisionProvider => GetCollisionProvider();
        
        /// <summary>
        /// Получение провайдера обработчика коллизий.
        /// </summary>
        /// <returns>Провайдер обработчика коллизий.</returns>
        public CollisionProvider GetCollisionProvider()
        {
            if (_collisionProviderBase)
            {
                return _collisionProviderBase;
            }
            
            var providerObj = new GameObject("CollisionProvider");
            providerObj.transform.parent = transform;
            _collisionProviderBase = providerObj.AddComponent<CollisionProvider>();

            return _collisionProviderBase;
        }

        /// <summary>
        /// Удаление объекта провайдера.
        /// </summary>
        private void OnDestroy()
        {
            if (_collisionProviderBase)
            {
                Destroy(CollisionProvider.gameObject);
            }
        }
    }
}