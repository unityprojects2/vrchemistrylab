﻿using UnityEngine;
using Varwin.Public;

namespace Varwin.SocketLibrary
{
    /// <summary>
    /// Базовые компоненты.
    /// </summary>
    public abstract class BaseComponentsBehaviour : MonoBehaviour
    {
        /// <summary>
        /// Контейнер идентификатора.
        /// </summary>
        private ObjectId _objectId;

        /// <summary>
        /// Идентификатор.
        /// </summary>
        public int Id => gameObject.GetWrapper().GetInstanceId();

        /// <summary>
        /// Твердое тело.
        /// </summary>
        private Rigidbody _rigidbody;

        /// <summary>
        /// Твердое тело.
        /// </summary>
        public Rigidbody Rigidbody
        {
            get
            {
                if (!_rigidbody)
                {
                    _rigidbody = GetComponent<Rigidbody>();
                }

                return _rigidbody;
            }
        }

        /// <summary>
        /// Компонент взаимодействий.
        /// </summary>
        private InteractableObjectBehaviour _interactableObjectBehaviour;

        /// <summary>
        /// Компонент взаимодействий.
        /// </summary>
        public InteractableObjectBehaviour InteractableObjectBehaviour
        {
            get
            {
                if (!_interactableObjectBehaviour)
                {
                    _interactableObjectBehaviour = GetComponent<InteractableObjectBehaviour>();
                }

                return _interactableObjectBehaviour;
            }
        }

        /// <summary>
        /// Подсветка объекта.
        /// </summary>
        private VarwinHighlightEffect _varwinHighlightEffect;

        /// <summary>
        /// Подсветка объекта.
        /// </summary>
        public VarwinHighlightEffect VarwinHighlightEffect
        {
            get
            {
                if (!_varwinHighlightEffect)
                {
                    _varwinHighlightEffect = GetComponent<VarwinHighlightEffect>();
                }

                return _varwinHighlightEffect;
            }
        }

        /// <summary>
        /// Включить подсветку при подсоединении.
        /// </summary>
        public void SetJoinHighlight()
        {
            VarwinHighlightEffect.SetHighlightEnabled(true);
            VarwinHighlightEffect.SetConfiguration(DefaultHighlightConfigs.JointHighlight, null, false);
        }

        /// <summary>
        /// Выключить подсветку при подсоединении.
        /// </summary>
        public void ResetHighlight()
        {
            VarwinHighlightEffect.SetHighlightEnabled(false);
        }
    }
}