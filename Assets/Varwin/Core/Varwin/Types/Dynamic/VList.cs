#if !NET_STANDARD_2_0

using System;
using System.Collections;
using System.Collections.Generic;

namespace Varwin
{
    public static class VList
    {
        public static ListValue Value(string listName, string valueName)
        {
            return new ListValue(listName, valueName);
        }

        public static dynamic GetRandomItem(dynamic list, bool remove)
        {
            if (!IsList(list))
            {
                throw new Exception("Get list random item error! Variable is not a List<T>.");
            }

            if (list.Count == 0)
            {
                throw new Exception("Get list random item error! List is empty");
            }
            
            int x = Utils.RandomInt(0, list.Count - 1);
            dynamic result = list[x];

            if (remove)
            {
                list.RemoveAt(x);
            }

            return result;
        }

        public static dynamic GetItem(dynamic list, dynamic index, bool remove)
        {
            if (!IsList(list))
            {
                throw new Exception("Get list item error! Variable is not a List<T>.");
            }

            if (!int.TryParse(index.ToString(), out int i))
            {
                throw new Exception("Get list item error! Index must be integer.");
            }

            dynamic result = list[i]; 

            if (remove)
            {
                list.RemoveAt(i);
            }

            return result;
        }

        public static bool IsEmpty (dynamic list)
        {
            if (!IsList(list))
            {
                return true;
            }

            return list.Count <= 0;
        }

        public static int GetItemIndexOf(dynamic list, dynamic item)
        {
            if (!IsList(list))
            {
                throw new Exception("Get list item error! Variable is not a List<T>.");
            }

            int result = list.IndexOf(item) + 1; //Humans count elements from 1

            return result;
        }
        
        public static int GetItemLastIndexOf(dynamic list, dynamic item)
        {
            if (!IsList(list))
            {
                throw new Exception("Get list item error! Variable is not a List<T>.");
            }

            int result = list.LastIndexOf(item) + 1; //Humans count elements from 1

            return result;
        }

        public static void SetItem(dynamic list, dynamic index, dynamic value, bool insert)
        {
            if (!IsList(list))
            {
                throw new Exception("Set list item error! Variable is not a List<T>.");
            }
            
            if (!int.TryParse(index.ToString(), out int i))
            {
                throw new Exception("Set list item error! Index must be integer.");
            }

            if (!insert)
            {
                list[i] = value; 
            }
            else
            {
                list.Insert(i, value); 
            }
        }

        public static void SetRandomItem(dynamic list, dynamic value, bool insert)
        {
            if (!IsList(list))
            {
                throw new Exception("Set list item error! Variable is not a List<T>.");
            }
            
            int x = Utils.RandomInt(0, list.Count - 1);
            
            if (!insert)
            {
                list[x] = value;
            }
            else
            {
                list.Insert(x, value);
            }
        }

        public static dynamic GetSubList (dynamic list, string where1, int at1, string where2, int at2)
        {
            if (!IsList(list))
            {
                throw new Exception("Set list item error! Variable is not a List<T>.");
            }
            
            //copied from google blockly default
            var getIndex = new Func<dynamic, int, int>((where, at) =>
            {
                switch (@where)
                {
                    case "FROM_START":
                        at--;

                        break;
                    case "FROM_END":
                        at = list.Count - at;

                        break;
                    case "FIRST":
                        at = 0;

                        break;
                    case "LAST":
                        at = list.Count - 1;

                        break;
                    default:
                        throw new ApplicationException("Unhandled option (lists_getSublist).");
                }

                return at;
            });
            
            at1 = getIndex(where1, at1);
            at2 = getIndex(where2, at2);

            return list.GetRange(at1, at2 - at1 + 1);
        }

        public static dynamic GetFirstItem(dynamic list, bool remove)
        {
            if (!IsList(list))
            {
                throw new Exception("Get list first item error! Variable is not a List<T>.");
            }
            
            dynamic result = list.First();

            if (remove)
            {
                list.RemoveAt(0);
            }

            return result;
        }
        
        public static dynamic GetLastItem(dynamic list, bool remove)
        {
            if (!IsList(list))
            {
                throw new Exception("Get list first item error! Variable is not a List<T>.");
            }
            
            dynamic result = list.Last();

            if (remove)
            {
                list.RemoveAt(list.Count - 1);
            }

            return result;
        }

        private static bool IsList(object o)
        {
            if (o == null)
            {
                return false;
            }

            return o is IList && o.GetType().IsGenericType && o.GetType().GetGenericTypeDefinition().IsAssignableFrom(typeof(List<>));
        }
    }
}
#endif