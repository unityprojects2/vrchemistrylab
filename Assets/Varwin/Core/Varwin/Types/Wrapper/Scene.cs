﻿using System;
using NLog;
using Varwin.Data.ServerData;

// ReSharper disable once CheckNamespace
namespace Varwin 
{
    public static class Scene
    {
        private static string _lastSid = string.Empty;

        public static void Load(string sid)
        {
            if (Settings.Instance.Multiplayer)
            {
                throw new Exception("Switching between scenes in multiplayer is not possible now");
            }
            
            if (_lastSid == sid)
            {
                return;
            }
            
            var projectScene = ProjectData.ProjectStructure.GetProjectScene(sid);
            
            if (projectScene == null)
            {
                LogManager.GetCurrentClassLogger().Error("Project scene not found!");
                return;
            }
            
            _lastSid = sid;
            LoaderAdapter.LoadProject(ProjectData.ProjectId, projectScene.Id, ProjectData.ProjectConfigurationId);
            
            ProjectData.SceneCleared += ResetLastSid;
            ProjectData.SceneLoaded += RespawnPlayer;

            void ResetLastSid()
            {
                ProjectData.SceneCleared -= ResetLastSid;
                _lastSid = string.Empty;
            }
            
            void RespawnPlayer()
            {
                ProjectData.SceneLoaded -= RespawnPlayer;
                PlayerManager.Respawn();
            }
        }

        
    }
    
    public static class Configuration
    {
        private static string _lastSid = String.Empty;

        public static void Load(string sid)
        {
            if (Settings.Instance.Multiplayer)
            {
                throw new Exception("Switching between configurations in multiplayer is not possible now");
            }
            
            if (_lastSid == sid)
            {
                return;
            }
             
            var projectConfiguration = ProjectData.ProjectStructure.GetConfiguration(sid);
            
            if (projectConfiguration == null)
            {
                LogManager.GetCurrentClassLogger().Error("Project configuration not found!");
                return;
            }
            
            _lastSid = sid;
            LoaderAdapter.LoadProjectConfiguration(projectConfiguration.Id);
            ProjectData.SceneCleared += ResetLastSid;
            
            void ResetLastSid()
            {
                ProjectData.SceneCleared -= ResetLastSid;
                _lastSid = string.Empty;
            }
            
        }  

        
    }
}
