﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using Object = System.Object;

namespace Varwin
{
    public class WrapperCollection : DynamicCollection<Wrapper>
    {
        public WrapperCollection(List<Wrapper> collection): base(collection) { }
        public override bool TryInvokeMember(InvokeMemberBinder binder, object[] args, out object result)
        {
            var success = base.TryInvokeMember(binder, args, out result);

            if (success && binder.Name == "GetBehaviour")
            {
                result = new DynamicCollection<dynamic>(result as List<dynamic>);
            }

            return success;
        }
    }
}