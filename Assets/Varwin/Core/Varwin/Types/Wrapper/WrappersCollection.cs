﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using NLog;
using UnityEngine;
using Varwin.Core.Behaviours.ConstructorLib;

namespace Varwin
{
    public class WrappersCollection
    {
        private readonly Dictionary<int, Wrapper> _wrappers = new Dictionary<int, Wrapper>();
        
        private readonly Dictionary<string, Type> _types = new Dictionary<string, Type>();

        /// <summary>
        /// Add new wrapper to collection
        /// </summary>
        /// <param name="idInstance">Object id</param>
        /// <param name="wrapper">Object wrapper</param>
        public void Add(int idInstance, Wrapper wrapper)
        {
            _wrappers.Add(idInstance, wrapper);
        }

        /// <summary>
        /// Get all wrappers from collection
        /// </summary>
        /// <returns></returns>
        public List<Wrapper> Wrappers()
        {
            List<Wrapper> result = new List<Wrapper>();
            result.AddRange(_wrappers.Values);
            return result;
        }

        /// <summary>
        /// Get all wrappers of type from collection
        /// </summary>
        /// <param name="typeName">Wrapper type name</param>
        /// <returns>List of wrappers of type</returns>
        public List<Wrapper> GetWrappersOfType(string typeName)
        {
            Type type = GetType(typeName);
            
            var result = new List<Wrapper>();
            
            foreach (Wrapper wrapper in _wrappers.Values) 
            {
                if (wrapper.GetType() == type)
                {
                    result.Add(wrapper);
                }
            }
            
            return result;
        }

        /// <summary>
        /// Get wrapper by id
        /// </summary>
        /// <param name="id">Object id</param>
        /// <returns></returns>
        public Wrapper Get(int id)
        {
            if (_wrappers.ContainsKey(id))
            {
                return _wrappers[id];
            }

            throw new Exception($"Wrapper with {id} not found!");
        }
        
        /// <summary>
        /// Get collection of wrappers by type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public dynamic Get(string type)
        {
            var wrappers = GetWrappersOfType(type);
            WrapperCollection collection = new WrapperCollection(wrappers);
            
            return collection;
        }
        
        /// <summary>
        /// Determines whether the collection contains the specified object instance id.
        /// </summary>
        /// <param name="id">Object instance id</param>
        /// <returns></returns>
        public bool ContainsKey(int id) => _wrappers.ContainsKey(id);

        /// <summary>
        /// Determines whether the collection contains the wrapper.
        /// </summary>
        /// <param name="wrapper">Wrapper instance</param>
        /// <returns></returns>
        public bool ContainsValue(Wrapper wrapper) => _wrappers.ContainsValue(wrapper);

        /// <summary>
        /// Check existing object with id
        /// </summary>
        /// <param name="id">Object instance id</param>
        /// <returns></returns>
        public bool Exist(int id) => _wrappers.ContainsKey(id);

        /// <summary>
        /// Get wrappers of children object 
        /// </summary>
        /// <param name="target">Target wrapper</param>
        /// <returns></returns>
        public List<Wrapper> GetChildren(Wrapper target)
        {
            List<Wrapper> result = new List<Wrapper>();

            foreach (ObjectController child in target.GetObjectController().Children)
            {
                result.Add(child.Entity.wrapper.Value);
            }

            return result;
        }

        /// <summary>
        /// Get wrappers of descendants object
        /// </summary>
        /// <param name="target">Target wrapper</param>
        /// <returns></returns>
        public List<Wrapper> GetDescendants(Wrapper target)
        {
            List<Wrapper> result = new List<Wrapper>();

            foreach (ObjectController child in target.GetObjectController().Descendants)
            {
                result.Add(child.Entity.wrapper.Value);
            }

            return result;
        }

        /// <summary>
        /// Get wrapper of parent object
        /// </summary>
        /// <param name="target">Target wrapper</param>
        /// <returns></returns>
        public Wrapper GetParent(Wrapper target)
        {
            ObjectController objectController = target.GetObjectController();
            return objectController.Parent?.Entity.wrapper.Value;
        }

        /// <summary>
        /// Get wrapper of ancestry object
        /// </summary>
        /// <param name="target">Target wrapper</param>
        /// <returns></returns>
        public List<Wrapper> GetAncestry(Wrapper target)
        {
            List<Wrapper> result = new List<Wrapper>();

            ObjectController objectController = target.GetObjectController();
            ObjectController parent = objectController.Parent;

            while (parent != null)
            {
                result.Add(parent.Entity.wrapper.Value);
                parent = parent.Parent;
            }

            return result;
        }

        /// <summary>
        /// Clear all wrappers from collection
        /// </summary>
        public void Clear()
        {
            _wrappers.Clear();
        }

        /// <summary>
        /// Remove wrapper by id
        /// </summary>
        /// <param name="id"></param>
        public void Remove(int id)
        {
            if (_wrappers.ContainsKey(id))
            {
                _wrappers.Remove(id);
            }
            else
            {
                LogManager.GetCurrentClassLogger().Info($"Object {id} have no wrapper!");
            }
        }

        private Type GetType(string typeName)
        {
            Type type = null;
            if (_types.ContainsKey(typeName))
            {
                type = _types[typeName];
            }
            else
            {
                var wrapper = _wrappers.Values.FirstOrDefault(w => w.GetType().ToString() == typeName);
                if (wrapper != null)
                {
                    type = wrapper.GetType();
                    _types.Add(typeName, type);
                }
            }

            return type;
        }
    }

}


