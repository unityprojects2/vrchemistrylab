﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Dynamic;
using System.Reflection;
using System.Text.RegularExpressions;
using NLog;
using UnityEngine;
using Varwin.Core;
using Varwin.Public;
using Varwin.Core.Behaviours;

namespace Varwin
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    public abstract class Wrapper : DynamicObject
    {
        protected GameEntity Entity { get; set; }
        protected GameObject GameObject { get; set; }
        protected GameEntity ObjectEntity { get; set; }
        
        protected ObjectController ObjectController { get; set; }
        
        private bool _isEnabled = true;

        private bool _grabEnabled = true;
        public bool GrabEnabled
        {
            get => _grabEnabled;
            set => SwitchGrabEnabled(value);
        }

        private void SwitchGrabEnabled(bool value)
        {
            if (value == _grabEnabled)
            {
                return;
            }
            
            _grabEnabled = value;

            if (_grabEnabled)
            {
                EnableGrab();
            }
            else
            {
                DisableGrab();
            }
        }
        
        private bool _useEnabled = true;
        public bool UseEnabled
        {
            get => _useEnabled;
            set => SwitchUseEnabled(value);
        }

        private void SwitchUseEnabled(bool value)
        {
            if (value == _useEnabled)
            {
                return;
            }
            
            _useEnabled = value;

            if (_useEnabled)
            {
                EnableUse();
            }
            else
            {
                DisableUse();
            }
        }
        
        private bool _touchEnabled = true;
        public bool TouchEnabled
        {
            get => _touchEnabled;
            set => SwitchTouchEnabled(value);
        }

        private void SwitchTouchEnabled(bool value)
        {
            if (value == _touchEnabled)
            {
                return;
            }
            
            _touchEnabled = value;

            if (_touchEnabled)
            {
                EnableTouch();
            }
            else
            {
                DisableTouch();
            }
        }

        
        protected Wrapper(GameEntity entity)
        {
            Entity = entity;
        }
        
        protected Wrapper(GameObject gameObject)
        {
            GameObject = gameObject;
        }
        
        public void InitEntity(GameEntity entity)
        {
            ObjectEntity = entity;
        }

        public void InitObjectController(ObjectController controller)
        {
            ObjectController = controller;
        }

        protected Dictionary<Type, VarwinBehaviour> Behaviours { get; set; } = new Dictionary<Type, VarwinBehaviour>();

        public void AddBehaviour(Type behaviourType, VarwinBehaviour behaviour)
        {
            if (!Behaviours.ContainsKey(behaviourType))
            {
                Behaviours.Add(behaviourType, behaviour);
            }
        }

        public T GetBehaviour<T>() where T : VarwinBehaviour
        {
            return (T) GetBehaviour(typeof(T));
        }

        public VarwinBehaviour GetBehaviour(Type behaviourType)
        {
            if (Behaviours.ContainsKey(behaviourType))
            {
                return Behaviours[behaviourType];
            }

            return null;
        }

        public bool IsActive()
        {
            return Activity;
        }

        public bool IsInactive()
        {
            return !Activity;
        }

        public void Activate()
        {
            Activity = true;
        }

        public void Deactivate()
        {
            Activity = false;
        }
        
        public bool Activity
        {
            get => ObjectController.ActiveInHierarchy;
            set => this.SetActivity(value);
        }
        
        public bool Enabled
        {
            get => _isEnabled;
            set => SetEnabled(value);
        }

        private void SetEnabled(bool value)
        {
            if (value == _isEnabled)
            {
                return;
            }
            
            _isEnabled = value;

            if (_isEnabled)
            {
                if (!ObjectEntity.hasInputControls)
                {
                    return;
                }

                var controls = ObjectEntity.inputControls.Values.Values;

                foreach (InputController control in controls)
                {
                    control.EnableViewInput();
                }
            }
            else
            {
                if (!ObjectEntity.hasInputControls)
                {
                    return;
                }

                var controls = ObjectEntity.inputControls.Values.Values;

                foreach (InputController control in controls)
                {
                    control.DisableViewInput();
                }
            }
        }

        public void Enable()
        {
            Enabled = true;
        }

        public void Disable()
        {
            Enabled = false;
        }

        public bool IsEnabled() => _isEnabled;

        public bool IsDisabled() => !_isEnabled;

        public string GetName() => ObjectEntity.name.Value;
        
        public GameObject GetGameObject()
        {
            if (Entity != null)
            {
                return Entity.gameObject.Value;
            }

            if (GameObject != null)
            {
                return GameObject;
            }

            return null;
        }

        public ObjectController GetObjectController() => ObjectController;

        public InputController GetInputController(GameObject go)
        {
            if (!ObjectEntity.hasInputControls)
            {
                return null;
            }
            
            ObjectId objectId = go.GetComponent<ObjectId>();

            if (objectId == null)
            {
                return null;
            }

            int id = objectId.Id;
            
            var controls = ObjectEntity.inputControls.Values;

            return !controls.ContainsKey(id) ? null : controls[id];
        }

        #region INPUT LOGIC
        public void EnableUse()
        {
            if (!ObjectEntity.hasInputControls)
            {
                return;
            }

            if (!ProjectData.IsPlayMode)
            {
                return;
            }

            var controls = ObjectEntity.inputControls.Values.Values;
            foreach (InputController control in controls)
            {
                control.EnableViewUsing(); 
            }
        }
        
        public void DisableUse()
        {
            if (!ObjectEntity.hasInputControls)
            {
                return;
            }

            if (!ProjectData.IsPlayMode)
            {
                return;
            }
            
            var controls = ObjectEntity.inputControls.Values.Values;
            foreach (InputController control in controls)
            {
                control.DisableViewUsing();
            }
        }
        
        public void EnableTouch()
        {
            if (!ObjectEntity.hasInputControls)
            {
                return;
            }
            
            if (!ProjectData.IsPlayMode)
            {
                return;
            }
            
            var controls = ObjectEntity.inputControls.Values.Values;
            foreach (InputController control in controls)
            {
                control.EnableViewTouch(); 
            }
        }
        
        public void DisableTouch()
        {
            if (!ObjectEntity.hasInputControls)
            {
                return;
            }

            if (!ProjectData.IsPlayMode)
            {
                return;
            }
            
            var controls = ObjectEntity.inputControls.Values.Values;
            foreach (InputController control in controls)
            {
                control.DisableViewTouch(); 
            }
        }
        
        /// <summary>
        /// Enable grab for all grabbable in object
        /// </summary>
        public void EnableGrab()
        {
            if (!ObjectEntity.hasInputControls)
            {
                return;
            }

            if (!ProjectData.IsPlayMode)
            {
                return;
            }
            
            var controls = ObjectEntity.inputControls.Values.Values;
            foreach (InputController control in controls)
            {
                control.EnableViewGrab();
            }
        }

        /// <summary>
        /// Disable grab for all grabbable in objects
        /// </summary>
        public void DisableGrab()
        {
            if (!ObjectEntity.hasInputControls)
            {
                return;
            }

            if (!ProjectData.IsPlayMode)
            {
                return;
            }
            
            var controls = ObjectEntity.inputControls.Values.Values;
            foreach (InputController control in controls)
            {
                control.DisableViewGrab();
            }
        }


        public int GetInstanceId() => ObjectEntity.id.Value;

        public void EnableGrabForObject(GameObject go)
        {
            InputController control = GetControlOnGameObject(go);

            control?.EnableViewGrab();
        }

        public void DisableGrabForObject(GameObject go)
        {
            InputController control = GetControlOnGameObject(go);

            control?.DisableViewGrab();
        }

        public void VibrateWithObject(GameObject go, GameObject controllerObject, float strength, float duration, float interval)
        {
            InputController control = GetControlOnGameObject(go);

            control?.Vibrate(controllerObject, strength, duration, interval);
        }

        private InputController GetControlOnGameObject(GameObject go)
        {
            if (!ObjectEntity.hasInputControls)
            {
                return null;
            }

            var controls = ObjectEntity.inputControls.Values.Values;
            foreach (InputController control in controls)
            {
                if (control.IsConnectedToGameObject(go))
                {
                    return control;
                }
            }

            return null;
        }
        
        #endregion

        #region Dynamic Methods

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            result = binder.Name;
            return true;
        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            Type thisType = GetType();
            FieldInfo fieldInfo = thisType.GetField(binder.Name);

            if (fieldInfo != null)
            {
                if (value.GetType() != fieldInfo.FieldType)
                {
                    Converter.CastValue(fieldInfo, value, this);
                }
            }
            else
            {
                PropertyInfo propertyInfo = thisType.GetProperty(binder.Name);

                if (propertyInfo == null)
                {
                    throw new Exception($"Missing property {binder.Name} in {GetType().FullName}");
                }
                
                if (value.GetType() != propertyInfo?.PropertyType)
                {
                    Converter.CastValue(propertyInfo, value, this);
                }
            }

            return true;
        }

        public override bool TryInvokeMember(InvokeMemberBinder binder, object[] args, out object result)
        {
            Type thisType = GetType();
            MethodInfo methodInfo = thisType.GetMethod(binder.Name);

            if (methodInfo == null)
            {
                result = false;
                //Debug.Log("Method " + binder.Name + " not found!");
                return true;
            }

            var parametres = methodInfo.GetParameters();

            if (args.Length == parametres.Length)
            {
                bool error = false;
                for (int p = 0; p < args.Length; p++)
                {
                    var callArg = args[p];
                    var parametrInfo = parametres[p];

                    if (callArg.GetType() != parametrInfo.ParameterType)
                    {
                        if (!Converter.CastValue(parametrInfo.ParameterType, callArg, out callArg))
                        {
                            error = true;
                            break;
                        }

                        args[p] = callArg;
                    }

                }

                if (!error)
                {
                    methodInfo.Invoke(this, args);
                }

                else
                {
                    //Debug.Log("Can not invoke method " + binder.Name);
                }
            }

            else
            {
                //Debug.Log("Diferent arguments count on method " + binder.Name);
            }

            result = true;
            return true;
        }

        public bool HasProperty(string name)
        {
            Type thisType = GetType();
            var property = thisType.GetProperty(name);
            return property != null;
        }

        public bool HasField(string name)
        {
            Type thisType = GetType();
            var field = thisType.GetField(name);
            return field != null;
        }
        
        public bool HasMethod(string name)
        {
            Type thisType = GetType();
            var method = thisType.GetMethod(name);
            return method != null;
        }

        #endregion

        public void CallMethod(string methodName, params object[] parameters)
        {
            Type thisType = GetType();

            if (!HasMethod(methodName))
            {
                LogManager.GetCurrentClassLogger().Info($"{thisType.Name} has no method {methodName}");

                return;
            }

            MethodInfo method = thisType.GetMethod(methodName);

            if (method == null)
            {
                return;
            }

            try
            {
                method.Invoke(this, parameters);
            }
            catch (Exception e)
            {
                LogManager.GetCurrentClassLogger().Error($"Can not invoke method \"{methodName}\" in {thisType.Name}: {e.Message}");
            }
        }

        public object GetValueFromValueList(ListValue listValue)
        {
            FieldInfo field = GetType().GetField(GetValidValueListName(listValue.ListName));

            if (field == null)
            {
                throw new Exception("dictionary not found for the valueList " + listValue.ListName);
            }

#if !NET_STANDARD_2_0
            try
            {
                object value = field.GetValue(this);
                dynamic dictionary = value;
                object result = dictionary[listValue.ValueName];
                
                return result;
            }
            catch (Exception e)
            {
                LogManager.GetCurrentClassLogger().Error($"Can not get value \"{listValue.ValueName}\" from valueList {listValue.ListName}: {e.Message}");
            }
#endif
            return null; 
        }

        public static string GetValidValueListName(string valueListName)
        {
            Regex rgx = new Regex("[^a-zA-Z0-9_]");
            string validValueListName = "ValueList_" + rgx.Replace(valueListName, "_");

            return validValueListName;
        }
    }
    
    public static class WrapperEx
    {
        [Obsolete("This will be removed in the future.", false)]
        public static void DisableInputUsing(this IWrapperAware self, GameObject go) => self.Wrapper().GetInputController(go)?.DisableViewUsing();
        
        [Obsolete("This will be removed in the future.", false)]
        public static void EnableInputUsing(this IWrapperAware self, GameObject go) => self.Wrapper().GetInputController(go)?.EnableViewUsing();
        
        [Obsolete("This will be removed in the future.", false)]
        public static void DisableInputGrab(this IWrapperAware self, GameObject go) => self.Wrapper().GetInputController(go)?.DisableViewGrab();
        
        [Obsolete("This will be removed in the future.", false)]
        public static void EnableInputGrab(this IWrapperAware self, GameObject go) => self.Wrapper().GetInputController(go)?.EnableViewGrab();
        
        [Obsolete("This will be removed in the future.", false)]
        public static void DisableTouch(this IWrapperAware self, GameObject go) => self.Wrapper().GetInputController(go)?.DisableViewTouch();
        
        [Obsolete("This will be removed in the future.", false)]
        public static void EnableTouch(this IWrapperAware self, GameObject go) => self.Wrapper().GetInputController(go)?.EnableViewTouch();
    }

}
