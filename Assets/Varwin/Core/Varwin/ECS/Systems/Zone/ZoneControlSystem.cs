﻿using System;
using System.Collections.Generic;
using Entitas;
using UnityEngine;

namespace Varwin.ECS.Systems
{
    public sealed class ZoneControlSystem : IExecuteSystem, ICleanupSystem
    {
        private readonly IGroup<GameEntity> _zoneEntities;
        private readonly IGroup<GameEntity> _allEntites;
        private List<Wrapper> _overlappingObjects;

        private int entityCount;
        
        private GameObject[] _entityGameObjects;
        private Wrapper[] _entityWrappers;
        private Collider[] _entityColliders;
        private bool[] _entityColliderRigidbodiesKinematicOrAwake;

        private Dictionary<GameObject, Vector3> _zonePositions;
        private Dictionary<GameObject, Quaternion> _zoneRotations;
        private Dictionary<GameObject, Bounds> _zoneBounds;
        private Dictionary<Collider, Rigidbody> _collidersRigidbodies = new Dictionary<Collider, Rigidbody>();
        
        private bool _wasInEditMode = true;
        
        public ZoneControlSystem(Contexts contexts)
        {
            _zoneEntities = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.Zone, GameMatcher.GameObject,
                GameMatcher.ColliderAware));
            _allEntites = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.Wrapper, GameMatcher.Collider));
        }

        public void Execute()
        {
            if (ProjectData.GameMode != GameMode.View && ProjectData.GameMode != GameMode.Preview)
            {
                _wasInEditMode = true;
                return;
            }

            bool isAdded = false;
            bool isRemoved = false;

            if (_wasInEditMode || _allEntites.count != entityCount)
            {
                entityCount = _allEntites.count;
                UpdateCaches();

                _wasInEditMode = false;
            }
            else
            {
                UpdateRigidbodyAwakeStatus(false);
            }
            
            foreach (var zoneEntity in _zoneEntities.GetEntities())
            {
                var zoneCollider = zoneEntity.gameObject.Value.GetComponentInChildren<Collider>();

                if (!zoneCollider)
                {
                    continue;
                }

                GameObject zoneGameObject = zoneEntity.gameObject.Value;

                bool zoneMoved = zoneGameObject.transform.position != _zonePositions[zoneGameObject]
                                 || zoneGameObject.transform.rotation != _zoneRotations[zoneGameObject]
                                 || zoneCollider.bounds != _zoneBounds[zoneGameObject];
                
                _zonePositions[zoneGameObject] = zoneGameObject.transform.position;
                _zoneRotations[zoneGameObject] = zoneGameObject.transform.rotation;
                _zoneBounds[zoneGameObject] = zoneCollider.bounds;
                
                var wrappersList = zoneEntity.zone.WrappersList;

                _overlappingObjects = new List<Wrapper>(wrappersList);
                
                for (int i = 0; i < _entityGameObjects.Length; i++)
                {
                    if (_entityGameObjects[i] == zoneGameObject)
                    {
                        continue;
                    }

                    if (_entityWrappers[i] == null || !_entityColliders[i])
                    {
                        continue;
                    }

                    if (!zoneMoved && !_entityColliderRigidbodiesKinematicOrAwake[i] && !wrappersList.Contains(_entityWrappers[i]))
                    {
                        continue;
                    }

                    if (!zoneCollider.bounds.Intersects(_entityColliders[i].bounds))
                    {
                        if (wrappersList.Contains(_entityWrappers[i]))
                        {
                            isRemoved = true;
                                
                            _overlappingObjects.Remove(_entityWrappers[i]);
                        }
                            
                        continue;
                    }

                    var zoneTransform = zoneCollider.transform;
                    var otherTransform = _entityColliders[i].transform;
                        
                    if (Physics.ComputePenetration(zoneCollider, zoneTransform.position, zoneTransform.rotation,
                        _entityColliders[i], otherTransform.position, otherTransform.rotation,
                        out Vector3 _, out float distance))
                    {
                        if (!wrappersList.Contains(_entityWrappers[i]))
                        {
                            isAdded = true;
                                
                            _overlappingObjects.Add(_entityWrappers[i]);
                        }
                    }
                    else if (wrappersList.Contains(_entityWrappers[i]))
                    {
                        isRemoved = true;

                        _overlappingObjects.Remove(_entityWrappers[i]);
                    }
                }
                
                zoneEntity.ReplaceZone(_overlappingObjects);

                if (isAdded)
                {
                    try
                    {
                        zoneEntity.colliderAware.Value?.OnObjectEnter(_overlappingObjects.ToArray());
                    }
                    catch (Exception e)
                    {
                        Debug.LogError($"On object enter has error in object {zoneEntity.gameObject.Value}\n{e}", zoneEntity.gameObject.Value);
                    }
                }

                if (isRemoved)
                {
                    try
                    {
                        zoneEntity.colliderAware.Value?.OnObjectExit(_overlappingObjects.ToArray());
                    }
                    catch (Exception e)
                    {
                        Debug.LogError($"On object enter has error in object {zoneEntity.gameObject.Value}\n{e}", zoneEntity.gameObject.Value);
                    }
                }
            }
        }

        private void UpdateCaches()
        {
            var entities = _allEntites.GetEntities();
            
            _zonePositions = new Dictionary<GameObject, Vector3>();
            _zoneRotations = new Dictionary<GameObject, Quaternion>();
            _zoneBounds = new Dictionary<GameObject, Bounds>();
            _collidersRigidbodies.Clear();
            
            foreach (var zoneEntity in _zoneEntities.GetEntities())
            {
                var zoneCollider = zoneEntity.gameObject.Value.GetComponentInChildren<Collider>();

                if (!zoneCollider)
                {
                    continue;
                }

                GameObject zoneGameObject = zoneEntity.gameObject.Value;
                
                if (!_zonePositions.ContainsKey(zoneGameObject))
                {
                    _zonePositions.Add(zoneGameObject, zoneGameObject.transform.position);
                    _zoneRotations.Add(zoneGameObject, zoneGameObject.transform.rotation);
                    _zoneBounds.Add(zoneGameObject, zoneCollider.bounds);
                }
            }
       
            _entityGameObjects = new GameObject[entities.Length];
            _entityWrappers = new Wrapper[entities.Length];
            _entityColliders = new Collider[entities.Length];
            _entityColliderRigidbodiesKinematicOrAwake = new bool[entities.Length];
            
            for (int i = 0; i < entities.Length; i++)
            {
                _entityGameObjects[i] = entities[i].gameObject.Value;
                _entityWrappers[i] = entities[i].wrapper.Value;
                _entityColliders[i] = _entityWrappers[i] != null ? entities[i].collider.Value : null;
            }
            
            UpdateRigidbodyAwakeStatus(true);
        }

        private void UpdateRigidbodyAwakeStatus(bool wakeUp)
        {
            for (int i = 0; i < _entityColliders.Length; i++)
            {
                Collider collider = _entityColliders[i];

                if (!collider)
                {
                    continue;
                }
                
                if (!_collidersRigidbodies.TryGetValue(collider, out Rigidbody rigidbody))
                {
                    rigidbody = collider ? collider.attachedRigidbody : null;
                
                    if (!rigidbody)
                    {
                        rigidbody = collider.GetComponentInParent<Rigidbody>();
                    }
                    
                    if (rigidbody)
                    {
                        _collidersRigidbodies.Add(collider, rigidbody);
                    }
                }

                if (!rigidbody)
                {
                    _entityColliderRigidbodiesKinematicOrAwake[i] = false;
                    continue;
                }

                if (wakeUp)
                {
                    rigidbody.WakeUp();
                }
                
                _entityColliderRigidbodiesKinematicOrAwake[i] = rigidbody.isKinematic || !rigidbody.IsSleeping();
            }
        }

        public void Cleanup()
        {
        }
    }
}