﻿using Entitas;
using Photon.Pun;
using Varwin.PUN;

namespace Varwin.ECS.Systems
{
    public sealed class LogicExecuteSystem : IExecuteSystem
    {
        private readonly IGroup<GameEntity> _entities;
        public LogicExecuteSystem(Contexts contexts)
        {
            _entities = contexts.game.GetGroup(GameMatcher.Logic);
        }

        public void Execute()
        {
            if (!ProjectData.ObjectsAreLoaded)
            {
                return;
            }
            
            if (ProjectData.GameMode == GameMode.Edit)
            {
                return;
            }
            
            if (Settings.Instance.Multiplayer && !PhotonNetwork.IsMasterClient)
            {
                return;
            }

            foreach (GameEntity entity in _entities.GetEntities())
            {
                entity.logic.Value.ExecuteLogic();
            }
        }
    }
}
