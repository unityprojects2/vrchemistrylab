﻿using Entitas;

namespace Varwin.ECS.Systems
{
    public sealed class DestroyAssetSystem : IExecuteSystem
    {
        private readonly IGroup<GameEntity> _prefabEntities;
        
        public DestroyAssetSystem(Contexts contexts)
        {
            _prefabEntities = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.ServerObject));
        }
        public void Execute()
        {
            foreach (GameEntity entity in _prefabEntities.GetEntities())
            {
                entity.Destroy();
            }
        }

    }
}