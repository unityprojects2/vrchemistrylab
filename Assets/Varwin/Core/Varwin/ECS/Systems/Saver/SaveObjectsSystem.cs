﻿using System;
using System.Collections.Generic;
using System.Linq;
using Entitas;
using NLog;
using Varwin.Data;
using Varwin.Data.ServerData;
using Varwin.WWW;
using ObjectData = Varwin.Data.ServerData.ObjectData;

namespace Varwin.ECS.Systems.Saver
{
    public sealed class SaveObjectsSystem : IExecuteSystem
    {
        private readonly IGroup<GameEntity> _allEntities;

        public SaveObjectsSystem(Contexts contexts)
        {
            _allEntities = contexts.game.GetGroup(GameMatcher.AllOf(
                GameMatcher.RootGameObject,
                GameMatcher.IdServer, GameMatcher.Name, GameMatcher.Id, GameMatcher.IdObject));
        }

        public void Execute()
        {
            var dictionary = new Dictionary<int, SceneObjectDto>();
            var result = new List<SceneObjectDto>();
            var uniqueObjects = new HashSet<int>();
            var objectBehavioursData = new List<ObjectBehavioursData>();
            var hierarchyTreeViewStates = new Dictionary<int, bool>();
            ProjectData.Joints = new Dictionary<int, JointData>();
            Data.ServerData.Scene projectScene = ProjectData.ProjectStructure.Scenes.GetProjectScene(ProjectData.SceneId);
            
            var onDemandedResourceGuids = new HashSet<string>();
            
            foreach (GameEntity entity in _allEntities)
            {
                ObjectController objectController = GameStateData.GetObjectControllerInSceneById(entity.id.Value);

                if (objectController.IsSceneTemplateObject)
                {
                    continue;
                }

                var transforms = objectController.GetTransforms();
                var joints = objectController.GetJointData();
                var usingResourcesData = objectController.GetUsingResourcesData();

                var objectOnDemandedResourceGuids = objectController.GetOnDemandedResourceGuids();
                foreach (string objectOnDemandedResourceGuid in objectOnDemandedResourceGuids)
                {
                    if (!onDemandedResourceGuids.Contains(objectOnDemandedResourceGuid))
                    {
                        onDemandedResourceGuids.Add(objectOnDemandedResourceGuid);
                    }
                }
                
                var objectBehaviours = objectController.GetObjectBehaviours();
                var propertiesData = objectController.GetInspectorPropertiesData();
                
                var newTreeSceneObjectDto = new SceneObjectDto
                {
                    Id = projectScene.HasObjectOnServer(entity.idServer.Value) ? entity.idServer.Value : 0,
                    Name = entity.name.Value,
                    InstanceId = entity.id.Value,
                    ObjectId = entity.idObject.Value,
                    Data = new ObjectData
                    {
                        Transform = transforms,
                        JointData = joints,
                        InspectorPropertiesData = propertiesData,
                        LockChildren = objectController.LockChildren,
                        IsDisabled = !objectController.ActiveSelf,
                        IsDisabledInHierarchy = !objectController.ActiveInHierarchy,
                        DisableSelectabilityInEditor = !objectController.SelectableInEditor,
                        Index = objectController.Index
                    },
                    DisableSceneLogic = !objectController.EnableInLogicEditor,
                    Resources = usingResourcesData,
                    SceneObjects = new List<SceneObjectDto>(),
                };
                
                if (joints != null)
                {
                    ProjectData.Joints.Add(entity.id.Value, joints);
                }

                if (entity.hasIdParent)
                {
                    newTreeSceneObjectDto.ParentId = entity.idParent.Value;
                }
                else
                {
                    newTreeSceneObjectDto.ParentId = null;
                }

                dictionary.Add(entity.id.Value, newTreeSceneObjectDto);
                hierarchyTreeViewStates[entity.id.Value] = objectController.HierarchyExpandedState;

                if (uniqueObjects.Contains(entity.idObject.Value))
                {
                    continue;
                }

                uniqueObjects.Add(entity.idObject.Value);
                objectBehavioursData.Add(
                    new ObjectBehavioursData
                    {
                        ObjectId = entity.idObject.Value,
                        Behaviours = objectBehaviours
                    });
            }

            foreach (SceneObjectDto treeObject in dictionary.Values.OrderBy(x => x.Data.Index))
            {
                if (treeObject.ParentId != null)
                {
                    dictionary[treeObject.ParentId.Value].SceneObjects.Add(treeObject);
                }
                else
                {
                    result.Add(treeObject);
                }
            }

            var objectsData = new SceneTemplateObjectsDto
            {
                SceneId = ProjectData.SceneId,
                SceneObjects = result,
                SceneData = new CustomSceneData
                {
                    CameraSpawnPosition = CameraManager.DesktopEditorCamera.transform.position,
                    CameraSpawnRotation = CameraManager.DesktopEditorCamera.transform.rotation,
                    HierarchyExpandStates = hierarchyTreeViewStates,
                    OnDemandedResourceGuids = onDemandedResourceGuids
                },
                ObjectBehaviours = objectBehavioursData
            };
            ProjectData.UpdateSceneData(objectsData.SceneData);

            const string apiRoute = ApiRoutes.SaveSceneObjectsRequest;
            var requestApi = new RequestApi(apiRoute, RequestType.Post, objectsData.ToJson());
            requestApi.OnFinish += response =>
            {
                var responseApi = (ResponseApi)response;
                LogManager.GetCurrentClassLogger().Info(RequestApi.GetResponseInfoMessage(requestApi, responseApi));

                if (!Helper.IsResponseGood(responseApi))
                {
                    Helper.ProcessBadResponse(requestApi, responseApi);
                    return;
                }

                SceneTemplateObjectsDto data;
                
                try
                {
                    string json = responseApi.Data.ToString();                    
                    data = json.JsonDeserialize<SceneTemplateObjectsDto>();
                    LogManager.GetCurrentClassLogger().Info($"SceneObjects on scene {ProjectData.SceneId} was saved!");
                }
                catch (Exception e)
                {
                    LogManager.GetCurrentClassLogger().Fatal(
                        RequestApi.GetResponseErrorMessage(requestApi, responseApi)
                        + Environment.NewLine
                        + $"Exception = {e}");
                    return;
                }
                
                projectScene.UpdateProjectSceneObjects(data.SceneObjects);
                ProjectData.OnSave?.Invoke();
            };
            requestApi.OnError += response =>
            {
                LogManager.GetCurrentClassLogger().Error($"Can't save scene: {response}");
            };
        }
    }
}
