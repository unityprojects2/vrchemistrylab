﻿using Entitas;
using Photon.Pun;

namespace Varwin.ECS.Components.UnityBehaviour
{
    public sealed class PhotonViewComponent : IComponent
    {
        public PhotonView Value;
    }
}
