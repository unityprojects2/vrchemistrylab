﻿namespace Varwin.Public
{
    public interface ITouchStartAware : IVarwinInputAware
    {
        void OnTouchStart();
    }

    public interface ITouchEndAware : IVarwinInputAware
    {
        void OnTouchEnd();
    }
}
