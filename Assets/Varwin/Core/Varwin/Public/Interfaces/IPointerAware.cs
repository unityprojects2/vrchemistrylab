﻿namespace Varwin.Public
{
    public interface IPointerClickAware : IVarwinInputAware
    {
        void OnPointerClick();
    }

    public interface IPointerInAware : IVarwinInputAware
    {
        void OnPointerIn();
    }

    public interface IPointerOutAware : IVarwinInputAware
    {
        void OnPointerOut();
    }
    
    public interface IPointerDownAware : IVarwinInputAware
    {
        void OnPointerDown();
    }
    
    public interface IPointerUpAware : IVarwinInputAware
    {
        void OnPointerUp();
    }
}
