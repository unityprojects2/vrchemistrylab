﻿namespace Varwin.Public
{
    public interface IUseStartAware : IVarwinInputAware
    {
        void OnUseStart(UsingContext context);
    }

    public interface IUseEndAware : IVarwinInputAware
    {
        void OnUseEnd();
    }
}
