﻿using System;
using UnityEngine;

namespace Varwin.Public
{
    public enum HighlightQuality
    {
        Fastest,
        High,
        Highest
    }

    public interface IHighlightAware
    {
        HightLightConfig HighlightConfig();
    }

    [Serializable]
    public class HightLightConfig
    {
        //Outline
        public bool Outline;
        public float OutlineWidth;
        public Color OutlineColor;
        public bool OutlineAlwaysOnTop;
        public HighlightQuality OutlineQuality = HighlightQuality.Fastest;

        //Glow
        public bool Glow;
        public float GlowWidth;
        public float GlowOffset;
        public float GlowAlpha;
        public Color GlowColor;

        //See through
        public bool SeeThrough;
        public float SeeThroughIntensity;
        public float SeeThroughAlpha;
        public Color SeeThroughColor;

        //Overlay
        public bool Overlay;
        public float OverlayAlpha;
        public Color OverlayColor;
        public float OverlayAnimationSpeed;

        public HightLightConfig(
            bool outline,
            float outlineWidth,
            Color? outlineColor,
            bool outlineAlwaysOnTop,
            HighlightQuality outlineQuality,
            bool glow,
            float glowWidth,
            float glowOffset,
            float glowAlpha,
            Color? glowColor,
            bool seeThrough = false,
            float seeThroughIntensity = 0.8f,
            float seeThroughAlpha = 0.5f,
            Color? seeThroughColor = null,
            bool overlay = false,
            float overlayAlpha = 0.0f,
            float overlayAnimationSpeed = 0.0f,
            Color? overlayColor = null)
        {
            Outline = outline;
            OutlineColor = outlineColor ?? Color.clear;
            OutlineWidth = outlineWidth;
            OutlineAlwaysOnTop = outlineAlwaysOnTop;
            OutlineQuality = outlineQuality;

            Glow = glow;
            GlowWidth = glowWidth;
            GlowOffset = glowOffset;
            GlowAlpha = glowAlpha;
            GlowColor = glowColor ?? Color.clear;

            SeeThrough = seeThrough;
            SeeThroughIntensity = seeThroughIntensity;
            SeeThroughAlpha = seeThroughAlpha;
            SeeThroughColor = seeThroughColor ?? Color.clear;

            Overlay = overlay;
            OverlayAlpha = overlayAlpha;
            OverlayColor = overlayColor ?? Color.red;
            OverlayAnimationSpeed = overlayAnimationSpeed;
        }
    }

    public static class DefaultHighlightConfigs
    {
        public static readonly HightLightConfig EditorSelected = new HightLightConfig(
            true,
            0.2f,
            Color.cyan,
            true,
            HighlightQuality.High,
            false,
            0.2f,
            0.1f,
            0.3f,
            Color.cyan,
            false,
            0f,
            0f,
            Color.red,
            false,
            0f,
            1.0f,
            Color.red);

        public static readonly HightLightConfig EditorHovered = new HightLightConfig(
            true,
            0.2f,
            Color.yellow,
            true,
            HighlightQuality.High,
            false,
            0.2f,
            0.1f,
            0.3f,
            Color.yellow,
            false,
            0f,
            0f,
            Color.red,
            false,
            0f,
            1.0f,
            Color.red);

        public static readonly HightLightConfig CollisionHighlight = new HightLightConfig(
            true,
            0.3f,
            Color.red,
            false,
            HighlightQuality.Fastest,
            false,
            0.2f,
            0.1f,
            0.3f,
            Color.red,
            false,
            0.8f,
            0.5f,
            Color.red,
            true,
            0.5f,
            1.0f,
            Color.red);

        public static readonly HightLightConfig JointHighlight = new HightLightConfig(
            true,
            0.3f,
            Color.green,
            false,
            HighlightQuality.Fastest,
            false,
            0.2f,
            0.1f,
            0.3f,
            Color.green,
            false,
            0.8f,
            0.5f,
            Color.green,
            true,
            0.5f,
            1.0f,
            Color.green);

        public static readonly HightLightConfig TouchHighlight = new HightLightConfig(
            true,
            0.3f,
            Color.cyan,
            false,
            HighlightQuality.Fastest,
            false,
            0.2f,
            0.1f,
            0.3f,
            Color.cyan,
            false,
            0f,
            0f,
            Color.red,
            false,
            0f,
            1.0f,
            Color.red);

        public static readonly HightLightConfig UseHighlight = new HightLightConfig(
            true,
            0.3f,
            Color.yellow,
            false,
            HighlightQuality.Fastest,
            false,
            0.2f,
            0.1f,
            0.3f,
            Color.yellow,
            false,
            0f,
            0f,
            Color.red,
            false,
            0f,
            1.0f,
            Color.red);
    }
}