﻿using UnityEngine;

namespace Varwin.Public
{
    public interface IGrabStartAware : IVarwinInputAware
    {
        void OnGrabStart(GrabingContext context);
    }

    public interface IGrabEndAware : IVarwinInputAware
    {
        void OnGrabEnd();
    }

    public interface IGrabPointAware
    {
        Transform GetLeftGrabPoint();
        Transform GetRightGrabPoint();
    }
}
