﻿using UnityEngine;
using HighlightPlus;
using QualityLevel = HighlightPlus.QualityLevel;

namespace Varwin.Public
{
    public class VarwinHighlightEffect : MonoBehaviour
    {
        public HightLightConfig Configuration;

        public bool IsSelected;

        private HighlightEffect _effectHolder;

        public HighlightEffect Effect
        {
            get
            {
                if (!_effectHolder && gameObject)
                {
                    _effectHolder = gameObject.GetComponent<HighlightEffect>();

                    if (!_effectHolder)
                    {
                        _effectHolder = gameObject.AddComponent<HighlightEffect>();
                    }
                }

                return _effectHolder;
            }
        }

        public bool IsHighlightEnabled => Effect.highlighted;

        private void Start()
        {
            SetupWithConfig(Configuration);
        }

        private void OnDisable()
        {
            SetHighlightEnabled(false);
        }

        private void OnDestroy()
        {
            if (!_effectHolder)
            {
                Destroy(_effectHolder);
            }
        }

        private void SetupWithConfig(HightLightConfig config, HighlightEffect newEffect = null, bool needsRefresh = true)
        {
            if (config == null)
            {
                return;
            }

            if (newEffect)
            {
                _effectHolder = newEffect;
            }

            if (config.Outline)
            {
                Effect.outline = 1;
                Effect.outlineWidth = config.OutlineWidth;
                Effect.outlineColor = config.OutlineColor;
                Effect.outlineAlwaysOnTop = config.OutlineAlwaysOnTop;

                switch (config.OutlineQuality)
                {
                    case HighlightQuality.Fastest:
                        Effect.outlineQuality = QualityLevel.Fastest;
                        break;
                    case HighlightQuality.High:
                        Effect.outlineQuality = QualityLevel.High;
                        break;
                    case HighlightQuality.Highest:
                        Effect.outlineQuality = QualityLevel.Highest;
                        break;
                }
            }
            else
            {
                Effect.outline = 0;
            }

            if (config.Glow)
            {
                Effect.glow = 1;
                Effect.glowWidth = config.GlowWidth;

                var glowPass = new GlowPassData
                {
                    alpha = config.GlowAlpha,
                    offset = config.GlowOffset,
                    color = config.GlowColor
                };

                Effect.glowHQColor = config.GlowColor;

                Effect.glowPasses[0] = glowPass;

                Effect.glowDithering = false;
                Effect.glowAnimationSpeed = 0;
            }
            else
            {
                Effect.glow = 0;
            }

            if (config.SeeThrough)
            {
                Effect.seeThrough = SeeThroughMode.WhenHighlighted;
                Effect.seeThroughIntensity = config.SeeThroughIntensity;
                Effect.seeThroughTintAlpha = config.SeeThroughAlpha;
                Effect.seeThroughTintColor = config.SeeThroughColor;
            }
            else
            {
                Effect.seeThrough = SeeThroughMode.Never;
            }

            if (config.Overlay)
            {
                Effect.overlayColor = config.OverlayColor;
                Effect.overlayMinIntensity = 0.5f;
                Effect.overlay = config.OverlayAlpha;
                Effect.overlayAnimationSpeed = config.OverlayAnimationSpeed;
            }
            else
            {
                Effect.overlayMinIntensity = 0f;
                Effect.overlay = 0.0f;
            }

            Effect.glowQuality = QualityLevel.Fastest;
            Effect.outlineDownsampling = 1;
            Effect.ignoreObjectVisibility = true;

            if (needsRefresh)
            {
                Effect.Refresh();
            }
        }

        public void SetConfiguration(HightLightConfig config, HighlightEffect newEffect = null, bool needsRefresh = true)
        {
            Configuration = config;
            SetupWithConfig(config, newEffect, needsRefresh);
        }

        public void SetHighlightEnabled(bool enabled)
        {
            if (Effect)
            {
                Effect.highlighted = enabled;
            }
        }
    }
}