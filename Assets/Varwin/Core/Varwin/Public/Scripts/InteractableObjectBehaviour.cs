﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Varwin.Public
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(VarwinObjectDescriptor))]
    public class InteractableObjectBehaviour : MonoBehaviour, IGrabStartAware, IGrabEndAware, IUseStartAware,
        IUseEndAware, ITouchStartAware, ITouchEndAware
    {
        [Header("Interaction settings")]
        [SerializeField]
        private bool _isGrabbable;

        [SerializeField]
        private bool _isUsable;

        [SerializeField]
        private bool _isTouchable;

        private Wrapper _wrapper;
        private Wrapper Wrapper => _wrapper ?? (_wrapper = gameObject.GetWrapper());

        public bool IsInteractable => IsTouchable || IsUsable || IsGrabbable;
        
        public bool IsGrabbable
        {
            get => _isGrabbable;
            set
            {
                _isGrabbable = value;
                Wrapper.GrabEnabled = value;
            }
        }

        public bool IsUsable
        {
            get => _isUsable;
            set
            {
                _isUsable = value;
                Wrapper.UseEnabled = value;
            }
        }

        public bool IsTouchable
        {
            get => _isTouchable;
            set
            {
                _isTouchable = value;
                Wrapper.TouchEnabled = value;
            }
        }

        [NonSerialized]
        public bool IsGrabbed = false;

        [NonSerialized]
        public bool IsUsed = false;

        [NonSerialized]
        public bool IsTouched = false;


        [Space(5)]
        [Header("Events")]
        public UnityEvent OnGrabStarted = new UnityEvent();
        public UnityEvent OnGrabEnded = new UnityEvent();
        public UnityEvent OnUseStarted = new UnityEvent();
        public UnityEvent OnUseEnded = new UnityEvent();
        public UnityEvent OnTouchStarted = new UnityEvent();
        public UnityEvent OnTouchEnded = new UnityEvent();

        public void OnGrabStart(GrabingContext context)
        {
            if (!_isGrabbable)
            {
                return;
            }
            
            IsGrabbed = true;
            OnGrabStarted?.Invoke();
        }

        public void OnGrabEnd()
        {
            if (!_isGrabbable)
            {
                return;
            }

            IsGrabbed = false;
            OnGrabEnded?.Invoke();
        }

        public void OnUseStart(UsingContext context)
        {
            if (!_isUsable)
            {
                return;
            }

            IsUsed = true;
            OnUseStarted?.Invoke();
        }

        public void OnUseEnd()
        {
            if (!_isUsable)
            {
                return;
            }

            IsUsed = false;
            OnUseEnded?.Invoke();
        }

        public void OnTouchStart()
        {
            if (!_isTouchable)
            {
                return;
            }

            IsTouched = true;
            OnTouchStarted?.Invoke();
        }

        public void OnTouchEnd()
        {
            if (!_isTouchable)
            {
                return;
            }

            IsTouched = false;
            OnTouchEnded?.Invoke();
        }

        public void SetIsGrabbable(bool state)
        {
            _isGrabbable = state;
        }
        
        public void SetIsUsable(bool state)
        {
            _isUsable= state;
        }
        
        public void SetIsTouchable(bool state)
        {
            _isTouchable = state;
        }
    }
}
