using UnityEngine;

namespace Varwin.Public
{
    [System.Serializable]
    public class LocalizationString
    {
        public SystemLanguage key;
        
        public string value;

        public LocalizationString() { }

        public LocalizationString(SystemLanguage key, string value)
        {
            this.key = key;
            this.value = value;
        }
    }
}