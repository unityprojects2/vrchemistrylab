﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace Varwin.Public
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    public class VarwinObject : MonoBehaviour, IWrapperAware
    {
        private Wrapper _wrapper;
        public Wrapper Wrapper() => _wrapper ?? (_wrapper = gameObject.GetWrapper());

        
        [Observe]
        public bool IsObjectEnabled
        {
            set => Wrapper().Enabled = value;
            get => Wrapper().Enabled;
        }
        
        [Observe]
        public bool IsObjectActive
        {
            set => Wrapper().Activity = value;
            get => Wrapper().Activity;
        }

        [Observe]
        public bool GrabEnabled
        {
            get => Wrapper().GrabEnabled;
            set => Wrapper().GrabEnabled = value;
        }
        
        [Observe]
        public bool UseEnabled
        {
            get => Wrapper().UseEnabled;
            set => Wrapper().UseEnabled = value;
        }
        
        [Observe]
        public bool TouchEnabled
        {
            get => Wrapper().TouchEnabled;
            set => Wrapper().TouchEnabled = value;
        }

        public void EnableObject()
        {
            IsObjectEnabled = true;
        }

        public void DisableObject()
        {
            IsObjectEnabled = false;
        }

        public void EnableGrab()
        {
            GrabEnabled = true;
        }

        public void DisableGrab()
        {
            GrabEnabled = false;
        }

        public void EnableUse()
        {
            UseEnabled = true;
        }

        public void DisableUse()
        {
            UseEnabled = false;
        }

        public void EnableTouch()
        {
            TouchEnabled = true;
        }

        public void DisableTouch()
        {
            TouchEnabled = false;
        }

        public void VibrateWithObject(
            GameObject go,
            GameObject controllerObject,
            float strength,
            float duration,
            float interval)
        {
            Wrapper().VibrateWithObject(go, controllerObject, strength, duration, interval);
        }
    }
}