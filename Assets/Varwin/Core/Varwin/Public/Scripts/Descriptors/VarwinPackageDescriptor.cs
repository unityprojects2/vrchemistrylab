﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Varwin.Public
{
    [CreateAssetMenu(menuName = "Varwin Package", order = 92)]
    public class VarwinPackageDescriptor : ScriptableObject
    {
        public LocalizationDictionary Name;
        
        public LocalizationDictionary Description;

        public Texture2D Icon;
        public Texture2D View;
        public Texture2D Thumbnail;
        
        public string Guid;
        public string RootGuid;
        
        public string AuthorName;
        public string AuthorEmail;
        public string AuthorUrl;

        public string LicenseCode;
        public string LicenseVersion;
        
        public string BuiltAt;

        public GameObject[] Objects;
        
        public bool CurrentVersionWasBuilt;
    }
}
