﻿using System;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace Varwin.Public
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Varwin/Varwin Object Descriptor")]
    public class VarwinObjectDescriptor : MonoBehaviour, IWrapperAware
    {
        [TextArea] public string ConfigBlockly;

        [TextArea] public string ConfigAssetBundle;

        public Texture2D Icon;
        
        public string Name;
        
        [SerializeField]
        public LocalizationDictionary DisplayNames = new LocalizationDictionary();

        [SerializeField]
        public LocalizationDictionary Description = new LocalizationDictionary();

        public AssetBundlePart[] AssetBundleParts;
        
        public string Prefab;
        public string PrefabGuid;

        public string Guid;
        public string RootGuid;

        public bool Locked;
        public bool Embedded;
        
        public bool SourcesIncluded;
        public bool MobileReady;
        
        public bool DisableSceneLogic;
        public bool AddBehavioursAtRuntime;

        public string AuthorName;
        public string AuthorEmail;
        public string AuthorUrl;
        
        public string LicenseCode;
        public string LicenseVersion;
        
        public string BuiltAt;

        public bool CurrentVersionWasBuilt;
        public bool CurrentVersionWasBuiltAsMobileReady;

        public ComponentReferenceCollection Components;

        public string Namespace => $"{Name}_{RootGuid.Replace("-", "")}";
        public string WrapperAssemblyQualifiedName => $"Varwin.Types.{Namespace}.{Name}Wrapper, {Namespace}, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null";

        public bool IsVarwinObject => !string.IsNullOrEmpty(RootGuid) || !string.IsNullOrEmpty(Guid);

        public bool IsFirstVersion => string.Equals(Guid, RootGuid);

        private Wrapper _wrapper;
        private bool _tryGetOldWrapper;
        
        private void Reset()
        {
            Validate();
        }

        private void OnValidate()
        {
            Validate();
        }
        
        public void Validate()
        {
            if (string.IsNullOrWhiteSpace(Guid))
            {
                return;
            }
            
            if (string.IsNullOrWhiteSpace(RootGuid))
            {
                if (string.IsNullOrWhiteSpace(Guid))
                {
                    Debug.LogError($"Object {gameObject.name} has empty guid. New guid was generated.", this);
                    Guid = System.Guid.NewGuid().ToString();
                }
                
                RootGuid = Guid;
            }

            if (string.IsNullOrWhiteSpace(AuthorName))
            {
                AuthorName = "Anonymous";
            }

            if (string.IsNullOrWhiteSpace(LicenseCode))
            {
                LicenseCode = "cc-by";
            }

            if (string.IsNullOrWhiteSpace(LicenseVersion))
            {
                LicenseVersion = "4.0";
            }
        }

        public void PreBuild()
        {
            CurrentVersionWasBuilt = true;

            CurrentVersionWasBuiltAsMobileReady = MobileReady;
            
            BuiltAt = DateTimeOffset.Now.ToString();
            
            Validate();
        }

        public void RegenerateGuid()
        {
            Guid = System.Guid.NewGuid().ToString(); 
            RootGuid = Guid;
        }

        public void CleanVarwinObjectInfo()
        {
            Prefab = null;
            PrefabGuid = null;
            Icon = null;
        }

        public void CleanBuiltInfo()
        {
            BuiltAt = null;
            CurrentVersionWasBuilt = false;
            CurrentVersionWasBuiltAsMobileReady = false;
        }

        public Wrapper Wrapper()
        {
            if (_wrapper != null)
            {
                return _wrapper;
            }

            if (string.IsNullOrEmpty(RootGuid))
            {
                RootGuid = Guid;
            }

            // Backward Compatibility
            if (!_tryGetOldWrapper)
            {
                _wrapper = GetWrapperInOldIWrapperAware();
                if (_wrapper != null)
                {
                    return _wrapper;
                }
            }

            Type wrapperType = null;
            if (!string.IsNullOrEmpty(WrapperAssemblyQualifiedName))
            {
                wrapperType = Type.GetType(WrapperAssemblyQualifiedName);
            }

            if (wrapperType == null)
            {
                var varwinObjectInheritors = GetComponentsInChildren<VarwinObject>().Where(x => x.GetType().IsSubclassOf(typeof(VarwinObject)));
                
                var targetVarwinObject = varwinObjectInheritors.FirstOrDefault(x => x.GetType().FullName.IndexOf(Name) >= 0);
                if (targetVarwinObject)
                {
                    var targetAssembly = Assembly.GetAssembly(targetVarwinObject.GetType());
                    wrapperType = targetAssembly.GetTypes().FirstOrDefault(x => x.IsSubclassOf(typeof(Wrapper)));
                }

                if (wrapperType == null)
                {
                    foreach (var varwinObjectInheritor in varwinObjectInheritors)
                    {
                        var targetAssembly = Assembly.GetAssembly(varwinObjectInheritor.GetType());
                        wrapperType = targetAssembly.GetTypes().FirstOrDefault(x => x.IsSubclassOf(typeof(Wrapper)));
                        if (wrapperType != null)
                        {
                            break;
                        }
                    }
                }
            }
            
            if (wrapperType == null)
            {
                return new NullWrapper(gameObject);
            }

            _wrapper = (Wrapper) Activator.CreateInstance(wrapperType, new object[] {gameObject});

            return _wrapper;
        }

        private Wrapper GetWrapperInOldIWrapperAware()
        {
            if (_tryGetOldWrapper)
            {
                return null;
            }

            _tryGetOldWrapper = true;
            
            var allWrapperAwares = GetComponents<IWrapperAware>();

            var onlyGenericWrapperAwares = allWrapperAwares
                .Where(x => !x.GetType().IsAssignableFrom(typeof(VarwinObject)))
                .Where(x => !x.GetType().IsAssignableFrom(typeof(VarwinObjectDescriptor))).ToArray();

            if (onlyGenericWrapperAwares.Length != 1)
            {
                return null;
            }
            
            IWrapperAware oldWrapperAware = onlyGenericWrapperAwares[0];
            Type oldWrapperAwareType = oldWrapperAware.GetType();

            if (!oldWrapperAwareType.FullName.EndsWith("Type"))
            {
                return null;
            }
            
            string oldWrapperAwareName = oldWrapperAwareType.FullName.SubstringBeforeLast("Type");

            if (!oldWrapperAwareName.Equals($"Varwin.Types.{Namespace}.{Name}"))
            {
                return null;
            }
            
            Assembly oldWrapperAwareAssembly = oldWrapperAwareType.Assembly;
            Type oldWrapperAwareWrapperType = oldWrapperAwareAssembly.GetType(oldWrapperAwareName + "Wrapper");
                
            if (oldWrapperAwareWrapperType != null)
            {
                return oldWrapperAware.Wrapper();
            }

            return null;
        }
    }
}