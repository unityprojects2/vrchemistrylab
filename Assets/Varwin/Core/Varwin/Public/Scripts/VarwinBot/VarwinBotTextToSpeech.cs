﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Varwin.TextToSpeech;

namespace Varwin.Public
{
    [RequireComponent(typeof(VarwinBot))] 
    public class VarwinBotTextToSpeech : MonoBehaviour
    {

        public enum SpeechGender
        {
            [Item(English: "Male", Russian: "Мужской")]
            Male,

            [Item(English: "Female", Russian: "Женский")]
            Female
        }

        public enum SpeechLanguage
        {
            [Item(English: "English", Russian: "Английский")]
            English,

            [Item(English: "Russian", Russian: "Русский")]
            Russian
        }
        
        public AudioSource VoiceAudioSource;
        public SkinnedMeshRenderer LipSyncMesh;
        public int [] Visemes = Enumerable.Repeat(-1, VisemeCount).ToArray();

        private const int VisemeCount = 15;
        
        private float[] blendShapeInitialValues;
        
        private OVRLipSyncContext _lipSyncContext;
        private OVRLipSyncContextMorphTarget _lipSyncContextMorphTarget;
        
        private SpeechLanguage _language;
        private SpeechGender _gender;
        private string _specificVoiceName;

        private Animator _animator;
        
        [VarwinInspector(English: "male voice", Russian: "мужской голос")]
        public bool MaleGender
        {
            set => _gender = value ? SpeechGender.Male : SpeechGender.Female;
            get => _gender == SpeechGender.Male;
        }

        [VarwinInspector(English: "english language", Russian: "английский голос")]
        public bool EnglishLanguage
        {
            set => _language = value ? SpeechLanguage.English : SpeechLanguage.Russian;
            get => _language == SpeechLanguage.English;
        }
        
        [VarwinInspector(English: "specified voice name", Russian: "точное название голоса")]
        public string VoiceName
        {
            set => _specificVoiceName = value;
            get => _specificVoiceName;
        }
        
        [VarwinInspector(English: "use google cloud", Russian: "использовать google cloud")]
        public bool GoogleCloud
        {
            set => VarwinSpeechManager.SetOfflineMode(!value);
            get => VarwinSpeechManager.IsOfflineMode;
        }

        [VarwinInspector(English: "google cloud API key")]
        public string GoogleCloudApiKey
        {
            get => VarwinSpeechManager.GoogleCloudApiKey;
            set => VarwinSpeechManager.GoogleCloudApiKey = value;
        }
        
        [Action(English: "turn Google Cloud speech on", Russian: "включить Google Cloud")]
        [ArgsFormat(English:"with API Key {%}", Russian: "с ключом API {%}")]
        public void TurnOnGoogleCloud(string ApiKey)
        {
            VarwinSpeechManager.GoogleCloudApiKey = ApiKey;
            VarwinSpeechManager.SetOfflineMode(false);
        }
        
        [Action(English: "turn Google Cloud speech off", Russian: "выключить Google Cloud")]
        public void TurnOffGoogleCloud()
        {
            VarwinSpeechManager.GoogleCloudApiKey = "";
            VarwinSpeechManager.SetOfflineMode(true);
        }

        [Checker(English: "is google cloud on", Russian: "google cloud включен")]
        public bool IsOnline()
        {
            return !VarwinSpeechManager.IsOfflineMode;
        }
        
        [Action(English:"use", Russian: "использовать")]
        [ArgsFormat(English: "{%} {%} voice", Russian: "{%} {%} голос")]
        public void SetVoiceWith(SpeechGender gender, SpeechLanguage language)
        {
            _gender = gender;
            _language = language;
        }
        
        [Action(English: "use voice with specific name", Russian: "использовать голос с заданным именем")]
        public void SetVoice(string voiceName)
        {
            _specificVoiceName = voiceName;
        }
        
        public event Action SpeechCompleted;

        public void SayText(string text)
        {
            if (string.IsNullOrEmpty(_specificVoiceName))
            {
                VarwinSpeechManager.SayText(text, VoiceAudioSource, () =>
                    {
                        SpeechCompleted?.Invoke();
                    }, 
                    _gender == SpeechGender.Male,
                    GetCultureFromLanguage(_language));
            }
            else
            {
                VarwinSpeechManager.SayText(text, VoiceAudioSource, _specificVoiceName,
                    () =>
                    {
                        SpeechCompleted?.Invoke();
                    });
            }
        }

        public void StopSpeaking()
        {
            VarwinSpeechManager.StopSpeaking();
            
            SetSpeechState(false);
            SetSpeechState(true);
        }
        
        private void Start()
        {
            _animator = GetComponent<Animator>();
            
            if (!VoiceAudioSource)
            {
                VoiceAudioSource = GetComponentInChildren<AudioSource>();
            }

            if (!VoiceAudioSource)
            {
                Transform head = _animator.GetBoneTransform(HumanBodyBones.Head);

                if (!head)
                {
                    head = transform;
                }
                    
                VoiceAudioSource = head.gameObject.AddComponent<AudioSource>();

                VoiceAudioSource.spatialBlend = 1;
                VoiceAudioSource.minDistance = 1;
                VoiceAudioSource.maxDistance = 30;
            }
            
            if (LipSyncMesh)
            {
                int shapeCount = LipSyncMesh.sharedMesh.blendShapeCount;
                
                blendShapeInitialValues = new float[shapeCount];
                
                for (int i = 0; i < shapeCount; i++)
                {
                    blendShapeInitialValues[i] = LipSyncMesh.GetBlendShapeWeight(i);
                }

                GameObject lipSyncContextGameObject = VoiceAudioSource.gameObject;
                
                _lipSyncContext = lipSyncContextGameObject.AddComponent<OVRLipSyncContext>();

                _lipSyncContext.audioLoopback = true;
                _lipSyncContext.loopbackKey = KeyCode.None;
                _lipSyncContext.debugVisemesKey = KeyCode.None;
                _lipSyncContext.debugLaughterKey = KeyCode.None;

                _lipSyncContextMorphTarget = VoiceAudioSource.gameObject.AddComponent<OVRLipSyncContextMorphTarget>();

                _lipSyncContextMorphTarget.skinnedMeshRenderer = LipSyncMesh;
                _lipSyncContextMorphTarget.visemeToBlendTargets = Visemes;
                _lipSyncContextMorphTarget.laughterBlendTarget = -1;
            }
        }

        public void SetSpeechState(bool state)
        {
            if (VoiceAudioSource)
            {
                VoiceAudioSource.enabled = state;
            }
            
            if (!LipSyncMesh)
            {
                return;
            }
            
            _lipSyncContext.enabled = state;
            _lipSyncContextMorphTarget.enabled = state;

            if (!state)
            {
                VoiceAudioSource.clip = null;
                RestoreBlendShapeValues();
            }
        }

        private void RestoreBlendShapeValues()
        {
            if (!LipSyncMesh)
            {
                return;
            }
            
            int shapeCount = LipSyncMesh.sharedMesh.blendShapeCount;
                
            for (int i = 0; i < shapeCount; i++)
            {
                LipSyncMesh.SetBlendShapeWeight(i, blendShapeInitialValues[i]);
            }
        }
        
        private string GetCultureFromLanguage(SpeechLanguage language)
        {
            switch (language)
            {
                case SpeechLanguage.English: return "en_US";
                case SpeechLanguage.Russian: return "ru_RU";
            }

            return "en";
        }
    }
}