﻿using System.Collections;
using Photon.Pun;
using Varwin.PUN;

namespace Varwin.Core.Behaviours
{
    public abstract class VarwinBehaviour : MonoBehaviourPunCallbacks, IPunObservable
    {
        protected bool IsMultiplayerAndMasterClient => ProjectData.IsMultiplayerAndMasterClient;

        protected RpcTarget _rpcTarget = RpcTarget.Others;

        private void Awake()
        {
            StartCoroutine(SetupMultiplayer());
            AwakeOverride();
        }

        protected abstract void AwakeOverride();
        
        private IEnumerator SetupMultiplayer()
        {
            if (!Settings.Instance.Multiplayer)
            {
                yield break;
            }

            while (!photonView)
            {
                yield return null;
            }
            
            photonView.ObservedComponents.Add(this);

            MasterLoading.SpectatorJoined += ResendRpcCalls;
        }

        protected virtual void ResendRpcCalls()
        {
            if (!IsMultiplayerAndMasterClient)
            {
                return;
            }
        }

        public virtual void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
        }
    }
}
