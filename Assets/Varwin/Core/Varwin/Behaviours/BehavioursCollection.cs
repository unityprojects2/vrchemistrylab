﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Varwin.Core.Behaviours.ConstructorLib;

namespace Varwin.Core.Behaviours
{
    public static class BehavioursCollection
    {
        private static HashSet<VarwinBehaviourContainer> _varwinBehaviourContainers;

        private static HashSet<VarwinBehaviourContainer> BehaviourContainers
        {
            get
            {
                if (_varwinBehaviourContainers == null)
                {
                    _varwinBehaviourContainers = new HashSet<VarwinBehaviourContainer>()
                    {
                        new VarwinBehaviourContainer(typeof(MaterialChangeBehaviour), new MaterialChangeBehaviourHelper()),
                        new VarwinBehaviourContainer(typeof(MovableBehaviour)),
                        new VarwinBehaviourContainer(typeof(ScalableBehaviour)),
                        new VarwinBehaviourContainer(typeof(InteractableBehaviour), new InteractableBehaviourHelper()),
                        new VarwinBehaviourContainer(typeof(LightBehaviour)),
                    };
                }

                return _varwinBehaviourContainers;
            }
        }

        public static List<string> GetBehaviours(ObjectController objectController)
        {
            var behaviours = new List<string>();

            foreach (var behaviourContainer in BehaviourContainers)
            {
                var behaviour = objectController.RootGameObject.GetComponentInChildren(behaviourContainer.BehaviourType) as VarwinBehaviour;
                if (behaviour == null)
                {
                    continue;
                }
                
                var wrapper = objectController.WrappersCollection.Get(objectController.Id);
                wrapper.AddBehaviour(behaviourContainer.BehaviourType, behaviour);
                behaviours.Add(behaviourContainer.BehaviourType.FullName);
            }

            return behaviours;
        }

        public static List<string> AddBehaviours(ObjectController objectController)
        {
            var behaviours = new List<string>();
            if (CanAddBehaviours(objectController))
            {
                AddBehaviours(objectController.RootGameObject, objectController.WrappersCollection.Get(objectController.Id), behaviours);
            }

            return behaviours;
        }

        private static void AddBehaviours(GameObject gameObject, Wrapper wrapper, List<string> behaviours)
        {
            foreach (var behaviourContainer in BehaviourContainers)
            {
                if (behaviourContainer.CanAddBehaviour(gameObject))
                {
                    var behaviour = (VarwinBehaviour) gameObject.AddComponent(behaviourContainer.BehaviourType);
                    wrapper.AddBehaviour(behaviourContainer.BehaviourType, behaviour);
                    behaviours.Add(behaviourContainer.BehaviourType.FullName);
                }
            }
        }

        private static bool CanAddBehaviours(ObjectController objectController)
        {
            return !(objectController.IsEmbedded
                     || objectController.IsSceneTemplateObject
                     || !objectController.VarwinObjectDescriptor
                     || !objectController.VarwinObjectDescriptor.AddBehavioursAtRuntime
                     || objectController.VarwinObjectDescriptor.Components.ComponentReferences.Any(x => x.Type.ToString().Contains("Varwin.ConstructorLib.v1")));
        }
    }
}

