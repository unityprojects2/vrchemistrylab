﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Varwin.Public;

namespace Varwin.Core.Behaviours.ConstructorLib
{
    [VarwinComponent(English: "Movement", Russian: "Передвижение")]
    public class MovableBehaviour : VarwinBehaviour
    {
        public enum AxisDirection
        {
            [Item(English: "+X")]
            PosX = 0,
            [Item(English: "+Y")]
            PosY,
            [Item(English: "+Z")]
            PosZ,
            [Item(English: "-X")]
            NegX,
            [Item(English: "-Y")]
            NegY,
            [Item(English: "-Z")]
            NegZ
        }

        public enum MovementType
        {
            [Item(English: "Once", Russian: "Один раз")]
            Once = 0,
            [Item(English: "Repeatedly", Russian: "Повторяясь")]
            Repeat,
            [Item(English: "Back and forth", Russian: "Туда-сюда")]
            PingPong
        }
        
        public delegate void MovementCompleteHandler(bool finished);
        public delegate void TargetReachedHandler(Wrapper target);
        public delegate void PathPointReachedHandler(int id, Wrapper point);
        public delegate void CollisionHandler(Wrapper target);
        
        private Rigidbody _body;
        private Rigidbody Body
        {
            get
            {
                if (!_body)
                {
                    _body = GetComponent<Rigidbody>();
                }

                return _body;
            }
        }
        
        private Collider[] _colliders;
        private Collider[] Colliders => _colliders ?? (_colliders = GetComponentsInChildren<Collider>());

        private bool _forceStopMoving;
        private bool _pauseMovement;

        private bool _forceStopRotating;
        private bool _pauseRotating;
        
        private float _targetObjectMinimalDistance = 0.1f;
        private float _targetLookAtMinimalAngle = 2.0f;

        private Coroutine _moveInDirectionCoroutine;
        private Coroutine _moveTowardsCoroutine;
        private Coroutine _moveAlongPathCoroutine;
        private Coroutine _rotateAroundCoroutine;
        private Coroutine _lookAtCoroutine;

        protected override void AwakeOverride()
        {
            
        }
        
        private void OnDisable()
        {
            StopAllCoroutines();
        }

        [VarwinInspector(English:"Rotate player with object", Russian: "Поворачивать игрока вместе с объектом")]
        public bool RotatePlayerWithObject
        {
            get => _rotatePlayerWithObject;
            set => _rotatePlayerWithObject = value;
        }

        private bool _rotatePlayerWithObject;

        #region COLLISIONS 
        
        private void OnCollisionEnter(Collision other)
        {
            if (ProjectData.GameMode == GameMode.Edit)
            {
                return;
            }
            
            VarwinObjectDescriptor potentialObject = other.gameObject.GetComponentInParent<VarwinObjectDescriptor>();

            if (potentialObject && potentialObject.Wrapper() != null)
            {
                OnCollisionStart?.Invoke(potentialObject.Wrapper());
            }
        }

        private void OnCollisionExit(Collision other)
        {
            if (ProjectData.GameMode == GameMode.Edit)
            {
                return;
            }
            
            VarwinObjectDescriptor potentialObject = other.gameObject.GetComponentInParent<VarwinObjectDescriptor>();

            if (potentialObject && potentialObject.Wrapper() != null)
            {
                OnCollisionEnd?.Invoke(potentialObject.Wrapper());
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (ProjectData.GameMode == GameMode.Edit)
            {
                return;
            }
            
            VarwinObjectDescriptor potentialObject = other.gameObject.GetComponentInParent<VarwinObjectDescriptor>();

            if (potentialObject && potentialObject.Wrapper() != null)
            {
                OnTriggerStart?.Invoke(potentialObject.Wrapper());
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (ProjectData.GameMode == GameMode.Edit)
            {
                return;
            }
            
            VarwinObjectDescriptor potentialObject = other.gameObject.GetComponentInParent<VarwinObjectDescriptor>();

            if (potentialObject && potentialObject.Wrapper() != null)
            {
                OnTriggerEnd?.Invoke(potentialObject.Wrapper());
            }
        }

        [EventGroup("CollisionEvents")]
        [Event(English:"on collision start", Russian: "началось столкновение")]
        public event CollisionHandler OnCollisionStart;

        [EventGroup("CollisionEvents")]
        [Event(English:"on collision end", Russian: "столкновение закончилось")]
        public event CollisionHandler OnCollisionEnd;
        
        [EventGroup("TriggerEvents")]
        [Event(English:"object got inside of target object", Russian: "объект попал внутрь целевого")]
        public event CollisionHandler OnTriggerStart;

        [EventGroup("TriggerEvents")]
        [Event(English:"object got outside target object", Russian: "объект оказался снаружи целевого")]
        public event CollisionHandler OnTriggerEnd;
        
        #endregion

        #region MOVE BLOCKS

        [Action(English: "Move in position:", Russian: "Переместится в позицию:")]
        [ArgsFormat(English:" x: {%}, y: {%}, z: {%}", Russian:" x: {%}, y: {%}, z: {%}")]
        public void TeleportToCoordinates(float x, float y, float z)
        {
            Body.position = new Vector3(x, y, z);
        }
        
        [Action(English: "Move in center of ", Russian: "Переместится в центр ")]
        public void TeleportInCenterOfObject(Wrapper obj)
        {
            GameObject destinationObj = obj.GetGameObject();

            if (destinationObj)
            {
                Body.position = destinationObj.transform.position;
            }
        }
        
        [Action(English: "Set target stop distance", Russian: "Задать расстояние остановки перед целевым объектом")]
        public void SetMinObjectDistance(float objectDistance)
        {
            _targetObjectMinimalDistance = objectDistance;
        }
        
        [Action(English: "Move", Russian: "Перемещаться")]
        [ArgsFormat(English:"in direction {%} at a speed of {%} m/s", Russian: "в направлении {%} со скоростью {%}м/сек")]
        public void MoveWithSpeed(AxisDirection direction, float speed)
        {
            if (!isActiveAndEnabled)
            {
                return;
            }
            
            if (_moveInDirectionCoroutine != null)
            {
                StopCoroutine(_moveInDirectionCoroutine);
            }

            _moveInDirectionCoroutine = StartCoroutine(MoveInDirectionCoroutine(direction, speed, -1, MovementType.Once));
        }

        [Action(English: "Move", Russian: "Перемещаться")]
        [ArgsFormat(English:"in direction {%} to a distance of {%}m at a speed of {%}m/s {%}", Russian: "в направлении {%} на расстояние {%}м со скоростью {%}м/сек {%}")]
        public void MoveWithDistance(AxisDirection direction, float distance, float speed, MovementType type)
        {
            if (!isActiveAndEnabled)
            {
                return;
            }
            
            MoveWithDuration(direction, distance / speed, speed, type);
        }
        
        [Action(English: "Move", Russian: "Перемещаться")]
        [ArgsFormat(English:"in direction {%} for {%}s at a speed of {%}m/s {%}", Russian: "в направлении {%} в течение {%}сек со скоростью {%}м/сек {%}")]
        public void MoveWithDuration(AxisDirection direction, float duration, float speed, MovementType type)
        {
            if (!isActiveAndEnabled)
            {
                return;
            }
            
            if (_moveInDirectionCoroutine != null)
            {
                StopCoroutine(_moveInDirectionCoroutine);
            }
            
            _moveInDirectionCoroutine = StartCoroutine(MoveInDirectionCoroutine(direction, speed, duration, type));
        }

        [Action(English: "Move", Russian: "Перемещаться")]
        [ArgsFormat(English: "towards the object {%} at a speed of {%}m/s without stopping {%}", Russian: "в сторону объекта {%} со скоростью {%}м/сек не прекращая {%}")]
        public void MoveTowards(Wrapper wrapper, float speed, bool infinite)
        {
            if (!isActiveAndEnabled)
            {
                return;
            }
            
            if(_moveTowardsCoroutine != null)
            {
                StopCoroutine(_moveTowardsCoroutine);
            }
            
            _moveTowardsCoroutine = StartCoroutine(MoveTowardsCoroutine(wrapper, speed, infinite));
        }

        [Action(English: "Move", Russian: "Перемещаться")]
        [ArgsFormat(English: "along the path {%} at a speed of {%}m/s {%}", Russian: "по маршруту {%} со скоростью {%}м/сек {%}")]
        public void MoveByPath(dynamic points, float speed, MovementType type)
        {
            if (!isActiveAndEnabled)
            {
                return;
            }
            
#if !NET_STANDARD_2_0
            List<dynamic> pointsList;
            
            if (points is List<dynamic>)
            {
                pointsList = points;
            }
            else if(points is Wrapper)
            {
                pointsList = new List<dynamic> {points};
            }
            else
            {
                return;
            }
            
            List<Wrapper> wrappersList = new List<Wrapper>();

            foreach (dynamic point in pointsList)
            {
                Wrapper wrapper = point as Wrapper;

                if (wrapper != null)
                {
                    wrappersList.Add(wrapper);
                }
            }

            if (wrappersList.Count == 0)
            {
                return;
            }
            
            if(_moveAlongPathCoroutine != null)
            {
                StopCoroutine(_moveAlongPathCoroutine);
            }
            
            _moveAlongPathCoroutine = StartCoroutine(MoveAlongPath(wrappersList, speed, type));
#endif
        }

        [ActionGroup("PauseContinueStopMove")]
        [Action(English: "Pause movement", Russian: "Приостановить перемещение")]
        public void PauseMovement()
        {
            _pauseMovement = true;
        }

        [ActionGroup("PauseContinueStopMove")]
        [Action(English: "Continue movement", Russian: "Возобновить перемещение")]
        public void ContinueMovement()
        {
            _pauseMovement = false;
        }

        [ActionGroup("PauseContinueStopMove")]
        [Action(English: "Stop movement", Russian: "Остановить перемещение")]
        public void StopMovements()
        {
            _forceStopMoving = true;
        }

        [Event(English: "movement complete", Russian: "движение завершено")]
        public event MovementCompleteHandler MovementComplete;
        
        [Event(English: "target reached", Russian: "целевой объект достигнут")]
        public event TargetReachedHandler TargetReached;

        [Event(English: "path point reached", Russian: "Точка пути достигнута")]
        public event PathPointReachedHandler PathPointReached;
        
        #endregion

        #region ROTATE BLOCKS

        [ActionGroup("RotationWithObjects")]
        [Action(English: "Rotate in direction of ", Russian: "Повернуться в сторону ")]
        public void RotateInDirection(Wrapper obj)
        {
            GameObject targetGameObject = obj.GetGameObject();

            if (targetGameObject)
            {
                Body.transform.LookAt(targetGameObject.transform);
            }
        }
        
        [ActionGroup("RotationWithObjects")]
        [Action(English: "Rotate as ", Russian: "Повернуться как ")]
        public void RotateAsObject(Wrapper obj)
        {
            GameObject targetGameObject = obj.GetGameObject();

            if (targetGameObject)
            {
                Body.transform.rotation = targetGameObject.transform.rotation;
            }
        }

        [Action(English: "Turn around ", Russian: "Повернуть вокруг ")]
        [ArgsFormat(English:"x axis on angle {%}, y axis on angle {%}, z axis on angle {%}", Russian:"по оси X на угол {%}, по оси Y на угол {%}, по оси Z на угол{%}")]
        public void RotateOnAxis(float xAngle, float yAngle, float zAngle)
        { 
            Quaternion rotation = Quaternion.Euler(xAngle, yAngle, zAngle);
            Body.transform.rotation = rotation;
        }
        
        [Action(English: "Set min target object look at angle", Russian: "Задать минимальный угол поворота к целевому объекту")]
        public void SetMinObjectLookAtAngle(float angle)
        {
            _targetLookAtMinimalAngle = angle;
        }

        [Action(English:"Rotate", Russian: "Вращаться")]
        [ArgsFormat(English: "around {%} axis at a speed of {%} degree/s", Russian: "вокруг оси {%} со скоростью {%}градусов/сек")]
        public void RotateWithSpeed(AxisDirection axis, float speed)
        {
            if (!isActiveAndEnabled)
            {
                return;
            }
            
            if(_rotateAroundCoroutine != null)
            {
                StopCoroutine(_rotateAroundCoroutine);
            }

            _rotateAroundCoroutine = StartCoroutine(RotateAroundCoroutine(axis,
                speed,
                -1,
                MovementType.Once));
        }

        [Action(English:"Rotate", Russian: "Вращаться")]
        [ArgsFormat(English: "around {%} axis for {%} sec at a speed of {%} degree/s {%}", Russian: "вокруг оси {%} в течение {%} с со скоростью {%}градусов/сек {%}")]
        public void RotateWithDuration(AxisDirection axis, float duration, float speed, MovementType type)
        {
            if (!isActiveAndEnabled)
            {
                return;
            }
            
            if(_rotateAroundCoroutine != null)
            {
                StopCoroutine(_rotateAroundCoroutine);
            }

            _rotateAroundCoroutine = StartCoroutine(RotateAroundCoroutine(axis,
                speed,
                duration,
                type));
        }

        [Action(English:"Rotate", Russian: "Вращаться")]
        [ArgsFormat(English: "around {%} axis for {%} degrees at a speed of {%} degree/s {%}", Russian: "вокруг оси {%} на {%} градусов со скоростью {%}градусов/сек {%}")]
        public void RotateWithAngle(AxisDirection axis, float angle, float speed, MovementType type)
        {
            if (!isActiveAndEnabled)
            {
                return;
            }
            
            RotateWithDuration(axis, angle/speed, speed, type);
        }

        [Action(English:"Look at", Russian: "Повернуться")]
        [ArgsFormat(English:"the object {%} at a speed of {%} degree/s without stopping {%}", Russian: "к объекту {%} со скоростью {%}градусов/сек не прекращая {%}")]
        public void LookAt(Wrapper target, float speed, bool infinite)
        {
            if (!isActiveAndEnabled)
            {
                return;
            }
            
            if(_lookAtCoroutine != null)
            {
                StopCoroutine(_lookAtCoroutine);
            }
            
            _lookAtCoroutine = StartCoroutine(LookAtCoroutine(target, speed, infinite));
        }
        
        [ActionGroup("PauseContinueStopRotation")]
        [Action(English: "Pause Rotation", Russian: "Приостановить вращение")]
        public void PauseRotation()
        {
            _pauseRotating = true;
        }

        [ActionGroup("PauseContinueStopRotation")]
        [Action(English: "Continue Rotation", Russian: "Возобновить вращение")]
        public void ContinueRotation()
        {
            _pauseRotating = false;
        }

        [ActionGroup("PauseContinueStopRotation")]
        [Action(English: "Stop Rotation", Russian: "Остановить вращение")]
        public void StopRotation()
        {
            _forceStopRotating = true;
        }
        
        [Event(English: "rotation complete", Russian: "вращение завершено")]
        public event MovementCompleteHandler RotationComplete;        
        
        [Event(English: "look at finished", Russian: "поворот к объекту завершен")]
        public event TargetReachedHandler LookAtFinished;

        #endregion
        
        #region MOVE COROUTINES
        
        IEnumerator MoveInDirectionCoroutine(AxisDirection direction, float speed, float duration, MovementType type)
        {
            _forceStopMoving = false;
            Vector3 localDirection = VectorFromDirection(direction);
            Vector3 beginDirection = transform.position;
            
            float timer = 0.0f;

            bool movementComplete = false;
            bool movePlayer = IsTeleportable();
            
            while (!movementComplete)
            {
                while (_pauseMovement)
                {
                    if (_forceStopMoving)
                    {
                        break;
                    }
                    yield return null;
                }
                
                if (_forceStopMoving)
                {
                    break;
                }
                
                Vector3 deltaToMove = transform.rotation * localDirection * (speed * Time.deltaTime);

                if (movePlayer)
                {
                    if (IsPlayerOverObject())
                    {
                        PlayerManager.CurrentRig.SetPosition(PlayerManager.CurrentRig.Position + deltaToMove);
                    }
                }

                Body.transform.Translate(deltaToMove, Space.World);
                timer += Time.deltaTime;

                movementComplete = timer >= duration && duration > 0.0f;

                if (movementComplete)
                {
                    switch (type)
                    {
                        case MovementType.Once:
                            break;
                        
                        case MovementType.Repeat: 
                            Body.position = beginDirection;
                            timer = 0.0f;
                            movementComplete = false;
                            MovementComplete?.Invoke(false);
                            break;
                        
                        case MovementType.PingPong: 
                            timer = 0.0f;
                            localDirection = -localDirection;
                            movementComplete = false;
                            MovementComplete?.Invoke(false);
                            break;
                    }
                }

                yield return null;
            }
            
            MovementComplete?.Invoke(true);
        }

        IEnumerator MoveTowardsCoroutine(Wrapper wrapper, float speed, bool infinite)
        {
            _forceStopMoving = false;
            bool targetReached = false;
            bool movePlayer = IsTeleportable();

            Transform target = wrapper.GetGameObject().transform;
            
            while (infinite || !targetReached)
            {
                if (_forceStopMoving)
                {
                    break;
                }

                while (_pauseMovement)
                {
                    if (_forceStopMoving)
                    {
                        break;
                    }
                    yield return null;
                }
                
                Vector3 direction = (target.position - transform.position).normalized;
                Vector3 deltaToMove = targetReached ? Vector3.zero : direction * (speed * Time.deltaTime);

                if (movePlayer)
                {
                    if (IsPlayerOverObject())
                    {
                        PlayerManager.CurrentRig.SetPosition(PlayerManager.CurrentRig.Position + deltaToMove);
                    }
                }
                
                Body.transform.Translate(deltaToMove, Space.World);
                
                targetReached = Vector3.Distance(transform.position, target.position) <= _targetObjectMinimalDistance;

                yield return null;
            }
            
            TargetReached?.Invoke(wrapper);
        }

        IEnumerator MoveAlongPath(List<Wrapper> path, float speed, MovementType type)
        {
            int currentPoint = 0;

            bool movementComplete = false;
            bool movePlayer = IsTeleportable();

            while (!movementComplete)
            {
                if (_forceStopMoving)
                {
                    break;
                }

                while (_pauseMovement)
                {
                    if (_forceStopMoving)
                    {
                        break;
                    }
                    yield return null;
                }

                Vector3 currentTarget = path[currentPoint].GetGameObject().transform.position;
                
                if (Vector3.Distance(transform.position, currentTarget) <= _targetObjectMinimalDistance)
                {
                    PathPointReached?.Invoke(currentPoint, path[currentPoint]);
                    
                    currentPoint++;

                    if (currentPoint >= path.Count)
                    {
                        movementComplete = true;
                    }
                }

                if (movementComplete)
                {
                    switch (type)
                    {
                        case MovementType.Once:
                            break;
                        
                        case MovementType.Repeat: 
                            currentPoint = 0;
                            movementComplete = false;
                            MovementComplete?.Invoke(false);
                            break;
                        
                        case MovementType.PingPong: 
                            currentPoint = 0;
                            path.Reverse();
                            movementComplete = false;
                            MovementComplete?.Invoke(false);
                            break;
                    }
                }
                else
                {
                    Vector3 direction = (currentTarget - transform.position).normalized;
                    Vector3 deltaToMove = direction * (speed * Time.deltaTime);
                
                    if (movePlayer)
                    {
                        if (IsPlayerOverObject())
                        {
                            PlayerManager.CurrentRig.SetPosition(PlayerManager.CurrentRig.Position + deltaToMove);
                        }
                    }
                
                    Body.transform.Translate(deltaToMove, Space.World);
                }
                
                yield return null;
            }
            
            MovementComplete?.Invoke(true);
        }

        #endregion

        #region ROTATE COROUTINES

        IEnumerator RotateAroundCoroutine(
            AxisDirection axis,
            float speed,
            float duration,
            MovementType type)
        {
            _forceStopRotating = false;

            bool rotationComplete = false;
            float timer = 0.0f;
            bool movePlayer = IsTeleportable() && _rotatePlayerWithObject;

            Quaternion beginRotation = transform.rotation;
            Vector3 localAxis = VectorFromDirection(axis);

            while (!rotationComplete)
            {
                while (_pauseRotating)
                {
                    if (_forceStopRotating)
                    {
                        break;
                    }
                    yield return null;
                }
                
                if (_forceStopRotating)
                {
                    break;
                }

                Quaternion deltaRotation = Quaternion.AngleAxis(speed * Time.deltaTime, localAxis);

                if (movePlayer)
                {
                    if (IsPlayerOverObject())
                    {
                        yield return new WaitForEndOfFrame();
                        PlayerManager.CurrentRig.SetRotation(PlayerManager.CurrentRig.Rotation * deltaRotation);
                    }
                }
                
                Body.rotation *= deltaRotation;
                Body.transform.rotation = Body.rotation;
                
                timer += Time.deltaTime;
                
                rotationComplete = timer >= duration && duration > 0.0f;

                if (rotationComplete)
                {
                    switch (type)
                    {
                        case MovementType.Once:
                            break;
                        
                        case MovementType.Repeat: 
                            Body.rotation = beginRotation;
                            timer = 0.0f;
                            rotationComplete = false;
                            RotationComplete?.Invoke(false);
                            break;
                        
                        case MovementType.PingPong: 
                            timer = 0.0f;
                            localAxis = -localAxis;
                            rotationComplete = false;
                            RotationComplete?.Invoke(false);
                            break;
                    }
                }

                yield return null;
            }
            
            RotationComplete?.Invoke(true);
        }

        IEnumerator LookAtCoroutine(Wrapper wrapper, float speed, bool infinite)
        {
            _forceStopRotating = false;
            bool movePlayer = IsTeleportable() && _rotatePlayerWithObject;
            bool targetReached = false;
            
            Transform target = wrapper.GetGameObject().transform;

            while (!targetReached)
            {
                while (_pauseRotating)
                {
                    if (_forceStopRotating)
                    {
                        break;
                    }

                    yield return null;
                }

                if (_forceStopRotating)
                {
                    break;
                }
                
                Quaternion lookOnLook = Quaternion.LookRotation(target.position - transform.position);
                Quaternion deltaRotation = Quaternion.Slerp(transform.rotation, lookOnLook, speed * Time.deltaTime);
                
                if (movePlayer)
                {
                    if (IsPlayerOverObject())
                    {
                        yield return new WaitForEndOfFrame();
                        PlayerManager.CurrentRig.SetRotation(PlayerManager.CurrentRig.Rotation * deltaRotation);
                    }
                }
                
                Body.rotation = deltaRotation;
                Body.transform.rotation = deltaRotation;

                if (!infinite)
                {
                    targetReached = Quaternion.Angle(Body.rotation, lookOnLook) <= _targetLookAtMinimalAngle;
                }

                yield return null;
            }
            
            LookAtFinished?.Invoke(wrapper);
        }

        #endregion
        
        public Vector3 VectorFromDirection(AxisDirection direction)
        {
            switch (direction)
            {
                case AxisDirection.PosX:
                    return new Vector3(1.0f, 0.0f, 0.0f);

                case AxisDirection.PosY:
                    return new Vector3(0.0f, 1.0f, 0.0f);
                
                case AxisDirection.PosZ:
                    return new Vector3(0.0f, 0.0f, 1.0f);
                
                case AxisDirection.NegX:
                    return new Vector3(-1.0f, 0.0f, 0.0f);

                case AxisDirection.NegY:
                    return new Vector3(0.0f, -1.0f, 0.0f);
                
                case AxisDirection.NegZ:
                    return new Vector3(0.0f, 0.0f, -1.0f);
            }

            return Vector3.zero;
        }
        
        private bool IsTeleportable()
        {
            InteractableBehaviour interactableBehaviour = GetComponent<InteractableBehaviour>();

            if (interactableBehaviour)
            {
                return interactableBehaviour.Teleportable && interactableBehaviour.Collision;
            }

            return false;
        }

        private bool IsPlayerOverObject()
        {
            Vector3 playerPosition = PlayerManager.CurrentRig.Position;

            if (playerPosition.y < transform.position.y)
            {
                return false;
            }
            
            Ray playerRay = new Ray(playerPosition + Vector3.up, Vector3.down);

            foreach (Collider objectCollider in Colliders)
            {
                if (objectCollider.bounds.IntersectRay(playerRay))
                {
                    return true;
                }
            }
            

            return false;
        }
    }
}