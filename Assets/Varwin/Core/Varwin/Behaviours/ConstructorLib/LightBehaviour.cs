﻿using Photon.Pun;
using UnityEngine;
using Varwin.Public;

namespace Varwin.Core.Behaviours.ConstructorLib
{
    [RequireComponentInChildren(typeof(Light))]
    [VarwinComponent(English: "Light", Russian: "Свет")]
    public class LightBehaviour : VarwinBehaviour, ISwitchModeSubscriber
    {
        [SerializeField] private GameObject PreviewRender;
        [SerializeField] private Material  PreviewRenderMaterial;
        
        private Light _light;
        private Light LightComponent
        {
            get
            {
                if (!_light)
                {
                    _light = GetComponentInChildren<Light>();
                }

                return _light;
            }
        }

        private Color _color;
        [VarwinInspector("Light Color", Russian: "Цвет")]
        public Color MainColor
        {
            get => _color;
            set
            {
                if (_color == value)
                {
                    return;
                }

                _color = value;
                LightComponent.color = _color;
                SetPreviewRendererColor(_color);

                if (IsMultiplayerAndMasterClient)
                {
                    photonView.RPC(nameof(SetColor), _rpcTarget, _color);
                }
            }
        }

        private float _range;
        [VarwinInspector("Range", Russian: "Дистанция")]
        public float Range
        {
            get => _range;
            set
            {
                if (_range.ApproximatelyEquals(value))
                {
                    return;
                }

                _range = value;
                
                LightComponent.range = _range;

                if (IsMultiplayerAndMasterClient)
                {
                    photonView.RPC(nameof(SetRange), _rpcTarget, _range);
                }
            }
        }

        private float _intensity;
        [VarwinInspector(English:"Intensity", Russian:"Интенсивность")]
        public float Intensity
        {
            get => _intensity;
            set
            {
                if (_intensity.ApproximatelyEquals(value))
                {
                    return;
                }

                _intensity = value;
                
                LightComponent.intensity = _intensity;

                if (IsMultiplayerAndMasterClient)
                {
                    photonView.RPC(nameof(SetIntensity), _rpcTarget, _intensity);
                }
            }
        }

        [Action(English:"Change intensity", Russian:"Изменить интенсивность")]
        public void ChangeIntensity(float value)
        {
            Intensity = value;
        }
        
        [Action(English:"Change range", Russian:"Изменить дальность")]
        public void ChangeRange(float value)
        {
            Range = value;
        }
        
        [Action(English:"Change color", Russian:"Изменить цвет")]
        [ArgsFormat(English:" (values 0-1) r{%} g{%} b{%} a{%}", Russian: " (значения 0-1) r{%} g{%} b{%} a{%}")]
        public void ChangeColor(float r, float g, float b, float a)
        {
            MainColor = new Color(r,g,b,a);
        }

        private Material _previewMaterial;
        private Renderer[] _renderers;
        private Renderer[] Renderers => _renderers ?? (_renderers = PreviewRender.GetComponentsInChildren<Renderer>());
        
        protected override void AwakeOverride()
        {
            _intensity = LightComponent.intensity;
            _range = LightComponent.range;
            _color = LightComponent.color;
            
            if (!PreviewRenderMaterial || !PreviewRender)
            {
                return;
            }

            _previewMaterial = new Material(PreviewRenderMaterial);

            foreach (var rend in Renderers)
            {
                rend.material = _previewMaterial;
            }
        }

        private void SetPreviewRendererColor(Color value)
        {
            if (!PreviewRenderMaterial)
            {
                return;
            }

            var newColor = new Color(value.r, value.g, value.b, PreviewRenderMaterial.color.a);
            foreach (var rend in Renderers)
            {
                rend.material.color = newColor;
            }
        }

        public void OnSwitchMode(GameMode newMode, GameMode oldMode)
        {
            if (!PreviewRender)
            {
                return;
            }

            PreviewRender.SetActive(!ProjectData.IsPlayMode);
        }

        protected override void ResendRpcCalls()
        {
            base.ResendRpcCalls();
            
            photonView.RPC(nameof(SetRange), _rpcTarget, _range);
            photonView.RPC(nameof(SetIntensity), _rpcTarget, _intensity);
            photonView.RPC(nameof(SetColor), _rpcTarget, _color);
        }
        
        #region RPC calls

        [PunRPC]
        public void SetRange(float range)
        {
            Range = range;
        }

        [PunRPC]
        public void SetIntensity(float intensity)
        {
            Intensity = intensity;
        }

        [PunRPC]
        public void SetColor(Color color)
        {
            MainColor = color;
        }

        #endregion
    }
}
