﻿using System;
using System.Collections;
using UnityEngine;

namespace Varwin.Core.Behaviours.ConstructorLib
{
    [VarwinComponent(English: "Scaling", Russian: "Масштабирование")]
    public class ScalableBehaviour : VarwinBehaviour
    {
        private bool _forceStopScaling;
        private bool _pauseScaling;
        private bool _backfront;

        private Coroutine _scaleOnXCoroutine;
        private Coroutine _scaleOnYCoroutine;
        private Coroutine _scaleOnZCoroutine;

        private bool _scaleOnXRunning;
        private bool _scaleOnYRunning;
        private bool _scaleOnZRunning;
        
        public enum Axis
        {
            [Item(English: "X")]
            X,
            [Item(English: "Y")]
            Y,
            [Item(English: "Z")]
            Z,
        }
        
        public enum ScaleType
        {
            [Item(English: "Once", Russian: "Один раз")]
            Once = 0,
            [Item(English: "Repeatedly", Russian: "Повторяясь")]
            Repeat,
            [Item(English: "Back and forth", Russian: "Туда-сюда")]
            PingPong
        }
        
        public delegate void ScalingCompleteHandler (bool finished);

        protected override void AwakeOverride()
        {
            
        }

        private void OnDisable()
        {
            StopAllCoroutines();
        }

        [ActionGroup("PauseContinueStopScaling")]
        [Action(English: "Pause scaling", Russian: "Приостановить масштабирование")]
        public void PauseScaling()
        {
            _pauseScaling = true;
        }

        [ActionGroup("PauseContinueStopScaling")]
        [Action(English: "Continue scaling", Russian: "Возобновить масштабирование")]
        public void ContinueScaling()
        {
            _pauseScaling = false;
        }

        [ActionGroup("PauseContinueStopScaling")]
        [Action(English: "Stop scaling", Russian: "Остановить масштабирование")]
        public void StopScaling()
        {
            _forceStopScaling = true;
        }
        
        [Event(English: "scaling complete", Russian: "масштабирование завершено")]
        public event ScalingCompleteHandler OnScalingComplete;
        
        [Action(English: "Scale", Russian: "Масштабировать")]
        [ArgsFormat(English:" to {%} on axis {%} with time {%}s {%}", Russian: " в {%} раз по оси {%} в течение {%}сек {%}")]
        public void ScaleWithTimeAndSize(float scale, Axis axis, float time, ScaleType type)
        {
            if (!isActiveAndEnabled)
            {
                return;
            }
            
            Vector3 startScale = transform.localScale;
            
            float axisStep = 0f;
            float finalScale = 0f;
            float startAxisScale = 0f;

            time = Mathf.Clamp(time, Time.deltaTime, time);

            if (axis == Axis.X)
            {
                finalScale = startScale.x * scale;
                startAxisScale = startScale.x;
                axisStep = (finalScale - startScale.x) / time;

                StartScalingCoroutine(ref _scaleOnXCoroutine, ref _scaleOnXRunning, type, startAxisScale, finalScale, axis, axisStep);
            }
            else if(axis == Axis.Y)
            {
                finalScale = startScale.y * scale;
                startAxisScale = startScale.y;
                axisStep = (finalScale - startScale.y) / time;

                StartScalingCoroutine(ref _scaleOnYCoroutine, ref _scaleOnYRunning, type, startAxisScale, finalScale, axis, axisStep);
            }
            else if(axis == Axis.Z)
            {
                finalScale = startScale.z * scale;
                startAxisScale = startScale.z;
                axisStep = (finalScale - startScale.z) / time;

                StartScalingCoroutine(ref _scaleOnZCoroutine, ref _scaleOnZRunning, type, startAxisScale, finalScale, axis, axisStep);
            }
        }

        private void StartScalingCoroutine(ref Coroutine coroutine, ref bool scaleRunning, ScaleType type, float startAxisScale, float finalScale, Axis axis, float axisStep)
        {
            if (coroutine != null)
            {
                StopCoroutine(coroutine);
            }

            scaleRunning = true;
            
            if (type == ScaleType.PingPong)
            {
                coroutine = StartCoroutine(PingPongScaleCoroutine(startAxisScale, finalScale, axis, axisStep));
            }
            else
            {
                coroutine = StartCoroutine(ScaleWithSizeCoroutine(startAxisScale, finalScale, axis, axisStep, type));
            }
        }
        
        [Action(English: "Scale", Russian: "Масштабировать")]
        [ArgsFormat(English:" with rate {%}m/sec on axis {%} with time {%}s {%}", Russian: " со скоростью {%}м/с по оси {%} в течение {%}сек {%}")]
        public void ScaleWithTimeAndSpeed(float speed, Axis axis, float time, ScaleType type)
        {
            if (!isActiveAndEnabled)
            {
                return;
            }
            
            Vector3 startScale = transform.localScale;
            
            float scale = speed * time;
            
            if (axis == Axis.X)
            {
                scale += startScale.x;
            }
            else if(axis == Axis.Y)
            {
                scale += startScale.y;
            }
            else if(axis == Axis.Z)
            {
                scale += startScale.z;
            }
            
            ScaleWithTimeAndSize(scale, axis, time, type);
        }
        
        [Action(English: "Set axis scale", Russian: "Масштабировать")]
        [ArgsFormat(English:" for axis {%} to {%}", Russian: " по оси {%} в {%} раз ")]
        public void SetAxisScale(Axis axis, float size)
        {
            if (!isActiveAndEnabled)
            {
                return;
            }
            
            Vector3 currentScale = transform.localScale;
            Vector3 newScale = currentScale;
            
            if (axis == Axis.X)
            {
                newScale = new Vector3(currentScale.x * size, currentScale.y, currentScale.z);
            }
            else if (axis == Axis.Y)
            {
                newScale = new Vector3(currentScale.x, currentScale.y * size, currentScale.z);
            }
            else if (axis == Axis.Z)
            {
                newScale = new Vector3(currentScale.x, currentScale.y, currentScale.z * size);
            }

            transform.localScale = newScale;
        }

        private IEnumerator PingPongScaleCoroutine(float startAxisScale, float finalScale, Axis axis, float axisStep)
        {
            while (true)
            {
                Vector3 currentScale = transform.localScale;
                
                while (_pauseScaling)
                {
                    if (_forceStopScaling)
                    {
                        SetScalingStateOnAxis(axis, false);
                        OnScalingComplete?.Invoke(false);

                        yield break;
                    }
                    yield return null;
                }
                
                if (_forceStopScaling)
                {
                    OnScalingComplete?.Invoke(false);
                    yield break;
                }
                
                Vector3 newScale = currentScale;
                float changeAxis = 0f;

                float minValue = Mathf.Min(startAxisScale, finalScale);
                float maxValue = Mathf.Max(startAxisScale, finalScale);

                if (axis == Axis.X)
                {
                    changeAxis = Mathf.Clamp(currentScale.x + axisStep * Time.deltaTime, minValue, maxValue);
                    newScale = new Vector3(changeAxis, currentScale.y,
                        currentScale.z);
                }
                else if (axis == Axis.Y)
                {
                    changeAxis = Mathf.Clamp(currentScale.y + axisStep * Time.deltaTime, minValue, maxValue);
                    newScale = new Vector3(currentScale.x, changeAxis,
                        currentScale.z);
                }
                else if(axis == Axis.Z)
                {
                    changeAxis = Mathf.Clamp(currentScale.z + axisStep * Time.deltaTime, minValue, maxValue);
                    newScale = new Vector3(currentScale.x, currentScale.y,
                        changeAxis);
                }

                transform.localScale = newScale;
                
                float targetScale = axisStep > 0 ? maxValue : minValue;
                
                if (Math.Abs(changeAxis - targetScale) < Mathf.Epsilon)
                {
                    axisStep *= -1;

                    yield return null;
                } 
                
                yield return null;
            }
        }

        private IEnumerator ScaleWithSizeCoroutine(float startAxisScale, float finalScale, Axis axis, float axisStep, ScaleType type)
        {
            bool complete = false;
 
            while (!complete)
            {
                Vector3 currentScale = transform.localScale;
                
                while (_pauseScaling)
                {
                    if (_forceStopScaling)
                    {
                        SetScalingStateOnAxis(axis, false);
                        OnScalingComplete?.Invoke(false);
                        yield break;
                    }
                    yield return null;
                }
                
                if (_forceStopScaling)
                {
                    SetScalingStateOnAxis(axis, false);
                    OnScalingComplete?.Invoke(false);
                    yield break;
                }
                
                Vector3 newScale = currentScale;
                float changeAxis = 0f;

                float minValue = Mathf.Min(startAxisScale, finalScale);
                float maxValue = Mathf.Max(startAxisScale, finalScale);
                
                if (axis == Axis.X)
                {
                    changeAxis = Mathf.Clamp(currentScale.x + axisStep * Time.deltaTime, minValue, maxValue);
                    newScale = new Vector3(changeAxis, currentScale.y,
                        currentScale.z);
                }
                else if (axis == Axis.Y)
                {
                    changeAxis = Mathf.Clamp(currentScale.y + axisStep * Time.deltaTime, minValue, maxValue);
                    newScale = new Vector3(currentScale.x, changeAxis,
                        currentScale.z);
                }
                else if(axis == Axis.Z)
                {
                    changeAxis = Mathf.Clamp(currentScale.z + axisStep * Time.deltaTime, minValue, maxValue);
                    newScale = new Vector3(currentScale.x, currentScale.y,
                        changeAxis);
                }

                transform.localScale = newScale;
                
                if (Math.Abs(changeAxis - finalScale) < Mathf.Epsilon)
                {
                    if (type == ScaleType.Once)
                    {
                        complete = true;
                        SetScalingStateOnAxis(axis, false);
                        OnScalingComplete?.Invoke(!_scaleOnXRunning && !_scaleOnYRunning && !_scaleOnZRunning);
                    }
                    else
                    {
                        Vector3 scale = transform.localScale;
                            
                        if (axis == Axis.X)
                        {
                            scale.x = startAxisScale;
                        }
                        else if (axis == Axis.Y)
                        {
                            scale.y = startAxisScale;
                        }
                        else if (axis == Axis.Z)
                        {
                            scale.z = startAxisScale;
                        }

                        transform.localScale = scale;
                        
                        OnScalingComplete?.Invoke(false);
                    }

                    yield return null;
                } 
                
                yield return null;
            }
        }

        private void SetScalingStateOnAxis(Axis axis, bool value)
        {
            switch (axis)
            {
                case Axis.X:
                    _scaleOnXRunning = value;
                    break;
                case Axis.Y:
                    _scaleOnYRunning = value;
                    break;
                case Axis.Z:
                    _scaleOnZRunning = value;
                    break;
            }
        }
    }
}
