﻿using System;
using System.Linq;
using Photon.Pun;
using Varwin.Public;
using UnityEngine;

namespace Varwin.Core.Behaviours.ConstructorLib
{
    public class InteractableBehaviourHelper : VarwinBehaviourHelper
    {
        public override bool CanAddBehaviour(GameObject gameObject, Type behaviourType)
        {
            if (!base.CanAddBehaviour(gameObject, behaviourType))
            {
                return false;
            }

            return !gameObject.GetComponentInChildren<JointPoint>();
        }
    }

    [RequireComponentInChildren(typeof(Rigidbody))]
    [RequireComponent(typeof(InteractableObjectBehaviour))]
    [VarwinComponent(English: "Interaction", Russian: "Взаимодействие")]
    public class InteractableBehaviour : VarwinBehaviour
    {
        private Collider[] _colliders;
        private Collider[] Colliders
        {
            get
            {
                if (_colliders == null)
                {
                    UpdateColliders();
                }

                return _colliders;
            }
        }

        private Rigidbody _rigidbody;
        private Rigidbody Rigidbody
        {
            get
            {
                if (!_rigidbody)
                {
                    _rigidbody = gameObject.GetComponent<Rigidbody>();
                }

                return _rigidbody;
            }
        }

        private InteractableObjectBehaviour _interactableObjectBehaviour;
        private InteractableObjectBehaviour InteractableObjectBehaviour
        {
            get
            {
                if (!_interactableObjectBehaviour)
                {
                    _interactableObjectBehaviour = gameObject.GetComponent<InteractableObjectBehaviour>();
                }

                return _interactableObjectBehaviour;
            }
        }

        private void OnEnable()
        {
            ProjectData.GameModeChanging += TrySetPhysicsSettings;
        }

        private void OnDisable()
        {
            ProjectData.GameModeChanging -= TrySetPhysicsSettings;
        }

        protected override void AwakeOverride()
        {
            VarwinCommonMethods.UpdateColliders += VarwinCommonMethodsOnUpdateColliders;
            _mass = Rigidbody.mass;
            _bounciness = Colliders.Length == 0 ? 0 : Colliders[0].material.bounciness;
        }
        
        private void OnDestroy()
        {
            ProjectData.GameModeChanging -= TrySetPhysicsSettings;
            VarwinCommonMethods.UpdateColliders -= VarwinCommonMethodsOnUpdateColliders;
        }

        private void Start()
        {
            TrySetPhysicsSettings();
            
            InteractableObjectBehaviour.OnGrabStarted.AddListener(OnGrabStart);
            InteractableObjectBehaviour.OnGrabEnded.AddListener(OnGrabEnd);
            InteractableObjectBehaviour.OnUseStarted.AddListener(OnUseStart);
            InteractableObjectBehaviour.OnUseEnded.AddListener(OnUseEnd);
            InteractableObjectBehaviour.OnTouchStarted.AddListener(OnTouchStart);
            InteractableObjectBehaviour.OnTouchEnded.AddListener(OnTouchEnd);
        }
        
        private void VarwinCommonMethodsOnUpdateColliders(Wrapper wrapper)
        {
            if (wrapper == gameObject.GetWrapper())
            {
                UpdateColliders();
            }
        }
        
        public void UpdateColliders()
        {
            _colliders = gameObject.GetComponentsInChildren<Collider>();
  
            if (!_colliders.All(x => x.isTrigger))
            {
                _colliders = _colliders.Where(x => !x.isTrigger).ToArray();
            }
        }
        
        public void TrySetPhysicsSettings()
        {
            TrySetPhysicsSettings(ProjectData.GameMode);
        }

        private void TrySetPhysicsSettings(GameMode mode)
        {
            if (!ProjectData.IsPlayMode)
            {
                return;
            }
            
            ObjectController controller = gameObject.GetWrapper().GetObjectController();
            if (controller == null || controller.Parent != null && controller.Parent.LockChildren)
            {
                return;
            }
            
            Rigidbody.useGravity = Gravity;
            Rigidbody.isKinematic = IsStatic;
        }

        #region PHYSICS PARAMETERS

        private bool _isKinematic;
        private bool _isKinematicInitialized;
        
        [Variable(English: "Is static", Russian: "Статичный")]
        [VarwinInspector(English: "Is static", Russian: "Статичный")]
        public bool IsStatic
        {
            get
            {
                if (!_isKinematicInitialized)
                {
                    _isKinematic = Rigidbody.isKinematic;
                    _isKinematicInitialized = true;
                }
                return _isKinematic;
            }
            set
            {
                if (_isKinematic == value)
                {
                    return;
                }
                
                _isKinematic = value;
                TrySetPhysicsSettings();

                if (IsMultiplayerAndMasterClient)
                {
                    photonView.RPC(nameof(SetKinematic), _rpcTarget, _isKinematic);
                }
            }
        }

        private bool _useGravity;
        private bool _isGravityInitialized;
        
        [Variable(English:"Use gravity", Russian:"Гравитация")]
        [VarwinInspector(English: "Use gravity", Russian: "Гравитация")]
        public bool Gravity
        {
            get
            {
                if (!_isGravityInitialized)
                {
                    _useGravity = Rigidbody.useGravity;
                    _isGravityInitialized = true;
                }
                return _useGravity;
            }
            set
            {
                if (_useGravity == value)
                {
                    return;
                }
                
                _useGravity = value;
                TrySetPhysicsSettings();
                
                if (IsMultiplayerAndMasterClient)
                {
                    photonView.RPC(nameof(SetGravity), _rpcTarget, _useGravity);
                }
            }
        }

        [Variable(English:"Teleport area", Russian:"Зона телепорта")]
        [VarwinInspector(English: "Teleport area", Russian: "Зона телепорта")]
        public bool Teleportable
        {
            get => gameObject.CompareTag("TeleportArea");
            set => gameObject.tag = value ? "TeleportArea" : "Untagged";
        }


        private bool _isTrigger;
        private bool _isCollisionInitialized;
        
        [Variable(English:"Is obstacle", Russian:"Препятствие")]
        [VarwinInspector(English: "Is obstacle", Russian: "Препятствие")]
        public bool Collision
        {
            get
            {
                if (!_isCollisionInitialized)
                {
                    _isTrigger = Colliders[0].isTrigger;
                    _isCollisionInitialized = true;
                }

                if (Colliders.Length == 0)
                {
                    return false;
                }

                return !_isTrigger;
            }
            set
            {
                if (_isTrigger == !value)
                {
                    return;
                }
                
                _isTrigger = !value;
                
                foreach (Collider objectCollider in Colliders)
                {
                    objectCollider.isTrigger = _isTrigger;
                }
                
                if (IsMultiplayerAndMasterClient)
                {
                    photonView.RPC(nameof(SetCollision), _rpcTarget, value);
                }
            }
        }

        private float _mass;
        [Variable(English:"Mass", Russian:"Масса")]
        [VarwinInspector(English: "Mass", Russian: "Масса")]
        public float Mass
        {
            get => _mass;
            set
            {
                if (_mass.ApproximatelyEquals(value))
                {
                    return;
                }

                _mass = value;
                Rigidbody.mass = _mass;

                if (IsMultiplayerAndMasterClient)
                {
                    photonView.RPC(nameof(SetMass), _rpcTarget, _mass);
                }
            }
        }

        private float _bounciness;
        [Variable(English: "Bounciness", Russian: "Пружинистость")]
        [VarwinInspector(English: "Bounciness", Russian: "Пружинистость")]
        public float Bounciness
        {
            get => _bounciness;
            set
            {
                if (_bounciness == value)
                {
                    return;
                }

                _bounciness = value;
                
                foreach (Collider objectCollider in Colliders)
                {
                    objectCollider.material.bounciness = Mathf.Clamp01(value);
                }

                if (IsMultiplayerAndMasterClient)
                {
                    photonView.RPC(nameof(SetBounciness), _rpcTarget, _bounciness);
                }
            }
        }
        
        #endregion //PHYSICS PARAMETERS
        
        [Variable(English:"Grabbable", Russian:"Можно брать в руку")]
        [VarwinInspector(English: "Grabbable", Russian: "Можно брать в руку")]
        public bool Grabbable
        {
            get => InteractableObjectBehaviour.IsGrabbable;
            set => InteractableObjectBehaviour.IsGrabbable = value;
        }
        
        
        [Variable(English:"Usable", Russian:"Можно использовать")]
        [VarwinInspector(English: "Usable", Russian: "Можно использовать")]
        public bool Usable
        {
            get => InteractableObjectBehaviour.IsUsable;
            set => InteractableObjectBehaviour.IsUsable = value;
        }

        [Variable("Touchable", Russian:"Можно дотронуться")]
        [VarwinInspector(English: "Touchable", Russian: "Можно дотронуться")]
        public bool Touchable
        {
            get => InteractableObjectBehaviour.IsTouchable;
            set => InteractableObjectBehaviour.IsTouchable = value;
        }
        
        [EventGroup("InteractionEvents")]
        [Event(English:"on grab start", Russian: "объект взят в руку")]
        public event Action GrabStart;
        
        [EventGroup("InteractionEvents")]
        [Event(English: "on grab end", Russian: "объект отпущен из руки")]
        public event Action GrabEnd;
        
        [EventGroup("InteractionEvents")]
        [Event(English:"on use start", Russian: "объект начали использовать")]
        public event Action UseStart;
        
        [EventGroup("InteractionEvents")]
        [Event(English: "on use end", Russian: "объект закончили использовать")]
        public event Action UseEnd;
        
        [EventGroup("InteractionEvents")]
        [Event(English:"on touch start", Russian: "до объекта дотронулись")]
        public event Action TouchStart;
        
        [EventGroup("InteractionEvents")]
        [Event(English: "on touch end", Russian: "объект прекратили трогать")]
        public event Action TouchEnd;

        #region AWARE INTERFACES
        
        public void OnGrabStart()
        {
           GrabStart?.Invoke();
        }

        public void OnUseStart()
        {
           UseStart?.Invoke();
        }

        public void OnTouchStart()
        {
            TouchStart?.Invoke();
        }

        public void OnGrabEnd()
        {
           GrabEnd?.Invoke();
        }

        public void OnUseEnd()
        {
            UseEnd?.Invoke();
        }

        public void OnTouchEnd()
        {
           TouchEnd?.Invoke();
        }
        
        #endregion

        protected override void ResendRpcCalls()
        {
            base.ResendRpcCalls();
            
            photonView.RPC(nameof(SetCollision), _rpcTarget, !_isTrigger);
            photonView.RPC(nameof(SetGravity), _rpcTarget, _useGravity);
            photonView.RPC(nameof(SetKinematic), _rpcTarget, _isKinematic);
            photonView.RPC(nameof(SetMass), _rpcTarget, _mass);
            photonView.RPC(nameof(SetBounciness), _rpcTarget, _bounciness);
        }
        
        #region RPC calls

        [PunRPC]
        public void SetCollision(bool collision)
        {
            Collision = collision;
        }

        [PunRPC]
        public void SetGravity(bool gravity)
        {
            Gravity = gravity;
        }
        
        [PunRPC]
        public void SetKinematic(bool isKinematic)
        {
            IsStatic = isKinematic;
        }

        [PunRPC]
        public void SetMass(float mass)
        {
            Mass = mass;
        }

        [PunRPC]
        public void SetBounciness(float bounciness)
        {
            Bounciness = bounciness;
        }

        #endregion
    }
}
