﻿using System;
using System.Linq;
using Photon.Pun;
using UnityEngine;

namespace Varwin.Core.Behaviours.ConstructorLib
{
    public class MaterialChangeBehaviourHelper : VarwinBehaviourHelper
    {
        private static readonly string[] RequiredMaterialProperties = 
        {
            "_Color",
            "_MainTex",
            "_Glossiness",
            "_Metallic"
        };
        
        public override bool CanAddBehaviour(GameObject gameObject, Type behaviourType)
        {
            if (!base.CanAddBehaviour(gameObject, behaviourType))
            {
                return false;
            }

            var renderers = gameObject.GetComponentsInChildren<Renderer>();

            if (renderers.Length <= 0)
            {
                return false;
            }

            var materials = renderers.Select(x => x.material).Where(HasRequiredProperties);
            Material firstMaterial = materials.FirstOrDefault();
            
            if (!firstMaterial)
            {
                return false;
            }

            Color firstColor = firstMaterial.color;
                
            return materials.All(x => x.color == firstColor);
        }

        private bool HasRequiredProperties(Material material)
        {
            return RequiredMaterialProperties.All(material.HasProperty);
        }
    }
    
    [RequireComponentInChildren(typeof(Renderer))]
    [VarwinComponent(English: "Material", Russian: "Материал")]
    public class MaterialChangeBehaviour : VarwinBehaviour
    {
        private Renderer[] _renderers;
        private Renderer[] Renderers => _renderers ?? (_renderers = GetComponentsInChildren<Renderer>());

        private Material[] _unlitMaterials;
        private Material[] _litMaterials;

        protected override void AwakeOverride()
        {
            InitializeMaterials();
            InitializeProperties();
        }

        private void InitializeMaterials()
        {
            Shader _unlitShader = Shader.Find("Unlit/TransparentColor");
            Shader _litShader = Shader.Find("Standard");

            _unlitMaterials = new Material[Renderers.Length];
            _litMaterials = new Material[Renderers.Length];
            for (int i = 0; i < Renderers.Length; i++)
            {
                _unlitMaterials[i] = new Material(_unlitShader);
                _litMaterials[i] = new Material(_litShader);
            }
        }

        private void InitializeProperties()
        {
            MaterialTexture = Renderers[0].material.mainTexture;
            MainColor = Renderers[0].material.HasProperty("_Color") ? Renderers[0].material.color : _mainColor;
            
            Metallic = Renderers[0].material.HasProperty("_Metallic")
                ? Renderers[0].material.GetFloat("_Metallic")
                : 0;

            Smoothness = Renderers[0].material.HasProperty("_Glossiness")
                ? Renderers[0].material.GetFloat("_Glossiness")
                : 0;

            TilingX = Renderers[0].material.mainTextureScale.x;
            TilingY = Renderers[0].material.mainTextureScale.y;
            OffsetX = Renderers[0].material.mainTextureOffset.x;
            OffsetY = Renderers[0].material.mainTextureOffset.y;
        }

        #region MATERIAL PARAMETERS

        private Texture _materialTexture;
        [VarwinInspector(English: "Material texture", Russian: "Текстура материала")]
        public Texture MaterialTexture
        {
            get => _materialTexture;
            set
            {
                if (_materialTexture == value)
                {
                    return;
                }

                _materialTexture = value;
                
                foreach (var meshRenderer in Renderers)
                {
                    meshRenderer.material.mainTexture = _materialTexture;
                }
            }
        }

        private bool _transparent;
        private Color _mainColor = Color.white;
        [VarwinInspector(English: "Material color", Russian: "Цвет материала")]
        public Color MainColor
        {
            get => _mainColor;
            set
            {
                if (_mainColor == value)
                {
                    return;
                }

                if (value.a < 1 != _transparent)
                {
                    _transparent = value.a < 1;
                    if (!_unlit)
                    {
                        SetMaterial();
                    }
                }

                _mainColor = value;
                SetColor();

                if (IsMultiplayerAndMasterClient)
                {
                    photonView.RPC(nameof(SetColor), _rpcTarget, _mainColor);
                }
            }
        }

        private void SetMaterial()
        {
            if (_unlit)
            {
                for (int i = 0; i < Renderers.Length; i++)
                {
                    _unlitMaterials[i].CopyPropertiesFromMaterial(Renderers[i].material);
                    Renderers[i].material = _unlitMaterials[i];
                }
            }
            else
            {
                for (int i = 0; i < Renderers.Length; i++)
                {
                    _litMaterials[i].CopyPropertiesFromMaterial(Renderers[i].material);
                    
                    if (_transparent)
                    {
                        _litMaterials[i].ToTransparentMode();
                    }
                    else
                    {
                        _litMaterials[i].ToOpaqueMode();
                    }
                    
                    Renderers[i].material = _litMaterials[i];
                }
            }
        }
        
        private void SetColor()
        {
            foreach (var meshRenderer in Renderers)
            {
                meshRenderer.material.color = _mainColor;
            }
        }

        [Action(English:"Change color", Russian:"Изменить цвет")]
        [ArgsFormat(English:" (values 0-1) r{%} g{%} b{%} a{%}", Russian: " (значения 0-1) r{%} g{%} b{%} a{%}")]
        public void ChangeColor(float r, float g, float b, float a)
        {
            MainColor = new Color(r,g,b,a);
        }
   
        private bool _unlit;
        [Variable(English: "Unlit", Russian: "Неосвещенный материал")]
        [VarwinInspector("Unlit", Russian: "Неосвещенный материал")]
        public bool Unlit
        {
            get => _unlit;
            set
            {
                if (_unlit == value)
                {
                    return;
                }

                _unlit = value;
                SetMaterial();

                if (Renderers[0].material.color == _mainColor)
                {
                    SetColor();
                }
                
                if (IsMultiplayerAndMasterClient)
                {
                    photonView.RPC(nameof(SetUnlit), _rpcTarget, _unlit);
                }
            }
        }

        private float _metallic;
        [Variable(English: "Material metalness", Russian: "Металличность материала")]
        [VarwinInspector(English: "Material metalness", Russian: "Металличность материала")]
        public float Metallic
        {
            get => _metallic;
            set
            {
                if (_metallic.ApproximatelyEquals(value))
                {
                    return;
                }

                _metallic = value;
                
                foreach (var meshRenderer in Renderers)
                {
                    if (meshRenderer.material.HasProperty("_Metallic"))
                    {
                        meshRenderer.material.SetFloat("_Metallic", value);
                    }
                }
                
                if (IsMultiplayerAndMasterClient)
                {
                    photonView.RPC(nameof(SetMetallic), _rpcTarget, _metallic);
                }
            }
        }

        private float _glossiness;
        [Variable(English: "Material smoothness", Russian: "Гладкость материала")]
        [VarwinInspector(English: "Material smoothness", Russian: "Гладкость материала")]
        public float Smoothness
        {
            get => _glossiness;
            set
            {
                if (_glossiness.ApproximatelyEquals(value))
                {
                    return;
                }

                _glossiness = value;
                
                foreach (var meshRenderer in Renderers)
                {
                    if (meshRenderer.material.HasProperty("_Glossiness"))
                    {
                        meshRenderer.material.SetFloat("_Glossiness", value);
                    }
                }
                
                if (IsMultiplayerAndMasterClient)
                {
                    photonView.RPC(nameof(SetSmoothness), _rpcTarget, _glossiness);
                }
            }
        }

        private float _tilingX;
        [Variable(English: "Texture tiling X", Russian: "Тайлинг текстуры по X")]
        [VarwinInspector(English: "Texture tiling X", Russian: "Тайлинг текстуры по X")]
        public float TilingX
        {
            get => _tilingX;
            set
            {
                if (_tilingX.ApproximatelyEquals(value))
                {
                    return;
                }

                _tilingX = value;
                
                foreach (var meshRenderer in Renderers)
                {
                    meshRenderer.material.mainTextureScale = new Vector2(value, meshRenderer.material.mainTextureScale.y);
                }
                
                if (IsMultiplayerAndMasterClient)
                {
                    photonView.RPC(nameof(SetTilingX), _rpcTarget, _tilingX);
                }
            }
        }

        private float _tilingY;
        [Variable(English: "Texture tiling Y", Russian: "Тайлинг текстуры по Y")]
        [VarwinInspector(English: "Texture tiling Y", Russian: "Тайлинг текстуры по Y")]
        public float TilingY
        {
            get => _tilingY;
            set
            {
                if (_tilingY.ApproximatelyEquals(value))
                {
                    return;
                }

                _tilingY = value;
                
                foreach (var meshRenderer in Renderers)
                {
                    meshRenderer.material.mainTextureScale = new Vector2(meshRenderer.material.mainTextureScale.x, value);
                }
                
                if (IsMultiplayerAndMasterClient)
                {
                    photonView.RPC(nameof(SetTilingY), _rpcTarget, _tilingY);
                }
            }
        }

        private float _offsetX;
        [Variable(English: "Texture offset X", Russian: "Смещение текстуры по X")]
        [VarwinInspector(English: "Texture offset X", Russian: "Смещение текстуры по X")]
        public float OffsetX
        {
            get => _offsetX;
            set
            {
                if (_offsetX.ApproximatelyEquals(value))
                {
                    return;
                }

                _offsetX = value;
                
                foreach (var meshRenderer in Renderers)
                {
                    meshRenderer.material.mainTextureOffset = new Vector2(value, meshRenderer.material.mainTextureOffset.y);
                }
                
                if (IsMultiplayerAndMasterClient)
                {
                    photonView.RPC(nameof(SetOffsetX), _rpcTarget, _offsetX);
                }
            }
        }

        private float _offsetY;
        [Variable(English: "Texture offset Y", Russian: "Смещение текстуры по Y")]
        [VarwinInspector(English: "Texture offset Y", Russian: "Смещение текстуры по Y")]
        public float OffsetY
        {
            get => _offsetY;
            set
            {
                if (_offsetY.ApproximatelyEquals(value))
                {
                    return;
                }

                _offsetY = value;
                
                foreach (var meshRenderer in Renderers)
                {
                    meshRenderer.material.mainTextureOffset = new Vector2(meshRenderer.material.mainTextureOffset.x, value);
                }

                if (IsMultiplayerAndMasterClient)
                {
                    photonView.RPC(nameof(SetOffsetY), _rpcTarget, _offsetY);
                }
            }
        }

        #endregion //MATERIAL PARAMETERS


        protected override void ResendRpcCalls()
        {
            base.ResendRpcCalls();
            
            photonView.RPC(nameof(SetColor), _rpcTarget, _mainColor);
            photonView.RPC(nameof(SetUnlit), _rpcTarget, _unlit);
            photonView.RPC(nameof(SetMetallic), _rpcTarget, _metallic);
            photonView.RPC(nameof(SetSmoothness), _rpcTarget, _glossiness);
            photonView.RPC(nameof(SetTilingX), _rpcTarget, _tilingX);
            photonView.RPC(nameof(SetTilingY), _rpcTarget, _tilingY);
            photonView.RPC(nameof(SetOffsetX), _rpcTarget, _offsetX);
            photonView.RPC(nameof(SetOffsetY), _rpcTarget, _offsetY);
        }

        #region RPC calls

        [PunRPC]
        public void SetColor(Color color)
        {
            MainColor = color;
        }

        [PunRPC]
        public void SetUnlit(bool isUnlit)
        {
            Unlit = isUnlit;
        }

        [PunRPC]
        public void SetMetallic(float metallic)
        {
            Metallic = metallic;
        }

        [PunRPC]
        public void SetSmoothness(float smoothness)
        {
            Smoothness = smoothness;
        }

        [PunRPC]
        public void SetTilingX(float tiling)
        {
            TilingX = tiling;
        }

        [PunRPC]
        public void SetTilingY(float tiling)
        {
            TilingY = tiling;
        }
        
        [PunRPC]
        public void SetOffsetX(float offset)
        {
            OffsetX = offset;
        }

        [PunRPC]
        public void SetOffsetY(float offset)
        {
            OffsetY = offset;
        }
        
        #endregion
    }
}