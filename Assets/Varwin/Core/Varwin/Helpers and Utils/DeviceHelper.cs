using UnityEngine.XR;

namespace Varwin
{
    public static class DeviceHelper
    {
        public static DeviceModel CurrentModel
        {
            get
            {
                if (IsOculus)
                    return DeviceModel.Oculus;
                if (IsVive)
                    return DeviceModel.Vive;
                if (IsWmr)
                    return DeviceModel.WMR;
                return DeviceModel.Unknown;
            }
        }
        
        public static bool IsOculus => XRDevice.model.Contains("Oculus");
        
        public static bool IsIndex => XRDevice.model == "Index";
        
        public static bool IsVive => XRDevice.model.ToLower().Contains("vive");
        
        public static bool IsWmr => XRDevice.model.ToLower().Contains("wmr") || XRDevice.model.ToLower().Contains("windows");
    }

    public enum DeviceModel
    {
        Oculus,
        Vive,
        Index,
        WMR,
        Unknown
    }
}