﻿using System;
using UnityEngine;

public class GCManager : MonoBehaviour
{
    private const float DebounceTime = 15f;
    
    private static GCManager Instance { get; set; }

    private float _collectTimeout;
    private bool _collectRequired;

    private void Update()
    {
        _collectTimeout += Time.deltaTime;
        if (_collectRequired && _collectTimeout > DebounceTime)
        {
            _collectRequired = false;
            _collectTimeout = 0f;
            GC.Collect();
        }
    }

    private void Awake()
    {
        Instance = this;
    }

    private void CollectInstance()
    {
        _collectRequired = true;
    }

    public static void Collect()
    {
        if (!Instance)
        {
            Instance = new GameObject().AddComponent<GCManager>();
            Instance.gameObject.name = "GCManager";
            DontDestroyOnLoad(Instance.gameObject);
        }
        Instance.CollectInstance();
    }
}
