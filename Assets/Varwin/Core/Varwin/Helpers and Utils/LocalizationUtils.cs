using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using Varwin.Core;
using Varwin.Public;

namespace Varwin
{
    public static class LocalizationUtils
    {
        public static List<LocalizationString> GetLocalizationStrings(Component component)
        {
            return GetLocalizationStrings(component.GetType());
        }
        
        public static List<LocalizationString> GetLocalizationStrings(Type type)
        {
            var localizationStrings = new List<LocalizationString>();
            
            var locales = type.GetCustomAttributes<LocaleAttribute>(true);

            var localeAttributes = locales as LocaleAttribute[] ?? locales.ToArray();

            if (localeAttributes.Length > 0)
            {
                foreach (var locale in localeAttributes)
                {
                    if (locale.I18n != null)
                    {
                        return GetLocalizationStringsFromI18n(locale.I18n);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(locale.Strings[0]))
                        {
                            localizationStrings.Add(new LocalizationString(locale.Language, locale.Strings[0].Trim()));
                        }
                    }
                }
            }
            else
            {
                var objectName = type.GetCustomAttribute<ObjectNameAttribute>(true);

                if (objectName != null)
                {
                    return GetLocalizationStringsFromI18n(objectName.LocalizedNames);
                }
                else
                {
                    var varwinComponent = type.GetCustomAttribute<VarwinComponentAttribute>(true);
                    if (varwinComponent != null)
                    {
                        return GetLocalizationStringsFromI18n(varwinComponent.LocalizedNames);
                    }
                }
            }

            return localizationStrings;
        }

        public static I18n GetI18n(Component component)
        {
            return GetI18n(component.GetType());
        }

        public static I18n GetI18n(Type type)
        {
            var i18n = new I18n();
            
            var locales = type.GetCustomAttributes<LocaleAttribute>(true);
            var localeAttributes = locales as LocaleAttribute[] ?? locales.ToArray();
            if (localeAttributes.Length > 0)
            {
                foreach (var locale in localeAttributes)
                {
                    if (locale.I18n != null)
                    {
                        return locale.I18n;
                    }
                    else
                    {
                        i18n.SetLocale(locale.Code, locale.Strings[0]);
                    }
                }
            }
            else
            {
                var objectName = type.GetCustomAttribute<ObjectNameAttribute>(true);
                if (objectName != null)
                {
                    return objectName.LocalizedNames;
                }
                else
                {
                    var varwinComponent = type.GetCustomAttribute<VarwinComponentAttribute>(true);
                    if (varwinComponent != null)
                    {
                        return varwinComponent.LocalizedNames;
                    }
                }
            }

            return i18n;
        }
        
        public static List<LocalizationString> GetLocalizationStringsFromI18n(I18n i18n)
        {
            var localizationStrings = new List<LocalizationString>();
            
            if (!string.IsNullOrWhiteSpace(i18n.en))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.English, i18n.en.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.ru))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Russian, i18n.ru.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.af))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Afrikaans, i18n.af.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.ar))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Afrikaans, i18n.ar.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.ba))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Basque, i18n.ba.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.be))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Belarusian, i18n.be.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.bu))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Bulgarian, i18n.bu.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.ca))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Catalan, i18n.ca.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.cn))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Chinese, i18n.cn.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.cn_s))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.ChineseSimplified, i18n.cn_s.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.cn_t))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.ChineseTraditional, i18n.cn_t.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.cz))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Czech, i18n.cz.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.da))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Danish, i18n.da.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.du))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Dutch, i18n.du.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.es))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Estonian, i18n.es.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.fa))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Faroese, i18n.fa.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.fi))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Finnish, i18n.fi.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.fr))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.French, i18n.fr.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.ge))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.German, i18n.ge.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.gr))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Greek, i18n.gr.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.he))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Hebrew, i18n.he.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.hu))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Hungarian, i18n.hu.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.ic))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Icelandic, i18n.ic.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.it))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Italian, i18n.it.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.jp))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Japanese, i18n.jp.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.ko))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Korean, i18n.ko.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.la))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Latvian, i18n.la.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.li))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Lithuanian, i18n.li.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.no))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Norwegian, i18n.no.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.pg))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Portuguese, i18n.pg.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.po))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Polish, i18n.po.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.ro))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Romanian, i18n.ro.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.se))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.SerboCroatian, i18n.se.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.sk))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Slovak, i18n.sk.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.sp))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Spanish, i18n.sp.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.sv))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Slovenian, i18n.sv.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.sw))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Swedish, i18n.sw.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.th))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Thai, i18n.th.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.tu))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Turkish, i18n.tu.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.uk))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Ukrainian, i18n.uk.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.vi))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Vietnamese, i18n.vi.Trim()));
            }
            
            if (!string.IsNullOrWhiteSpace(i18n.ind))
            {
                localizationStrings.Add(new LocalizationString(SystemLanguage.Indonesian, i18n.ind.Trim()));
            }
            
            return localizationStrings;
        }

        public static I18n GetI18nFromLocalizationStrings(List<LocalizationString> localizationStrings)
        {
            var i18n = new I18n();
            foreach (var displayName in localizationStrings)
            {
                i18n.SetLocale(displayName.key.GetCode(), displayName.value);
            }

            return i18n;
        }
    }
}