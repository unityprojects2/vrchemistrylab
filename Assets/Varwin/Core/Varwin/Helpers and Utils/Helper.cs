﻿using System;
using System.Collections;
using System.Collections.Generic;
using Varwin.Errors;
using Varwin.Data;
using DesperateDevs.Utils;
using NLog;
using Photon.Pun;
using Smooth;
using UnityEngine;
using Varwin.Commands;
using Varwin.Data.ServerData;
using Varwin.ECS.Systems.Saver;
using Varwin.ECS.Systems.UI;
using Varwin.Models.Data;
using Varwin.ObjectsInteractions;
using Varwin.Public;
using Varwin.Types;
using Varwin.UI;
using Varwin.UI.VRErrorManager;
using Varwin.UI.VRMessageManager;
using Varwin.PlatformAdapter;
using Varwin.WWW;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;

namespace Varwin
{
    /// <summary>
    ///     Класс помошник для разных методов. По мере наполнения, надо будет группировать методы по логиге и разбивать на
    ///     классы
    /// </summary>
    public static class Helper
    {
        private static GameObject _spawnView;
        private static readonly SaveObjectsSystem SaveObjectsSystem = new SaveObjectsSystem(Contexts.sharedInstance);
        private static readonly ShowUiObjects ShowUiObjectsSystem = new ShowUiObjects(Contexts.sharedInstance);
        private static readonly HideUiObjects HideUiObjectsSystem = new HideUiObjects(Contexts.sharedInstance);
        private static NLog.Logger _logger = LogManager.GetCurrentClassLogger();
        public static event Action<string> OnWrongResponce;
        
        public static PhotonView AddPhoton(GameObject go, int id)
        {
            if (!Settings.Instance.Multiplayer)
            {
                return null;
            }

            PhotonView photonV = go.AddComponent<PhotonView>();
            photonV.ViewID = id;
            photonV.Synchronization = ViewSynchronization.UnreliableOnChange;
            photonV.ObservedComponents = new List<Component>();

            return photonV;
        }

        public static bool IsResponseGood(ResponseApi requestResponse)
        {
            return requestResponse.Status != "error";
        }

        public static void ProcessBadResponse(RequestApi requestApi, ResponseApi responsetApi)
        {
            OnWrongResponce?.Invoke(responsetApi.Message);
            _logger.Fatal(RequestApi.GetResponseErrorMessage(requestApi, responsetApi));
            CoreErrorManager.Error(new Exception(responsetApi.Message));
            if (VRErrorManager.Instance)
            {
                VRErrorManager.Instance.Show(responsetApi.Message);
            }
        }
        
        public static void SpawnSceneObjects(int newSceneId, int oldSceneId)
        {
            if (newSceneId == oldSceneId && !ProjectData.IsRichSceneTemplate)
            {
                _logger.Info($"Objects for scene {newSceneId} already loaded");

                return;
            }

            _logger.Info($"Loading objects for scene {newSceneId}");

            if (oldSceneId != 0 && LoaderAdapter.LoaderType == typeof(ApiLoader))
            {
                AMQPClient.UnSubscribeLogicChange(oldSceneId);
                AMQPClient.UnSubscribeCompilationError(oldSceneId);
                AMQPClient.UnSubscribeObjectChange(oldSceneId);
            }
            
            ProjectData.ObjectsAreChanged = false;
            Data.ServerData.Scene projectScene = ProjectData.ProjectStructure.Scenes.GetProjectScene(newSceneId);
            LogicInstance logicInstance = new LogicInstance(newSceneId);
            GameStateData.RefreshLogic(logicInstance, projectScene.AssemblyBytes);
            ProjectDataListener.Instance.StartCoroutine(CreateSpawnEntities(projectScene.SceneObjects, newSceneId));

            if (LoaderAdapter.LoaderType == typeof(ApiLoader))
            {
                AMQPClient.SubscribeLogicChange(newSceneId);
                AMQPClient.SubscribeCompilationError(newSceneId);
                AMQPClient.SubscribeObjectChange(ProjectData.ProjectId, newSceneId);
            }
        }

        public static void ReloadScene()
        {
            if (ProjectData.IsRichSceneTemplate)
            {
                LoaderAdapter.LoadProject(ProjectData.ProjectId, ProjectData.SceneId, ProjectData.ProjectConfigurationId);
            }
            else
            {
                ReloadSceneObjects();
            }
        }

        public static void ReloadSceneObjects()
        {
            ProjectDataListener.Instance.StartCoroutine(ReloadSceneObjectsCoroutine());
        }

        private static IEnumerator ReloadSceneObjectsCoroutine()
        {
            yield return new WaitForEndOfFrame();
            
            _logger.Info($"Reloading objects on scene {ProjectData.SceneId}");
            GameStateData.ClearObjects();
            ProjectData.ObjectsAreChanged = false;
            Data.ServerData.Scene projectScene = ProjectData.ProjectStructure.Scenes.GetProjectScene(ProjectData.SceneId);
            LogicInstance logicInstance = new LogicInstance(ProjectData.SceneId);
            GameStateData.RefreshLogic(logicInstance, projectScene.AssemblyBytes);
            
            ProjectDataListener.Instance.StartCoroutine(CreateSpawnEntities(projectScene.SceneObjects, ProjectData.SceneId));
        }
        

        private static IEnumerator CreateSpawnEntities(List<SceneObjectDto> sceneObjects, int groupId)
        {
            yield return new WaitForEndOfFrame();
            
            foreach (SceneObjectDto o in sceneObjects)
            {
                CreateSpawnEntity(o, groupId);
            }
        }

        private static void CreateSpawnEntity(SceneObjectDto o, int sceneId, int? parentId = null)
        {
            bool embedded = false;
            PrefabObject prefabObject = ProjectData.ProjectStructure.Objects.GetById(o.ObjectId);

            if (prefabObject != null)
            {
                embedded = prefabObject.Embedded;
            }
            else
            {
                Debug.LogError("Object is not contains in project structure");
            }

            SpawnInitParams param = new SpawnInitParams
            {
                IdScene = sceneId,
                IdInstance = o.InstanceId,
                IdObject = o.ObjectId,
                IdServer = o.Id,
                Name = o.Name,
                ParentId = parentId,
                Embedded = embedded,
                DisableSceneLogic = o.DisableSceneLogic
            };

            if (o.Data != null)
            {
                param.Transforms = o.Data.Transform;
                param.Joints = o.Data.JointData;
                param.InspectorPropertiesData = o.Data.InspectorPropertiesData;
                param.LockChildren = o.Data.LockChildren;
                param.IsDisabled = o.Data.IsDisabled;
                param.IsDisabledInHierarchy = o.Data.IsDisabledInHierarchy;
                param.DisableSelectabilityInEditor = o.Data.DisableSelectabilityInEditor;
                param.Index = o.Data.Index;
            }

            Spawner.Instance.SpawnAsset(param);

            foreach (SceneObjectDto dto in o.SceneObjects)
            {
                CreateSpawnEntity(dto, sceneId, o.InstanceId);
            }
        }

        public static void SpawnObject(int objectId)
        {
            if (objectId == 0)
            {
                return;
            }

            ProjectData.ObjectsAreChanged = true;

            var transforms = new Dictionary<int, TransformDT>();
            GameObject gameObject = GameStateData.GetPrefabGameObject(objectId);
            int id = gameObject.GetComponent<ObjectId>().Id;
            transforms.Add(id, _spawnView.transform.ToTransformDT());

            SpawnInitParams param = new SpawnInitParams
            {
                IdObject = objectId,
                IdScene = ProjectData.SceneId,
                Transforms = transforms
            };

            ICommand command = new SpawnCommand(param);
            command.Execute();
        }

        public static bool CanObjectBeSpawned()
        {
            if (_spawnView == null)
            {
                return false;
            }

            CollisionController _controller = _spawnView.GetComponent<CollisionController>();

            if (_controller != null)
            {
                return !_controller.IsBlocked();
            }

            return false;
        }

        public static void SetSpawnedObject(int objectId)
        {
            ProjectData.SelectedObjectIdToSpawn = objectId;

            if (_spawnView)
            {
                Object.Destroy(_spawnView);
            }

            GameObject go = GameStateData.GetPrefabGameObject(objectId);
            _spawnView = Object.Instantiate(go, GameObjects.Instance.SpawnPoint);
            SetGameObjectToSpawn(_spawnView);
        }

        private static void SetGameObjectToSpawn(GameObject go)
        {
            go.transform.localPosition = Vector3.zero;
            
            if (ProjectData.PlatformMode == PlatformMode.Vr)
            {
                go.transform.localRotation = Quaternion.identity;
                go.transform.LookAt(GameObjects.Instance.Head);
            }
            else if (ProjectData.PlatformMode == PlatformMode.Desktop)
            {
                go.transform.localEulerAngles = Vector3.zero;
            }

            go.AddComponent<ObjectForSpawn>();
            go.AddComponent<CollisionController>().InitializeController();

            var transforms = go.GetComponentsInChildren<Transform>();

            foreach (Transform child in transforms)
            {
                Rigidbody body = child.GetComponent<Rigidbody>();

                if (body)
                {
                    body.isKinematic = true;
                }

                Animator animator = child.GetComponent<Animator>();

                if (animator)
                {
                    animator.enabled = false;
                }

                MonoBehaviour monoBehaviour = child.GetComponent<MonoBehaviour>();

                if (monoBehaviour)
                {
                    monoBehaviour.enabled = false;
                }
            }
        }

        public static void ResetSpawnObject()
        {
            ObjectForSpawn spawn = Object.FindObjectOfType<ObjectForSpawn>();

            if (spawn)
            {
                Object.Destroy(spawn.gameObject);
                spawn = null;
            }

            ProjectData.SelectedObjectIdToSpawn = 0;
        }

        public static void SaveSceneObjects()
        {
            if (ProjectData.GameMode == GameMode.Edit)
            {
                ProjectData.OnPreSave?.Invoke();
                
                SaveObjectsSystem.Execute();
                
                string message = SmartLocalization.LanguageManager.Instance.GetTextValue("SAVED");
                NotificationWindowManager.Show(message, 1f);
            }
        }

        public static void AskUserToDo(string question, Action actionYes, Action actionNo, Action actionCancel = null)
        {
            if (VRMessageManager.Instance && VRMessageManager.Instance.IsShowing)
            {
                return;
            }

            HideUi();
            VRMessageManager.Instance.Show(question);

            VRMessageManager.Instance.Result = result =>
            {
                switch (result)
                {
                    case DialogResult.Cancel:
                        actionCancel?.Invoke();

                        break;
                    case DialogResult.Yes:
                        actionYes?.Invoke();

                        break;
                    case DialogResult.No:
                        actionNo?.Invoke();
                        break;
                }
            };
        }

        public static void HideUi(bool switchMenuMode = false)
        {
            HideUiObjectsSystem.Execute();       
            InputAdapter.Instance.PointerController.IsMenuOpened = switchMenuMode;
        }

        public static void ShowUi()
        {
            if (VRMessageManager.Instance && VRMessageManager.Instance.IsShowing)
            {
                return;
            }

            if (VRErrorManager.Instance && VRErrorManager.Instance.IsShowing)
            {
                return;
            }
        
            ShowUiObjectsSystem.Execute();
            
            InputAdapter.Instance.PointerController.IsMenuOpened = true;
        }

        public static void ShowFatalErrorLoadObject(PrefabObject o, string error)
        {
                        
            string message = $"Load object {o.Name.en} error!\n{error}.";
            LogManager.GetCurrentClassLogger().Fatal(message);            
            CoreErrorManager.Error(new Exception(message));            
            var errorManagerInstance = LauncherErrorManager.Instance;

            if (!errorManagerInstance)
            {
                throw new Exception("LauncherErrorManager.Instance is null");
            }

            errorManagerInstance.ShowFatal(message, string.Empty);
        }
        
        public static void ShowErrorLoadResource(ResourceDto o, string error)
        {
            string message = $"Load resource {o.Name} error!\n{error}.";
            CoreErrorManager.Error(new Exception(message));
            LogManager.GetCurrentClassLogger().Fatal(message);
            LauncherErrorManager.Instance.ShowFatal(message, string.Empty);
        }

        public static void ShowErrorLoadScene()
        {
            string message = ErrorHelper.GetErrorDescByCode(ErrorCode.LoadSceneError);
            CoreErrorManager.Error(new Exception(message));
            LogManager.GetCurrentClassLogger().Fatal("Scene is not loaded");
            LauncherErrorManager.Instance.Show(message);
        }

        public static bool HaveSaveTransform(MonoBehaviour behaviour)
        {
            Type type = behaviour.GetType();

            return type.ImplementsInterface<ISaveTransformAware>();
        }

        public static bool HaveInputs(MonoBehaviour behaviour)
        {
            Type type = behaviour.GetType();

            return type.ImplementsInterface<IVarwinInputAware>();
        }


        /// <summary>
        ///     Initialize object in platform
        /// </summary>
        /// <param name="idObject">Object type id. Used for save.</param>
        /// <param name="spawnInitParams">Parameters for spawn</param>
        /// <param name="spawnedGameObject">Game object for init</param>
        public static void InitObject(int idObject, SpawnInitParams spawnInitParams, GameObject spawnedGameObject, I18n localizedNames)
        {
            //var photonView = AddPhoton(spawnedGameObject, spawnInitParams.spawnAsset.IdPhoton);

            PhotonView photonView = AddPhoton(spawnedGameObject, spawnInitParams.IdPhoton);

            GameObject gameObjectLink = spawnedGameObject;
            int idScene = spawnInitParams.IdScene;
            int idServer = spawnInitParams.IdServer;
            int idInstance = spawnInitParams.IdInstance;
            bool embedded = spawnInitParams.Embedded;
            string name = spawnInitParams.Name;
            var parentId = spawnInitParams.ParentId;
            var resources = spawnInitParams.InspectorPropertiesData;
            bool lockChildren = spawnInitParams.LockChildren;
            bool disableSelectabilityFromScene = spawnInitParams.DisableSelectabilityInEditor;
            bool disableSceneLogic = spawnInitParams.DisableSceneLogic;
            bool isDisabled = spawnInitParams.IsDisabled;
            bool isDisabledInHierarchy = spawnInitParams.IsDisabledInHierarchy;
            int index = spawnInitParams.Index;
            bool sceneTemplateObject = spawnInitParams.SceneTemplateObject;
            
            ObjectController parent = null;

            if (parentId != null)
            {
                parent = GameStateData.GetObjectControllerInSceneById(parentId.Value);
            }

            WrappersCollection wrappersCollection = null;

            if (idScene != 0)
            {
                wrappersCollection = GameStateData.GetWrapperCollection();
            }

            Collaboration collaboration = Collaboration.SinglePlayer;

            if (Settings.Instance.Multiplayer)
            {
                collaboration = Collaboration.AllPlayers;
            }

            InitObjectParams initObjectParams = new InitObjectParams
            {
                Id = idInstance,
                IdObject = idObject,
                IdScene = idScene,
                IdServer = idServer,
                Asset = gameObjectLink,
                Name = name,
                Photonview = photonView,
                RootGameObject = spawnedGameObject,
                WrappersCollection = wrappersCollection,
                Parent = parent,
                Embedded = embedded,
                Collaboration = collaboration,
                LocalizedNames = localizedNames,
                ResourcesPropertyData = resources,
                LockChildren = lockChildren,
                DisableSelectabilityInEditor = disableSelectabilityFromScene,
                DisableSceneLogic = disableSceneLogic,
                IsDisabled = isDisabled,
                IsDisabledInHierarchy = isDisabledInHierarchy,
                Index = index,
                SceneTemplateObject = sceneTemplateObject,
            };

            var newController = new ObjectController(initObjectParams);

            try
            {
                ProjectData.OnObjectSpawned(newController);
            }
            catch (Exception e)
            {
                 Debug.LogError("Can not invoke method on spawn object in " + newController.Name);
                 Debug.LogError(e.StackTrace);
            }
            
        }

        public static Dictionary<int, JointPoint> GetJointPoints(GameObject go)
        {
            var objectIds = go.GetComponentsInChildren<ObjectId>();
            Dictionary<int, JointPoint> jointPoints = new Dictionary<int, JointPoint>();

            foreach (ObjectId objectId in objectIds)
            {
                var jointPoint = objectId.gameObject.GetComponent<JointPoint>();

                if (!jointPoint)
                {
                    continue;
                }

                if (!jointPoints.ContainsKey(objectId.Id))
                {
                    jointPoints.Add(objectId.Id, jointPoint);
                }
            }

            return jointPoints;
        }

        public static void ReloadJointConnections(ObjectController objectController, JointData saveJointData)
        {
            JointBehaviour jointBehaviour = objectController.RootGameObject.GetComponent<JointBehaviour>();
            var jointPoints = GetJointPoints(objectController.RootGameObject);

            foreach (var jointConnectionsData in saveJointData.JointConnectionsData)
            {
                int pointId = jointConnectionsData.Key;
                JointPoint myJointPoint = jointPoints[pointId];
                JointConnectionsData connectionData = jointConnectionsData.Value;
                ObjectController otherObjectController = GameStateData.GetObjectControllerInSceneById(connectionData.ConnectedObjectInstanceId);
                var otherJointPoints = GetJointPoints(otherObjectController.RootGameObject);
                JointPoint otherJointPoint = otherJointPoints[connectionData.ConnectedObjectJointPointId];
                jointBehaviour.ConnectToJointPoint(myJointPoint, otherJointPoint);
                myJointPoint.CanBeDisconnected = !connectionData.ForceLocked;
                otherJointPoint.CanBeDisconnected = !connectionData.ForceLocked;
            }
        }

        public static T FindMyComponent<T>(PhotonView photonView) where T : Component
        {
            var allComponents = Object.FindObjectsOfType<T>();
    
            foreach (T component in allComponents)
            {
                PhotonView p = component.GetComponent<PhotonView>();
    
                if (!p || p.Owner == null || !photonView || photonView.Owner == null)
                {
                    continue;
                }
    
                if (p.Owner.UserId == photonView.Owner.UserId)
                {
                    return component;
                }
            }
    
            return null;
        }

        public static void AddPhotonTransformView(GameObject gameObject, PhotonView photonView)
        {
            if (!Settings.Instance.Multiplayer)
            {
                return;
            }
            
            if (gameObject.GetComponent<PhotonTransformViewClassic>())
            {
                return;
            }

            PhotonTransformViewClassic photonTransformView = gameObject.AddComponent<PhotonTransformViewClassic>();
            
            photonTransformView.m_PositionModel.SynchronizeEnabled = true;

            photonTransformView.m_PositionModel.InterpolateOption =
                PhotonTransformViewPositionModel.InterpolateOptions.Lerp;
            photonTransformView.m_PositionModel.InterpolateLerpSpeed = 10f;

            photonTransformView.m_RotationModel.SynchronizeEnabled = true;
            photonTransformView.m_RotationModel.InterpolateOption =
                PhotonTransformViewRotationModel.InterpolateOptions.Lerp;
            photonTransformView.m_RotationModel.InterpolateLerpSpeed = 10f;
            
            photonTransformView.m_ScaleModel.SynchronizeEnabled = true;
            photonTransformView.m_ScaleModel.InterpolateOption =
                PhotonTransformViewScaleModel.InterpolateOptions.Lerp;
            photonTransformView.m_ScaleModel.InterpolateLerpSpeed = 10f;

            var smoothSync = gameObject.AddComponent<SmoothSyncPUN2>();
            
            photonView.ObservedComponents.Add(photonTransformView);
            photonView.ObservedComponents.Add(smoothSync);
            
        }
    }
}
