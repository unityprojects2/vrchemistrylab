using System;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Varwin
{
    public static class Converter
    {
        public static void CastValue(FieldInfo fieldInfo, object value, object thisObject)
        {
            if (fieldInfo.FieldType == typeof(bool))
            {
                try
                {
                    value = Convert.ToBoolean(value);
                    fieldInfo.SetValue(thisObject, value);
                }
                catch
                {
                    DefaultValue(fieldInfo, value, thisObject);
                }
                return;
            }
            
            if (fieldInfo.FieldType == typeof(float))
            {
                try
                {
                    value = Convert.ToSingle(value);
                    fieldInfo.SetValue(thisObject, value);
                }
                catch
                {
                    DefaultValue(fieldInfo, value, thisObject);
                }

                return;
            }

            if (fieldInfo.FieldType == typeof(int))
            {
                try
                {
                    value = Convert.ToInt32(value);
                    fieldInfo.SetValue(thisObject, value);
                }
                catch
                {
                    DefaultValue(fieldInfo, value, thisObject);
                }

                return;
            }

            if (fieldInfo.FieldType == typeof(decimal))
            {
                try
                {
                    value = Convert.ToDecimal(value);
                    fieldInfo.SetValue(thisObject, value);
                }
                catch
                {
                    DefaultValue(fieldInfo, value, thisObject);
                }

                return;
            }

            if (fieldInfo.FieldType == typeof(double))
            {
                try
                {
                    value = Convert.ToDouble(value);
                    fieldInfo.SetValue(thisObject, value);
                }
                catch
                {
                    DefaultValue(fieldInfo, value, thisObject);
                }

                return;
            }

            if (fieldInfo.FieldType == typeof(string))
            {
                try
                {
                    value = Convert.ToString(value);
                    fieldInfo.SetValue(thisObject, value);
                }
                catch
                {
                    DefaultValue(fieldInfo, value, thisObject);
                }
            }

            if (fieldInfo.FieldType == typeof(Color))
            {
                try
                {
                    fieldInfo.SetValue(thisObject, value);
                }
                catch
                {
                    DefaultValue(fieldInfo, value, thisObject);
                }
            }
        }

        public static void CastValue(PropertyInfo propertyInfo, object value, object thisObject)
        {
            if (propertyInfo.PropertyType.BaseType == typeof(Enum))
            {
                try
                {
                    if (value is string)
                    {
                        propertyInfo.SetValue(thisObject, Enum.Parse(propertyInfo.PropertyType, (string) value));
                    }
                    else
                    {
                        propertyInfo.SetValue(thisObject, value);
                    }
                }
                catch
                {
                    DefaultValue(propertyInfo, value, thisObject);
                }

                return;
            }
            
            if (propertyInfo.PropertyType == typeof(string))
            {
                try
                {
                    propertyInfo.SetValue(thisObject, value);
                }
                catch
                {
                    DefaultValue(propertyInfo, value, thisObject);
                }

                return;
            } 
            
            if (propertyInfo.PropertyType == typeof(bool))
            {
                try
                {
                    value = Convert.ToBoolean(value);
                    propertyInfo.SetValue(thisObject, value);
                }
                catch
                {
                    DefaultValue(propertyInfo, value, thisObject);
                }
            }
            else if (propertyInfo.PropertyType == typeof(float))
            {
                try
                {
                    value = Convert.ToSingle(value);
                    propertyInfo.SetValue(thisObject, value);
                }
                catch
                {
                    DefaultValue(propertyInfo, value, thisObject);
                }
                
            }
            else if (propertyInfo.PropertyType == typeof(int))
            {
                try
                {
                    value = Convert.ToInt32(value);
                    propertyInfo.SetValue(thisObject, value);
                }
                catch
                {
                    DefaultValue(propertyInfo, value, thisObject);
                }
                
            }
            else if (propertyInfo.PropertyType == typeof(decimal))
            {
                try
                {
                    value = Convert.ToDecimal(value);
                    propertyInfo.SetValue(thisObject, value);
                }
                catch
                {
                    DefaultValue(propertyInfo, value, thisObject);
                }
                
            }
            else if (propertyInfo.PropertyType == typeof(double))
            {
                try
                {
                    value = Convert.ToDouble(value);
                    propertyInfo.SetValue(thisObject, value);
                }
                catch
                {
                    DefaultValue(propertyInfo, value, thisObject);
                }
                
            }
            else if (propertyInfo.PropertyType == typeof(string))
            {
                try
                {
                    value = Convert.ToString(value);
                    propertyInfo.SetValue(thisObject, value);
                }
                catch
                {
                    DefaultValue(propertyInfo, value, thisObject);
                }
            }
            else if (propertyInfo.PropertyType == typeof(Color))
            {
                try
                {
                    if (value is string || value is JObject)
                    {
                        value = JsonConvert.DeserializeObject<Color>(value.ToString(), new WanzyeeStudio.Json.ColorConverter());
                    }
                    propertyInfo.SetValue(thisObject, value);
                }
                catch
                {
                    DefaultValue(propertyInfo, value, thisObject);
                }
            }
        }

        public static bool CastValue(Type valueType, object value, out object result)
        {
            if (valueType == typeof(bool))
            {
                try
                {
                    result = Convert.ToBoolean(value);
                    return true;
                }
                catch
                {
                    DefaultValue(valueType, value, out result);
                    return false;
                }
            }

            if (valueType == typeof(float))
            {
                try
                {
                    result = Convert.ToSingle(value);
                    return true;
                }
                catch
                {
                    DefaultValue(valueType, value, out result);
                    return false;
                }
            }

            if (valueType == typeof(int))
            {
                try
                {
                    result = Convert.ToInt32(value);
                    return true;
                }
                catch
                {
                    DefaultValue(valueType, value, out result);
                    return false;
                }
            }

            if (valueType == typeof(decimal))
            {
                try
                {
                    result = Convert.ToDecimal(value);
                    return true;
                }
                catch
                {
                    DefaultValue(valueType, value, out result);
                    return false;
                }
            }

            if (valueType == typeof(double))
            {
                try
                { 
                    result = Convert.ToDouble(value);
                    return true;
                }
                catch
                {
                    DefaultValue(valueType, value, out result);
                    return false;
                }
            }

            if (valueType == typeof(string))
            {
                try
                {
                    result = value.ToString();
                    return true;
                }
                catch
                {
                    DefaultValue(valueType, value, out result);
                    return false;
                }
            }

            if (valueType == typeof(Color))
            {
                try
                {
                    result = value.ToString();
                    return true;
                }
                catch
                {
                    DefaultValue(valueType, value, out result);
                    return false;
                }
            }

            result = null;

            return false;
        }
        
        private static void DefaultValue(Type type, object value, out object result)
        {
            if (type == typeof(bool))
            {
                result = false;
            }

            if (type == typeof(float) || type == typeof(decimal) || type == typeof(int))
            {
                result = 0;
            }

            if (type == typeof(string))
            {
                result = "";
            }

            if (type == typeof(Color))
            {
                result = Color.white;
            }

            result = null;
        }

        private static void DefaultValue(FieldInfo fieldInfo, object value, object thisObject)
        {
            if (fieldInfo.FieldType == typeof(bool))
            {
                fieldInfo.SetValue(thisObject, false);
            }

            if (fieldInfo.FieldType == typeof(float) || fieldInfo.FieldType == typeof(decimal) || fieldInfo.FieldType == typeof(int))
            {
                fieldInfo.SetValue(thisObject, 0);
            }

            if (fieldInfo.FieldType == typeof(string))
            {
                fieldInfo.SetValue(thisObject, "");
            }

            if (fieldInfo.FieldType == typeof(Color))
            {
                fieldInfo.SetValue(thisObject, Color.white);
            }
        }

        private static void DefaultValue(PropertyInfo propertyInfo, object value, object thisObject)
        {
            if (propertyInfo.PropertyType == typeof(bool))
            {
                propertyInfo.SetValue(thisObject, false);
            }

            if (propertyInfo.PropertyType == typeof(float) || propertyInfo.PropertyType == typeof(decimal) || propertyInfo.PropertyType == typeof(int))
            {
                propertyInfo.SetValue(thisObject, 0);
            }

            if (propertyInfo.PropertyType == typeof(string))
            {
                propertyInfo.SetValue(thisObject, "");
            }

            if (propertyInfo.PropertyType == typeof(Color))
            {
                propertyInfo.SetValue(thisObject, Color.white);
            }
        }

    }
}