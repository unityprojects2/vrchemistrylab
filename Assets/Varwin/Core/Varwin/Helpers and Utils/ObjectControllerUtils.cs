using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Varwin.Commands;
using Varwin.Data;
using Varwin.Errors;
using Varwin.WWW;
using Varwin.UI.VRErrorManager;

namespace Varwin
{
    public static class ObjectControllerUtils
    {
        public static Action ObjectCannotDelete;
        public static event Action<List<ObjectController>> ObjectsDeleted;
        
        public static IEnumerator DeleteObjectsWithChildren(List<ObjectController> objectControllers)
        {
            var objectControllersForRemove = new List<ObjectController>();
            
            foreach (ObjectController controller in objectControllers)
            {
                var removeCandidates = new List<ObjectController>();
                
                if (!objectControllersForRemove.Contains(controller))
                {
                    removeCandidates.Add(controller);
                }

                foreach (ObjectController child in controller.Descendants)
                {
                    if (!objectControllersForRemove.Contains(child))
                    {
                        removeCandidates.Add(child);
                    }
                }

                var canDelete = new List<bool>();

                foreach (ObjectController removeCandidate in removeCandidates)
                {
                    if (removeCandidate.IsEmbedded || removeCandidate.IsSceneTemplateObject)
                    {
                        canDelete.Add(false);
                        continue;
                    }

                    if (removeCandidate.IdServer != 0)
                    {
                        var request = new RequestApi(string.Format(ApiRoutes.CanDeleteObjectRequest, removeCandidate.IdServer));
                        request.OnFinish += response =>
                        {
                            ResponseApi jsend = (ResponseApi) response;
                            bool valid = false;
                            if (jsend.Data != null)
                            {
                                Boolean.TryParse(jsend.Data.ToString(), out valid);
                            }

                            if (jsend.Code == "404")
                            {
                                valid = true;
                            }

                            canDelete.Add(valid);
                        };
                        request.OnError += ShowError;
                    }
                    else
                    {
                        canDelete.Add(true);
                    }
                }

                while (canDelete.Count != removeCandidates.Count)
                {
                    yield return null;
                }

                bool ok = canDelete.All(can => can);

                if (ok)
                {
                    objectControllersForRemove.AddRange(removeCandidates);
                }
                else
                {
                    string errorMsg = ErrorHelper.GetErrorDescByCode(ErrorCode.CannotDeleteObjectLogic);
                    ShowError(errorMsg);

                    ObjectCannotDelete?.Invoke();
                }
            }

            if (objectControllersForRemove.Count > 0)
            {
                DeleteCommand(objectControllersForRemove);
                ObjectsDeleted?.Invoke(objectControllersForRemove);
            }
        }
        
        private static void ShowError(string errorMessage)
        {
            CoreErrorManager.Error(new Exception(errorMessage));
            if (VRErrorManager.Instance)
            {
                VRErrorManager.Instance.Show(errorMessage);
            }
        }

        private static void DeleteCommand(List<ObjectController> objectControllers)
        {
            ICommand command = new DeleteCommand(objectControllers);
            command.Execute();
        }
    }
}
