﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Varwin
{
    public static class Utils
    {
        public static string ConvertToString(object o)
        {
            return o.ToString();
        }

        public static List<T> GetElements<T>(IList list, int from, int to)
        {
            int i = 0;
            List<T> result = new List<T>();
            
            foreach (object o in list)
            {
                if (i >= from)
                {
                    result.Add((T)o);
                }

                if (i > to)
                {
                    break;
                }
            }

            return result;
        }

        #region Random Numbers Generator

            private static readonly System.Random randomService = new System.Random();

            public static int RandomInt(int min, int max)
            {
                lock (randomService)
                {
                    return randomService.Next(min, max);
                }
            }

            public static double RandomDouble()
            {
                lock (randomService)
                {
                    return randomService.NextDouble();
                }
            }

            public static float RandomFloat()
            {
                return Random.value;
            }

            /// <summary>
            /// Return true with specified probability (percentage)
            /// </summary>
            /// <param name="probabilityPercentage">Probability percentage (float number between 0 [inclusive] and 100 [inclusive])</param>
            /// <returns>True or false</returns>
            public static bool TrueWithProbability(float probabilityPercentage)
            {
                return Random.Range(Mathf.Epsilon, 100f - Mathf.Epsilon) <= probabilityPercentage;
            }

            /// <summary>
            /// Return true with specified probability (percentage)
            /// </summary>
            /// <param name="probabilityPercentage">Probability percentage (float number between 0 [inclusive] and 100 [inclusive])</param>
            /// <returns>True or false</returns>
            public static bool TrueWithProbability(double probabilityPercentage)
            {
                return TrueWithProbability((float) probabilityPercentage);
            }

            /// <summary>
            /// Return true with specified probability (percentage)
            /// </summary>
            /// <param name="probabilityPercentage">Probability percentage (float number between 0 [inclusive] and 100 [inclusive])</param>
            /// <returns>True or false</returns>
            public static bool TrueWithProbability(dynamic probabilityPercentage)
            {
#if !NET_STANDARD_2_0
                return TrueWithProbability(DynamicCast.ConvertToFloat(probabilityPercentage));
#else
                return Random.value > 0.5f;
#endif
            }

        #endregion
    }
}
