﻿using System;
using System.IO;
using NLog;
using NLog.Config;
using NLog.Targets;
using UnityEngine;

// ReSharper disable once CheckNamespace
namespace NLogger
{
    public static class NLoggerSettings
    {
        public static readonly bool LogDebug = false;
        private static object[] _objects;
        private const string BaseLogPath = "VarwinData/Logs/client/";
        private const string ProcessLogFile = "process.log";
        private const string ErrorLogFile = "error.log";

        public static void Init()
        {
            string LogDirectory = Path.GetPathRoot(Environment.SystemDirectory) + BaseLogPath;
            string ProcessLogPath = LogDirectory + ProcessLogFile;
            string ErrorLogPath = LogDirectory + ErrorLogFile;

            if (File.Exists(ProcessLogPath))
            {
                File.Delete(ProcessLogPath);
            }

            if (File.Exists(ErrorLogPath))
            {
                File.Delete(ErrorLogPath);
            }

            var config = new LoggingConfiguration();

            var processLogFile = new FileTarget("process") {FileName = ProcessLogPath};
            var errorLogFile = new FileTarget("error") {FileName = ErrorLogPath};

            var unityDebugLog = new MethodCallTarget("logconsole", LogEventAction);

            config.AddRule(LogLevel.Debug, LogLevel.Debug, unityDebugLog);
            config.AddRule(LogLevel.Info, LogLevel.Info, unityDebugLog);
            config.AddRule(LogLevel.Error, LogLevel.Error, unityDebugLog);
            config.AddRule(LogLevel.Fatal, LogLevel.Fatal, unityDebugLog);

            if (LogDebug)
            {
                config.AddRule(LogLevel.Debug, LogLevel.Debug, processLogFile);
            }

            config.AddRule(LogLevel.Info, LogLevel.Info, processLogFile);
            config.AddRule(LogLevel.Error, LogLevel.Debug, processLogFile);
            config.AddRule(LogLevel.Fatal, LogLevel.Info, processLogFile);

            config.AddRule(LogLevel.Error, LogLevel.Error, errorLogFile);
            config.AddRule(LogLevel.Fatal, LogLevel.Fatal, errorLogFile);

            LogManager.Configuration = config;
        }

        private static void LogEventAction(LogEventInfo logEventInfo, object[] objects)
        {
            if (LogDebug && logEventInfo.Level == LogLevel.Debug)
            {
                Debug.Log(logEventInfo.Message);
            }

            if (logEventInfo.Level == LogLevel.Info)
            {
                Debug.Log(logEventInfo.Message);
            }

            if (logEventInfo.Level == LogLevel.Error)
            {
                Debug.LogError(logEventInfo.Message);
            }
            
            if (logEventInfo.Level == LogLevel.Fatal)
            {
                Debug.LogError(logEventInfo.Message);
            }
        }
    }
}