﻿using System.Collections;
using UnityEngine;
using UnityEngine.XR;
using Varwin;
using Varwin.DesktopInput;
using Varwin.PlatformAdapter;
using Varwin.VRInput;
using Varwin.WWW;

public class AdapterLoader : MonoBehaviour
{    
    public PlatformMode PlatformMode = PlatformMode.Desktop;
    
    void Awake()
    {
        StartCoroutine(SwitchInputAdapter());
    }
    
    IEnumerator SwitchInputAdapter()
    {
        if (PlatformMode == PlatformMode.Vr)
        {
            InputAdapter.ChangeCurrentAdapter(new SteamVRAdapter());

            if (!XRSettings.enabled)
            {
                XRSettings.LoadDeviceByName("OpenVR");

                yield return null;

                XRSettings.enabled = true;
            }
        }
        else
        {
            InputAdapter.ChangeCurrentAdapter(new DesktopAdapter());
        }
    }
}
